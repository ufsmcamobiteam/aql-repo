package br.ufsm.hql.resolvers

import hql.HqlFactory

class HqlParamFunctionResolver {
	def dispatch hql.Expression resolveParam (hql.Expression arg) {
		throw new IllegalStateException ('Polymormic call not found. Parameter Function was not found. ' + arg.getClass())
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.Expression arg) {
		throw new IllegalStateException ('Polymormic call not found. Parameter Function was not found. ' + arg.getClass())
	}

	def dispatch hql.StringLiteral toLiteral (br.ufsm.aql.Expression arg) {
		throw new IllegalStateException ('Polymormic call not found. Parameter ToLiteral was not found. ' + arg.getClass())
	}

	def dispatch hql.Expression resolveParam (hql.Identifier arg) { // clone
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(arg.id)
		return result
	}
	
	def dispatch hql.Expression resolveParam (br.ufsm.aql.Identifier arg) {
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(arg.id)
		return result
	}
	
	def dispatch hql.Expression resolveParam (hql.StringLiteral arg) { // clone
		val hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.StringLiteral arg) {
		val hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.Expression resolveParam (hql.IntegerLiteral arg) { // clone
		val hql.IntegerLiteral result = HqlFactory::eINSTANCE.createIntegerLiteral
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.IntegerLiteral arg) {
		val hql.IntegerLiteral result = HqlFactory::eINSTANCE.createIntegerLiteral
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (hql.FloatLiteral arg) { // clone
		val hql.FloatLiteral result = HqlFactory::eINSTANCE.createFloatLiteral
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.FloatLiteral arg) {
		val hql.FloatLiteral result = HqlFactory::eINSTANCE.createFloatLiteral
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.Expression resolveParam (hql.True arg) { // clone
		val hql.True result = HqlFactory::eINSTANCE.createTrue
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.True arg) {
		val hql.True result = HqlFactory::eINSTANCE.createTrue
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.Expression resolveParam (hql.False arg) { //clone
		val hql.False result = HqlFactory::eINSTANCE.createFalse
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.False arg) {
		val hql.False result = HqlFactory::eINSTANCE.createFalse
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (hql.Alias arg) { //clone
		val hql.False result = HqlFactory::eINSTANCE.createFalse
		result.setValue(arg.value)
		return result
	}

	def dispatch hql.Expression resolveParam (hql.QualifiedName arg) { // clone
		val hql.QualifiedName result = HqlFactory::eINSTANCE.createQualifiedName
		if (null == arg.granular)
			result.setGranular('')	
		else	
			result.setGranular(arg.granular)
		
		for (hql.QualifiedElement e : arg.qualifiers) 
			result.qualifiers.add(e.convertToHqlQualifiedName)	
		return result
	}

	def dispatch hql.Expression resolveParam (br.ufsm.aql.QualifiedName arg) {
		val hql.QualifiedName result = HqlFactory::eINSTANCE.createQualifiedName
		if (null == arg.granular)
			result.setGranular('')	
		else	
			result.setGranular(arg.granular)
		for (br.ufsm.aql.QualifiedElement e : arg.qualifiers) 
			result.qualifiers.add(e.convertToHqlQualifiedName)	
		return result
	}
	
	def dispatch hql.Expression resolveParam (br.ufsm.aql.ParsExpression arg) { // clone
		val hql.ParsExpression result = HqlFactory::eINSTANCE.createParsExpression
		for (br.ufsm.aql.Expression e : arg.expr)
			result.expressions.add(e.resolveParam)
		return result
	}

	def dispatch hql.Expression resolveParam (hql.ParsExpression arg) { // clone
		val hql.ParsExpression result = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression e : arg.expressions)
			result.expressions.add(e.resolveParam)
		return result
	}
	
	def dispatch hql.QualifiedElement convertToHqlQualifiedName (hql.Identifier arg) { // clone
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(arg.id)
		return result
	}

	def dispatch hql.QualifiedElement convertToHqlQualifiedName (br.ufsm.aql.Identifier arg) {
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(arg.id)
		return result
	}

	def dispatch hql.QualifiedElement convertToHql (hql.Function arg) { //clone
		val hql.Function result = HqlFactory::eINSTANCE.createFunction
		result.setName(arg.name)
		for (hql.Expression e : arg.params)  
			result.params.add(e.resolveParam)
		return result
	}
		 
	def dispatch hql.QualifiedElement convertToHql (br.ufsm.aql.Function arg) {
		val hql.Function result = HqlFactory::eINSTANCE.createFunction
 		result.setName(arg.name)
		for (br.ufsm.aql.Expression e : arg.params)  
			result.params.add(e.resolveParam)
		return result
	}	

	def dispatch hql.Expression convertToHql (hql.ParsExpression arg) { 
		val hql.ParsExpression result = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression e : arg.expressions)  
			result.expressions.add(e.resolveParam)
		return result
	}	

	def dispatch hql.Expression convertToHql (br.ufsm.aql.ParsExpression arg) { 
		val hql.ParsExpression result = HqlFactory::eINSTANCE.createParsExpression
		for (br.ufsm.aql.Expression e : arg.expr)  
			result.expressions.add(e.resolveParam)
		return result
	}	

	def dispatch hql.StringLiteral toLiteral (br.ufsm.aql.StringLiteral arg) {
		val hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.IntegerLiteral toLiteral (br.ufsm.aql.IntegerLiteral arg) {
		val hql.IntegerLiteral result = HqlFactory::eINSTANCE.createIntegerLiteral
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.FloatLiteral toLiteral (br.ufsm.aql.FloatLiteral arg) {
		val hql.FloatLiteral result = HqlFactory::eINSTANCE.createFloatLiteral
		result.setValue(arg.value)
		return result
	}		
	
	def dispatch hql.True toLiteral (br.ufsm.aql.True arg) {
		val hql.True result = HqlFactory::eINSTANCE.createTrue
		result.setValue(arg.value)
		return result
	}		

	def dispatch hql.False toLiteral (br.ufsm.aql.False arg) {
		val hql.False result = HqlFactory::eINSTANCE.createFalse
		result.setValue(arg.value)
		return result
	}
	
	def dispatch hql.Null toLiteral (br.ufsm.aql.Null arg) {
		val hql.Null result = HqlFactory::eINSTANCE.createNull
		result.setValue(arg.value)
		return result
	}			
}