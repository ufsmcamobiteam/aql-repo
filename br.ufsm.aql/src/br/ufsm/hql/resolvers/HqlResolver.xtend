package br.ufsm.hql.resolvers  
 
import br.ufsm.aql.exception.ResolutionErrorException
import br.ufsm.aql.resource.Qualified
import br.ufsm.aql.resource.Setting

import static extension br.ufsm.hql.transformation.HqlFromAlias.*
//import static extension br.ufsm.hqlmodel.HqlQualifiedNameHelper.*
import br.ufsm.hql.transformation.HqlExpressionShadow
import br.ufsm.hql.transformation.HqlTransformFunction
import br.ufsm.hql.transformation.HqlExpressionShadowImpl
  
/*   
 * Defines how to dispatch different services based on 
 * qualified names
 * 
 * Ex: a.pointcut.name, invokes pointcut analysis
 */
       
class HqlResolver  {
	private extension HqlExpressionShadow hqlExpressionShadow
	private extension Setting settings = Setting::getINSTANCE(Setting::defaulURI)

	def hql.ObjectSource resolveObjectQualifiedPath (String qualifiedPath) {
		return qualifiedPath.searchOnSymbolsByQualifiedPath
	}	

	def dispatch hql.ObjectSource resolveObjectAlias (hql.Identifier element) {
		return element.id.searchOnSymbolsByAlias
	}	

	new() {
		hqlExpressionShadow = new HqlExpressionShadowImpl();
	}

	/* 
	 * Cannot resolve a ObjectAlias of an element other than Identifier
	 */
	def dispatch hql.ObjectSource resolveObjectAlias (hql.QualifiedElement element) {
		return null
	}	
	
	def private hql.QualifiedElement searchInAst (hql.QualifiedElement shadow, hql.QualifiedName name) {
		var hql.QualifiedElement result = null 
		result = name.qualifiers.findFirst(e | e.label.equalsIgnoreCase(shadow.label))
		return result
	}
 
	def public void resolve (hql.QualifiedName qName, hql.Query ast) {
		var Qualified qualifiedResolved
		var hql.QualifiedName qNameShadow = hqlExpressionShadow.clone(qName) as hql.QualifiedName;
		var int i = 0
		while (i < qNameShadow.qualifiers.size) { // Use shadows because transformation will change the ast. You can ConcurrentChange Exception as a gift
			// Search for next token in the ast that matches next shadow tokens 
			val hql.QualifiedElement e = qNameShadow.qualifiers.get(i).searchInAst(qName)  //qName.qualifiers.get(i)
			//if (i < qName.qualifiers.size) { // Remind size can be changed since ast ca be modified
			if (null != e) { // Remind size can be changed since ast can be modified
					
					print ("\nResolving Element : " + e)
					
					val hql.ObjectSource objectSource = e.resolveObjectAlias
					if (null != objectSource) { // Resolve element as Object Alias
						print ("\nResolved as Alias : " + objectSource.alias + " ClassName path : " + objectSource.resolvedPath)
						qualifiedResolved = qualifiedResolved.addToQualifiedResolved(objectSource.resolvedPath)
					}
					else { // Try to resolve as a mapped function
						qualifiedResolved = qualifiedResolved.addToQualifiedResolved(e.label)
						var String className = qualifiedResolved.functionClassName
						if (! className.isEmpty)  // It's mapped, then try to invoke the function
							invoke (e, className, qualifiedResolved, ast)
					} 
			}
			i = i + 1			
		}
	} 
 
	def private String getFunctionClassName (Qualified qualified) {
		print("\n==> Try to resolve fullQualifiedName: " + qualified.toFullString)
		var String className = qualified.toFullString.resolve // try to resolve fullQualifiedName like AOJAspectDeclaration.pointcut
		if ( (null == className) || (className.isEmpty) )  {
			print("\n==> Try to resolve as single name: " + qualified.toString)
			className = qualified.toString.resolve // If the function does not exist, try to resolve by single name (ie, isPrivileged) - This is used to resolve functions without any context 
		}
		
		if (null == className) {
			className = "";
			print("\n==> Resolved to [None]")
		} else	
			print("\n==> Resolved to [" + className + "]")
						
		return className 
	}

	def dispatch String getLabel (hql.Identifier element) {
		return element.id
	}

	def dispatch String getLabel (hql.Function element) {
		return element.name
	}

	def private Qualified addToQualifiedResolved (Qualified qualifiedResolved, String element) {
		var Qualified result = qualifiedResolved 
		if (null == result)
			result = Qualified::toQualified(element)
		else
			result = result.getTail().add(element)	
		return result			
	}
	
//	def Constructor<HqlTransformFunction> getConstructorQualifiedNameTransformFunction (Class clazz) {
//		val List<Class<?>> argsCtor = new ArrayList<Class<?>>()
//		argsCtor.add(typeof(br.ufsm.aql.QualifiedFunction))
//		argsCtor.add(typeof(hql.Query)) 
//		return clazz.getConstructor(argsCtor.toArray as Class[])
//	} 
	
	def private Class<?> loadClass (String name, Class type) {
		var Class<?> clazz = null
		try {
	 	 	clazz = Class::forName(name)
		 	if (! clazz.getClass().isInstance(type))
				throw new ResolutionErrorException ("The Class " + name + " must implement interface HqlTransformQualifiedNameFunction")
		} catch (ClassNotFoundException e) {		
			throw new ResolutionErrorException ("Could not resolve class " + name)
		}
		
		return clazz		
	}
	
	/* 
	 * Invoke transform method of the className
	   Resolution Error is raised if Class is not found in aqlmap.xml or
	   the class does not implements HqlTransformationQualifiedNameFunction
	   This resolves some structure like a.pointcut.name, which pointcut is
	   not a function itself, but requires ast transformations
	 */
	def private void invoke (hql.QualifiedElement element, String className, Qualified resolved, hql.Query ast) {
		var Class<?> clazz = loadClass (className, typeof(HqlTransformFunction))
		val HqlTransformFunction function = clazz.newInstance() as HqlTransformFunction
		function.transform(element, resolved, ast)
	}
}