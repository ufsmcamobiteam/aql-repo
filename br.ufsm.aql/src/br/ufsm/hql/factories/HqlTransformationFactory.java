package br.ufsm.hql.factories;
 
import br.ufsm.aql.resource.HqlAliasHandler;
import br.ufsm.hql.ast.HqlASTSearch;
import br.ufsm.hql.ast.HqlASTTool;
import br.ufsm.hql.resolvers.HqlResolver;
import br.ufsm.hql.transformation.HqlReplaceAlias;
import br.ufsm.hql.transformation.HqlReplaceNode;
import br.ufsm.hql.transformation.HqlTransformation1;
import br.ufsm.hql.transformation.HqlTransformation2;
    
public interface HqlTransformationFactory {
	HqlTransformationFactory eINSTANCE = br.ufsm.hql.factories.HqlTransformationFactoryImpl.init();
	public HqlTransformation1 createHqlTransformation1();
	public HqlTransformation2 createHqlTransformation2();
	public HqlASTTool createHqlASTTool();  
	public HqlASTSearch createHqlASTSearch();  
	public HqlReplaceAlias createHqlReplaceAlias();
	public HqlReplaceNode createHqlReplaceNode();
	public HqlResolver createResolver();
	public HqlAliasHandler createAliasHandler(); 
	public HqlAliasHandler getAliasHandlerInstance();
}   
 