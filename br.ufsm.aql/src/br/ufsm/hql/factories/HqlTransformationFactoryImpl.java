package br.ufsm.hql.factories;
  
import br.ufsm.aql.resource.HqlAliasHandler;
import br.ufsm.hql.ast.HqlASTSearch;
import br.ufsm.hql.ast.HqlASTTool;
import br.ufsm.hql.resolvers.HqlResolver;
import br.ufsm.hql.transformation.HqlReplaceAlias;
import br.ufsm.hql.transformation.HqlReplaceNode;
import br.ufsm.hql.transformation.HqlTransformation1;
import br.ufsm.hql.transformation.HqlTransformation2;
   
public class HqlTransformationFactoryImpl implements HqlTransformationFactory {
	private static HqlTransformationFactory hqlTransformationFactory;
	private HqlASTTool hqlASTTool; 
	private HqlASTSearch hqlASTSearch; 
	private HqlReplaceNode hqlReplaceNode;
	private HqlReplaceAlias hqlReplaceAlias;
	private HqlAliasHandler hqlAliasHandler;
	     
	public static HqlTransformationFactory init() {
		if (hqlTransformationFactory == null)
			hqlTransformationFactory = new HqlTransformationFactoryImpl();
		return hqlTransformationFactory;
	} 
   
	@Override
	public HqlTransformation1 createHqlTransformation1() {
		return new br.ufsm.hql.transformation.HqlTransformationPass1Impl(); 
	} 

	@Override 
	public HqlTransformation2 createHqlTransformation2() { 
		return new br.ufsm.hql.transformation.HqlTransformationPass2Impl(); 
	}
 
	@Override
	public HqlASTTool createHqlASTTool() {
		if (hqlASTTool == null)
			hqlASTTool = new HqlASTTool();
		return hqlASTTool;
	} 

	@Override
	public HqlASTSearch createHqlASTSearch() {
		if (hqlASTSearch == null)
			hqlASTSearch = new HqlASTSearch();
		return hqlASTSearch;
	} 

	@Override
	public HqlReplaceAlias createHqlReplaceAlias() {
		if (hqlReplaceAlias == null)
			hqlReplaceAlias = new HqlReplaceAlias();
		return hqlReplaceAlias;
	}
 
	@Override
	public HqlReplaceNode createHqlReplaceNode() {
		if (hqlReplaceNode == null)
			hqlReplaceNode = new HqlReplaceNode();
		return hqlReplaceNode;
	}
 
	@Override
	public HqlResolver createResolver() {
		return new HqlResolver();
	}
	
	@Override
	public HqlAliasHandler createAliasHandler() {
		hqlAliasHandler = new HqlAliasHandler();
		return hqlAliasHandler;
	}
	
	@Override
	public HqlAliasHandler getAliasHandlerInstance() {
		if (hqlAliasHandler == null)
			hqlAliasHandler = createAliasHandler();
		return hqlAliasHandler;
	}	
}
