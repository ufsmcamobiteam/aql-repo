package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlCommonTransformation
import hql.HqlFactory
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static br.ufsm.aql.resource.StringUtil.*

import static extension br.ufsm.aql.resource.HqlDictionary.* 
       
/**
 * @author Cristiano De Faveri
 * Federal University of Santa Maria 
 * 
 * This function transforms .modifier in a sequence of joins
 * Recall modifier itself will be changed by modifier alias
 * ex : a.modifier(public) will be transformed to
 *      aojModifier_X in ('public')  
 *
 */ 
  
class HqlModifierFunction extends HqlCommonTransformation {
		private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
		private extension HqlReplaceNode replaceNode = HqlTransformationFactory::eINSTANCE.createHqlReplaceNode
	
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Check if qualifier is already resolved as identifier
		if ((isResolved) && (function instanceof hql.Function)) { //  ex : a.modifier(public) becomes modifierAlias in ('public)
			var hql.InClause in = createInClause(resolvedObject, function as hql.Function) // Ex : modifier in ("public", "private", etc)
			replaceNode(function.eContainer as hql.Expression, in)
			return
		}
		
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		// ex : aspect.modifiers aojmodifier_1
		var hql.ObjectSource modifierObject = objectSource.alias.createModifierAlias(ALIASMODIFIERPROPERTY.getAlias)
		// ex : aspect.modifiers.descriptors aojmodifier_2
		var hql.ObjectSource stringModifierObject = modifierObject.alias.createModifierDescriptorAlias(ALIASMODIFIERPROPERTY.getAlias)
		 
		modifierObject.addToAliasList
		stringModifierObject.setResolvedPath(resolved.toFullString)
		stringModifierObject.addToAliasList
		var hql.LeftJoin join1 = hqlASTTool.createLeftJoin(leftSide, modifierObject) 
		var hql.InnerJoin join2 = hqlASTTool.createInnerJoin(join1, stringModifierObject) 
		ast.addToFromClause (leftSide, join2)

		if (function instanceof hql.Function) { //  ex : a.modifier(public) becomes modifierAlias in ('public)
			var hql.InClause in = createInClause(stringModifierObject, function as hql.Function) // Ex : modifier in ("public", "private", etc)
			replaceNode(function.eContainer as hql.Expression, in)
		}
		
		val String oldAlias1 = Qualified::qualify(objectSource.alias).add(FUNCTIONMODIFIER).add(PROPERTYMODIFIERNAME).toFullString
		ast.replaceAlias (oldAlias1, stringModifierObject.alias)
		val String oldAlias2 = Qualified::qualify(objectSource.alias).add(FUNCTIONMODIFIER).add(PROPERTYMODIFIERCODE).toFullString
		ast.replaceAlias (oldAlias2, Qualified::qualify(modifierObject.alias).add(PROPERTYMODIFIERCODE).toFullString)
	}
	
	def hql.InClause createInClause (hql.ObjectSource source, hql.Function function) {
		var hql.InClause result = HqlFactory::eINSTANCE.createInClause
		val hql.QualifiedName sl = HqlFactory::eINSTANCE.createQualifiedName
		val hql.Identifier identifier = HqlFactory::eINSTANCE.createIdentifier
		identifier.setId(source.alias)
		sl.qualifiers.add(identifier)
		result.left = sl
	
		// Load Function 	Params
		val hql.ParsExpression pExpression = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression p : function.params) 
			pExpression.expressions.add(p.toLiteral)
		result.setRight(pExpression)			
		return result
	}
	
	def hql.StringLiteral toLiteral (hql.Expression arg) {
		val hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue(arg.asString.toUpperCase)
		return result
	} 
	
	def hql.ObjectSource createModifierAlias (String qualifierString, String alias) {
		var hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource
		result.setClassName(Qualified::qualify(qualifierString).add(PROPERTYMODIFIER).toFullString)
		result.setAlias(alias)
		return result
	}

	def hql.ObjectSource createModifierDescriptorAlias (String qualifierString, String alias) {
		var hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource
		result.setClassName(Qualified::qualify(qualifierString).add(PROPERTYMODIFIERDESCRIPTORS).toFullString)
		result.setAlias(alias)
		return result
	}

	
}