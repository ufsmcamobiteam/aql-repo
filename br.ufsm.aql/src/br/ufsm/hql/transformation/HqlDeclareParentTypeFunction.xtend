package br.ufsm.hql.transformation

import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.hql.ast.HqlASTTool 
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
   
class HqlDeclareParentTypeFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	 
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONPARENTTYPEQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}
		
	/*
	 * Create Object Source for join
	 * Ex : aojdeclareparents_1.parentType aojparenttype_1 
	 * 
	 */
	
	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYPARENTTYPE).toFullString)
		result.setAlias(ALIASPARENTTYPE.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}		
}