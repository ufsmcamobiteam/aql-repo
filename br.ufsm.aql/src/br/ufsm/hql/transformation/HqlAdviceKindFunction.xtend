package br.ufsm.hql.transformation

import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.aql.resource.Setting
import org.eclipse.emf.common.util.EList
       
class HqlAdviceKindFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private extension Setting settings = Setting::getINSTANCE(Setting::defaulURI)

	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// If kind is an identifier (like a.b.kind), then just skip transformation in this case
		if (function instanceof hql.Identifier)
			return

		// Create qualifier :: advice.kind (left in clause)
		val hql.QualifiedName adviceKindProperty = HqlFactory::eINSTANCE.createQualifiedName // LEFT
		adviceKindProperty.qualifiers.add(objectSource.alias.createIdentifier)
		adviceKindProperty.qualifiers.add(PROPERTYADVICEKIND.createIdentifier)

		// Create In Clause
		var hql.InClause in = HqlFactory::eINSTANCE.createInClause // IN
		in.setLeft(adviceKindProperty)
		
		// Load Params (right in clause)
		val hql.Function hqlFunction = function as hql.Function
		
		// Validate Params
		hqlFunction.params.validate
		 
		val hql.ParsExpression pExpression = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression p : hqlFunction.params)
			pExpression.expressions.add(p.toLiteral) 
		in.setRight(pExpression)
		// Replace Function by in clause
		if (function instanceof hql.Function)
			replaceNode(function.eContainer as hql.Expression, in)
	} 
	
	def hql.Literal toLiteral (hql.Expression expression) {
		val hql.StringLiteral result = expression.asString.toUpperCase.translate.createLiteral
		return result
	}
	
	def private void validate (EList<hql.Expression> params) {
		if (params.size == 0)
			throw new IllegalArgumentException ("There's no parameters on advice.kind function")	
	}
	
	def private String translate (String id) {
		if (null == id)
			throw new IllegalArgumentException ("Could not translate null parameter on advice kind function")
		
		val String result = id.toLowerCase.resolve
		if (null == result)
			throw new IllegalArgumentException ("Unkown advice kind function parameter = " + id)
		
		return result //id.toLowerCase.resolve 			
	}

}