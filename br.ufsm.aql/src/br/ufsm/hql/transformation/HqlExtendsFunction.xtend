package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlCommonTransformation
import hql.HqlFactory
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static br.ufsm.aql.resource.HqlDictionary.*
 
/*     
 *  AQL Reference
 *  
 *  find class c s 
    where c.extends(s) 
    returns c.id, c.fullQualifiedName, s.fullQualifiedName 
 */
 
 /* 
  * HQL Reference
  * 
  * select c.id, c.fullQualifiedName, s.fullQualifiedName
  *	from AOJClassDeclaration c , AOJClassDeclaration s, AOJContainer aojextends_0
  *	where c.superType.id = aojextends_0.id
  * and s.fullQualifiedName = aojextends_0.fullQualifiedName
  */

class HqlExtendsFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private extension HqlExpressionShadow hqlExpressionShadow = new HqlExpressionShadowImpl()
	
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {		
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.ThetaJoin join = hqlASTTool.createThetaJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)

		// Create Filters
		var hql.BinaryExpression filter = null
		if (function instanceof hql.Function) {
			filter = createFilters(rightSide, function as hql.Function)
			replaceNode(function.eContainer as hql.Expression, filter)
		} else {
			filter = createEqualsSuperId(rightSide)	
			ast.addToWhereClause(filter)
		}

		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONEXTENDSQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
		
	}	
	
	def private hql.BinaryExpression createFilters (hql.ObjectSource arg, hql.Function function) {
		val hql.And result = HqlFactory::eINSTANCE.createAnd
		result.setLeft(createEqualsSuperId(arg))		
		result.setRight(createFullNameCondition(arg, function))		
		return result
	}
	
	def private hql.BinaryExpression createEqualsSuperId (hql.ObjectSource arg) {
		val hql.Equals result = HqlFactory::eINSTANCE.createEquals
		val hql.QualifiedName qNameLeft = Qualified::qualify(qualifiedObject).add(PROPERTYEXTENDS).add(EXTENDSID).toQualifiedName
		val hql.QualifiedName qNameRight = Qualified::qualify(arg.alias).add(EXTENDSID).toQualifiedName
		result.setLeft(qNameLeft)
		result.setRight(qNameRight)		
		return result
	}

	def private hql.BinaryExpression createFullNameCondition (hql.ObjectSource arg, hql.Function function) {
		val hql.InClause result = HqlFactory::eINSTANCE.createInClause
		val hql.QualifiedName qNameLeft = Qualified::qualify(arg.alias).add(PROPERTYFULLQUALIFY).toQualifiedName

		// Create right side (Parsexpression)
		val hql.ParsExpression pExpression = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression p : function.params) 
			pExpression.expressions.add(p.toAddFullQualifiedName)

		result.setLeft(qNameLeft)
		result.setRight(pExpression)		
		return result
	}

	def private hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(PROPERTYEXTENDSASSOCIATIONTYPE).toFullString)
		result.setAlias(ALIASEXTENDSPROPERTY.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}
	
	def private hql.QualifiedName toAddFullQualifiedName (hql.Expression arg) {
		val hql.QualifiedName result = hqlExpressionShadow.clone(arg) as hql.QualifiedName 
		val hql.Identifier fullQName = HqlFactory::eINSTANCE.createIdentifier
		fullQName.setId(PROPERTYFULLQUALIFY)
		result.qualifiers.add(fullQName)
		return result
	} 
	
}