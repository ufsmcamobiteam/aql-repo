package br.ufsm.hql.transformation;

/**
 * Provides services for cloning expressions
 * Useful for transformation process which sometimes cannot can done
 * using the original expression. Then a temporary shadow expression
 * is created to be transformed and then moved to the original tree
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public interface HqlExpressionShadow {
	public hql.Expression clone(hql.Expression arg0);
}

 