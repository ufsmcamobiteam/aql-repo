package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import hql.HqlFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
    
class HqlAnnotationFunction extends HqlCommonTransformation implements HqlTransformFunction {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
  
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONANNOTATIONQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}

	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYANNOTATIONS).toFullString)
		result.setAlias(ALIASANNOTATIONS.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}		
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}
	
}