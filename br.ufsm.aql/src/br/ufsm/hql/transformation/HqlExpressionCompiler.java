package br.ufsm.hql.transformation;

/**
 * Represents the interface for AQL expression compilers
 * Any additional statement on expressions should be added here 
 * and implemented by the compiler itself.
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public interface HqlExpressionCompiler {
	public hql.Expression compile(br.ufsm.aql.Expression arg0);
}
 