package br.ufsm.hql.transformation;

import br.ufsm.aql.resource.CommonTransformation;

public interface HqlTransformation extends CommonTransformation {
	public hql.Query getHqlASTRoot();
} 
 