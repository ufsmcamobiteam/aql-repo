package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
 
class HqlEnumConstantFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private extension HqlReplaceNode replaceNode = HqlTransformationFactory::eINSTANCE.createHqlReplaceNode
 
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONENUMCONSTANTQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}
		
	/*
	 * Create Object Source for join
	 * Ex : e.constants aojenumconstant_1
	 * 
	 */
	
	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYENUMCONSTANT).toFullString)
		result.setAlias(ALIASENUMCONSTANT.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}		
}