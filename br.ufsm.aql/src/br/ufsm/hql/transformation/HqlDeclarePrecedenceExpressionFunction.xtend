package br.ufsm.hql.transformation

import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.hql.ast.HqlASTTool 
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
   
class HqlDeclarePrecedenceExpressionFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool

	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.InnerJoin join = hqlASTTool.createInnerJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONDECLAREPRECEDENCEEXPRESSIONSQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}	
	
	/*
	 * aojdeclareprecedence_x.expressions aojdeclareprecedenceexpressions_x  
	 */
	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYDECLAREPRECEDENCEEXPRESSIONS).toFullString)
		result.setAlias(ALIASDECLAREPRECEDENCEEXPRESSIONS.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}		 	
}