package br.ufsm.hql.transformation
 
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import static extension br.ufsm.aql.resource.HqlDictionary.*
import hql.HqlFactory
  
class HqlConstructorFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	 
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONCONSTRUCTORQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)							
	}
	
	/*
	 * Create Object Source for join
	 * Ex : c.members.constructors aojctor_1
	 * 
	 */
	
	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYCONSTRUCTORSMEMBERS).add(PROPERTYCONSTRUCTORS).toFullString)
		result.setAlias(ALIASCONSTRUCTORS.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
 			
	}
}