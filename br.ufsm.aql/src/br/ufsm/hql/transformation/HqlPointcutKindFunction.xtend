package br.ufsm.hql.transformation
 
import br.ufsm.hql.transformation.HqlCommonTransformation
import br.ufsm.hql.transformation.HqlTransformFunction
import hql.HqlFactory
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
    
/*  
 * Transforms pointcut.kind(k1, k2, k3...) function in
 * (pointcut.code like '%k1%' or pointcut.code like '%k2'...)
 */
 
class HqlPointcutKindFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		function.transformKindFunction(resolved, ast)
	}

	def dispatch void transformKindFunction (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Just to trigger polymorphic dispatcher of xtend
	}
	
	def dispatch void transformKindFunction (hql.Function function, Qualified resolved, hql.Query ast) {
		var hql.ParsExpression parExpression = HqlFactory::eINSTANCE.createParsExpression
		var hql.Expression head = null
 
		for (hql.Expression param : function.params) {
			val String paramAsString = param.asString
					
			/*
			 * Create '<QualifiedName> like <StringLiteral> 
			 * clause ; ex : upper(alias.code) like 'call 
			 */
			var hql.Like like = HqlFactory::eINSTANCE.createLike
			/*  
			 * left "Like" (upper(alias.code)), for example
			 */
			val hql.QualifiedName codeAttribute = qualifiedObject.createQualifiedName 
			/*  
			 * right "Like" ('call'), for example
			 */
			val hql.StringLiteral literal = paramAsString.createLiteral
			/* 
			 * Set left/right like statement
			 */
			like.setLeft(codeAttribute)
			like.setRight(literal)
			/* 
			 *  Add to expression tree
			 */
			if (null == head)
				head = like
			else
				head = head.add(like) 
		}
		 
		parExpression.expressions.add(head)	 
		
		// AddParsExpression to EContainer
		function.eContainer.replaceNode(parExpression)
		
//		// Remove Kind Function
//		(function.eContainer as hql.QualifiedName).delete(true)
	}

	def dispatch hql.Expression add (hql.Expression head, hql.Like like) {
		// Just to invoke polymorphic xtend mechanisc
	}

	def dispatch hql.Expression add (hql.Like head, hql.Like like) {
		var hql.Or result = HqlFactory::eINSTANCE.createOr
		result.setLeft(head)
		result.setRight(like)
		return result 
	}
	
	def dispatch hql.Expression add (hql.BinaryExpression head, hql.Like like) {
		var hql.Or result = HqlFactory::eINSTANCE.createOr
		result.setLeft(head)
		result.setRight(like)
		return result 
	}
	
	
	def private hql.StringLiteral createLiteral (String param) {
		var hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue('%'+param.toUpperCase+'%') // or could resolve the string to another string
		return result
	}
	
	def private hql.QualifiedName createQualifiedName (String qObject) {
		// Create QualifiedName
		var hql.QualifiedName result = HqlFactory::eINSTANCE.createQualifiedName

		// Create String Param (alias.code) 
		val String params = Qualified::qualify(qualifiedObject).add("code").toFullString
		val hql.Identifier identifier = HqlFactory::eINSTANCE.createIdentifier
		identifier.setId(params)

		// Create UpperFunction
		val hql.Function upper = HqlFactory::eINSTANCE.createFunction
		upper.setName('upper')
		upper.params.add(identifier)

		result.qualifiers.add(upper)
		return result	
	}
	 
	override preProcessing(hql.QualifiedElement element, Qualified resolved,  hql.Query ast) {
		super.preProcessing(element, resolved,  ast)
		element.ensureFunctionType
	}
	
}