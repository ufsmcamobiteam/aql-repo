package br.ufsm.hql.transformation
 
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
   
class HqlPointcutParamFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	 
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	} 
	
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		function.transformParamFunction(resolved, ast)
	}
	
	def dispatch void transformParamFunction (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Just to trigger polymorphic dispatcher of xtend
	}
	
	def dispatch void transformParamFunction (hql.Identifier function, Qualified resolved, hql.Query ast) {
		// Get left side of pointcut join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		
		// Create Alias Params Pointcut
		var hql.ObjectSource primitivesObject = HqlFactory::eINSTANCE.createObjectSource
		val alias = ALIASPOINTCUTPARAMS.getAlias
		primitivesObject.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYPOINTCUTPARAMS).toFullString)
		primitivesObject.setAlias(alias)
		
		primitivesObject.setResolvedPath(resolved.toFullString)
		primitivesObject.addToAliasList
		
		// Create Join
		val join = hqlASTTool.createLeftJoin(leftSide, primitivesObject)
		// Add Join to From Clause
		ast.addToFromClause (leftSide, join)
		// Replace Alias to primitives Alias
		
		val String oldAlias = Qualified::qualify(qualifiedObject).add(PROPERTYHQLPOINTCUTPARAMS).toFullString
		ast.replaceAlias(oldAlias, alias)
	}		
}