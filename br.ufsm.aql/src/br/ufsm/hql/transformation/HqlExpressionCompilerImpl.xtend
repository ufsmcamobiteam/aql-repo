package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlExpressionCompiler
import hql.HqlFactory
import br.ufsm.aql.Expression
 
class HqlExpressionCompilerImpl implements HqlExpressionCompiler {
	/* Qualified Names */
	def private dispatch hql.Expression internalCompile(br.ufsm.aql.QualifiedName qName) {
		val hql.QualifiedName hqlQName = HqlFactory::eINSTANCE.createQualifiedName
		if (null == qName.granular)
			hqlQName.setGranular('')	
		else	
			hqlQName.setGranular(qName.granular)
			
		for (br.ufsm.aql.QualifiedElement e : qName.qualifiers) 
			hqlQName.qualifiers.add(e.internalCompile as hql.QualifiedElement)
								
		return hqlQName
	}

	def private dispatch hql.QualifiedElement internalCompile (br.ufsm.aql.Function function) {
		val hql.Function hqlFunction = HqlFactory::eINSTANCE.createFunction
		hqlFunction.setName(function.name)
		for (br.ufsm.aql.Expression p : function.params) 
			hqlFunction.params.add(p.compile as hql.Expression) //p.resolveParam 			
			
		return hqlFunction
	}

	def private dispatch hql.QualifiedElement internalCompile (br.ufsm.aql.Identifier identifier) {
		val hql.Identifier hqlIdentifier = HqlFactory::eINSTANCE.createIdentifier
		hqlIdentifier.setId(identifier.id)
		return hqlIdentifier
	} 
  
	/* 
	 * Expression translation
	 */

	/* Expression with parentesis */
	def private dispatch hql.ParsExpression internalCompile(br.ufsm.aql.ParsExpression expression) {
		val hql.ParsExpression parExpr = HqlFactory::eINSTANCE.createParsExpression
		for (br.ufsm.aql.Expression e : expression.expr)
			parExpr.expressions.add(e.compile as hql.Expression)
		return parExpr
	}

	/* Relational expressions */
	def private dispatch hql.Equals internalCompile(br.ufsm.aql.Equals operation) {
		val equals = HqlFactory::eINSTANCE.createEquals
		equals.setLeft(operation.left.compile as hql.Expression)
		equals.setRight(operation.right.compile as hql.Expression)
		return equals
	}

	def private dispatch hql.NotEquals internalCompile(br.ufsm.aql.NotEquals operation) {
		val notEquals = HqlFactory::eINSTANCE.createNotEquals
		notEquals.setLeft(operation.left.compile as hql.Expression)
		notEquals.setRight(operation.right.compile as hql.Expression)
		return notEquals
	}

	def private dispatch hql.Less internalCompile(br.ufsm.aql.Less operation) {
		val less = HqlFactory::eINSTANCE.createLess
		less.setLeft(operation.left.compile as hql.Expression)
		less.setRight(operation.right.compile as hql.Expression)
		return less
	}
	
	def private dispatch hql.LessEqual internalCompile(br.ufsm.aql.LessEqual operation) {
		val lessEqual = HqlFactory::eINSTANCE.createLessEqual
		lessEqual.setLeft(operation.left.compile as hql.Expression)
		lessEqual.setRight(operation.right.compile as hql.Expression)
		return lessEqual
	}
	
	def private dispatch hql.Greater internalCompile(br.ufsm.aql.Greater operation) {
		val greater = HqlFactory::eINSTANCE.createGreater
		greater.setLeft(operation.left.compile as hql.Expression)
		greater.setRight(operation.right.compile as hql.Expression)
		return greater
	}
		
	def private dispatch hql.GreaterEqual internalCompile(br.ufsm.aql.GreaterEqual operation) {
		val greaterEqual = HqlFactory::eINSTANCE.createGreaterEqual
		greaterEqual.setLeft(operation.left.compile as hql.Expression)
		greaterEqual.setRight(operation.right.compile as hql.Expression)
		return greaterEqual
	}

	def private dispatch hql.Like internalCompile(br.ufsm.aql.Like operation) {
		val like = HqlFactory::eINSTANCE.createLike
		like.setLeft(operation.left.compile as hql.Expression)
		like.setRight(operation.right.compile as hql.Expression)
		return like
	}

	/* Set expressions */
	
	def private dispatch hql.InClause internalCompile(br.ufsm.aql.In operation) {
		val inClause = HqlFactory::eINSTANCE.createInClause
		val parExpression = HqlFactory::eINSTANCE.createParsExpression
		inClause.setLeft(operation.left.compile as hql.Expression)
		inClause.setRight(parExpression)
		if (operation.right instanceof br.ufsm.aql.ParsExpression) {
			val br.ufsm.aql.ParsExpression pe = operation.right as br.ufsm.aql.ParsExpression // must be a ParsExpression, validate process should check  
			for (br.ufsm.aql.Expression p : pe.expr) 
				parExpression.expressions.add(p.internalCompile)  	
		}			
		return inClause
	}
	
	/* Logical expressions */
	
	def private dispatch hql.And internalCompile(br.ufsm.aql.And operation) {
		val and = HqlFactory::eINSTANCE.createAnd
		and.setLeft(operation.left.compile as hql.Expression)
		and.setRight(operation.right.compile as hql.Expression)
		return and
	}
	
	def private dispatch hql.Or internalCompile(br.ufsm.aql.Or operation) {
		val or = HqlFactory::eINSTANCE.createOr
		or.setLeft(operation.left.compile as hql.Expression)
		or.setRight(operation.right.compile as hql.Expression)
		return or
	}
	
	def private dispatch hql.Not internalCompile(br.ufsm.aql.Not operation) {
		val not = HqlFactory::eINSTANCE.createNot
		not.setExpression(operation.exp.compile as hql.Expression)
		return not
	}
	
	/* Arithmetical expressions */
	
	def private dispatch hql.Add internalCompile(br.ufsm.aql.Add operation) {
		val add = HqlFactory::eINSTANCE.createAdd
		add.setLeft(operation.left.compile as hql.Expression)
		add.setRight(operation.right.compile as hql.Expression)
		return add
	}
	
	def private dispatch hql.Minus internalCompile(br.ufsm.aql.Minus operation) {
		val minus = HqlFactory::eINSTANCE.createMinus
		minus.setLeft(operation.left.compile as hql.Expression)
		minus.setRight(operation.right.compile as hql.Expression)
		return minus
	}
	
	def private dispatch hql.Mult internalCompile(br.ufsm.aql.Mult operation) {
		val mult = HqlFactory::eINSTANCE.createMult
		mult.setLeft(operation.left.compile as hql.Expression)
		mult.setRight(operation.right.compile as hql.Expression)
		return mult
	}
	
	def private dispatch hql.Div internalCompile(br.ufsm.aql.Div operation) {
		val div = HqlFactory::eINSTANCE.createDiv
		div.setLeft(operation.left.compile as hql.Expression)
		div.setRight(operation.right.compile as hql.Expression)
		return div
	}

	def private dispatch hql.Mod internalCompile(br.ufsm.aql.Mod operation) {
		val mod = HqlFactory::eINSTANCE.createMod
		mod.setLeft(operation.left.compile as hql.Expression)
		mod.setRight(operation.right.compile as hql.Expression)
		return mod
	}
	
	def private dispatch hql.UnaryMinus internalCompile(br.ufsm.aql.UnaryMinus operation) {
		val unaryMinus = HqlFactory::eINSTANCE.createUnaryMinus
		unaryMinus.setExpression(operation.expr.compile as hql.Expression)
		return unaryMinus
	}
	
	/* Literals */
	
	def private dispatch hql.Literal internalCompile(br.ufsm.aql.StringLiteral literal) {
		val result = HqlFactory::eINSTANCE.createStringLiteral 
		result.setValue(literal.value)
		return result
	}
		
	def private dispatch hql.IntegerLiteral internalCompile(br.ufsm.aql.IntegerLiteral literal) {
		val result = HqlFactory::eINSTANCE.createIntegerLiteral
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.FloatLiteral internalCompile(br.ufsm.aql.FloatLiteral literal) {
		val result = HqlFactory::eINSTANCE.createFloatLiteral
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.Null internalCompile(br.ufsm.aql.Null literal) {
		val result = HqlFactory::eINSTANCE.createNull
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.True internalCompile(br.ufsm.aql.True literal) {
		val result = HqlFactory::eINSTANCE.createTrue
		result.setValue(literal.value)
		return result
	} 
	
	def private dispatch hql.False internalCompile(br.ufsm.aql.False literal) {
		val result = HqlFactory::eINSTANCE.createFalse
		result.setValue(literal.value)
		return result
	}	

	override hql.Expression compile(Expression arg0) {
		return internalCompile(arg0)		
	}
	
}