package br.ufsm.hql.transformation

import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.factories.HqlTransformationFactory
import hql.HqlFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
 
/* SELECT a.name, b.kind, b.code
FROM AOJAspectDeclaration a, AOJAdviceDeclaration b
LEFT JOIN a.members.allAdvices aa
WHERE a.memebers.allAdvices.id = aa.id
*/
    
class HqlAdviceFunction extends HqlCommonTransformation { 
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	 
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Create Theta Join ..., AOJAdviceDeclaration aojadvice_1
		val hql.ObjectSource adviceObject = function.createAliasAdviceDeclaration(resolved, ast)
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val theta = leftSide.createThetaJoin(adviceObject)
		// Create left Join (LEFT JOIN a.members.allAdvices aojadvice_2)
		val hql.ObjectSource adviceAssociationObject = function.createAdviceAssociation(resolved, ast)
		val left1 = theta.createLeftJoin(adviceAssociationObject)
		ast.addToFromClause (objectSource, left1)
		// Create Condition
		val hql.Equals equals = adviceObject.createJoinCondition (adviceAssociationObject, function, resolved, ast)
		ast.addToWhereClause(equals)
		// Replace Alias
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONADVICE).toFullString
		ast.replaceAlias(oldAlias, adviceObject.alias)
	}
	
	def hql.ObjectSource createAdviceAssociation(hql.QualifiedElement function, Qualified resolved, hql.Query ast) { 
		var hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYADVICEMEMBERS).add(PROPERTYADVICEALLADVICES).toFullString)
		val alias = ALIASADVICE.getAlias
		result.setAlias(alias) 
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result
	}
 	 
	def hql.Equals createJoinCondition(hql.ObjectSource adviceObject, hql.ObjectSource associationObject, hql.QualifiedElement function, Qualified resolved, hql.Query ast) { 
		val hql.Equals result = HqlFactory::eINSTANCE.createEquals
		val hql.QualifiedName qNameLeft = Qualified::qualify(adviceObject.alias).add(ADVICEID).toQualifiedName
		val hql.QualifiedName qNameRight = Qualified::qualify(associationObject.alias).add(ADVICEID).toQualifiedName
		result.setLeft(qNameLeft)
		result.setRight(qNameRight)
		return result	
	}
	
	
	def private hql.ObjectSource createAliasAdviceDeclaration (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		var hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource
		result.setClassName(Qualified::qualify(PROPERTYADVICEASSOCIATIONTYPE).toFullString)
		val alias = ALIASADVICE.getAlias
		result.setAlias(alias) 
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result		
	}
	
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved,  ast)
		element.ensureIdentifierType
	}	
}