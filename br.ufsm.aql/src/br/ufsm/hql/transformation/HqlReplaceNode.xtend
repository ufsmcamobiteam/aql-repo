package br.ufsm.hql.transformation
 
import org.eclipse.emf.ecore.EObject  
   
class HqlReplaceNode {
	def public void replaceNode  (hql.Expression old, hql.Expression newOne) {
		replaceNode (old.eContainer, old, newOne)		
	}
	 
	def dispatch void replaceNode (hql.BinaryExpression container, hql.Expression old, hql.Expression newOne) {
		if (container.left.equals(old))
			container.setLeft(newOne)
		else
			container.setRight(newOne)			
	} 

	def dispatch void replaceNode (hql.FromClause container, hql.FromExpression old, hql.FromExpression newOne) {
		container.objectList.remove(old)
		container.objectList.add(newOne)
	}

	def dispatch void replaceNode (hql.FromClause container, hql.FromExpression old, hql.BinaryFromExpression newOne) {
		container.objectList.remove(old)
		container.binaryObjectList.add(newOne)
	}

	def dispatch void replaceNode (hql.FromClause container, hql.BinaryFromExpression old, hql.BinaryFromExpression newOne) {
		container.binaryObjectList.remove(old)
		container.binaryObjectList.add(newOne)
	}

	def dispatch void replaceNode (hql.WhereClause container, hql.Expression old, hql.Expression newOne) {
		container.expression = newOne
	}

	def dispatch void replaceNode (hql.QueryStatement container, hql.Expression old, hql.Expression newOne) {
		// This state is ilegal, since we are trying to remove a expression from upper level node
		throw new IllegalStateException("A tentative of removing an expression of queryStatement node was performed. Illegal state was detected.")
	}
	
	/* Polymorphic calls mistakes due to lack of implementation 
	 * This method should sign calls that is not implemented yet and reach upper EObject*/
	
	def dispatch void replaceNode (EObject container, EObject old, EObject newOne) {
		// This indicates that some implementation is missing on polymorphic call
		throw new IllegalArgumentException ("Polymorphic call is missing for types : " + container.eClass.name + " : " + old.eClass.name + " : " + newOne.eClass.name )
	}
}