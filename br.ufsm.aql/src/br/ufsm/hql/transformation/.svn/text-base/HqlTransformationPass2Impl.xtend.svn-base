package br.ufsm.hql.transformation
   
import org.eclipse.emf.ecore.EObject
import java.util.List
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.hql.resolvers.HqlResolver
import br.ufsm.aql.exception.ResolutionErrorException
    
class HqlTransformationPass2Impl implements HqlTransformation2 {
	@Property private hql.Query astHQL  
	@Property private br.ufsm.aql.Query astAQL
    private extension HqlResolver resolver 
	        
	new() {   
		resolver = HqlTransformationFactory::eINSTANCE.createResolver		
	}
	
	override registerTransformation (hql.Query astHQL, br.ufsm.aql.Query astAQL) {
		this.astHQL = astHQL  
		this.astAQL = astAQL 
	}  
	
	def void checkAST() {
		if ( (null == astHQL) || (null == getAstAQL) )
			throw new IllegalStateException ("No HQL or AQL ast was found for transformation")
	}
	
	override void run() {
		checkAST		
		processQualifiedNames
		processPostProcessing // Includes function definitions
	}
	
	/*
	 * Clear static counters of shared transformations
	 */
	
	def void processQualifiedNames() {
		// Process Qualified Name on Select, Where and Order By clauses
		// Do not iterate over Qualified Names using eAllContents to prevent concurrent problem (qualifiers can be modified during ast transformation)
		
		println ('\nPass 2 :: Processing QualifiedNames...')
		
		var i = 0
		val List<hql.QualifiedName> qNamesSelect = astHQL.queryStatement.selectClause.eAllContents.toIterable.filter(typeof(hql.QualifiedName)).filter(e | e.resolved == false).toList
		while (i < qNamesSelect.size) {
			println ('\n[P2] - Resolving Qualified Name of Select Clause ' + i + '  ' + qNamesSelect.get(i))
			qNamesSelect.get(i).resolve(astHQL)
			i = i + 1	
		}	
	
		if (null != astHQL.queryStatement.whereClause) {
			val List<hql.QualifiedName> qNamesWhere = astHQL.queryStatement.whereClause.eAllContents.toIterable.filter(typeof(hql.QualifiedName)).filter(e | e.resolved == false).toList
			i = 0
			while (i < qNamesWhere.size) {
				println ('\n[P2] - Resolving Qualified Name of Where Clause ' + i + '  ' + qNamesWhere.get(i).qualifiers)
				try {
					qNamesWhere.get(i).resolve(astHQL)
				} catch (ResolutionErrorException e) {
					println (' -> ' + e.message + ' ignoring invoking...')
				}
				i = i + 1
			}
		}
		
		if (null != astHQL.queryStatement.orderByClause) {
			val List<hql.QualifiedName> qNamesOrderBy = astHQL.queryStatement.orderByClause.eAllContents.toIterable.filter(typeof(hql.QualifiedName)).filter(e | e.resolved == false).toList
			i = 0
			while (i < qNamesOrderBy.size) {
				println ('\n[P2] - Resolving Qualified Name of Order By Clause ' + i + '  ' + qNamesOrderBy.get(i))
				qNamesOrderBy.get(i).resolve(astHQL)
				i = i + 1	
			}
		}	
	}  
 
	@Deprecated
	def void processPostProcessing() {
		// Process PostProcessing - Used in the past, inactive for while :)
		for(e: astHQL.queryStatement.eAllContents.toIterable.filter(typeof(hql.PostProcessing)))   
			e.sourceNode.compile
	} 
	
	def void compile (EObject function) {
	}

	override getHqlASTRoot() {
		return astHQL
	}
	
	override getASTRoot() {
		return astHQL
	}
	
}