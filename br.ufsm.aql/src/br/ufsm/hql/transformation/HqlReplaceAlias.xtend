package br.ufsm.hql.transformation
   
import br.ufsm.aql.resource.Qualified
import static extension br.ufsm.aql.resource.Qualified.*
import hql.HqlFactory
import java.util.List
import java.util.ArrayList
import java.util.Map
import java.util.Iterator
import br.ufsm.aql.exception.ResolutionErrorException
   
class HqlReplaceAlias {
	def dispatch replaceAlias (hql.WhereClause node, String from, String to) {
		print ("\n -----> Where processing expression " + node.expression.getClass.getName);
		node.expression.replaceAlias(from, to)
	} 

	/*
	 *  Replace from / to string inside node
	 *  Search for multiples tokens to match the from string
	 *  ex : a.b, from = a, to = x -> x.b
	 *  ex : a , from = a, to = x -> x
	 *  ex : a.b.c, from = a.b, to = x -> x.c 
	 * 
	 */
	def dispatch replaceAlias (hql.QualifiedName node, String from, String to) {
		if (node.resolved) // Should not touch resolved QNames
			return null

		var int start = 0
		var int end = 0
		
		// Replace all Function Params
		for (hql.Function func : node.qualifiers.filter(typeof(hql.Function)))
			func.replaceAlias(from, to);
		
		// Find start and end elements

		val Qualified qFrom = Qualified::toQualified(from)
		val Qualified qNode = Qualified::toQualified(node)
		val Map<Integer,Integer> map= qNode.contains(qFrom) // Check whether from exists in node. Returns a pair different of (-1,-1) if so.
		if (map.entrySet.head.key != -1) { 
			start = map.entrySet.head.key
			end = map.entrySet.head.value		
			
			// Rebuild QualifiedName with to + node[end to node.size]
			var List<hql.QualifiedElement> qualifiedElements = new ArrayList<hql.QualifiedElement>()
			var i = 0
			
			// Algorithm
			// Start from original qualifiers
			// If index within range (start/end) replace first and walk till the end
			// Otherwise add original
			
			while (i < qNode.size) {
				if ( (i >= start) && (i <= end) ) {
					if (i == start) { // if this is the first token, add the replacement
						var hql.Identifier identifier = HqlFactory::eINSTANCE.createIdentifier
						identifier.setId(to)
						qualifiedElements.add(identifier)
					} 
				} else
				   	qualifiedElements.add(node.qualifiers.get(i)) // add the original one
					
				i = i + 1
			} 
			
			node.qualifiers.clear
			node.qualifiers.addAll(qualifiedElements) // replace QualifiedName.elements by new One
		}	
	}

	def dispatch replaceAlias (hql.Function node, String from, String to) {
		for (hql.Expression expr : node.params) 
			expr.replaceAlias(from, to)
	}

	def dispatch replaceAlias (hql.ParsExpression node, String from, String to) {
		for (hql.Expression expr : node.expressions) 
			expr.replaceAlias(from, to)
	}
	
	def dispatch replaceAlias (hql.BinaryExpression node, String from, String to) {
		print ("\n -----> Expression left " + node.left.getClass.getName);
		print ("\n -----> Expression right " + node.right.getClass.getName);
		
		node.left.replaceAlias(from, to)
		node.right.replaceAlias(from, to)
	}

	def dispatch replaceAlias (hql.Expression node, String from, String to) {
		if  (node instanceof hql.ObjectSource)
			(node as hql.ObjectSource).replaceAlias(from, to); 
	}
	
	def dispatch replaceAlias (hql.ObjectSource object, String from, String to) {
		if (object.alias.equals(from))
			object.setAlias(to)
	}

	def dispatch replaceAlias (hql.SelectClause node, String from, String to) {
		val List<hql.QualifiedName> qNames = node.eAllContents.toIterable.filter(typeof(hql.QualifiedName)).toList
		
		// TODO : Apply shadow here
		for (q : qNames) {
			q.replaceAlias(from, to) 
		}			
	}

	def dispatch replaceAlias (hql.OrderByClause node, String from, String to) {
		val List<hql.QualifiedName> qNames = node.eAllContents.toIterable.filter(typeof(hql.QualifiedName)).toList
		
		// TODO : Apply shadow here
		for (q : qNames) {
			q.replaceAlias(from, to)
		}			
	}
	
	def dispatch replaceAlias (hql.FromClause node, String from, String to) {
		// TODO : Apply shadow here
		for (oSource : node.binaryObjectList.filter(typeof(hql.ObjectSource)))
			oSource.replaceAlias(from , to)		
		// TODO : Apply shadow here
		for (oSource : node.objectList.filter(typeof(hql.ObjectSource)).filter(e | e.alias == from))
			oSource.replaceAlias(from , to)		
	}
	
}