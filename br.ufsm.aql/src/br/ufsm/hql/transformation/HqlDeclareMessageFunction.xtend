package br.ufsm.hql.transformation

import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.aql.resource.Qualified 
import hql.HqlFactory
   
class HqlDeclareMessageFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private String functionQualifier;
	private String propertyDeclareMessage;
	
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		function.loadMessageModel
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		val String oldAlias = Qualified::qualify(qualifiedObject).add(functionQualifier).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}
		
	/*
	 * Create Object Source for join
	 * Ex : c.members.constructors aojctor_1
	 * 
	 */
	
	def hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYDECLAREMESSAGEMEMBERS).add(propertyDeclareMessage).toFullString)
		result.setAlias(ALIASDECLAREMESSAGE.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}					
	
	def private void loadMessageModel (hql.QualifiedElement function) {
		if (function instanceof hql.Identifier)
			if ( (function as hql.Identifier).id.equalsIgnoreCase(FUNCTIONDECLAREERRORQUALIFIER)) {
				functionQualifier = FUNCTIONDECLAREERRORQUALIFIER
				propertyDeclareMessage = PROPERTYDECLAREERRORS
			}	
			else {
		    	functionQualifier = FUNCTIONDECLAREWARNQUALIFIER
				propertyDeclareMessage = PROPERTYDECLAREWARNINGS
			}
		else
			if ( (function as hql.Function).name.equalsIgnoreCase(FUNCTIONDECLAREERRORQUALIFIER)) {
				functionQualifier = FUNCTIONDECLAREERRORQUALIFIER
				propertyDeclareMessage = PROPERTYDECLAREERRORS
			}	
			else {
		    	functionQualifier = FUNCTIONDECLAREWARNQUALIFIER
				propertyDeclareMessage = PROPERTYDECLAREWARNINGS
			}				
	}  
}