package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
   
/*  
 * Add pointcut.primitives capability on pointcut.fragment function
 * ...LEFT JOIN aojpointcuts_1.primitives aojprimitives_1 
 *  
 */  
 
class HqlPointcutFragmentFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool

	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}
		
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		function.transformFragmentFunction(resolved, ast)
	}
	
	def dispatch void transformFragmentFunction (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Just to trigger polymorphic dispatcher of xtend
	}
	
	def dispatch void transformFragmentFunction (hql.Identifier function, Qualified resolved, hql.Query ast) {
		// Get left side of pointcut join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		
		// Create Alias Primitives
		var hql.ObjectSource primitivesObject = HqlFactory::eINSTANCE.createObjectSource
		val alias = ALIASPRIMITIVES.getAlias
		primitivesObject.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYPRIMITIVESCLASS).toFullString)
		primitivesObject.setAlias(alias)
		
		primitivesObject.setResolvedPath(resolved.toFullString)
		primitivesObject.addToAliasList
		
		// Create Join
		val join = hqlASTTool.createLeftJoin(leftSide, primitivesObject)
		// Add Join to From Clause
		ast.addToFromClause (leftSide, join)
		// Replace Alias to primitives Alias
		val String oldAlias = Qualified::qualify(qualifiedObject).add(PROPERTYFRAGMENT).toFullString
		val newAlias = Qualified::qualify(alias).add(PROPERTYFRAGMENT).toFullString
		ast.replaceAlias(oldAlias, newAlias)
	}
}