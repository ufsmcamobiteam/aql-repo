package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlExpressionShadow
import hql.Expression
import hql.HqlFactory
import hql.InClause
 
class HqlExpressionShadowImpl implements HqlExpressionShadow {
	/* Qualified Names */
	def private dispatch hql.Expression internalClone(hql.QualifiedName qName) {
		val hql.QualifiedName hqlQName = HqlFactory::eINSTANCE.createQualifiedName
		if (null == qName.granular)
			hqlQName.setGranular('')	
		else	
			hqlQName.setGranular(qName.granular)
			
		for (hql.QualifiedElement e : qName.qualifiers) 
			hqlQName.qualifiers.add(e.internalClone as hql.QualifiedElement)
								
		return hqlQName
	}	

	def private dispatch hql.QualifiedElement internalClone (hql.Function function) {
		val hql.Function hqlFunction = HqlFactory::eINSTANCE.createFunction
		hqlFunction.setName(function.name)
		for (hql.Expression p : function.params) 
			hqlFunction.params.add(p.internalClone as hql.Expression) //p.resolveParam 			
			
		return hqlFunction
	}

	def private dispatch hql.QualifiedElement internalClone (hql.Identifier identifier) {
		val hql.Identifier hqlIdentifier = HqlFactory::eINSTANCE.createIdentifier
		hqlIdentifier.setId(identifier.id)
		return hqlIdentifier
	} 
  
	/* 
	 * Expression translation
	 */

	/* Expression with parentesis */
	def private dispatch hql.ParsExpression internalClone(hql.ParsExpression expression) {
		val hql.ParsExpression parExpr = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression e : expression.expressions)
			parExpr.expressions.add(e.internalClone as hql.Expression)
		return parExpr
	}

	/* Relational expressions */
	def private dispatch hql.Equals internalClone(hql.Equals operation) {
		val equals = HqlFactory::eINSTANCE.createEquals
		equals.setLeft(operation.left.internalClone as hql.Expression)
		equals.setRight(operation.right.internalClone as hql.Expression)
		return equals
	}

	def private dispatch hql.NotEquals internalClone(hql.NotEquals operation) {
		val notEquals = HqlFactory::eINSTANCE.createNotEquals
		notEquals.setLeft(operation.left.internalClone as hql.Expression)
		notEquals.setRight(operation.right.internalClone as hql.Expression)
		return notEquals
	}

	def private dispatch hql.Less internalClone(hql.Less operation) {
		val less = HqlFactory::eINSTANCE.createLess
		less.setLeft(operation.left.internalClone as hql.Expression)
		less.setRight(operation.right.internalClone as hql.Expression)
		return less
	}
	
	def private dispatch hql.LessEqual internalClone(hql.LessEqual operation) {
		val lessEqual = HqlFactory::eINSTANCE.createLessEqual
		lessEqual.setLeft(operation.left.internalClone as hql.Expression)
		lessEqual.setRight(operation.right.internalClone as hql.Expression)
		return lessEqual
	}
	
	def private dispatch hql.Greater internalClone(hql.Greater operation) {
		val greater = HqlFactory::eINSTANCE.createGreater
		greater.setLeft(operation.left.internalClone as hql.Expression)
		greater.setRight(operation.right.internalClone as hql.Expression)
		return greater
	}
		
	def private dispatch hql.GreaterEqual internalClone(hql.GreaterEqual operation) {
		val greaterEqual = HqlFactory::eINSTANCE.createGreaterEqual
		greaterEqual.setLeft(operation.left.internalClone as hql.Expression)
		greaterEqual.setRight(operation.right.internalClone as hql.Expression)
		return greaterEqual
	}

	def private dispatch hql.Like internalClone(hql.Like operation) {
		val like = HqlFactory::eINSTANCE.createLike
		like.setLeft(operation.left.internalClone as hql.Expression)
		like.setRight(operation.right.internalClone as hql.Expression)
		return like
	}

	/* Set expressions */
	
	def private dispatch hql.InClause internalClone(InClause operation) {
		val inClause = HqlFactory::eINSTANCE.createInClause
		val parExpression = HqlFactory::eINSTANCE.createParsExpression
		inClause.setLeft(operation.left.internalClone as hql.Expression)
		inClause.setRight(parExpression)
		if (operation.right instanceof hql.ParsExpression) {
			val hql.ParsExpression pe = operation.right as hql.ParsExpression // must be a ParsExpression, validate process should check  
			for (hql.Expression p : pe.expressions) 
				parExpression.expressions.add(p.internalClone)  	
		}			
		return inClause
	}
	
	/* Logical expressions */
	
	def private dispatch hql.And internalClone(hql.And operation) {
		val and = HqlFactory::eINSTANCE.createAnd
		and.setLeft(operation.left.internalClone as hql.Expression)
		and.setRight(operation.right.internalClone as hql.Expression)
		return and
	}
	
	def private dispatch hql.Or internalClone(hql.Or operation) {
		val or = HqlFactory::eINSTANCE.createOr
		or.setLeft(operation.left.internalClone as hql.Expression)
		or.setRight(operation.right.internalClone as hql.Expression)
		return or
	}
	
	def private dispatch hql.Not internalClone(hql.Not operation) {
		val not = HqlFactory::eINSTANCE.createNot
		not.setExpression(operation.expression.internalClone as hql.Expression)
		return not
	}
	
	/* Arithmetical expressions */
	
	def private dispatch hql.Add internalClone(hql.Add operation) {
		val add = HqlFactory::eINSTANCE.createAdd
		add.setLeft(operation.left.internalClone as hql.Expression)
		add.setRight(operation.right.internalClone as hql.Expression)
		return add
	}
	
	def private dispatch hql.Minus internalClone(hql.Minus operation) {
		val minus = HqlFactory::eINSTANCE.createMinus
		minus.setLeft(operation.left.internalClone as hql.Expression)
		minus.setRight(operation.right.internalClone as hql.Expression)
		return minus
	}
	
	def private dispatch hql.Mult internalClone(hql.Mult operation) {
		val mult = HqlFactory::eINSTANCE.createMult
		mult.setLeft(operation.left.internalClone as hql.Expression)
		mult.setRight(operation.right.internalClone as hql.Expression)
		return mult
	}
	
	def private dispatch hql.Div internalClone(hql.Div operation) {
		val div = HqlFactory::eINSTANCE.createDiv
		div.setLeft(operation.left.internalClone as hql.Expression)
		div.setRight(operation.right.internalClone as hql.Expression)
		return div
	}

	def private dispatch hql.Mod internalClone(hql.Mod operation) {
		val mod = HqlFactory::eINSTANCE.createMod
		mod.setLeft(operation.left.internalClone as hql.Expression)
		mod.setRight(operation.right.internalClone as hql.Expression)
		return mod
	}
	
	def private dispatch hql.UnaryMinus internalClone(hql.UnaryMinus operation) {
		val unaryMinus = HqlFactory::eINSTANCE.createUnaryMinus
		unaryMinus.setExpression(operation.expression.internalClone as hql.Expression)
		return unaryMinus
	}
	
	/* Literals */
	
	def private dispatch hql.Literal internalClone(hql.StringLiteral literal) {
		val result = HqlFactory::eINSTANCE.createStringLiteral 
		result.setValue(literal.value)
		return result
	}
		
	def private dispatch hql.IntegerLiteral internalClone(hql.IntegerLiteral literal) {
		val result = HqlFactory::eINSTANCE.createIntegerLiteral
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.FloatLiteral internalClone(hql.FloatLiteral literal) {
		val result = HqlFactory::eINSTANCE.createFloatLiteral
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.Null internalClone(hql.Null literal) {
		val result = HqlFactory::eINSTANCE.createNull
		result.setValue(literal.value)
		return result
	}

	def private dispatch hql.True internalClone(hql.True literal) {
		val result = HqlFactory::eINSTANCE.createTrue
		result.setValue(literal.value)
		return result
	} 
	
	def private dispatch hql.False internalClone(hql.False literal) {
		val result = HqlFactory::eINSTANCE.createFalse
		result.setValue(literal.value)
		return result
	}	

	override clone(Expression arg0) {
		return internalClone (arg0)
	}
	
}