package br.ufsm.hql.transformation

import org.eclipse.emf.ecore.EObject
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static br.ufsm.aql.resource.HqlDictionary.*

import static extension br.ufsm.hql.transformation.HqlFromAlias.*
import static extension br.ufsm.aql.resource.HqlAliasHandler.*
import br.ufsm.aql.exception.ResolutionErrorException
           
abstract class HqlCommonTransformation implements HqlTransformFunction {
	protected extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	/*
	 * Holds if this transformation origins from a function
	 */
	private boolean isFunction
	/*
	 * Holds if this path is already resolved
	 */
	private boolean isResolved
	/*
	 * Holds the object that resolved the path previously
	 */
	private hql.ObjectSource resolvedObject;

	/*   
	 * Holds the object that owns the element
	 * Ex: element = a.pointcut, qualifiedObject = a
	 */ 
	private String qualifiedObject
	
	/* 
	 * Holds the first object that owns the qualified Element
	 * Ex: element = a.pointcut.param, qualifiedRootObject = a
	 */ 
	private String qualifiedRootObject

	/* 
	 * Holds the object source of the qualifiedRootObject
	 */ 
	private hql.ObjectSource objectSourceRoot
	
	/*
	 * Holds the FromObject according to the qualifiedObject (if exists)
	 */
	private hql.ObjectSource objectSource	        
	          
	def void preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		isFunction = element instanceof hql.Function
		resolvedObject = resolved.getResolvedFromSymbol
		isResolved = resolvedObject != null
		qualifiedObject = element.resolveObjectName 
		objectSource = ast.queryStatement.fromClause.getAliasFromFunction(qualifiedObject)
		if (objectSource == null)
			throw new ResolutionErrorException("Object Source not found on FROM clause : " + qualifiedObject);
		qualifiedRootObject = element.resolveRootObjectName
		objectSourceRoot = ast.queryStatement.fromClause.getAliasFromFunction(qualifiedRootObject)
	}
	
	def String resolveRootObjectName(hql.QualifiedElement element) {
		if (element.eContainer instanceof hql.QualifiedName) 
			return (element.eContainer as hql.QualifiedName).qualifiers.get(0).label
		
		return "" 
	}
   
	def dispatch String asString (hql.Expression expression) {
		throw new IllegalArgumentException ('Argument type error on function param')
	}
	
	def dispatch String asString (hql.Identifier arg) {
		return arg.id.toUpperCase
	}
	
	def dispatch String asString (hql.QualifiedName arg) {
		return Qualified::toQualified(arg).toFullString.toUpperCase
	}
	
	def dispatch String asString (hql.StringLiteral arg) {
		return arg.value.toUpperCase
	}

	def dispatch String asString (hql.Alias arg) {
		return arg.value.toUpperCase
	}	  
	
	def void postProcessing(hql.QualifiedElement element, Qualified resolved,  hql.Query ast) {

	}
	 
	override void transform (hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		preProcessing (element, resolved, ast)
		internalTransform (element, resolved, ast)
		postProcessing (element, resolved, ast)
	}
	
	def protected void internalTransform (hql.QualifiedElement element, Qualified resolved,  hql.Query ast)
	
	def boolean containerIsWhereClause (EObject node) {
		if (node instanceof hql.WhereClause)
			return true
		
		if ( (node instanceof hql.QueryStatement) || (node instanceof hql.Query) )
			return false

		return node.eContainer.containerIsWhereClause
	}
	
	def boolean containerIsFromClause (EObject node) {
		if (node instanceof hql.FromClause)
			return true
		
		if ( (node instanceof hql.QueryStatement) || (node instanceof hql.Query) )
			return false

		return node.eContainer.containerIsFromClause
	}
	
	def boolean containerIsSelectClause (EObject node) {
		if (node instanceof hql.SelectClause)
			return true
		
		if ( (node instanceof hql.QueryStatement) || (node instanceof hql.Query) )
			return false

		return node.eContainer.containerIsSelectClause
	}
	
	def void addToAliasList (hql.ObjectSource source) {
		source.addAtSymbolsIfDoesNotExist
	}
	
	def String resolveObjectName (hql.QualifiedElement function) {
		// Resolve from first element to function element
		// ex: a , function a -> resolves to a
		// ex: a.pointcut, function pointcut -> resolves to a
		// ex: a.pointcut.name -> function pointcut resolves to a
		// ex: a.pointcut.name -> function name resolves to a.pointcut
		if (function.eContainer instanceof hql.QualifiedName) {
			var Qualified q = null
			val hql.QualifiedName container = function.eContainer as hql.QualifiedName
			var i = 0
			while ( (i < container.qualifiers.size) && (! container.qualifiers.get(i).equals(function)) ) {
				q = container.qualifiers.get(i).addToQualified(q)
				i = i + 1
			}
					
			if (null == q)
				q = function.addToQualified(q)	
					
			return q.toFullString
		}
		return ""
	}
	
	def private Qualified addToQualified (hql.QualifiedElement element, Qualified q) {
		if (null == q)
			return Qualified::qualify(element.label)
		return q.add(element.label)	
	} 
	
	def ensureFunctionType(hql.QualifiedElement element) { 
		if (!(element instanceof hql.Function))
			throw new IllegalArgumentException ("Cannot process transform method. Argument must be of type Function." + getClass())
	}
	
	def ensureIdentifierType(hql.QualifiedElement element) { 
		if (!(element instanceof hql.Identifier))
			throw new IllegalArgumentException ("Cannot process transform method. Argument must be of type Identifier." + getClass())
	}
	
	def public String getQualifiedObject() {
		return this.qualifiedObject
	}
 
	def public String getQualifiedRootObject() {
		return this.qualifiedRootObject
	}
	
	def public hql.ObjectSource getObjectSource() {
		return this.objectSource
	}

	def public hql.ObjectSource getObjectSourceRoot() {
		return this.objectSourceRoot
	}
	
	def String getAlias(String alias) {
		return alias + alias.functionUsage
//		
//		counter = counter + 1
//		return counter
	}
	
	def boolean isResolved() {
		return isResolved
	}
	
	def hql.ObjectSource getResolvedObject() {
		return resolvedObject
	}
} 