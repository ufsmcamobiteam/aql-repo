package br.ufsm.hql.transformation

import hql.HqlFactory
import org.eclipse.emf.ecore.EObject
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
        
class HqlBooleanFilterFunction extends HqlCommonTransformation {
	/*   
	   from (QualifiedName.xxxx) 
	   to   (= QualifiedName.xxxx true)
	   * 
	   * Some booleans constructions like "where a.isPrivileged" should be
	   * converted to "where a.isPrivileged = true"   
	*/
	  
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	
	def dispatch void transformElement (hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		// Just a placeholder for xtend polymorphic dispatch
	}

	def dispatch void transformElement (hql.Identifier identifier, Qualified resolved, hql.Query ast) {
		if (identifier.containerIsWhereClause)  {
			if ( (! identifier.eContainer.existComparator) ) { 
				val hql.Equals equals = HqlFactory::eINSTANCE.createEquals
				val hql.True truee = HqlFactory::eINSTANCE.createTrue
				truee.setValue('true')
				identifier.eContainer.replaceNode(equals)
				equals.setLeft(identifier.eContainer as hql.Expression) // This changes identifier.eContainer automatically
				equals.setRight(truee)
			} 
		}  
	} 

	override internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		function.transformElement(resolved, ast)
	}
	
	def boolean existComparator (EObject node) {
		var result = false
		switch (node.eContainer) {
			hql.Equals : result = true
			hql.NotEquals : result = true
		}
		return result
	}
	
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}
} 