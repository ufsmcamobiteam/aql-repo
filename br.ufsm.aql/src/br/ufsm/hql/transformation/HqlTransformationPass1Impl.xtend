package br.ufsm.hql.transformation
 
import hql.HqlFactory
import br.ufsm.aql.resource.CommonTransformationImpl
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import br.ufsm.aql.resource.ASTPrinter
import org.eclipse.emf.ecore.EObject
import br.ufsm.aql.resource.Setting
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.aql.resource.HqlAliasHandler
import br.ufsm.hql.resolvers.HqlParamFunctionResolver

import static extension br.ufsm.hql.transformation.HqlFromAlias.*
//import static extension br.ufsm.hql.transformation.HqlExpressionCompiler.*
   
class HqlTransformationPass1Impl extends CommonTransformationImpl implements HqlTransformation1 {
	@Property private hql.Query hqlASTRoot
	@Property private hql.Clause currentClause
	private extension ASTPrinter printer
	private extension HqlParamFunctionResolver paramResolver
	private Setting settings = Setting::getINSTANCE(Setting::defaulURI)
	@Property private HqlExpressionCompiler expressionCompiler;
	
	override registerTransformation (Resource resource, IFileSystemAccess fsa) {
		fromResource = resource
		this.fsa = fsa
		checkResources
	}
	
	new() {
		expressionCompiler = new HqlExpressionCompilerImpl();
		paramResolver = new HqlParamFunctionResolver();	
	}
	
	def void checkResources () {
		if ( (null == fromResource) || (null == fsa) ) 
			throw new IllegalStateException ("No Source resource or File System Access was found ! Null state found")
	}
	
	override run() {
		preProcessing
		
		checkResources		
		
		// Get aql AST  
		val aqlAST = fromResource.contents

		// Load aql AST root
		aqlASTRoot = aqlAST.get(0) as br.ufsm.aql.Query
		aqlASTRoot.compile

		// create hql Model
		//var URI hqlURI = URI::createURI("http://www.ufsm.br/HQL")
		//var resourceHql = fromResource.resourceSet.createResource(hqlURI)
		phase2
		postProcessing
	}

	def dispatch void compile (br.ufsm.aql.Query aqlQuery) { 
		// Phase 1 - Create hql AST applying primary transformations
		print("\nCompiling aql query - Phase 1...")		
		createHqlASTRootNode
		hqlASTRoot.setQueryStatement(createHqlQueryStatement)
		if (null != aqlASTRoot.query.find)
			aqlASTRoot.query.find.compile 

		if (null != aqlASTRoot.query.where)
			aqlASTRoot.query.where.compile
			
		if (null != aqlASTRoot.query.^return)
			aqlASTRoot.query.^return.compile
 
		if (null != aqlASTRoot.query.orderby)
			aqlASTRoot.query.orderby.compile
			
		print("\nAST - HQL - Fase 1")
		checkNodesNull;
		println;
		hqlASTRoot.queryStatement.selectClause.printAST('hql');
		println;
		hqlASTRoot.queryStatement.fromClause.printAST('hql');
		println;
		hqlASTRoot.queryStatement.whereClause.printAST('hql');
	}
	 
	def void phase2() {
		// Phase 2 - Resolve PlaceHolders and other transformations
		val HqlTransformation2 hqlT2 = HqlTransformationFactory::eINSTANCE.createHqlTransformation2
		hqlT2.registerTransformation(hqlASTRoot, aqlASTRoot)
		hqlT2.run
		hqlASTRoot = hqlT2.hqlASTRoot // update new hqlAST
	}
	
	def void preProcessing() {
		HqlFromAlias::sourceList.clear		
		HqlAliasHandler::functionCounterMap.clear
	} 
	  
	def void postProcessing() {
		cleanup
	}
	
	def void cleanup() {
		cleanupEmptyWhere
		debug()
	}
	 
	def void debug() {
		println("\nAST - AQL")
		aqlASTRoot.query.printAST ('aql')
		print("\nAST - HQL")
		checkNodesNull
		println
		hqlASTRoot.queryStatement.selectClause.printAST('hql')
		println
		hqlASTRoot.queryStatement.fromClause.printAST('hql')
		println
		hqlASTRoot.queryStatement.whereClause.printAST('hql')
	}
	
	def void checkNodesNull () {
		hqlASTRoot.queryStatement.selectClause.printNodesNull
		hqlASTRoot.queryStatement.fromClause.printNodesNull
		hqlASTRoot.queryStatement.whereClause.printNodesNull
	} 

	def void cleanupEmptyWhere() {
		if (null != hqlASTRoot.queryStatement.whereClause)
			if (hqlASTRoot.queryStatement.whereClause.eContents.empty)
				hqlASTRoot.queryStatement.whereClause = null
	} 
	
	def dispatch void compile(br.ufsm.aql.ReturnsClause clause) { 
		hqlASTRoot.queryStatement.setSelectClause(createSelectClause) 
		for (br.ufsm.aql.Expression e : clause.expressions) 
			hqlASTRoot.queryStatement.selectClause.expressions.add(expressionCompiler.compile(e))
		for (String alias : clause.resultAlias) 
			hqlASTRoot.queryStatement.selectClause.alias.add(alias)
	}
	
	def dispatch void compile(br.ufsm.aql.WhereClause clause) {
		hqlASTRoot.queryStatement.setWhereClause(createWhereClause)
		hqlASTRoot.queryStatement.whereClause.setExpression(expressionCompiler.compile(clause.expression))
	}
	
	def dispatch void compile(br.ufsm.aql.FindClause clause) { 
		hqlASTRoot.queryStatement.setFromClause(createFromClause)
		currentClause = hqlASTRoot.queryStatement.fromClause
		clause.bindingObject.compile
	}
	
	def dispatch void compile(br.ufsm.aql.OrderByClause clause) { 
		hqlASTRoot.queryStatement.setOrderByClause(createOrderByClause)
		val char lOption = clause.option.optionAsChar
		hqlASTRoot.queryStatement.orderByClause.setOption(lOption)
		for (br.ufsm.aql.QualifiedName param : clause.expressions.filter(typeof(br.ufsm.aql.QualifiedName))) // Should already resolve this cast on Semantic Analysis
			hqlASTRoot.queryStatement.orderByClause.expressions.add(param.resolveParam as hql.QualifiedName)		
	}	
	
	def char optionAsChar (String arg) {
		if (null != arg)
			return arg.charAt(0)
		return ' '.charAt(0)
	}  
	
	def dispatch void compile (EList<br.ufsm.aql.BindingObject> bindingObject) {
		for (br.ufsm.aql.BindingObject bind: bindingObject) {
			for (alias : bind.alias)  
				hqlASTRoot.queryStatement.fromClause.objectList.add(createFromObject (bind.type, alias))
		}
		
		/* Replace list of object binding by Theta joins */
		if (hqlASTRoot.queryStatement.fromClause.objectList.size > 1) {
			val hql.ThetaJoin join = hqlASTRoot.queryStatement.fromClause.objectList.transformToThetaJoin
			hqlASTRoot.queryStatement.fromClause.objectList.clear
			hqlASTRoot.queryStatement.fromClause.binaryObjectList.add(join)
		}
	}
	
	def hql.ThetaJoin transformToThetaJoin (EList<hql.FromExpression> expressions) {
		val hql.ThetaJoin join = HqlFactory::eINSTANCE.createThetaJoin
		
		join.setLeft(expressions.get(0)) // setLeft already decount 1 from list size (take care of this behavior to not going to crazy mode)
		join.setRight(join.createThetaChildren(expressions, 0))
		return join		
	}
	
	def hql.FromExpression createThetaChildren (hql.ThetaJoin parent, EList<hql.FromExpression> expressions, int index) {
		if (index > expressions.size)
			throw new IndexOutOfBoundsException ("Index parameters is out of expression.size when creating Theta Join")
		
		if (index == expressions.size - 1) 
			return expressions.get(index)	
		
		val hql.ThetaJoin join = HqlFactory::eINSTANCE.createThetaJoin
		parent.setLeft(expressions.get(index))
		parent.setRight(join.createThetaChildren(expressions, 0))
		return join
	}
	
	def hql.ObjectSource createFromObject(br.ufsm.aql.ObjectType type, String alias) {
		val hql.ObjectSource objectSource = HqlFactory::eINSTANCE.createObjectSource
		objectSource.className = type.translateTypeName
		objectSource.alias = alias 
		objectSource.setResolvedPath(objectSource.className) 
		objectSource.addAtSymbolsIfDoesNotExist
		return objectSource
	}
	 
	def String translateTypeName (br.ufsm.aql.ObjectType type) {
		val String _type = settings.resolve(type.value)
		if (null == _type)
			throw new IllegalArgumentException ("Illegal Bind Object type " + type.eClass)
		
		return _type
	}
	
	def createSelectClause() { 
		val hql.SelectClause hqlSelect = HqlFactory::eINSTANCE.createSelectClause
		hqlSelect.clause = "Select"
		return hqlSelect
	}

	def createFromClause() { 
		val hql.FromClause hqlFrom = HqlFactory::eINSTANCE.createFromClause
		hqlFrom.clause = "From"
		return hqlFrom
	}
	  
	def createWhereClause() {  
		val hql.WhereClause hqlWhere = HqlFactory::eINSTANCE.createWhereClause
		hqlWhere.clause = "Where"
		return hqlWhere
	}

	def createOrderByClause() {  
		val hql.OrderByClause hqlOrderBy = HqlFactory::eINSTANCE.createOrderByClause
		hqlOrderBy.clause = "Order By"
		return hqlOrderBy
	}

	def void createHqlASTRootNode() { 
		hqlASTRoot = HqlFactory::eINSTANCE.createQuery  
	}

	def createHqlQueryStatement() { 
		return HqlFactory::eINSTANCE.createQueryStatement
	}
	
	override void setup() {
		hqlASTRoot = null 		// reset hqlAST tree 
	}

	override getASTRoot() {
		return hqlASTRoot
	}
	
	def void printAST (EObject node, String ASTKind) {
		// We need to instantiate for all checks because of level variable.
		if (null != node) {
			printer = new ASTPrinter(ASTKind)
			node.printTree
		}
	}
	
	def void printNodesNull (EObject node) {
		//printer = new ASTPrinter('hql')
		//node.printNull
	}
	
}