package br.ufsm.hql.transformation;

import br.ufsm.aql.resource.Qualified;

/**
 * @author defaveri
 * 
 */   
public interface HqlTransformFunction {
	/**
	 * @param element The element that was resolved as a function inside the QualifiedName
	 *                Ex: QualifiedName = a.pointcut, AOJAspectDeclaration.pointcut was resolved into a mapped Function
	 * @param resolved The canonical name of the resolved expression
	 *                Ex: QualifiedName = a.pointcut, resolved = AOJAspectDeclaration.pointcut
	 * @param ast The ast to be transformed
	 */
	public void transform (hql.QualifiedElement element, Qualified resolved, hql.Query ast);
}
