package br.ufsm.hql.transformation
 
import br.ufsm.hql.transformation.HqlCommonTransformation
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
       
// find aspect a returns a.name, a.pointcut.name

// select a.name, b.name  
// from AOJAspectDeclaration a
// inner join a.members.pointcuts b
   
class HqlPointcutFunction extends HqlCommonTransformation  {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Get left side of pointcut join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		
		// Create left join
		var hql.LeftJoin join = HqlFactory::eINSTANCE.createLeftJoin
		
		// Create join right side
		var hql.ObjectSource pointcutObject = HqlFactory::eINSTANCE.createObjectSource
		val String className = Qualified::qualify(objectSource.alias).add(PROPERTYHQLPOINTCUTMEMBERS).add(PROPERTYHQLPOINTCUT).toFullString
		pointcutObject.setClassName(className)
		val alias = ALIASPOINTCUTPROPERTY.getAlias
		pointcutObject.setAlias(alias)
		
		pointcutObject.setResolvedPath(resolved.toFullString)
		pointcutObject.addToAliasList 
		
		join.setLeft(leftSide)
		join.setRight(pointcutObject)
		
		// Replace source (from) to binary join
		ast.addToFromClause (leftSide, join)
		
		// Replace pointcut to alias
		// a.pointcut.name by alias.name 
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONAJQLPOINTCUTQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, alias)
	}	
	
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}
}