package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlCommonTransformation
import hql.HqlFactory
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static br.ufsm.aql.resource.HqlDictionary.*

/*     
 *  AQL Reference
 *  
 *  find aspect a, class c where a.affects(c) returns a.name, c.name 
 */
 
 /* 
  * HQL Reference
  * 
  * select a.name, c.name
	FROM AOJAspectDeclaration a, AOJClassDeclaration c 
	LEFT JOIN a.bindingModel.outBinding bindOut
	WHERE bindOut.id = c.id

  */
class HqlAffectsFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private extension HqlExpressionShadow hqlExpressionShadow = new HqlExpressionShadowImpl()

	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {		
		//create join
		val hql.ObjectSource rightSide = resolved.createElementJoin
		//create where (filter) 
		val filter = createFilters(rightSide, function as hql.Function)
		//get last From Expression				
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		//create left join
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		//add expression to from clause
		ast.addToFromClause (leftSide, join)
		// replace function by where expression
		replaceNode(function.eContainer as hql.Expression, filter)
		// update alias
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONBINDINGQUALIFIER).add(PROPERTYBINDINGMODELOUT).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)		 
	}
	 
	def private hql.BinaryExpression createFilters (hql.ObjectSource arg, hql.Function function) {
		val hql.Equals result = HqlFactory::eINSTANCE.createEquals
		val hql.QualifiedName qNameLeft = Qualified::qualify(arg.alias).add(BINDINGID).toQualifiedName
		var hql.QualifiedName param0
		if (function.params.get(0) instanceof hql.QualifiedName) // 1st parameter must be a QualifiedName
			param0 = function.params.get(0) as hql.QualifiedName
		else
			throw new IllegalArgumentException ("Function Affects argument is not valid")	

		val hql.QualifiedName qNameRight = Qualified::toQualified(param0).add(BINDINGID).toQualifiedName
		result.setLeft(qNameLeft)
		result.setRight(qNameRight)		
		return result
	}
	
	def private hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(qualifiedObject).add(PROPERTYBINDINGMODEL).add(PROPERTYBINDINGMODELOUT).toFullString)
		result.setAlias(ALIASBINDINGPROPERTY.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}
	
}