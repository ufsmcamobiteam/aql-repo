package br.ufsm.hql.transformation

import br.ufsm.hql.transformation.HqlCommonTransformation
import hql.HqlFactory
import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
 
import static br.ufsm.aql.resource.HqlDictionary.*

 /*     
  *  AQL Reference
  *  
  *  find aspect a returns a.name, a.inBinding.object 
  * 
  */
 
 /* 
  * HQL Reference
  * 
  * select a.name, inBinding.object from AOJAspectDeclaration a
 	left join a.bindingModel.inBinding inBinding

  */
class HqlBindingInFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool

	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {		
		//create join
		val hql.ObjectSource rightSide = resolved.createElementJoin
		//get last From Expression				
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		//create left join
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		//add expression to from clause
		ast.addToFromClause (leftSide, join)
		// update alias
		val String oldAlias = Qualified::qualify(qualifiedObject).add(PROPERTYBINDINGMODELIN).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)		 
	}
	
	def private hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(qualifiedObject).add(PROPERTYBINDINGMODEL).add(PROPERTYBINDINGMODELIN).toFullString)
		result.setAlias(ALIASBINDINGPROPERTY.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}
	
}