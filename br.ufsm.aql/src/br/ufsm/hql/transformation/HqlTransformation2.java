package br.ufsm.hql.transformation;

public interface HqlTransformation2 extends HqlTransformation {
	public void registerTransformation (hql.Query astHQL, br.ufsm.aql.Query astAQL);
}
 