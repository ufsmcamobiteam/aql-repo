package br.ufsm.hql.transformation
 
import br.ufsm.hql.ast.HqlASTTool
import static extension br.ufsm.aql.resource.HqlDictionary.*
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
   
class HqlFieldFunction extends HqlCommonTransformation { 
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	override public void internalTransform(hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Create object source :: "ObjectSource".members.attrribute aojfield_1" (right clause)
		val hql.ObjectSource rightSide = HqlFactory::eINSTANCE.createObjectSource 
		rightSide.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYFIELDSMEMBERS).add(PROPERTYFIELDS).toFullString)
		rightSide.setAlias(ALIASFIELDS.getAlias)
		rightSide.setResolvedPath(resolved.toFullString)
		rightSide.addToAliasList 
		
		// Get left side of join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)

		// Replace source (from) to binary join
		ast.addToFromClause (leftSide, join)

		// Replace advice.param to alias
		// a.advice.param by alias 
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONFIELDQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)			
	}
}