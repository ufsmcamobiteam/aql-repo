package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import hql.HqlFactory
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory

import static br.ufsm.aql.resource.HqlDictionary.*
    
class HqlParamTypeNameFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool

	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Create Alias for AOJType alias
		var hql.ObjectSource typeObject = HqlFactory::eINSTANCE.createObjectSource
		typeObject.setClassName(Qualified::qualify(PROPERTYPARAMTYPENAMESASSOCIATIONTYPE).toFullString)
		val alias = ALIASPOINTCUTPARAMSTYPENAME.getAlias
		typeObject.setAlias(alias)  
		typeObject.setResolvedPath(resolved.toFullString)
		typeObject.addToAliasList

		// Create Theta Join between and Param (ex : AOJAspectDeclaration, AOJType aojtype_1
		val theta = createThetaJoin(null, typeObject) // left side will be joined further by the addtoFromClause
		
		// Add Theta join to From clause
		theta.addThetaJoinToFromClause(ast)
		 
		// Create assigned theta join condition property
		// Create Equals expression
		val hql.Equals equals = HqlFactory::eINSTANCE.createEquals
		// Create left (Qualified Name)
		val hql.QualifiedName qNameLeft = Qualified::qualify(qualifiedObject).add(PROPERTYHQLPOINTCUTPARAMSTYPE).add(PARAMSTYPESID).toQualifiedName
		qNameLeft.setResolved(true) // I mean, do not touch this name anymore. it's resolved
		// Create right (Qualified Name)
		val hql.QualifiedName qNameRight = Qualified::qualify(alias).add(PARAMSTYPESID).toQualifiedName

		equals.setLeft(qNameLeft)
		equals.setRight(qNameRight)
		// Add to Where clause		
		ast.addToWhereClause(equals)
		
		// Replace alias param.type to  alias.name
        val String oldAlias = Qualified::qualify(qualifiedObject).add(PROPERTYHQLPOINTCUTPARAMSTYPE).toFullString
        val String newAlias = Qualified::qualify(alias).add(PROPERTYHQLPOINTCUTPARAMSTYPENAME).toFullString
		ast.replaceAlias(oldAlias, newAlias)
	}	
	
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}	
}