package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import hql.HqlFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
     
class HqlPointcutAnnotationFunction extends HqlCommonTransformation {
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool

	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Get left side of Annotable join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		 
		// Create join
		var hql.LeftJoin join = HqlFactory::eINSTANCE.createLeftJoin
		
		// Create join right side
		var hql.ObjectSource annotationObject = HqlFactory::eINSTANCE.createObjectSource
		val String className = Qualified::qualify(objectSource.alias).add(PROPERTYANNOTATIONS).toFullString
		annotationObject.setClassName(className)
		val alias = ALIASPOINTCUTANNOTATION.getAlias
		annotationObject.setAlias(alias)
		annotationObject.setResolvedPath(resolved.toFullString)
		annotationObject.addToAliasList 
		
		join.setLeft(leftSide)
		join.setRight(annotationObject)
		
		// Replace source (from) to binary join
		ast.addToFromClause (leftSide, join)
		
		// Replace pointcut to alias
		// a.pointcut.name by alias.name 
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONANNOTATIONQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, alias)			
	}
	
	override preProcessing(hql.QualifiedElement element, Qualified resolved, hql.Query ast) {
		super.preProcessing(element, resolved, ast)
		element.ensureIdentifierType
	}
	

}