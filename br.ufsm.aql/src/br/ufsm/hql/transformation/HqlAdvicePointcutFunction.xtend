package br.ufsm.hql.transformation

import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory

import static extension br.ufsm.aql.resource.HqlDictionary.*
  
class HqlAdvicePointcutFunction extends HqlCommonTransformation {
	    
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	 
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		// Get left side of pointcut join 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		
		// Create join right side
		var hql.ObjectSource advicePointcutObject = HqlFactory::eINSTANCE.createObjectSource
		val String className = Qualified::qualify(objectSource.alias).add(PROPERTYADVICEPOINTCUT).toFullString
		advicePointcutObject.setClassName(className)
		val alias = ALIASADVICEPOINTCUT.getAlias

		advicePointcutObject.setAlias(alias)
		advicePointcutObject.setResolvedPath(resolved.toFullString)
		advicePointcutObject.addToAliasList 
		
		var hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, advicePointcutObject)
		
		// Replace source (from) to binary join
		ast.addToFromClause (leftSide, join)
		
		// Replace advice.pointcut to alias
		// ex : a.advice.pointcut.name by alias.name 
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONADVICEPOINTCUTQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, alias)		
		
	}
}