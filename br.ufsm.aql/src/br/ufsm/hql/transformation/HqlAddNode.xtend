package br.ufsm.hql.transformation 
 
import org.eclipse.emf.ecore.EObject
import hql.HqlFactory
        
class HqlAddNode {  
	def dispatch void add (hql.WhereClause parent, hql.Expression content) {
		if (null == parent.expression)
			parent.setExpression(content)
		else { 
			// Add as AND expression at the beginning of where expression
			val hql.And and = HqlFactory::eINSTANCE.createAnd
			and.setLeft(content)
	 		and.setRight(parent.expression)
			parent.setExpression(and)
		}
	}

	def dispatch void add (hql.FromClause parent, hql.FromExpression content) {
		parent.objectList.add(content)
	}

	def dispatch void add (hql.FromClause parent, hql.BinaryFromExpression content) {
		parent.binaryObjectList.add(content)
	}
	
	def dispatch void add (hql.SelectClause parent,  hql.Expression content) {
		parent.expressions.add(content)
	}

	def dispatch void add (hql.ParsExpression parent, hql.Expression content) {
		parent.expressions.add(content)
	}

	def dispatch void add (hql.BinaryExpression parent, hql.Expression content) {
		if (null == parent.right)  
			parent.setRight(content)
		else
			parent.setLeft(content)
	}

	def dispatch void add (hql.BinaryExpression parent, EObject object, hql.Expression content) {
		if (parent.right.equals(object)) // Which side the original object is located ? 
			parent.setRight(content)
		else
			parent.setLeft(content)
	}

	def dispatch void add (hql.UnaryExpression parent, EObject object, hql.Expression content) {
		parent.setExpression(content)
	}
	
	/* Polymorphic calls mistakes due to lack of implementation 
 	 * This method should sign calls that is not implemented yet and reach upper EObject*/
	
	def dispatch void add (EObject parent, EObject content) {
		throw new IllegalArgumentException ("Polymorphic call is missing for types : " + parent.eClass.name + " : " + content.eClass.name )
	}
	
	def dispatch void add (EObject parent, EObject object, EObject content) {
		throw new IllegalArgumentException ("Polymorphic call is missing for types : " + parent.eClass.name + " : " + content.eClass.name )
	}
	
}