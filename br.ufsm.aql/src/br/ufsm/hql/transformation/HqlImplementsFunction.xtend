package br.ufsm.hql.transformation

import br.ufsm.aql.resource.Qualified
import br.ufsm.hql.ast.HqlASTTool
import br.ufsm.hql.factories.HqlTransformationFactory
import hql.HqlFactory
import static br.ufsm.aql.resource.HqlDictionary.*
    
class HqlImplementsFunction extends HqlCommonTransformation {
 
	private extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
	private extension HqlExpressionShadow hqlExpressionShadow = new HqlExpressionShadowImpl()
	 
	override void internalTransform (hql.QualifiedElement function, Qualified resolved, hql.Query ast) {
		val hql.ObjectSource rightSide = resolved.createElementJoin 
		var hql.FromExpression leftSide = ast.queryStatement.fromClause.getLastFromExpression
		val hql.LeftJoin join = hqlASTTool.createLeftJoin(leftSide, rightSide)
		ast.addToFromClause (leftSide, join)
		if (function instanceof hql.Function) {
			val hql.InClause in = createFilter(function as hql.Function, rightSide)
			replaceNode(function.eContainer as hql.Expression, in)
		}
		val String oldAlias = Qualified::qualify(qualifiedObject).add(FUNCTIONIMPLEMENTSQUALIFIER).toFullString
		ast.replaceAlias(oldAlias, rightSide.alias)
	}  
	
	/*
	 * Create Object Source for join
	 * Ex : a.implementedInterfaces aojimplements_
	 * 
	 */
	
	def private hql.ObjectSource createElementJoin(Qualified resolved) {
 		val hql.ObjectSource result = HqlFactory::eINSTANCE.createObjectSource	
		result.setClassName(Qualified::qualify(objectSource.alias).add(PROPERTYIMPLEMENTS).toFullString)
		result.setAlias(ALIASIMPLEMENTSPROPERTY.getAlias)
		result.setResolvedPath(resolved.toFullString)
		result.addToAliasList
		return result 
	}
	
	def private hql.InClause createFilter(hql.Function function, hql.ObjectSource element) {
		// Create left side InClause -> Left = alias.fullQualifiedName
		val hql.InClause result = HqlFactory::eINSTANCE.createInClause
		val hql.QualifiedName qNameLeft = Qualified::qualify(element.alias).add(PROPERTYFULLQUALIFY).toQualifiedName
		
		// Create right side (Parsexpression)
		val hql.ParsExpression pExpression = HqlFactory::eINSTANCE.createParsExpression
		for (hql.Expression p : function.params) 
			pExpression.expressions.add(p.toAddFullQualifiedName)
			
		result.setLeft(qNameLeft)
		result.setRight(pExpression)
		return result	
	}
	
	def private hql.QualifiedName toAddFullQualifiedName (hql.Expression arg) {
		val hql.QualifiedName result = hqlExpressionShadow.clone(arg) as hql.QualifiedName 
		val hql.Identifier fullQName = HqlFactory::eINSTANCE.createIdentifier
		fullQName.setId(PROPERTYFULLQUALIFY)
		result.qualifiers.add(fullQName)
		return result
	} 
}