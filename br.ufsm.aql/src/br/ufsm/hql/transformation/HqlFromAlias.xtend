package br.ufsm.hql.transformation

import hql.ObjectSource
import java.util.ArrayList
import java.util.List

import static extension br.ufsm.hql.transformation.HqlFromAlias.*
  
class HqlFromAlias {  
	private static List<ObjectSource> sourceList = new ArrayList<hql.ObjectSource>();
	 
	def public static List<hql.ObjectSource> getSourceList() {
		return sourceList
	} 
	
	def public static void addAtSymbolsIfDoesNotExist(hql.ObjectSource source) {
		if (null == searchOnSymbolsByAlias(source.alias)) {
			if (null == source.resolvedPath)
				source.resolvedPath =''
			sourceList.add(source)
		}
	} 
	
	def public static hql.ObjectSource searchOnSymbolsByAlias (String alias) {
//		print ("\n +++> search for Alias  = " + alias);
//		print ("\n +++> search on symbols By Alias sourceList = " + sourceList);
//		print ("\n +++> Encontrou " + sourceList.findFirst(e | e.alias.equalsIgnoreCase(alias)) + "\n");
		
		return sourceList.findFirst(e | e.alias.equalsIgnoreCase(alias))
	}

	def public static hql.ObjectSource searchOnSymbolsByQualifiedPath (String qualifiedPath) {
		return sourceList.findFirst(e | e.resolvedPath.equalsIgnoreCase(qualifiedPath))
	}

	def public static hql.ObjectSource searchOnSymbolsByObject (String object) {
		return sourceList.findFirst(e | e.className.equalsIgnoreCase(object))
	}

	def public static hql.ObjectSource searchOnSymbolsByObjectAlias (String object, String alias) {
		return sourceList.findFirst(e | e.alias.equalsIgnoreCase(alias) && e.className.equalsIgnoreCase(object))
	}
	
	def public static void removeOfSymbolsByAlias (String alias) {
		sourceList.remove(searchOnSymbolsByAlias(alias))
	}
	
}