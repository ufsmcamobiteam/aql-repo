package br.ufsm.hql.ast

import org.eclipse.emf.ecore.EObject
import hql.HqlFactory
import org.eclipse.xtext.EcoreUtil2
import br.ufsm.hql.transformation.HqlReplaceAlias
import br.ufsm.hql.transformation.HqlReplaceNode
import br.ufsm.hql.transformation.HqlAddNode

import static extension br.ufsm.hql.transformation.HqlFromAlias.*
import br.ufsm.aql.resource.Qualified

class HqlASTTool {
	private extension HqlReplaceAlias hqlReplaceAlias = new HqlReplaceAlias()
	private extension HqlReplaceNode hqlReplaceNode = new HqlReplaceNode()
	private extension HqlAddNode hqlAddNode = new HqlAddNode()
	//private extension HqlASTSearch hqlASTSearch = new HqlASTSearch()
	 
	def void addNode (EObject parent, EObject content) {
		parent.add(content)
	}
	 
	def void addNode (EObject parent, EObject object, EObject content) {
		parent.add(object, content)
	} 
	
	def hql.ObjectSource getResolvedFromSymbol (Qualified qualifiedPath) {
		return qualifiedPath.toFullString.searchOnSymbolsByQualifiedPath
	}
	
	def hql.ObjectSource getAliasFromFunction (hql.FromClause from, String qualifiedName) {
		// return first alias. More than one implies semantic violation
		print ("\n +++ Searching for aliasFromFunction " + qualifiedName);
		return qualifiedName.searchOnSymbolsByAlias 
		//return from.objectList.filter(typeof(hql.ObjectSource)).findFirst([o | o.alias == qualifiedName])
	}

	def void remove (hql.FromExpression node, hql.Query ast) {
		ast.queryStatement.fromClause.objectList.remove(node)
		ast.queryStatement.fromClause.binaryObjectList.remove(node)
	}

	/*
 	* This method try to figure out which node should be on the left size of a
 	* theta join.	 
 	*/
	def addThetaJoinToFromClause (hql.ThetaJoin join, hql.Query ast) {
		// TODO : check if join.right already exists
		var hql.FromExpression expr
		if (! ast.queryStatement.fromClause.objectList.isEmpty()) 
			expr =  (ast.queryStatement.fromClause.objectList.last)
		else if (! ast.queryStatement.fromClause.binaryObjectList.isEmpty()) 
			expr = ast.queryStatement.fromClause.binaryObjectList.head
		else
			throw new IllegalStateException ("The AST is a wrong state. No child nodes were found on From Clause")			
		
		val EObject container = expr.eContainer
		
		if (null == join.left)
			join.setLeft (expr)
		else	
			join.setRight (expr)
		
		container.replaceNode(expr,join)
	}
	
	def void addToFromClause (hql.Query ast, hql.FromExpression old, hql.BinaryFromExpression binary) {
		// Remove single binding from From clause
		old.remove(ast)
		// Add to From
		ast.queryStatement.fromClause.binaryObjectList.add(binary)
	} 
	
	def void removeFromObjectSourceByAlias (hql.Query ast, String alias) {
		// This delete from list should be checked if it does not violate any concurrent list rule
		var i = 0
		while (i < ast.queryStatement.fromClause.objectList.size) {
			var o = ast.queryStatement.fromClause.objectList.get(i)
			if (o instanceof hql.ObjectSource)
				if ( (o as hql.ObjectSource).alias.equalsIgnoreCase(alias) )
					(o as hql.ObjectSource).delete
		} 
	}
	 
	def private void createWhereIfNotExist (hql.Query ast) {
		if (null == ast.queryStatement.whereClause) {
			val where = HqlFactory::eINSTANCE.createWhereClause
			where.clause = "where"
			ast.queryStatement.setWhereClause(where)
		}
	}
	 
	def void addToWhereClause (hql.Query ast, hql.BinaryExpression binary) {
		ast.createWhereIfNotExist
		ast.queryStatement.whereClause.addNode(binary)
	}
	
	def hql.InnerJoin createInnerJoin (hql.FromExpression left, hql.FromExpression right) {
		var hql.InnerJoin join = HqlFactory::eINSTANCE.createInnerJoin
		join.setLeft(left)
		join.setRight(right)
		return join
	}

	def hql.LeftJoin createLeftJoin (hql.FromExpression left, hql.FromExpression right) {
		var hql.LeftJoin join = HqlFactory::eINSTANCE.createLeftJoin
		join.setLeft(left)
		join.setRight(right)
		return join
	}


	def hql.ThetaJoin createThetaJoin (hql.FromExpression left, hql.FromExpression right) {
		var hql.ThetaJoin join = HqlFactory::eINSTANCE.createThetaJoin
		join.setLeft(left)
		join.setRight(right)
		return join
	}
	
	def boolean containerIsWhereClause (EObject object) {
		var result = false
		switch (object.eContainer) {
			hql.WhereClause : result = true
			hql.QueryStatement : result = false
		default :
			result = object.eContainer.containerIsWhereClause	
		}
		return result
	}
	
	def hql.StringLiteral createLiteral (String id) {
		val hql.StringLiteral result = HqlFactory::eINSTANCE.createStringLiteral
		result.setValue(id)
		return result			
	}

	def hql.Identifier createIdentifier (String id) {
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(id)
		return result
	}	
		
	def void replaceNode (EObject old, EObject newOne) {
		old.eContainer.replaceNode(old, newOne)
	}

	def private dispatch void rebuild (hql.WhereClause container, hql.Expression old, hql.Expression current) {
		container.expression = current
	}

	def private dispatch void rebuild (hql.SelectClause container, hql.Expression old, hql.Expression current) {
		container.expressions.remove(old)
		if (null != current)
			container.expressions.add(current)
	}

	def private dispatch void rebuild (hql.FromClause container, hql.Expression old, hql.Expression current) {
		// Try both lists, since we do not know where is the expression to be removed
		container.binaryObjectList.remove(old)
		container.objectList.remove(old)
		if (null != current)
			if (current instanceof hql.BinaryFromExpression)
				container.binaryObjectList.add(current as hql.BinaryFromExpression)
			else
				container.objectList.add(current as hql.FromExpression)	
	}

	def private dispatch void rebuild (hql.BinaryExpression container, hql.Expression old,  hql.Expression current) {
		container.replaceNode(old, current) 
	}

	def private dispatch void rebuild (hql.SelectClause container, hql.Expression removed) {
		container.rebuild (removed, null)
	}

	def private dispatch void rebuild (hql.FromClause container, hql.Expression removed) {
		container.rebuild (removed, null)
	}

	def private dispatch void rebuild (hql.WhereClause container, hql.Expression removed) {
		container.rebuild (removed, null)
	}

	def private dispatch void rebuild (hql.BinaryExpression container, hql.Expression removed) {
		container.eContainer.rebuild (removed, container.getAnotherBinaryExpression(removed))
	}
	
	def private hql.Expression getAnotherBinaryExpression(hql.BinaryExpression container, EObject object) {
		if (container.left.equals(object))
			return container.right
		else
			return container.left	
	} 
	
	def void delete (hql.ObjectSource node) {
		EcoreUtil2::delete(node)
	} 
	
	def void delete (hql.Expression object, boolean recursive) {
		// Scenario 1 (where(and(x object)) -> (where x) :: OK
		// Scenario 2 (and (and x object) y) -> (and x y) 
		// Scenario 3 (and (and x object) and (y z)) -> (and x (and y z))
		
		// before removing object, we need to rearrange the AST, resolving the parent node
		object.eContainer.rebuild(object)
		EcoreUtil2::delete(object, recursive)
	}
 
	def replaceAlias (hql.Query ast, String from, String to) {
		if (null != ast.queryStatement.selectClause)
			ast.queryStatement.selectClause.replaceAlias(from, to)
		if (null != ast.queryStatement.whereClause)
			ast.queryStatement.whereClause.replaceAlias(from, to)
		if (null != ast.queryStatement.orderByClause)
			ast.queryStatement.orderByClause.replaceAlias(from, to)
	}
	 
	def private boolean isSelectClauseContainer (EObject object) {
		var result = false
		switch (object.eContainer) {
			hql.SelectClause : result = true
			hql.QueryStatement : result = false
		default :
			result = object.eContainer.isSelectClauseContainer	
		}
		return result
	}

	def private boolean containerIsFromClause (EObject object) {
		var result = false
		switch (object.eContainer) {
			hql.FromClause : result = true
			hql.QueryStatement : result = false
		default :
			result = object.eContainer.containerIsFromClause	
		}
		return result
	}
	
	def hql.FromExpression getLastFromExpression(hql.FromClause from) {
		var hql.FromExpression expr = from.binaryObjectList.last
		if (null == expr)
			expr = from.objectList.last
		return expr		
	}
	
	def dispatch String getLabel (hql.Identifier arg0) {
		return arg0.id
	}
	
	def dispatch String getLabel (hql.Function arg0) {
		return arg0.name
	}
	
	def int search (hql.QualifiedName qName, String string) {
		var int i = 0
		while (i < qName.qualifiers.size) {
			if (string.equalsIgnoreCase(qName.qualifiers.get(i).label))
				return i
			i = i + 1			
		}
		
		return -1		
	}
 		
}