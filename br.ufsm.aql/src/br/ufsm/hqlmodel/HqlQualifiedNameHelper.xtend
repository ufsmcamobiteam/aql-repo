package br.ufsm.hqlmodel

import hql.HqlFactory
import br.ufsm.hql.resolvers.HqlParamFunctionResolver
 
class HqlQualifiedNameHelper {
	def static hql.QualifiedName cloneQualifiedName (hql.QualifiedName qualifiedName) {
		val hql.QualifiedName result = HqlFactory::eINSTANCE.createQualifiedName
		for (hql.QualifiedElement e : qualifiedName.qualifiers) 
			result.qualifiers.add(cloneElement(e)  as hql.QualifiedElement)
		 
		return result	
	}

	def static dispatch hql.QualifiedElement cloneElement (hql.Identifier arg0) {
		val hql.Identifier result = HqlFactory::eINSTANCE.createIdentifier
		result.setId(arg0.id)	
		return result
	}
	
	def static dispatch hql.QualifiedElement cloneElement (hql.Function arg0) {
		val HqlParamFunctionResolver paramResolver = new HqlParamFunctionResolver();
		val hql.Function result = HqlFactory::eINSTANCE.createFunction
		result.setName(arg0.name)
		for (hql.Expression p : arg0.params)
			result.params.add(paramResolver.resolveParam(p))	
		return result
	}
	
}