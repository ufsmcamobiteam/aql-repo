package br.ufsm.validation;

public enum ErrorList {
	  SEMANTICANALYSIS_ERROR (0, "Error performing semantic analisys"),
	  DUPLICATE_BINDING (1, "Duplicated binding alias definition on find clause"),
	  BINDING_ERROR (2, "Unknown object. Check find clause");
	  
	  
	  private final int code;
	  private final String description;

	  private ErrorList(int code, String description) {
	    this.code = code;
	    this.description = description;
	  }

	  public String getDescription() {
	     return description;
	  }

	  public int getCode() {
	     return code;
	  }

	  @Override
	  public String toString() {
	    return code + ": " + description;
	  }
}
