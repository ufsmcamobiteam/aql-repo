package br.ufsm.validation;
 
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.validation.Check;

import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.BindingObject;
import br.ufsm.aql.Expression;
import br.ufsm.aql.FindClause;
import br.ufsm.aql.Identifier;
import br.ufsm.aql.QualifiedElement;
import br.ufsm.aql.QualifiedName;
import br.ufsm.aql.QueryStatement;
import br.ufsm.aql.ReturnsClause;
import br.ufsm.aql.resource.StringUtil;

/* Semantic Analysis List 
 * 
 * 2. Implements function using other than interfaces
 *    find class c, interface i where c.implements(i) and i.fullQualifiedName = 'Foo' - OK
 *    find class c, aspect i where c.implements(i) and i.fullQualifiedName = 'Foo' - NOT OK   
 * 
 * 3. Cannot resolve function that is no terminal
 *    Ex: a.pointcut.kind(..)
 *        pointcut should be resolved as a function because is not terminal
 *        kind is terminal but it's a function then should also be recognized as a function
 *        
 * 4. Modifier Function should have a domain parameter type list
 * 5. Pointcut.kind function should have a domain parameter type list
 *        
 */

public class AQLJavaValidator extends AbstractAQLValidator {
//	@Check
//	public void check (Query query) {
//		System.out.println("validating...");
//	}

	/**
	 * @param find the Find Clause
	 *  Duplicated alias on find clause
	 *  find aspect a b, class a 
	 */
	@Check
	public void checkDuplicatesBindingAlias (FindClause find) {
		for (BindingObject bind : find.getBindingObject()) {
			Set<String> set = new HashSet<String>(bind.getAlias());
			if (set.size() < bind.getAlias().size()) 
				error (ErrorList.DUPLICATE_BINDING.getDescription(), 
						AqlPackage.Literals.FIND_CLAUSE__CLAUSE, 
						ErrorList.DUPLICATE_BINDING.getCode());
				return;
		}		
	}	
	
	@Check
	public void checkSelectBindingAlias (ReturnsClause select) {
		FindClause from = getFindClause(select);
		if (from == null) {
			error(ErrorList.SEMANTICANALYSIS_ERROR.getDescription(), AqlPackage.Literals.RETURNS_CLAUSE__EXPRESSIONS, ErrorList.SEMANTICANALYSIS_ERROR.getCode());
			return;
		}	

		for (Expression selectExpression: select.getExpressions()) {
			EList<QualifiedName> qualified = new BasicEList<QualifiedName>();
			searchObjectIdentifier(selectExpression, qualified);
			for (QualifiedName obj : qualified) 
				if (! existBindingAlias(from, obj) ) {
					String element = "";// = StringUtil.qualifyRoot(obj.getQualifier()); 
					error(getMessageError("Type not found '{0}'[{1}:{2}]", 
							element, 
							selectExpression)
					   , AqlPackage.Literals.RETURNS_CLAUSE__EXPRESSIONS);
					return;
				}	
		}	
	}
	
	private EList<QualifiedName> searchObjectIdentifier (Expression expr, EList<QualifiedName> list) {
		if (expr instanceof QualifiedName) 
			list.add((QualifiedName)expr);
		else	
		for (EObject e : expr.eContents())
			if (e instanceof Expression)
				list = searchObjectIdentifier((Expression)e, list);
		
		return list;
	}
	
	private String getMessageError (String message, String element, EObject object) {
		INode node = NodeModelUtils.getNode(object);
		String lin = Integer.toString(node.getStartLine());
		String col = Integer.toString(node.getOffset());
		return StringUtil.getMessage(message, element, lin, col);
	}
	
	private FindClause getFindClause (EObject clause) {
		if (clause instanceof FindClause)
			return (FindClause)clause;
		EObject superClause = clause.eContainer();
		while ( (superClause != null) && (! (superClause instanceof QueryStatement)) )
			superClause = superClause.eContainer();
		
		if (superClause instanceof QueryStatement)
			return ((QueryStatement)superClause).getFind();
		
		return null; 
	}
	
	private boolean existBindingAlias (FindClause find, QualifiedName alias) {
		boolean result = true;
		QualifiedElement element = alias.getQualifiers().get(0);
		String aliasRoot = "";
		if (element instanceof Identifier) {
			aliasRoot = ((Identifier)element).getId();
			result = existAliasFind(find, aliasRoot);
		}
 		return result;
	}

	private boolean existAliasFind(FindClause find, String aliasRoot) {
		for (BindingObject b : find.getBindingObject()) 
			for (String a : b.getAlias())
				if (aliasRoot.equals(a))
					return true;
		return false;
	}
}