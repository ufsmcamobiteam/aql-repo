package br.ufsm.aql.exception;

@SuppressWarnings("serial")
public class ResolutionErrorException extends Exception {

	public ResolutionErrorException(String arg0) {
		super(arg0);
	}
	
}
