package br.ufsm.aql.exception;

public class InitializationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitializationException (Throwable e) {
		super(e);
	}
}
