package br.ufsm.aql.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.PropertyConfigurator;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;

import br.ufsm.aql.exception.InitializationException;

public class Setting {
	private String uri;
	private static Setting SETTINGSINSTANCE;
	private final String LOG4J = "log4J";
	private final String CONFIGFILE = "aql.conf";
	private final String CONFIGMAPFILE = "aqlmap.xml";
	private final String LANGUAGE = "language";
	private static final String DEFAULTURI = "conf/";
	
	private Multimap<String, String> semanticMap = ArrayListMultimap.create();
	private Map<String, String> resolverMap = new HashMap<String, String>(); 
	private Properties properties = new Properties();
	private ResourceBundle messageResource;

	public String getConfigFileUri () {
		return uri + CONFIGFILE;
	}

	public String getConfigMapFileUri () {
		return uri + CONFIGMAPFILE;
	}

	public String resolve (String in) {
		return resolverMap.get(in.toUpperCase());
	}
	
	public static String getDefaulURI() {
		return DEFAULTURI;
	}

	public static Setting getINSTANCE(String uri) {
		if ( (null == SETTINGSINSTANCE) || (! SETTINGSINSTANCE.getUri().equalsIgnoreCase(uri)) ) 
			SETTINGSINSTANCE = new Setting(uri);
			
		return SETTINGSINSTANCE;
	}
	
	public Setting(String uri) throws InitializationException {
		if (uri.length() == 0)
			uri = DEFAULTURI;
		this.setUri(uri);
		loadConfig();
		loadLogPropertyConfigurator();
		loadResourceBundle();
		loadResolverMap();
	}
	
	private void loadResourceBundle() throws InitializationException {
		Locale currentLocale = new Locale(properties.getProperty(LANGUAGE));
		messageResource = ResourceBundle.getBundle("resources.Messages", currentLocale);
	}

	private void loadLogPropertyConfigurator() throws InitializationException {
		String log4j = properties.getProperty(LOG4J);
		if (log4j != null) {
			PropertyConfigurator.configure(log4j);
		}	
	}

	private void loadConfig() throws InitializationException {
		String fileName = getConfigFileUri();
	    InputStream is;
		try {
			is = new FileInputStream(fileName);
			properties.load(is);
		} catch (FileNotFoundException e) {
			throw new InitializationException(e);
		} catch (IOException e) {
			throw new InitializationException(e);
		}
	}
	
	public void loadResolverMap() throws InitializationException {
		// TODO: Modularize these exceptions
		resolverMap.clear();
		File f = new File(getConfigMapFileUri());
		SAXBuilder sb = new SAXBuilder();
		try {
			Document d = sb.build(f);
			Element rootMapping = d.getRootElement();
			for (Element e : rootMapping.getChild("bindings").getChildren()) 
				resolverMap.put(e.getAttributeValue("in").toUpperCase(), e.getAttributeValue("out"));
		} catch (JDOMException e) {
			throw new InitializationException(e);
			//loadDefaultMap();
		} catch (IOException e) {
			//loadDefaultMap();
			throw new InitializationException(e);
		}
	}

	private void loadDefaultMap() {
		resolverMap.clear();
		resolverMap.put("PROJECT", "AOJProject");
		resolverMap.put("ASPECT", "AOJAspectDeclaration");
		resolverMap.put("CLASS", "AOJClassDeclaration");
		resolverMap.put("PACKAGE", "AOJPackageDeclaration");
		resolverMap.put("INTERFACE", "AOJInterfaceDeclaration");
	}

	public Map<String, String> getResolverMap() {
		return resolverMap;
	}

	public Multimap<String, String> getSemanticMap() {
		return semanticMap;
	}
	
	public ResourceBundle getMessageResource() {
		return messageResource;
	}

	public void setMessageResource(ResourceBundle messageResource) {
		this.messageResource = messageResource;
	}
	
	public String getMessage (String key, String...replacements) {
		return StringUtil.getMessage(getMessageResource().getString(key), replacements);
	}

	public String getUri() {
		return uri;
	}
	
	private void setUri (String uri) {
		this.uri = uri;
	}
}
