package br.ufsm.aql.resource

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import br.ufsm.hql.factories.HqlTransformationFactory
import br.ufsm.hql.ast.HqlASTTool
import org.eclipse.xtend.lib.annotations.Accessors

abstract class CommonTransformationImpl implements CommonTransformation {
	@Accessors br.ufsm.aql.Query aqlASTRoot
	@Accessors Resource fromResource  
	@Accessors IFileSystemAccess fsa  
	@Accessors extension HqlASTTool hqlASTTool = HqlTransformationFactory::eINSTANCE.createHqlASTTool
        
	override void run() { 
		setup
	}  
	
	 def void setup()
	
}