package br.ufsm.aql.resource;

public class StringUtil {
	public static String getMessage (String message, String... replacements) {
		int i = 0;
		String result = message;
		for (String replacement : replacements) {
			String from = "{"+ i++ +"}";
			result = result.replace(from, replacement);
		}
		return result;
	}
	 
//	public static String unqualify (String qualifiedName) {
//			int loc = qualifiedName.lastIndexOf(QUALIFIER);
//			return ( loc < 0 ) ? qualifiedName : qualifiedName.substring( qualifiedName.lastIndexOf(".") + 1 );
//	}
//	
//	public static String unqualifyRoot (String qualifiedName) {
//		int loc = qualifiedName.lastIndexOf(QUALIFIER);
//		return ( loc < 0 ) ? qualifiedName : qualifiedName.substring(0, loc);
//	}
	
//	public static String qualifyRoot (List<String> elements) {
//		//String qualify = qualify(elements);
//		//int loc = qualify.lastIndexOf(".");
//		//return (loc < 0)? qualify : qualify.substring(0, loc);
//		return null;
//	}

	
}
