package br.ufsm.aql.resource
 
import java.util.Map
import java.util.HashMap
  
class HqlAliasHandler {
	private static Map<String, Integer> functionCounter = new HashMap<String, Integer>()
	
	def public static int functionUsage (String functionName) {
		if (functionCounter.containsKey(functionName)) {
			val int counter = functionCounter.get(functionName) + 1
			functionCounter.put(functionName, counter)				
			return counter
		}
		
		functionCounter.put(functionName, 1)
		return 1	
	} 	 
	
	def public static Map<String, Integer> getFunctionCounterMap () {
		return functionCounter
	}
}