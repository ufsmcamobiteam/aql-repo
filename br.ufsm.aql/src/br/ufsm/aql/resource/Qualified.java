package br.ufsm.aql.resource;

import java.util.ArrayList;	
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufsm.aql.resource.HqlDictionary;
import hql.HqlFactory;
 
/**
 * @author Cristiano De Faveri
 * Federal University of Santa Maria
 *
 */
public class Qualified {
	private String qualifiedString = "";
	private Qualified before;
	private Qualified after;
	private boolean isRoot;
	private boolean isTail;
	private int index;
	
	/**
	 * @param qName The qualified name to be transformed in Qualified
	 * @return The Qualified object 
	 */
	public static Qualified toQualified (hql.QualifiedName qName) {
		List<String> elements = new ArrayList<String>();
		Qualified result = null;
		for (hql.QualifiedElement element : qName.getQualifiers()) {
			if (element instanceof hql.Identifier) // This should be an attribute of hql.QualifiedElement
				elements.add( ((hql.Identifier)element).getId() ); 
			else
			if (element instanceof hql.Function)
				elements.add( ((hql.Function)element).getName() );
		}
		
		for (String e : elements)
			if (null == result)
				result = Qualified.qualify(e);
			else
				result = result.add(e);
				
		return result;
	}

	/**
	 * @param fullString The String to be transformed in QualifiedName
	 * @return The Qualified object
	 */
	public static Qualified toQualified (String fullString) {
		validate(fullString);
		
		List<String> strs = new ArrayList<String>();
		String full = fullString;
		while (full.contains(HqlDictionary.QUALIFIER)) {
			strs.add(full.substring(0, full.indexOf(HqlDictionary.QUALIFIER)));
			full = full.substring(full.indexOf(HqlDictionary.QUALIFIER) + 1);
		}
		
		strs.add(full);
		
		Qualified qName = null;
		
		for (String part : strs) {
			if (null == qName) 
				qName = Qualified.qualify(part);
			else
				qName = qName.add(part);
		}
		
		return qName.getRoot();
	}
	
	/**
	 * Validate a string against qualified string rules
	 * @param fullString The string to be validated
	 */
	private static void validate (String fullString) {
		if (! isValidQualifier(fullString))
			throw new IllegalArgumentException("Qualified String is not valid = " + fullString);	
	}
	
	/**
	 * Use this method to start a Qualified name. Subsequent node should use add method.
	 * Ex : Qualified.qualify("a").add("b"), results in a.b
	 * @param element The element to be added
	 * @return The start Qualified object.
	 */
	public static Qualified qualify(String element) {
		validate(element);
		Qualified q = new Qualified();
		q.qualifiedString = element;
		q.index = 0;
		q.isRoot = true;
		q.isTail = true;
		
		return q; 
	}
		
	private Qualified () {
	}
	
	public String unqualify () { 
		return getTail().toString();
	}
	
//	private static String getLabel (hql.QualifiedElement element) {
//		if (element instanceof hql.Identifier) 
//			return ((hql.Identifier)element).getId();
//		else if (element instanceof hql.Function)
//			return ((hql.Function)element).getName();
//		return "";
//	}

	
	/**
	 * @return the entire string except the tail
	 * ex: a.b.c, unqualifyRoot returns a.b if current is c
	 * ex: a.b.c, unqualifyRoot returns a if current is b
	 * ex: a, unqualifiyRoot returns a
	 */
	public String unqualifyRoot () {
		StringBuilder sb = new StringBuilder();
		Qualified q = getRoot();
		if (q.numElements() == 1)
			sb.append(q.toString());
		else {
			while (q != this) { 
				sb.append(q.toString());
				if ( q.getNext() != this ) 
					sb.append(HqlDictionary.QUALIFIER);
				q = q.getNext();
			}	
		}	
		return sb.toString();	
	}

	/**
	 * @param element the element to be added
	 * @return The Qualified node of the string
	 */
	public Qualified add (String element) {
		validate(element);
		isTail = false; // this is not the last element anymore
		
		Qualified q = new Qualified(); 
		q.qualifiedString = element;
		q.isRoot = false; 
		q.isTail = true; // this is the last element
		q.before = this;
		this.after = q;
		q.index = index + 1;

		return q;
	}
	
	/**
	 * @return the index of the node, root node = 0
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * @return the next node of the qualified name
	 */
	public Qualified getNext() {
		return after;
	}
	
	/**
	 * @return true whether there is next node. False otherwise.
	 */
	public boolean hasNext() {
		return (after != null);
	}

	/**
	 * @return True whether there is previous node. False otherwise. 
	 */
	public boolean hasBefore() {
		return (before != null);
	}

	
	/**
	 * @return The number of elements in the qualified name
	 * ex : a.b, numElements returns 2
	 */
	public int numElements() {
		return getTail().index + 1;
	}
	
	public Qualified getBefore() {
		return before;
	}
	
	public boolean isRoot() {
		return isRoot;
	}
	
	public boolean isTail() {
		return isTail;
	}
	
	public Qualified getRoot() {
		return (isRoot ? this : before.getRoot());
	}

	public Qualified getTail() {
		return (isTail ? this : after.getTail());
	}
	
	/**
	 *  Match patterns from like
	 *  ex : a.b, from = a, to = x -> x.b
	 *  ex : a , from = a, to = x -> x
	 *  ex : a.b.c, from = a.b, to = x -> x.c 
	 * @param from The from Qualified to search
	 * @return A pair start index, end index of the from Qualified
	 */

	public Map<Integer, Integer> contains (Qualified from) {
		int start = -1;
		int end = -1;
		Map<Integer, Integer> result = new HashMap<Integer, Integer>();
		Qualified qThis = getRoot();
		Qualified qFrom = from;	
		
		int i = -1;
		int j = -1;		

		start = qThis.search(qFrom.getRoot().toString()); // try to match start point
		end = start;
		if (start >= 0) { // Match start point ?
			i = 1;		
			j = start + 1;
			while ( (i < qFrom.size()) && (j < qThis.size()) )  { // While is not the last token
				if (qThis.get(j).toString().equalsIgnoreCase((qFrom.get(i).toString()))) {//  matches ? keep searching
					end = qThis.get(j).getIndex();
					i++;
					j++;
				} else	
					break; // do not match, leave
			}
		}	

		if (i < qFrom.size()) { // did not consume all qFrom tokens
			start = -1;
			end = -1;
		}
			
		result.put(start, end); 

		return result;
	} 

	public int search(String element) {
		return search (element, false);
	}

	public int search(String element, boolean isCaseSensitive) {
		Qualified q = getRoot();
			
		if (! isCaseSensitive)
			while ( (! q.isTail) && (! q.toString().equalsIgnoreCase(element)) )
				q = q.getNext();
		else
			while ( (! q.isTail) && (! q.toString().equals(element)) )
				q = q.getNext();
		
		if (q.isTail)
			if (! isCaseSensitive) {
				if (q.toString().equalsIgnoreCase(element))
					return q.getIndex();
			} else if (q.toString().equals(element))
				return q.getIndex();

		return q.isTail ? -1 : q.getIndex();
	}
	
	public Qualified get(int i) {
		if (index == i)
			return this;

		if (i < index)
			if (!isRoot)
				return before.get(i);
			else	
				return null;
		else
		if (!isTail)
			return after.get(i);
		else
			return null;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * returns only the current QualifiedString
	 * Ex: a.b.c, if current Qualified carries b token, then b will be returned
	 */
	@Override
	public String toString() {
		return qualifiedString;
	}
	
	/**
	 * @return The full Qualified String 
	 * ex: a.b.c, regardeless the current Qualified, return function will be a.b.c
	 */
	public String toFullString() {
		StringBuilder fullStr = new StringBuilder();
		Qualified q = getRoot();
		while (! q.isTail) {
			fullStr.append(q.toString()).append(HqlDictionary.QUALIFIER);
			q = q.getNext();
		}	
		
		fullStr.append(q.toString());
		return fullStr.toString();
	}

	public static boolean isValidQualifier(String qualifiedAsString) {
		return (! qualifiedAsString.isEmpty())  
		    && (! qualifiedAsString.endsWith(HqlDictionary.QUALIFIER))
		    && (! containsSpecialChars(qualifiedAsString) );
	}
	
	private static boolean containsSpecialChars (String element) {
		String[] notAllowed = {"~", "`", "!", "@", "#", "$", "%", "^", "&", "(", ")", "[", "]", "{", "}", "+", "=", "," , "?", "\\", "/", "|", "'"};
		int i = 0;
		while ((i < notAllowed.length) && (! element.contains(notAllowed[i])))
			i++;
		return i < notAllowed.length;
	}
	
	/**
	 * The method just convert all elements to Identifiers
	 * Note that if some element represents a parameterized function,
	 * this method is not adequate.
	 * @return The corresponding qualified name for the qualified object
	 */
	public hql.QualifiedName toQualifiedName () {
		Qualified qualified = getRoot();
		hql.QualifiedName result = HqlFactory.eINSTANCE.createQualifiedName();
		while (qualified.getNext() != null) {
			hql.Identifier element = HqlFactory.eINSTANCE.createIdentifier();
			element.setId(qualified.toString());
			result.getQualifiers().add(element);
			qualified = qualified.getNext();
		}

		hql.Identifier element = HqlFactory.eINSTANCE.createIdentifier();
		element.setId(qualified.toString());
		result.getQualifiers().add(element);
		
		return result;
	}
	
	public int size() {
		return getTail().getIndex() + 1;
	}
	
	public List<Qualified> asList() {
		List<Qualified> result = new ArrayList<Qualified>();
		Qualified q = getRoot();
		result.add(q);
		while (q.hasNext()) {
			q = q.getNext();
			result.add(q);
		}
		return result;
	}
	
}
