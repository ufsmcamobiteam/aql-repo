package br.ufsm.aql.resource

import org.eclipse.emf.ecore.EObject
    
class ASTPrinter {
	private int level = 0;
	private String printPack ;
	
	new (String printPack) {
		this.printPack = printPack
	}
	
	def void printLinear (EObject node) {
		if (node.eContents.filter(o | o.eClass.EPackage.name.startsWith(printPack)).size > 0) {
			print('(')
			node.printMeOneLine	
			for (o : node.eContents.filter(o | o.eClass.EPackage.name.startsWith(printPack))) 
				o.printLinear
			print(' )')
		} else
			node.printMeOneLine	
	}
	
	def void printMeOneLine (EObject node) {
		print (' ' + node.eClass.EPackage.name + '.' + node.eClass.name)
	}
	
	def void printTree (EObject node) {
		node.printMe
		for (o : node.eContents.filter(o | o.eClass.EPackage.name.startsWith(printPack))) {
			level = level + 1
			o.printTree
			level = level - 1
		}
	}	
	
	def private printMe (EObject node) {
		var int i = 0;
		while (i < level) {
			if (i == 0)
				println(' ')
			else	
				print(' ')
			i = i + 1
		}
		if (null == node)
			print ('+- null')
		else
			print ('+- ' + node.eClass.EPackage.name + '.' + node.eClass.name + ' [' 
				+ node.toString + ']'
			)	
	} 
	
}