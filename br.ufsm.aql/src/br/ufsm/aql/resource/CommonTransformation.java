package br.ufsm.aql.resource;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
  
public interface CommonTransformation {
	public void run();
	public EObject getASTRoot();
}
  