package br.ufsm.serializer;

import br.ufsm.aql.Add;
import br.ufsm.aql.And;
import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.Aspect;
import br.ufsm.aql.BindingObject;
import br.ufsm.aql.Div;
import br.ufsm.aql.Equals;
import br.ufsm.aql.Exponential;
import br.ufsm.aql.False;
import br.ufsm.aql.FindClause;
import br.ufsm.aql.FloatLiteral;
import br.ufsm.aql.Function;
import br.ufsm.aql.Greater;
import br.ufsm.aql.GreaterEqual;
import br.ufsm.aql.GroupByClause;
import br.ufsm.aql.Identifier;
import br.ufsm.aql.In;
import br.ufsm.aql.IntegerLiteral;
import br.ufsm.aql.Interface;
import br.ufsm.aql.Less;
import br.ufsm.aql.LessEqual;
import br.ufsm.aql.Like;
import br.ufsm.aql.Minus;
import br.ufsm.aql.Mod;
import br.ufsm.aql.Mult;
import br.ufsm.aql.Not;
import br.ufsm.aql.NotEquals;
import br.ufsm.aql.Null;
import br.ufsm.aql.Or;
import br.ufsm.aql.OrderByClause;
import br.ufsm.aql.ParsExpression;
import br.ufsm.aql.Project;
import br.ufsm.aql.QualifiedName;
import br.ufsm.aql.Query;
import br.ufsm.aql.QueryStatement;
import br.ufsm.aql.ReturnsClause;
import br.ufsm.aql.StringLiteral;
import br.ufsm.aql.True;
import br.ufsm.aql.UnaryMinus;
import br.ufsm.aql.WhereClause;
import br.ufsm.services.AQLGrammarAccess;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class AQLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private AQLGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == AqlPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case AqlPackage.ADD:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Add(context, (Add) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.AND:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1()) {
					sequence_And(context, (And) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.ASPECT:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (Aspect) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.BINDING_OBJECT:
				if(context == grammarAccess.getBindingObjectRule()) {
					sequence_BindingObject(context, (BindingObject) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.CLASS:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (br.ufsm.aql.Class) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.DIV:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Mult(context, (Div) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.ENUM:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (br.ufsm.aql.Enum) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.EQUALS:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (Equals) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.EXPONENTIAL:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Exponential(context, (Exponential) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.FALSE:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (False) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.FIND_CLAUSE:
				if(context == grammarAccess.getFindClauseRule()) {
					sequence_FindClause(context, (FindClause) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.FLOAT_LITERAL:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (FloatLiteral) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.FUNCTION:
				if(context == grammarAccess.getFunctionRule() ||
				   context == grammarAccess.getQualifiedRule() ||
				   context == grammarAccess.getQualifiedElementRule()) {
					sequence_Function(context, (Function) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.GREATER:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (Greater) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.GREATER_EQUAL:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (GreaterEqual) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.GROUP_BY_CLAUSE:
				if(context == grammarAccess.getGroupByClauseRule()) {
					sequence_GroupByClause(context, (GroupByClause) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.IDENTIFIER:
				if(context == grammarAccess.getIdentifierRule() ||
				   context == grammarAccess.getQualifiedRule() ||
				   context == grammarAccess.getQualifiedElementRule() ||
				   context == grammarAccess.getSimpleQualifiedRule()) {
					sequence_Identifier(context, (Identifier) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.IN:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_In(context, (In) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.INTEGER_LITERAL:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (IntegerLiteral) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.INTERFACE:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (Interface) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.LESS:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (Less) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.LESS_EQUAL:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (LessEqual) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.LIKE:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (Like) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.MINUS:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Add(context, (Minus) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.MOD:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Mult(context, (Mod) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.MULT:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Mult(context, (Mult) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.NOT:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Unary(context, (Not) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.NOT_EQUALS:
				if(context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()) {
					sequence_Relation(context, (NotEquals) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.NULL:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (Null) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.OR:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1()) {
					sequence_Or(context, (Or) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.ORDER_BY_CLAUSE:
				if(context == grammarAccess.getOrderByClauseRule()) {
					sequence_OrderByClause(context, (OrderByClause) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.PACKAGE:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (br.ufsm.aql.Package) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.PARS_EXPRESSION:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getParsExpressionRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_ParsExpression(context, (ParsExpression) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.PROJECT:
				if(context == grammarAccess.getObjectTypeRule()) {
					sequence_ObjectType(context, (Project) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.QUALIFIED_NAME:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getQualifiedNameRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_QualifiedName(context, (QualifiedName) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSimpleQualifiedNameRule()) {
					sequence_SimpleQualifiedName(context, (QualifiedName) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.QUERY:
				if(context == grammarAccess.getQueryRule()) {
					sequence_Query(context, (Query) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.QUERY_STATEMENT:
				if(context == grammarAccess.getQueryStatementRule()) {
					sequence_QueryStatement(context, (QueryStatement) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.RETURNS_CLAUSE:
				if(context == grammarAccess.getReturnsClauseRule()) {
					sequence_ReturnsClause(context, (ReturnsClause) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.STRING_LITERAL:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (StringLiteral) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.TRUE:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getAtomRule() ||
				   context == grammarAccess.getExponentialRule() ||
				   context == grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getLiteralRule() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Literal(context, (True) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.UNARY_MINUS:
				if(context == grammarAccess.getAddRule() ||
				   context == grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1() ||
				   context == grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1() ||
				   context == grammarAccess.getAndRule() ||
				   context == grammarAccess.getAndAccess().getAndLeftAction_1_1() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getInRule() ||
				   context == grammarAccess.getInAccess().getInLeftAction_1_0_1() ||
				   context == grammarAccess.getMultRule() ||
				   context == grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1() ||
				   context == grammarAccess.getMultAccess().getModLeftAction_1_0_2_1() ||
				   context == grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1() ||
				   context == grammarAccess.getOrRule() ||
				   context == grammarAccess.getOrAccess().getOrLeftAction_1_0_1() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1() ||
				   context == grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1() ||
				   context == grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1() ||
				   context == grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1() ||
				   context == grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1() ||
				   context == grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1() ||
				   context == grammarAccess.getUnaryRule()) {
					sequence_Unary(context, (UnaryMinus) semanticObject); 
					return; 
				}
				else break;
			case AqlPackage.WHERE_CLAUSE:
				if(context == grammarAccess.getWhereClauseRule()) {
					sequence_WhereClause(context, (WhereClause) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (left=Add_Add_1_0_0_1 right=Mult)
	 */
	protected void sequence_Add(EObject context, Add semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.ADD__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.ADD__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.ADD__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.ADD__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAddAccess().getRightMultParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Add_Minus_1_0_1_1 right=Mult)
	 */
	protected void sequence_Add(EObject context, Minus semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MINUS__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MINUS__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MINUS__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MINUS__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAddAccess().getRightMultParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=And_And_1_1 right=Relation)
	 */
	protected void sequence_And(EObject context, And semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.AND__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.AND__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.AND__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.AND__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAndAccess().getAndLeftAction_1_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAndAccess().getRightRelationParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (type=ObjectType alias+=ID+)
	 */
	protected void sequence_BindingObject(EObject context, BindingObject semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Exponential_Exponential_1_1 right=Exponential)
	 */
	protected void sequence_Exponential(EObject context, Exponential semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.EXPONENTIAL__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.EXPONENTIAL__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.EXPONENTIAL__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.EXPONENTIAL__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getExponentialAccess().getRightExponentialParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (clause='find' bindingObject+=BindingObject bindingObject+=BindingObject*)
	 */
	protected void sequence_FindClause(EObject context, FindClause semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID params+=Expression params+=Expression*)
	 */
	protected void sequence_Function(EObject context, Function semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (clause='group by' expressions+=SimpleQualifiedName expressions+=SimpleQualifiedName*)
	 */
	protected void sequence_GroupByClause(EObject context, GroupByClause semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     id=ID
	 */
	protected void sequence_Identifier(EObject context, Identifier semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.IDENTIFIER__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.IDENTIFIER__ID));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIdentifierAccess().getIdIDTerminalRuleCall_0(), semanticObject.getId());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=In_In_1_0_1 right=Unary)
	 */
	protected void sequence_In(EObject context, In semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.IN__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.IN__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.IN__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.IN__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInAccess().getInLeftAction_1_0_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getInAccess().getRightUnaryParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='false'
	 */
	protected void sequence_Literal(EObject context, False semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.FALSE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.FALSE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=FLOAT
	 */
	protected void sequence_Literal(EObject context, FloatLiteral semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.FLOAT_LITERAL__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.FLOAT_LITERAL__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueFLOATTerminalRuleCall_1_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=INT
	 */
	protected void sequence_Literal(EObject context, IntegerLiteral semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.INTEGER_LITERAL__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.INTEGER_LITERAL__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_2_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='null'
	 */
	protected void sequence_Literal(EObject context, Null semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.NULL__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.NULL__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value=STRING
	 */
	protected void sequence_Literal(EObject context, StringLiteral semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.STRING_LITERAL__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.STRING_LITERAL__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueSTRINGTerminalRuleCall_0_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='true'
	 */
	protected void sequence_Literal(EObject context, True semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.TRUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.TRUE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Mult_Div_1_0_1_1 right=In)
	 */
	protected void sequence_Mult(EObject context, Div semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.DIV__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.DIV__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.DIV__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.DIV__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Mult_Mod_1_0_2_1 right=In)
	 */
	protected void sequence_Mult(EObject context, Mod semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MOD__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MOD__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MOD__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MOD__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMultAccess().getModLeftAction_1_0_2_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Mult_Mult_1_0_0_1 right=In)
	 */
	protected void sequence_Mult(EObject context, Mult semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MULT__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MULT__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.MULT__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.MULT__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='aspect'
	 */
	protected void sequence_ObjectType(EObject context, Aspect semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='class'
	 */
	protected void sequence_ObjectType(EObject context, br.ufsm.aql.Class semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='enum'
	 */
	protected void sequence_ObjectType(EObject context, br.ufsm.aql.Enum semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='interface'
	 */
	protected void sequence_ObjectType(EObject context, Interface semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='package'
	 */
	protected void sequence_ObjectType(EObject context, br.ufsm.aql.Package semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     value='project'
	 */
	protected void sequence_ObjectType(EObject context, Project semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OBJECT_TYPE__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Or_Or_1_0_1 right=And)
	 */
	protected void sequence_Or(EObject context, Or semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OR__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OR__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.OR__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.OR__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOrAccess().getOrLeftAction_1_0_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         clause='order by' 
	 *         expressions+=SimpleQualifiedName 
	 *         expressions+=SimpleQualifiedName* 
	 *         (option='asc' | option='desc')? 
	 *         havingExpression=Expression?
	 *     )
	 */
	protected void sequence_OrderByClause(EObject context, OrderByClause semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (expr+=Expression expr+=Expression*)
	 */
	protected void sequence_ParsExpression(EObject context, ParsExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((granular='distinct' | granular='all')? qualifiers+=Qualified qualifiers+=Qualified*)
	 */
	protected void sequence_QualifiedName(EObject context, QualifiedName semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (find=FindClause where=WhereClause? return=ReturnsClause orderby=OrderByClause? groupby=GroupByClause?)
	 */
	protected void sequence_QueryStatement(EObject context, QueryStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     query=QueryStatement
	 */
	protected void sequence_Query(EObject context, Query semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.QUERY__QUERY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.QUERY__QUERY));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getQueryAccess().getQueryQueryStatementParserRuleCall_0(), semanticObject.getQuery());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_Equals_1_0_0_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, Equals semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.EQUALS__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.EQUALS__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.EQUALS__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.EQUALS__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_Greater_1_0_1_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, Greater semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.GREATER__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.GREATER__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.GREATER__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.GREATER__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_GreaterEqual_1_0_2_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, GreaterEqual semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.GREATER_EQUAL__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.GREATER_EQUAL__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.GREATER_EQUAL__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.GREATER_EQUAL__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_Less_1_0_3_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, Less semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LESS__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LESS__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LESS__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LESS__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_LessEqual_1_0_4_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, LessEqual semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LESS_EQUAL__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LESS_EQUAL__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LESS_EQUAL__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LESS_EQUAL__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_Like_1_0_5_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, Like semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LIKE__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LIKE__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.LIKE__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.LIKE__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_NotEquals_1_0_6_1 right=Add)
	 */
	protected void sequence_Relation(EObject context, NotEquals semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.NOT_EQUALS__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.NOT_EQUALS__LEFT));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.NOT_EQUALS__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.NOT_EQUALS__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (clause='returns' expressions+=Expression resultAlias+=ID? (expressions+=Expression resultAlias+=ID?)*)
	 */
	protected void sequence_ReturnsClause(EObject context, ReturnsClause semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (qualifiers+=SimpleQualified qualifiers+=SimpleQualified*)
	 */
	protected void sequence_SimpleQualifiedName(EObject context, QualifiedName semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     exp=Unary
	 */
	protected void sequence_Unary(EObject context, Not semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.NOT__EXP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.NOT__EXP));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnaryAccess().getExpUnaryParserRuleCall_1_2_0(), semanticObject.getExp());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     expr=Unary
	 */
	protected void sequence_Unary(EObject context, UnaryMinus semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.UNARY_MINUS__EXPR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.UNARY_MINUS__EXPR));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUnaryAccess().getExprUnaryParserRuleCall_2_2_0(), semanticObject.getExpr());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (clause='where' expression=Expression)
	 */
	protected void sequence_WhereClause(EObject context, WhereClause semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.WHERE_CLAUSE__CLAUSE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.WHERE_CLAUSE__CLAUSE));
			if(transientValues.isValueTransient(semanticObject, AqlPackage.Literals.WHERE_CLAUSE__EXPRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, AqlPackage.Literals.WHERE_CLAUSE__EXPRESSION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0(), semanticObject.getClause());
		feeder.accept(grammarAccess.getWhereClauseAccess().getExpressionExpressionParserRuleCall_1_0(), semanticObject.getExpression());
		feeder.finish();
	}
}
