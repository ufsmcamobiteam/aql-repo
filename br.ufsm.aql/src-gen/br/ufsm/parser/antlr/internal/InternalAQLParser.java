package br.ufsm.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufsm.services.AQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAQLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_FLOAT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'find'", "','", "'project'", "'package'", "'class'", "'aspect'", "'interface'", "'enum'", "'where'", "'returns'", "'as'", "'group by'", "'order by'", "'asc'", "'desc'", "'having'", "'.'", "'or'", "'and'", "'='", "'>'", "'>='", "'<'", "'<='", "'like'", "'!='", "'+'", "'-'", "'*'", "'/'", "'%'", "'in'", "'not'", "'^'", "'('", "')'", "'null'", "'true'", "'false'", "'distinct'", "'all'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_FLOAT=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=5;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=10;

    // delegates
    // delegators


        public InternalAQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAQLParser.tokenNames; }
    public String getGrammarFileName() { return "../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g"; }



     	private AQLGrammarAccess grammarAccess;
     	
        public InternalAQLParser(TokenStream input, AQLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Query";	
       	}
       	
       	@Override
       	protected AQLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleQuery"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:67:1: entryRuleQuery returns [EObject current=null] : iv_ruleQuery= ruleQuery EOF ;
    public final EObject entryRuleQuery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuery = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
        	
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:71:2: (iv_ruleQuery= ruleQuery EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:72:2: iv_ruleQuery= ruleQuery EOF
            {
             newCompositeNode(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_ruleQuery_in_entryRuleQuery81);
            iv_ruleQuery=ruleQuery();

            state._fsp--;

             current =iv_ruleQuery; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuery91); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:82:1: ruleQuery returns [EObject current=null] : ( (lv_query_0_0= ruleQueryStatement ) ) ;
    public final EObject ruleQuery() throws RecognitionException {
        EObject current = null;

        EObject lv_query_0_0 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:86:28: ( ( (lv_query_0_0= ruleQueryStatement ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:87:1: ( (lv_query_0_0= ruleQueryStatement ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:87:1: ( (lv_query_0_0= ruleQueryStatement ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:88:1: (lv_query_0_0= ruleQueryStatement )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:88:1: (lv_query_0_0= ruleQueryStatement )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:89:3: lv_query_0_0= ruleQueryStatement
            {
             
            	        newCompositeNode(grammarAccess.getQueryAccess().getQueryQueryStatementParserRuleCall_0()); 
            	    
            pushFollow(FOLLOW_ruleQueryStatement_in_ruleQuery140);
            lv_query_0_0=ruleQueryStatement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQueryRule());
            	        }
                   		set(
                   			current, 
                   			"query",
                    		lv_query_0_0, 
                    		"QueryStatement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleQueryStatement"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:116:1: entryRuleQueryStatement returns [EObject current=null] : iv_ruleQueryStatement= ruleQueryStatement EOF ;
    public final EObject entryRuleQueryStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQueryStatement = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:117:2: (iv_ruleQueryStatement= ruleQueryStatement EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:118:2: iv_ruleQueryStatement= ruleQueryStatement EOF
            {
             newCompositeNode(grammarAccess.getQueryStatementRule()); 
            pushFollow(FOLLOW_ruleQueryStatement_in_entryRuleQueryStatement179);
            iv_ruleQueryStatement=ruleQueryStatement();

            state._fsp--;

             current =iv_ruleQueryStatement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQueryStatement189); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQueryStatement"


    // $ANTLR start "ruleQueryStatement"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:125:1: ruleQueryStatement returns [EObject current=null] : ( ( (lv_find_0_0= ruleFindClause ) ) ( (lv_where_1_0= ruleWhereClause ) )? ( (lv_return_2_0= ruleReturnsClause ) ) ( (lv_orderby_3_0= ruleOrderByClause ) )? ( (lv_groupby_4_0= ruleGroupByClause ) )? ) ;
    public final EObject ruleQueryStatement() throws RecognitionException {
        EObject current = null;

        EObject lv_find_0_0 = null;

        EObject lv_where_1_0 = null;

        EObject lv_return_2_0 = null;

        EObject lv_orderby_3_0 = null;

        EObject lv_groupby_4_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:128:28: ( ( ( (lv_find_0_0= ruleFindClause ) ) ( (lv_where_1_0= ruleWhereClause ) )? ( (lv_return_2_0= ruleReturnsClause ) ) ( (lv_orderby_3_0= ruleOrderByClause ) )? ( (lv_groupby_4_0= ruleGroupByClause ) )? ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:129:1: ( ( (lv_find_0_0= ruleFindClause ) ) ( (lv_where_1_0= ruleWhereClause ) )? ( (lv_return_2_0= ruleReturnsClause ) ) ( (lv_orderby_3_0= ruleOrderByClause ) )? ( (lv_groupby_4_0= ruleGroupByClause ) )? )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:129:1: ( ( (lv_find_0_0= ruleFindClause ) ) ( (lv_where_1_0= ruleWhereClause ) )? ( (lv_return_2_0= ruleReturnsClause ) ) ( (lv_orderby_3_0= ruleOrderByClause ) )? ( (lv_groupby_4_0= ruleGroupByClause ) )? )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:129:2: ( (lv_find_0_0= ruleFindClause ) ) ( (lv_where_1_0= ruleWhereClause ) )? ( (lv_return_2_0= ruleReturnsClause ) ) ( (lv_orderby_3_0= ruleOrderByClause ) )? ( (lv_groupby_4_0= ruleGroupByClause ) )?
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:129:2: ( (lv_find_0_0= ruleFindClause ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:130:1: (lv_find_0_0= ruleFindClause )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:130:1: (lv_find_0_0= ruleFindClause )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:131:3: lv_find_0_0= ruleFindClause
            {
             
            	        newCompositeNode(grammarAccess.getQueryStatementAccess().getFindFindClauseParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleFindClause_in_ruleQueryStatement235);
            lv_find_0_0=ruleFindClause();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQueryStatementRule());
            	        }
                   		set(
                   			current, 
                   			"find",
                    		lv_find_0_0, 
                    		"FindClause");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:147:2: ( (lv_where_1_0= ruleWhereClause ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==20) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:148:1: (lv_where_1_0= ruleWhereClause )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:148:1: (lv_where_1_0= ruleWhereClause )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:149:3: lv_where_1_0= ruleWhereClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getQueryStatementAccess().getWhereWhereClauseParserRuleCall_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWhereClause_in_ruleQueryStatement256);
                    lv_where_1_0=ruleWhereClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQueryStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"where",
                            		lv_where_1_0, 
                            		"WhereClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:165:3: ( (lv_return_2_0= ruleReturnsClause ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:166:1: (lv_return_2_0= ruleReturnsClause )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:166:1: (lv_return_2_0= ruleReturnsClause )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:167:3: lv_return_2_0= ruleReturnsClause
            {
             
            	        newCompositeNode(grammarAccess.getQueryStatementAccess().getReturnReturnsClauseParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleReturnsClause_in_ruleQueryStatement278);
            lv_return_2_0=ruleReturnsClause();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQueryStatementRule());
            	        }
                   		set(
                   			current, 
                   			"return",
                    		lv_return_2_0, 
                    		"ReturnsClause");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:183:2: ( (lv_orderby_3_0= ruleOrderByClause ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==24) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:184:1: (lv_orderby_3_0= ruleOrderByClause )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:184:1: (lv_orderby_3_0= ruleOrderByClause )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:185:3: lv_orderby_3_0= ruleOrderByClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getQueryStatementAccess().getOrderbyOrderByClauseParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleOrderByClause_in_ruleQueryStatement299);
                    lv_orderby_3_0=ruleOrderByClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQueryStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"orderby",
                            		lv_orderby_3_0, 
                            		"OrderByClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:201:3: ( (lv_groupby_4_0= ruleGroupByClause ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==23) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:202:1: (lv_groupby_4_0= ruleGroupByClause )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:202:1: (lv_groupby_4_0= ruleGroupByClause )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:203:3: lv_groupby_4_0= ruleGroupByClause
                    {
                     
                    	        newCompositeNode(grammarAccess.getQueryStatementAccess().getGroupbyGroupByClauseParserRuleCall_4_0()); 
                    	    
                    pushFollow(FOLLOW_ruleGroupByClause_in_ruleQueryStatement321);
                    lv_groupby_4_0=ruleGroupByClause();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQueryStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"groupby",
                            		lv_groupby_4_0, 
                            		"GroupByClause");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQueryStatement"


    // $ANTLR start "entryRuleFindClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:227:1: entryRuleFindClause returns [EObject current=null] : iv_ruleFindClause= ruleFindClause EOF ;
    public final EObject entryRuleFindClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFindClause = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:228:2: (iv_ruleFindClause= ruleFindClause EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:229:2: iv_ruleFindClause= ruleFindClause EOF
            {
             newCompositeNode(grammarAccess.getFindClauseRule()); 
            pushFollow(FOLLOW_ruleFindClause_in_entryRuleFindClause358);
            iv_ruleFindClause=ruleFindClause();

            state._fsp--;

             current =iv_ruleFindClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFindClause368); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFindClause"


    // $ANTLR start "ruleFindClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:236:1: ruleFindClause returns [EObject current=null] : ( ( (lv_clause_0_0= 'find' ) ) ( (lv_bindingObject_1_0= ruleBindingObject ) ) (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )* ) ;
    public final EObject ruleFindClause() throws RecognitionException {
        EObject current = null;

        Token lv_clause_0_0=null;
        Token otherlv_2=null;
        EObject lv_bindingObject_1_0 = null;

        EObject lv_bindingObject_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:239:28: ( ( ( (lv_clause_0_0= 'find' ) ) ( (lv_bindingObject_1_0= ruleBindingObject ) ) (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:240:1: ( ( (lv_clause_0_0= 'find' ) ) ( (lv_bindingObject_1_0= ruleBindingObject ) ) (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:240:1: ( ( (lv_clause_0_0= 'find' ) ) ( (lv_bindingObject_1_0= ruleBindingObject ) ) (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:240:2: ( (lv_clause_0_0= 'find' ) ) ( (lv_bindingObject_1_0= ruleBindingObject ) ) (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )*
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:240:2: ( (lv_clause_0_0= 'find' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:241:1: (lv_clause_0_0= 'find' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:241:1: (lv_clause_0_0= 'find' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:242:3: lv_clause_0_0= 'find'
            {
            lv_clause_0_0=(Token)match(input,12,FOLLOW_12_in_ruleFindClause411); 

                    newLeafNode(lv_clause_0_0, grammarAccess.getFindClauseAccess().getClauseFindKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFindClauseRule());
            	        }
                   		setWithLastConsumed(current, "clause", lv_clause_0_0, "find");
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:255:2: ( (lv_bindingObject_1_0= ruleBindingObject ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:256:1: (lv_bindingObject_1_0= ruleBindingObject )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:256:1: (lv_bindingObject_1_0= ruleBindingObject )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:257:3: lv_bindingObject_1_0= ruleBindingObject
            {
             
            	        newCompositeNode(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleBindingObject_in_ruleFindClause445);
            lv_bindingObject_1_0=ruleBindingObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFindClauseRule());
            	        }
                   		add(
                   			current, 
                   			"bindingObject",
                    		lv_bindingObject_1_0, 
                    		"BindingObject");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:273:2: (otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==13) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:273:4: otherlv_2= ',' ( (lv_bindingObject_3_0= ruleBindingObject ) )
            	    {
            	    otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleFindClause458); 

            	        	newLeafNode(otherlv_2, grammarAccess.getFindClauseAccess().getCommaKeyword_2_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:277:1: ( (lv_bindingObject_3_0= ruleBindingObject ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:278:1: (lv_bindingObject_3_0= ruleBindingObject )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:278:1: (lv_bindingObject_3_0= ruleBindingObject )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:279:3: lv_bindingObject_3_0= ruleBindingObject
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleBindingObject_in_ruleFindClause479);
            	    lv_bindingObject_3_0=ruleBindingObject();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFindClauseRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"bindingObject",
            	            		lv_bindingObject_3_0, 
            	            		"BindingObject");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFindClause"


    // $ANTLR start "entryRuleBindingObject"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:303:1: entryRuleBindingObject returns [EObject current=null] : iv_ruleBindingObject= ruleBindingObject EOF ;
    public final EObject entryRuleBindingObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBindingObject = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:304:2: (iv_ruleBindingObject= ruleBindingObject EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:305:2: iv_ruleBindingObject= ruleBindingObject EOF
            {
             newCompositeNode(grammarAccess.getBindingObjectRule()); 
            pushFollow(FOLLOW_ruleBindingObject_in_entryRuleBindingObject517);
            iv_ruleBindingObject=ruleBindingObject();

            state._fsp--;

             current =iv_ruleBindingObject; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBindingObject527); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBindingObject"


    // $ANTLR start "ruleBindingObject"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:312:1: ruleBindingObject returns [EObject current=null] : ( ( (lv_type_0_0= ruleObjectType ) ) ( (lv_alias_1_0= RULE_ID ) )+ ) ;
    public final EObject ruleBindingObject() throws RecognitionException {
        EObject current = null;

        Token lv_alias_1_0=null;
        EObject lv_type_0_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:315:28: ( ( ( (lv_type_0_0= ruleObjectType ) ) ( (lv_alias_1_0= RULE_ID ) )+ ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:316:1: ( ( (lv_type_0_0= ruleObjectType ) ) ( (lv_alias_1_0= RULE_ID ) )+ )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:316:1: ( ( (lv_type_0_0= ruleObjectType ) ) ( (lv_alias_1_0= RULE_ID ) )+ )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:316:2: ( (lv_type_0_0= ruleObjectType ) ) ( (lv_alias_1_0= RULE_ID ) )+
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:316:2: ( (lv_type_0_0= ruleObjectType ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:317:1: (lv_type_0_0= ruleObjectType )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:317:1: (lv_type_0_0= ruleObjectType )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:318:3: lv_type_0_0= ruleObjectType
            {
             
            	        newCompositeNode(grammarAccess.getBindingObjectAccess().getTypeObjectTypeParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleObjectType_in_ruleBindingObject573);
            lv_type_0_0=ruleObjectType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBindingObjectRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_0_0, 
                    		"ObjectType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:334:2: ( (lv_alias_1_0= RULE_ID ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:335:1: (lv_alias_1_0= RULE_ID )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:335:1: (lv_alias_1_0= RULE_ID )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:336:3: lv_alias_1_0= RULE_ID
            	    {
            	    lv_alias_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBindingObject590); 

            	    			newLeafNode(lv_alias_1_0, grammarAccess.getBindingObjectAccess().getAliasIDTerminalRuleCall_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getBindingObjectRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"alias",
            	            		lv_alias_1_0, 
            	            		"ID");
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBindingObject"


    // $ANTLR start "entryRuleObjectType"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:360:1: entryRuleObjectType returns [EObject current=null] : iv_ruleObjectType= ruleObjectType EOF ;
    public final EObject entryRuleObjectType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectType = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:361:2: (iv_ruleObjectType= ruleObjectType EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:362:2: iv_ruleObjectType= ruleObjectType EOF
            {
             newCompositeNode(grammarAccess.getObjectTypeRule()); 
            pushFollow(FOLLOW_ruleObjectType_in_entryRuleObjectType632);
            iv_ruleObjectType=ruleObjectType();

            state._fsp--;

             current =iv_ruleObjectType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleObjectType642); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectType"


    // $ANTLR start "ruleObjectType"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:369:1: ruleObjectType returns [EObject current=null] : ( ( () ( (lv_value_1_0= 'project' ) ) ) | ( () ( (lv_value_3_0= 'package' ) ) ) | ( () ( (lv_value_5_0= 'class' ) ) ) | ( () ( (lv_value_7_0= 'aspect' ) ) ) | ( () ( (lv_value_9_0= 'interface' ) ) ) | ( () ( (lv_value_11_0= 'enum' ) ) ) ) ;
    public final EObject ruleObjectType() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_0=null;
        Token lv_value_7_0=null;
        Token lv_value_9_0=null;
        Token lv_value_11_0=null;

         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:372:28: ( ( ( () ( (lv_value_1_0= 'project' ) ) ) | ( () ( (lv_value_3_0= 'package' ) ) ) | ( () ( (lv_value_5_0= 'class' ) ) ) | ( () ( (lv_value_7_0= 'aspect' ) ) ) | ( () ( (lv_value_9_0= 'interface' ) ) ) | ( () ( (lv_value_11_0= 'enum' ) ) ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:1: ( ( () ( (lv_value_1_0= 'project' ) ) ) | ( () ( (lv_value_3_0= 'package' ) ) ) | ( () ( (lv_value_5_0= 'class' ) ) ) | ( () ( (lv_value_7_0= 'aspect' ) ) ) | ( () ( (lv_value_9_0= 'interface' ) ) ) | ( () ( (lv_value_11_0= 'enum' ) ) ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:1: ( ( () ( (lv_value_1_0= 'project' ) ) ) | ( () ( (lv_value_3_0= 'package' ) ) ) | ( () ( (lv_value_5_0= 'class' ) ) ) | ( () ( (lv_value_7_0= 'aspect' ) ) ) | ( () ( (lv_value_9_0= 'interface' ) ) ) | ( () ( (lv_value_11_0= 'enum' ) ) ) )
            int alt6=6;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt6=1;
                }
                break;
            case 15:
                {
                alt6=2;
                }
                break;
            case 16:
                {
                alt6=3;
                }
                break;
            case 17:
                {
                alt6=4;
                }
                break;
            case 18:
                {
                alt6=5;
                }
                break;
            case 19:
                {
                alt6=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:2: ( () ( (lv_value_1_0= 'project' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:2: ( () ( (lv_value_1_0= 'project' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:3: () ( (lv_value_1_0= 'project' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:373:3: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:374:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getProjectAction_0_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:379:2: ( (lv_value_1_0= 'project' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:380:1: (lv_value_1_0= 'project' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:380:1: (lv_value_1_0= 'project' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:381:3: lv_value_1_0= 'project'
                    {
                    lv_value_1_0=(Token)match(input,14,FOLLOW_14_in_ruleObjectType695); 

                            newLeafNode(lv_value_1_0, grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_1_0, "project");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:395:6: ( () ( (lv_value_3_0= 'package' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:395:6: ( () ( (lv_value_3_0= 'package' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:395:7: () ( (lv_value_3_0= 'package' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:395:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:396:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getPackageAction_1_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:401:2: ( (lv_value_3_0= 'package' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:402:1: (lv_value_3_0= 'package' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:402:1: (lv_value_3_0= 'package' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:403:3: lv_value_3_0= 'package'
                    {
                    lv_value_3_0=(Token)match(input,15,FOLLOW_15_in_ruleObjectType743); 

                            newLeafNode(lv_value_3_0, grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_3_0, "package");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:417:6: ( () ( (lv_value_5_0= 'class' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:417:6: ( () ( (lv_value_5_0= 'class' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:417:7: () ( (lv_value_5_0= 'class' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:417:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:418:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getClassAction_2_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:423:2: ( (lv_value_5_0= 'class' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:424:1: (lv_value_5_0= 'class' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:424:1: (lv_value_5_0= 'class' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:425:3: lv_value_5_0= 'class'
                    {
                    lv_value_5_0=(Token)match(input,16,FOLLOW_16_in_ruleObjectType791); 

                            newLeafNode(lv_value_5_0, grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_5_0, "class");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:439:6: ( () ( (lv_value_7_0= 'aspect' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:439:6: ( () ( (lv_value_7_0= 'aspect' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:439:7: () ( (lv_value_7_0= 'aspect' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:439:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:440:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getAspectAction_3_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:445:2: ( (lv_value_7_0= 'aspect' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:446:1: (lv_value_7_0= 'aspect' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:446:1: (lv_value_7_0= 'aspect' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:447:3: lv_value_7_0= 'aspect'
                    {
                    lv_value_7_0=(Token)match(input,17,FOLLOW_17_in_ruleObjectType839); 

                            newLeafNode(lv_value_7_0, grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_7_0, "aspect");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:461:6: ( () ( (lv_value_9_0= 'interface' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:461:6: ( () ( (lv_value_9_0= 'interface' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:461:7: () ( (lv_value_9_0= 'interface' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:461:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:462:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getInterfaceAction_4_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:467:2: ( (lv_value_9_0= 'interface' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:468:1: (lv_value_9_0= 'interface' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:468:1: (lv_value_9_0= 'interface' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:469:3: lv_value_9_0= 'interface'
                    {
                    lv_value_9_0=(Token)match(input,18,FOLLOW_18_in_ruleObjectType887); 

                            newLeafNode(lv_value_9_0, grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_9_0, "interface");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:483:6: ( () ( (lv_value_11_0= 'enum' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:483:6: ( () ( (lv_value_11_0= 'enum' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:483:7: () ( (lv_value_11_0= 'enum' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:483:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:484:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getObjectTypeAccess().getEnumAction_5_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:489:2: ( (lv_value_11_0= 'enum' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:490:1: (lv_value_11_0= 'enum' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:490:1: (lv_value_11_0= 'enum' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:491:3: lv_value_11_0= 'enum'
                    {
                    lv_value_11_0=(Token)match(input,19,FOLLOW_19_in_ruleObjectType935); 

                            newLeafNode(lv_value_11_0, grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectTypeRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_11_0, "enum");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectType"


    // $ANTLR start "entryRuleWhereClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:512:1: entryRuleWhereClause returns [EObject current=null] : iv_ruleWhereClause= ruleWhereClause EOF ;
    public final EObject entryRuleWhereClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhereClause = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:513:2: (iv_ruleWhereClause= ruleWhereClause EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:514:2: iv_ruleWhereClause= ruleWhereClause EOF
            {
             newCompositeNode(grammarAccess.getWhereClauseRule()); 
            pushFollow(FOLLOW_ruleWhereClause_in_entryRuleWhereClause985);
            iv_ruleWhereClause=ruleWhereClause();

            state._fsp--;

             current =iv_ruleWhereClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhereClause995); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhereClause"


    // $ANTLR start "ruleWhereClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:521:1: ruleWhereClause returns [EObject current=null] : ( ( (lv_clause_0_0= 'where' ) ) ( (lv_expression_1_0= ruleExpression ) ) ) ;
    public final EObject ruleWhereClause() throws RecognitionException {
        EObject current = null;

        Token lv_clause_0_0=null;
        EObject lv_expression_1_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:524:28: ( ( ( (lv_clause_0_0= 'where' ) ) ( (lv_expression_1_0= ruleExpression ) ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:525:1: ( ( (lv_clause_0_0= 'where' ) ) ( (lv_expression_1_0= ruleExpression ) ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:525:1: ( ( (lv_clause_0_0= 'where' ) ) ( (lv_expression_1_0= ruleExpression ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:525:2: ( (lv_clause_0_0= 'where' ) ) ( (lv_expression_1_0= ruleExpression ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:525:2: ( (lv_clause_0_0= 'where' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:526:1: (lv_clause_0_0= 'where' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:526:1: (lv_clause_0_0= 'where' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:527:3: lv_clause_0_0= 'where'
            {
            lv_clause_0_0=(Token)match(input,20,FOLLOW_20_in_ruleWhereClause1038); 

                    newLeafNode(lv_clause_0_0, grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWhereClauseRule());
            	        }
                   		setWithLastConsumed(current, "clause", lv_clause_0_0, "where");
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:540:2: ( (lv_expression_1_0= ruleExpression ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:541:1: (lv_expression_1_0= ruleExpression )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:541:1: (lv_expression_1_0= ruleExpression )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:542:3: lv_expression_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getWhereClauseAccess().getExpressionExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleWhereClause1072);
            lv_expression_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWhereClauseRule());
            	        }
                   		set(
                   			current, 
                   			"expression",
                    		lv_expression_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhereClause"


    // $ANTLR start "entryRuleReturnsClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:566:1: entryRuleReturnsClause returns [EObject current=null] : iv_ruleReturnsClause= ruleReturnsClause EOF ;
    public final EObject entryRuleReturnsClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnsClause = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:567:2: (iv_ruleReturnsClause= ruleReturnsClause EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:568:2: iv_ruleReturnsClause= ruleReturnsClause EOF
            {
             newCompositeNode(grammarAccess.getReturnsClauseRule()); 
            pushFollow(FOLLOW_ruleReturnsClause_in_entryRuleReturnsClause1108);
            iv_ruleReturnsClause=ruleReturnsClause();

            state._fsp--;

             current =iv_ruleReturnsClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReturnsClause1118); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnsClause"


    // $ANTLR start "ruleReturnsClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:575:1: ruleReturnsClause returns [EObject current=null] : ( ( (lv_clause_0_0= 'returns' ) ) ( (lv_expressions_1_0= ruleExpression ) ) (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )? (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )* ) ;
    public final EObject ruleReturnsClause() throws RecognitionException {
        EObject current = null;

        Token lv_clause_0_0=null;
        Token otherlv_2=null;
        Token lv_resultAlias_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_resultAlias_7_0=null;
        EObject lv_expressions_1_0 = null;

        EObject lv_expressions_5_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:578:28: ( ( ( (lv_clause_0_0= 'returns' ) ) ( (lv_expressions_1_0= ruleExpression ) ) (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )? (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:579:1: ( ( (lv_clause_0_0= 'returns' ) ) ( (lv_expressions_1_0= ruleExpression ) ) (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )? (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:579:1: ( ( (lv_clause_0_0= 'returns' ) ) ( (lv_expressions_1_0= ruleExpression ) ) (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )? (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:579:2: ( (lv_clause_0_0= 'returns' ) ) ( (lv_expressions_1_0= ruleExpression ) ) (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )? (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )*
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:579:2: ( (lv_clause_0_0= 'returns' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:580:1: (lv_clause_0_0= 'returns' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:580:1: (lv_clause_0_0= 'returns' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:581:3: lv_clause_0_0= 'returns'
            {
            lv_clause_0_0=(Token)match(input,21,FOLLOW_21_in_ruleReturnsClause1161); 

                    newLeafNode(lv_clause_0_0, grammarAccess.getReturnsClauseAccess().getClauseReturnsKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReturnsClauseRule());
            	        }
                   		setWithLastConsumed(current, "clause", lv_clause_0_0, "returns");
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:594:2: ( (lv_expressions_1_0= ruleExpression ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:595:1: (lv_expressions_1_0= ruleExpression )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:595:1: (lv_expressions_1_0= ruleExpression )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:596:3: lv_expressions_1_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleReturnsClause1195);
            lv_expressions_1_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReturnsClauseRule());
            	        }
                   		add(
                   			current, 
                   			"expressions",
                    		lv_expressions_1_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:612:2: (otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:612:4: otherlv_2= 'as' ( (lv_resultAlias_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,22,FOLLOW_22_in_ruleReturnsClause1208); 

                        	newLeafNode(otherlv_2, grammarAccess.getReturnsClauseAccess().getAsKeyword_2_0());
                        
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:616:1: ( (lv_resultAlias_3_0= RULE_ID ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:617:1: (lv_resultAlias_3_0= RULE_ID )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:617:1: (lv_resultAlias_3_0= RULE_ID )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:618:3: lv_resultAlias_3_0= RULE_ID
                    {
                    lv_resultAlias_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReturnsClause1225); 

                    			newLeafNode(lv_resultAlias_3_0, grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_2_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getReturnsClauseRule());
                    	        }
                           		addWithLastConsumed(
                           			current, 
                           			"resultAlias",
                            		lv_resultAlias_3_0, 
                            		"ID");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:634:4: (otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )? )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==13) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:634:6: otherlv_4= ',' ( (lv_expressions_5_0= ruleExpression ) ) (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )?
            	    {
            	    otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleReturnsClause1245); 

            	        	newLeafNode(otherlv_4, grammarAccess.getReturnsClauseAccess().getCommaKeyword_3_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:638:1: ( (lv_expressions_5_0= ruleExpression ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:639:1: (lv_expressions_5_0= ruleExpression )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:639:1: (lv_expressions_5_0= ruleExpression )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:640:3: lv_expressions_5_0= ruleExpression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleExpression_in_ruleReturnsClause1266);
            	    lv_expressions_5_0=ruleExpression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getReturnsClauseRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expressions",
            	            		lv_expressions_5_0, 
            	            		"Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:656:2: (otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) ) )?
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==22) ) {
            	        alt8=1;
            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:656:4: otherlv_6= 'as' ( (lv_resultAlias_7_0= RULE_ID ) )
            	            {
            	            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleReturnsClause1279); 

            	                	newLeafNode(otherlv_6, grammarAccess.getReturnsClauseAccess().getAsKeyword_3_2_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:660:1: ( (lv_resultAlias_7_0= RULE_ID ) )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:661:1: (lv_resultAlias_7_0= RULE_ID )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:661:1: (lv_resultAlias_7_0= RULE_ID )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:662:3: lv_resultAlias_7_0= RULE_ID
            	            {
            	            lv_resultAlias_7_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReturnsClause1296); 

            	            			newLeafNode(lv_resultAlias_7_0, grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_3_2_1_0()); 
            	            		

            	            	        if (current==null) {
            	            	            current = createModelElement(grammarAccess.getReturnsClauseRule());
            	            	        }
            	                   		addWithLastConsumed(
            	                   			current, 
            	                   			"resultAlias",
            	                    		lv_resultAlias_7_0, 
            	                    		"ID");
            	            	    

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnsClause"


    // $ANTLR start "entryRuleGroupByClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:686:1: entryRuleGroupByClause returns [EObject current=null] : iv_ruleGroupByClause= ruleGroupByClause EOF ;
    public final EObject entryRuleGroupByClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGroupByClause = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:687:2: (iv_ruleGroupByClause= ruleGroupByClause EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:688:2: iv_ruleGroupByClause= ruleGroupByClause EOF
            {
             newCompositeNode(grammarAccess.getGroupByClauseRule()); 
            pushFollow(FOLLOW_ruleGroupByClause_in_entryRuleGroupByClause1341);
            iv_ruleGroupByClause=ruleGroupByClause();

            state._fsp--;

             current =iv_ruleGroupByClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGroupByClause1351); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGroupByClause"


    // $ANTLR start "ruleGroupByClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:695:1: ruleGroupByClause returns [EObject current=null] : ( ( (lv_clause_0_0= 'group by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ) ;
    public final EObject ruleGroupByClause() throws RecognitionException {
        EObject current = null;

        Token lv_clause_0_0=null;
        Token otherlv_2=null;
        EObject lv_expressions_1_0 = null;

        EObject lv_expressions_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:698:28: ( ( ( (lv_clause_0_0= 'group by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:699:1: ( ( (lv_clause_0_0= 'group by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:699:1: ( ( (lv_clause_0_0= 'group by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:699:2: ( (lv_clause_0_0= 'group by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )*
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:699:2: ( (lv_clause_0_0= 'group by' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:700:1: (lv_clause_0_0= 'group by' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:700:1: (lv_clause_0_0= 'group by' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:701:3: lv_clause_0_0= 'group by'
            {
            lv_clause_0_0=(Token)match(input,23,FOLLOW_23_in_ruleGroupByClause1394); 

                    newLeafNode(lv_clause_0_0, grammarAccess.getGroupByClauseAccess().getClauseGroupByKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getGroupByClauseRule());
            	        }
                   		setWithLastConsumed(current, "clause", lv_clause_0_0, "group by");
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:714:2: ( (lv_expressions_1_0= ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:715:1: (lv_expressions_1_0= ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:715:1: (lv_expressions_1_0= ruleSimpleQualifiedName )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:716:3: lv_expressions_1_0= ruleSimpleQualifiedName
            {
             
            	        newCompositeNode(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_ruleGroupByClause1428);
            lv_expressions_1_0=ruleSimpleQualifiedName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getGroupByClauseRule());
            	        }
                   		add(
                   			current, 
                   			"expressions",
                    		lv_expressions_1_0, 
                    		"SimpleQualifiedName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:732:2: (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==13) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:732:4: otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) )
            	    {
            	    otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleGroupByClause1441); 

            	        	newLeafNode(otherlv_2, grammarAccess.getGroupByClauseAccess().getCommaKeyword_2_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:736:1: ( (lv_expressions_3_0= ruleSimpleQualifiedName ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:737:1: (lv_expressions_3_0= ruleSimpleQualifiedName )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:737:1: (lv_expressions_3_0= ruleSimpleQualifiedName )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:738:3: lv_expressions_3_0= ruleSimpleQualifiedName
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSimpleQualifiedName_in_ruleGroupByClause1462);
            	    lv_expressions_3_0=ruleSimpleQualifiedName();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getGroupByClauseRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expressions",
            	            		lv_expressions_3_0, 
            	            		"SimpleQualifiedName");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGroupByClause"


    // $ANTLR start "entryRuleOrderByClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:762:1: entryRuleOrderByClause returns [EObject current=null] : iv_ruleOrderByClause= ruleOrderByClause EOF ;
    public final EObject entryRuleOrderByClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrderByClause = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:763:2: (iv_ruleOrderByClause= ruleOrderByClause EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:764:2: iv_ruleOrderByClause= ruleOrderByClause EOF
            {
             newCompositeNode(grammarAccess.getOrderByClauseRule()); 
            pushFollow(FOLLOW_ruleOrderByClause_in_entryRuleOrderByClause1500);
            iv_ruleOrderByClause=ruleOrderByClause();

            state._fsp--;

             current =iv_ruleOrderByClause; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderByClause1510); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrderByClause"


    // $ANTLR start "ruleOrderByClause"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:771:1: ruleOrderByClause returns [EObject current=null] : ( ( (lv_clause_0_0= 'order by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )? (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )? ) ;
    public final EObject ruleOrderByClause() throws RecognitionException {
        EObject current = null;

        Token lv_clause_0_0=null;
        Token otherlv_2=null;
        Token lv_option_4_1=null;
        Token lv_option_4_2=null;
        Token otherlv_5=null;
        EObject lv_expressions_1_0 = null;

        EObject lv_expressions_3_0 = null;

        EObject lv_havingExpression_6_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:774:28: ( ( ( (lv_clause_0_0= 'order by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )? (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )? ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:775:1: ( ( (lv_clause_0_0= 'order by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )? (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )? )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:775:1: ( ( (lv_clause_0_0= 'order by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )? (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )? )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:775:2: ( (lv_clause_0_0= 'order by' ) ) ( (lv_expressions_1_0= ruleSimpleQualifiedName ) ) (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )* ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )? (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )?
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:775:2: ( (lv_clause_0_0= 'order by' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:776:1: (lv_clause_0_0= 'order by' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:776:1: (lv_clause_0_0= 'order by' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:777:3: lv_clause_0_0= 'order by'
            {
            lv_clause_0_0=(Token)match(input,24,FOLLOW_24_in_ruleOrderByClause1553); 

                    newLeafNode(lv_clause_0_0, grammarAccess.getOrderByClauseAccess().getClauseOrderByKeyword_0_0());
                

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOrderByClauseRule());
            	        }
                   		setWithLastConsumed(current, "clause", lv_clause_0_0, "order by");
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:790:2: ( (lv_expressions_1_0= ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:791:1: (lv_expressions_1_0= ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:791:1: (lv_expressions_1_0= ruleSimpleQualifiedName )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:792:3: lv_expressions_1_0= ruleSimpleQualifiedName
            {
             
            	        newCompositeNode(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_ruleOrderByClause1587);
            lv_expressions_1_0=ruleSimpleQualifiedName();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOrderByClauseRule());
            	        }
                   		add(
                   			current, 
                   			"expressions",
                    		lv_expressions_1_0, 
                    		"SimpleQualifiedName");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:808:2: (otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==13) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:808:4: otherlv_2= ',' ( (lv_expressions_3_0= ruleSimpleQualifiedName ) )
            	    {
            	    otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleOrderByClause1600); 

            	        	newLeafNode(otherlv_2, grammarAccess.getOrderByClauseAccess().getCommaKeyword_2_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:812:1: ( (lv_expressions_3_0= ruleSimpleQualifiedName ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:813:1: (lv_expressions_3_0= ruleSimpleQualifiedName )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:813:1: (lv_expressions_3_0= ruleSimpleQualifiedName )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:814:3: lv_expressions_3_0= ruleSimpleQualifiedName
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSimpleQualifiedName_in_ruleOrderByClause1621);
            	    lv_expressions_3_0=ruleSimpleQualifiedName();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOrderByClauseRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expressions",
            	            		lv_expressions_3_0, 
            	            		"SimpleQualifiedName");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:830:4: ( ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=25 && LA13_0<=26)) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:831:1: ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:831:1: ( (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:832:1: (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:832:1: (lv_option_4_1= 'asc' | lv_option_4_2= 'desc' )
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==25) ) {
                        alt12=1;
                    }
                    else if ( (LA12_0==26) ) {
                        alt12=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 0, input);

                        throw nvae;
                    }
                    switch (alt12) {
                        case 1 :
                            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:833:3: lv_option_4_1= 'asc'
                            {
                            lv_option_4_1=(Token)match(input,25,FOLLOW_25_in_ruleOrderByClause1643); 

                                    newLeafNode(lv_option_4_1, grammarAccess.getOrderByClauseAccess().getOptionAscKeyword_3_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getOrderByClauseRule());
                            	        }
                                   		setWithLastConsumed(current, "option", lv_option_4_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:845:8: lv_option_4_2= 'desc'
                            {
                            lv_option_4_2=(Token)match(input,26,FOLLOW_26_in_ruleOrderByClause1672); 

                                    newLeafNode(lv_option_4_2, grammarAccess.getOrderByClauseAccess().getOptionDescKeyword_3_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getOrderByClauseRule());
                            	        }
                                   		setWithLastConsumed(current, "option", lv_option_4_2, null);
                            	    

                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:860:3: (otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==27) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:860:5: otherlv_5= 'having' ( (lv_havingExpression_6_0= ruleExpression ) )
                    {
                    otherlv_5=(Token)match(input,27,FOLLOW_27_in_ruleOrderByClause1702); 

                        	newLeafNode(otherlv_5, grammarAccess.getOrderByClauseAccess().getHavingKeyword_4_0());
                        
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:864:1: ( (lv_havingExpression_6_0= ruleExpression ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:865:1: (lv_havingExpression_6_0= ruleExpression )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:865:1: (lv_havingExpression_6_0= ruleExpression )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:866:3: lv_havingExpression_6_0= ruleExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getOrderByClauseAccess().getHavingExpressionExpressionParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleExpression_in_ruleOrderByClause1723);
                    lv_havingExpression_6_0=ruleExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOrderByClauseRule());
                    	        }
                           		set(
                           			current, 
                           			"havingExpression",
                            		lv_havingExpression_6_0, 
                            		"Expression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrderByClause"


    // $ANTLR start "entryRuleSimpleQualifiedName"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:890:1: entryRuleSimpleQualifiedName returns [EObject current=null] : iv_ruleSimpleQualifiedName= ruleSimpleQualifiedName EOF ;
    public final EObject entryRuleSimpleQualifiedName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleQualifiedName = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:891:2: (iv_ruleSimpleQualifiedName= ruleSimpleQualifiedName EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:892:2: iv_ruleSimpleQualifiedName= ruleSimpleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getSimpleQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_entryRuleSimpleQualifiedName1761);
            iv_ruleSimpleQualifiedName=ruleSimpleQualifiedName();

            state._fsp--;

             current =iv_ruleSimpleQualifiedName; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleQualifiedName1771); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleQualifiedName"


    // $ANTLR start "ruleSimpleQualifiedName"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:899:1: ruleSimpleQualifiedName returns [EObject current=null] : ( ( (lv_qualifiers_0_0= ruleSimpleQualified ) ) (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )* ) ;
    public final EObject ruleSimpleQualifiedName() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_qualifiers_0_0 = null;

        EObject lv_qualifiers_2_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:902:28: ( ( ( (lv_qualifiers_0_0= ruleSimpleQualified ) ) (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:903:1: ( ( (lv_qualifiers_0_0= ruleSimpleQualified ) ) (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:903:1: ( ( (lv_qualifiers_0_0= ruleSimpleQualified ) ) (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:903:2: ( (lv_qualifiers_0_0= ruleSimpleQualified ) ) (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )*
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:903:2: ( (lv_qualifiers_0_0= ruleSimpleQualified ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:904:1: (lv_qualifiers_0_0= ruleSimpleQualified )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:904:1: (lv_qualifiers_0_0= ruleSimpleQualified )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:905:3: lv_qualifiers_0_0= ruleSimpleQualified
            {
             
            	        newCompositeNode(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleSimpleQualified_in_ruleSimpleQualifiedName1817);
            lv_qualifiers_0_0=ruleSimpleQualified();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSimpleQualifiedNameRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_0_0, 
                    		"SimpleQualified");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:921:2: (otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==28) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:921:4: otherlv_1= '.' ( (lv_qualifiers_2_0= ruleSimpleQualified ) )
            	    {
            	    otherlv_1=(Token)match(input,28,FOLLOW_28_in_ruleSimpleQualifiedName1830); 

            	        	newLeafNode(otherlv_1, grammarAccess.getSimpleQualifiedNameAccess().getFullStopKeyword_1_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:925:1: ( (lv_qualifiers_2_0= ruleSimpleQualified ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:926:1: (lv_qualifiers_2_0= ruleSimpleQualified )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:926:1: (lv_qualifiers_2_0= ruleSimpleQualified )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:927:3: lv_qualifiers_2_0= ruleSimpleQualified
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSimpleQualified_in_ruleSimpleQualifiedName1851);
            	    lv_qualifiers_2_0=ruleSimpleQualified();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getSimpleQualifiedNameRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_2_0, 
            	            		"SimpleQualified");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleQualifiedName"


    // $ANTLR start "entryRuleSimpleQualified"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:951:1: entryRuleSimpleQualified returns [EObject current=null] : iv_ruleSimpleQualified= ruleSimpleQualified EOF ;
    public final EObject entryRuleSimpleQualified() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleQualified = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:952:2: (iv_ruleSimpleQualified= ruleSimpleQualified EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:953:2: iv_ruleSimpleQualified= ruleSimpleQualified EOF
            {
             newCompositeNode(grammarAccess.getSimpleQualifiedRule()); 
            pushFollow(FOLLOW_ruleSimpleQualified_in_entryRuleSimpleQualified1889);
            iv_ruleSimpleQualified=ruleSimpleQualified();

            state._fsp--;

             current =iv_ruleSimpleQualified; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleQualified1899); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleQualified"


    // $ANTLR start "ruleSimpleQualified"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:960:1: ruleSimpleQualified returns [EObject current=null] : this_Identifier_0= ruleIdentifier ;
    public final EObject ruleSimpleQualified() throws RecognitionException {
        EObject current = null;

        EObject this_Identifier_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:963:28: (this_Identifier_0= ruleIdentifier )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:965:5: this_Identifier_0= ruleIdentifier
            {
             
                    newCompositeNode(grammarAccess.getSimpleQualifiedAccess().getIdentifierParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleIdentifier_in_ruleSimpleQualified1945);
            this_Identifier_0=ruleIdentifier();

            state._fsp--;

             
                    current = this_Identifier_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleQualified"


    // $ANTLR start "entryRuleExpression"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:981:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
        	
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:985:2: (iv_ruleExpression= ruleExpression EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:986:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression1985);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression1995); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:996:1: ruleExpression returns [EObject current=null] : this_Or_0= ruleOr ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Or_0 = null;


         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1000:28: (this_Or_0= ruleOr )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1002:5: this_Or_0= ruleOr
            {
             
                    newCompositeNode(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleOr_in_ruleExpression2045);
            this_Or_0=ruleOr();

            state._fsp--;

             
                    current = this_Or_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOr"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1021:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1022:2: (iv_ruleOr= ruleOr EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1023:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr2083);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr2093); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1030:1: ruleOr returns [EObject current=null] : (this_And_0= ruleAnd ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1033:28: ( (this_And_0= ruleAnd ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1034:1: (this_And_0= ruleAnd ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1034:1: (this_And_0= ruleAnd ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1035:5: this_And_0= ruleAnd ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAnd_in_ruleOr2140);
            this_And_0=ruleAnd();

            state._fsp--;

             
                    current = this_And_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1043:1: ( (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==29) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1043:2: (otherlv_1= 'or' () ) ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1043:2: (otherlv_1= 'or' () )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1043:4: otherlv_1= 'or' ()
            	    {
            	    otherlv_1=(Token)match(input,29,FOLLOW_29_in_ruleOr2153); 

            	        	newLeafNode(otherlv_1, grammarAccess.getOrAccess().getOrKeyword_1_0_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1047:1: ()
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1048:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getOrAccess().getOrLeftAction_1_0_1(),
            	                current);
            	        

            	    }


            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1053:3: ( (lv_right_3_0= ruleAnd ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1054:1: (lv_right_3_0= ruleAnd )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1054:1: (lv_right_3_0= ruleAnd )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1055:3: lv_right_3_0= ruleAnd
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAnd_in_ruleOr2184);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getOrRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"And");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1079:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1080:2: (iv_ruleAnd= ruleAnd EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1081:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd2222);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd2232); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1088:1: ruleAnd returns [EObject current=null] : (this_Relation_0= ruleRelation (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_Relation_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1091:28: ( (this_Relation_0= ruleRelation (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1092:1: (this_Relation_0= ruleRelation (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1092:1: (this_Relation_0= ruleRelation (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1093:5: this_Relation_0= ruleRelation (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getAndAccess().getRelationParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleRelation_in_ruleAnd2279);
            this_Relation_0=ruleRelation();

            state._fsp--;

             
                    current = this_Relation_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1101:1: (otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==30) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1101:3: otherlv_1= 'and' () ( (lv_right_3_0= ruleRelation ) )
            	    {
            	    otherlv_1=(Token)match(input,30,FOLLOW_30_in_ruleAnd2291); 

            	        	newLeafNode(otherlv_1, grammarAccess.getAndAccess().getAndKeyword_1_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1105:1: ()
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1106:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getAndAccess().getAndLeftAction_1_1(),
            	                current);
            	        

            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1111:2: ( (lv_right_3_0= ruleRelation ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1112:1: (lv_right_3_0= ruleRelation )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1112:1: (lv_right_3_0= ruleRelation )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1113:3: lv_right_3_0= ruleRelation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAndAccess().getRightRelationParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleRelation_in_ruleAnd2321);
            	    lv_right_3_0=ruleRelation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAndRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Relation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleRelation"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1137:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1138:2: (iv_ruleRelation= ruleRelation EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1139:2: iv_ruleRelation= ruleRelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_ruleRelation_in_entryRuleRelation2359);
            iv_ruleRelation=ruleRelation();

            state._fsp--;

             current =iv_ruleRelation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelation2369); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1146:1: ruleRelation returns [EObject current=null] : (this_Add_0= ruleAdd ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )* ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject this_Add_0 = null;

        EObject lv_right_15_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1149:28: ( (this_Add_0= ruleAdd ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1150:1: (this_Add_0= ruleAdd ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1150:1: (this_Add_0= ruleAdd ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1151:5: this_Add_0= ruleAdd ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getRelationAccess().getAddParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAdd_in_ruleRelation2416);
            this_Add_0=ruleAdd();

            state._fsp--;

             
                    current = this_Add_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:1: ( ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=31 && LA19_0<=37)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:2: ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) ) ( (lv_right_15_0= ruleAdd ) )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:2: ( (otherlv_1= '=' () ) | (otherlv_3= '>' () ) | (otherlv_5= '>=' () ) | (otherlv_7= '<' () ) | (otherlv_9= '<=' () ) | (otherlv_11= 'like' () ) | (otherlv_13= '!=' () ) )
            	    int alt18=7;
            	    switch ( input.LA(1) ) {
            	    case 31:
            	        {
            	        alt18=1;
            	        }
            	        break;
            	    case 32:
            	        {
            	        alt18=2;
            	        }
            	        break;
            	    case 33:
            	        {
            	        alt18=3;
            	        }
            	        break;
            	    case 34:
            	        {
            	        alt18=4;
            	        }
            	        break;
            	    case 35:
            	        {
            	        alt18=5;
            	        }
            	        break;
            	    case 36:
            	        {
            	        alt18=6;
            	        }
            	        break;
            	    case 37:
            	        {
            	        alt18=7;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 18, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt18) {
            	        case 1 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:3: (otherlv_1= '=' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:3: (otherlv_1= '=' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1159:5: otherlv_1= '=' ()
            	            {
            	            otherlv_1=(Token)match(input,31,FOLLOW_31_in_ruleRelation2430); 

            	                	newLeafNode(otherlv_1, grammarAccess.getRelationAccess().getEqualsSignKeyword_1_0_0_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1163:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1164:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1170:6: (otherlv_3= '>' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1170:6: (otherlv_3= '>' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1170:8: otherlv_3= '>' ()
            	            {
            	            otherlv_3=(Token)match(input,32,FOLLOW_32_in_ruleRelation2459); 

            	                	newLeafNode(otherlv_3, grammarAccess.getRelationAccess().getGreaterThanSignKeyword_1_0_1_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1174:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1175:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1181:6: (otherlv_5= '>=' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1181:6: (otherlv_5= '>=' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1181:8: otherlv_5= '>=' ()
            	            {
            	            otherlv_5=(Token)match(input,33,FOLLOW_33_in_ruleRelation2488); 

            	                	newLeafNode(otherlv_5, grammarAccess.getRelationAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1185:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1186:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 4 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1192:6: (otherlv_7= '<' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1192:6: (otherlv_7= '<' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1192:8: otherlv_7= '<' ()
            	            {
            	            otherlv_7=(Token)match(input,34,FOLLOW_34_in_ruleRelation2517); 

            	                	newLeafNode(otherlv_7, grammarAccess.getRelationAccess().getLessThanSignKeyword_1_0_3_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1196:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1197:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 5 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1203:6: (otherlv_9= '<=' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1203:6: (otherlv_9= '<=' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1203:8: otherlv_9= '<=' ()
            	            {
            	            otherlv_9=(Token)match(input,35,FOLLOW_35_in_ruleRelation2546); 

            	                	newLeafNode(otherlv_9, grammarAccess.getRelationAccess().getLessThanSignEqualsSignKeyword_1_0_4_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1207:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1208:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 6 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1214:6: (otherlv_11= 'like' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1214:6: (otherlv_11= 'like' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1214:8: otherlv_11= 'like' ()
            	            {
            	            otherlv_11=(Token)match(input,36,FOLLOW_36_in_ruleRelation2575); 

            	                	newLeafNode(otherlv_11, grammarAccess.getRelationAccess().getLikeKeyword_1_0_5_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1218:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1219:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 7 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1225:6: (otherlv_13= '!=' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1225:6: (otherlv_13= '!=' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1225:8: otherlv_13= '!=' ()
            	            {
            	            otherlv_13=(Token)match(input,37,FOLLOW_37_in_ruleRelation2604); 

            	                	newLeafNode(otherlv_13, grammarAccess.getRelationAccess().getExclamationMarkEqualsSignKeyword_1_0_6_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1229:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1230:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1235:4: ( (lv_right_15_0= ruleAdd ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1236:1: (lv_right_15_0= ruleAdd )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1236:1: (lv_right_15_0= ruleAdd )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1237:3: lv_right_15_0= ruleAdd
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAdd_in_ruleRelation2636);
            	    lv_right_15_0=ruleAdd();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRelationRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_15_0, 
            	            		"Add");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleAdd"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1261:1: entryRuleAdd returns [EObject current=null] : iv_ruleAdd= ruleAdd EOF ;
    public final EObject entryRuleAdd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdd = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1262:2: (iv_ruleAdd= ruleAdd EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1263:2: iv_ruleAdd= ruleAdd EOF
            {
             newCompositeNode(grammarAccess.getAddRule()); 
            pushFollow(FOLLOW_ruleAdd_in_entryRuleAdd2674);
            iv_ruleAdd=ruleAdd();

            state._fsp--;

             current =iv_ruleAdd; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdd2684); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdd"


    // $ANTLR start "ruleAdd"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1270:1: ruleAdd returns [EObject current=null] : (this_Mult_0= ruleMult ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )* ) ;
    public final EObject ruleAdd() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Mult_0 = null;

        EObject lv_right_5_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1273:28: ( (this_Mult_0= ruleMult ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1274:1: (this_Mult_0= ruleMult ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1274:1: (this_Mult_0= ruleMult ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1275:5: this_Mult_0= ruleMult ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getAddAccess().getMultParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleMult_in_ruleAdd2731);
            this_Mult_0=ruleMult();

            state._fsp--;

             
                    current = this_Mult_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:1: ( ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=38 && LA21_0<=39)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:2: ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) ) ( (lv_right_5_0= ruleMult ) )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:2: ( (otherlv_1= '+' () ) | (otherlv_3= '-' () ) )
            	    int alt20=2;
            	    int LA20_0 = input.LA(1);

            	    if ( (LA20_0==38) ) {
            	        alt20=1;
            	    }
            	    else if ( (LA20_0==39) ) {
            	        alt20=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 20, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt20) {
            	        case 1 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:3: (otherlv_1= '+' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:3: (otherlv_1= '+' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1283:5: otherlv_1= '+' ()
            	            {
            	            otherlv_1=(Token)match(input,38,FOLLOW_38_in_ruleAdd2745); 

            	                	newLeafNode(otherlv_1, grammarAccess.getAddAccess().getPlusSignKeyword_1_0_0_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1287:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1288:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1294:6: (otherlv_3= '-' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1294:6: (otherlv_3= '-' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1294:8: otherlv_3= '-' ()
            	            {
            	            otherlv_3=(Token)match(input,39,FOLLOW_39_in_ruleAdd2774); 

            	                	newLeafNode(otherlv_3, grammarAccess.getAddAccess().getHyphenMinusKeyword_1_0_1_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1298:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1299:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1304:4: ( (lv_right_5_0= ruleMult ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1305:1: (lv_right_5_0= ruleMult )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1305:1: (lv_right_5_0= ruleMult )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1306:3: lv_right_5_0= ruleMult
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAddAccess().getRightMultParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleMult_in_ruleAdd2806);
            	    lv_right_5_0=ruleMult();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAddRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_5_0, 
            	            		"Mult");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdd"


    // $ANTLR start "entryRuleMult"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1330:1: entryRuleMult returns [EObject current=null] : iv_ruleMult= ruleMult EOF ;
    public final EObject entryRuleMult() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMult = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1331:2: (iv_ruleMult= ruleMult EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1332:2: iv_ruleMult= ruleMult EOF
            {
             newCompositeNode(grammarAccess.getMultRule()); 
            pushFollow(FOLLOW_ruleMult_in_entryRuleMult2844);
            iv_ruleMult=ruleMult();

            state._fsp--;

             current =iv_ruleMult; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMult2854); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMult"


    // $ANTLR start "ruleMult"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1339:1: ruleMult returns [EObject current=null] : (this_In_0= ruleIn ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )* ) ;
    public final EObject ruleMult() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject this_In_0 = null;

        EObject lv_right_7_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1342:28: ( (this_In_0= ruleIn ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1343:1: (this_In_0= ruleIn ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1343:1: (this_In_0= ruleIn ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1344:5: this_In_0= ruleIn ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getMultAccess().getInParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleIn_in_ruleMult2901);
            this_In_0=ruleIn();

            state._fsp--;

             
                    current = this_In_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:1: ( ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( ((LA23_0>=40 && LA23_0<=42)) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:2: ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) ) ( (lv_right_7_0= ruleIn ) )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:2: ( (otherlv_1= '*' () ) | (otherlv_3= '/' () ) | (otherlv_5= '%' () ) )
            	    int alt22=3;
            	    switch ( input.LA(1) ) {
            	    case 40:
            	        {
            	        alt22=1;
            	        }
            	        break;
            	    case 41:
            	        {
            	        alt22=2;
            	        }
            	        break;
            	    case 42:
            	        {
            	        alt22=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 22, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt22) {
            	        case 1 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:3: (otherlv_1= '*' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:3: (otherlv_1= '*' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1352:5: otherlv_1= '*' ()
            	            {
            	            otherlv_1=(Token)match(input,40,FOLLOW_40_in_ruleMult2915); 

            	                	newLeafNode(otherlv_1, grammarAccess.getMultAccess().getAsteriskKeyword_1_0_0_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1356:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1357:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1363:6: (otherlv_3= '/' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1363:6: (otherlv_3= '/' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1363:8: otherlv_3= '/' ()
            	            {
            	            otherlv_3=(Token)match(input,41,FOLLOW_41_in_ruleMult2944); 

            	                	newLeafNode(otherlv_3, grammarAccess.getMultAccess().getSolidusKeyword_1_0_1_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1367:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1368:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1374:6: (otherlv_5= '%' () )
            	            {
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1374:6: (otherlv_5= '%' () )
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1374:8: otherlv_5= '%' ()
            	            {
            	            otherlv_5=(Token)match(input,42,FOLLOW_42_in_ruleMult2973); 

            	                	newLeafNode(otherlv_5, grammarAccess.getMultAccess().getPercentSignKeyword_1_0_2_0());
            	                
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1378:1: ()
            	            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1379:5: 
            	            {

            	                    current = forceCreateModelElementAndSet(
            	                        grammarAccess.getMultAccess().getModLeftAction_1_0_2_1(),
            	                        current);
            	                

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1384:4: ( (lv_right_7_0= ruleIn ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1385:1: (lv_right_7_0= ruleIn )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1385:1: (lv_right_7_0= ruleIn )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1386:3: lv_right_7_0= ruleIn
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleIn_in_ruleMult3005);
            	    lv_right_7_0=ruleIn();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMultRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_7_0, 
            	            		"In");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMult"


    // $ANTLR start "entryRuleIn"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1410:1: entryRuleIn returns [EObject current=null] : iv_ruleIn= ruleIn EOF ;
    public final EObject entryRuleIn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIn = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1411:2: (iv_ruleIn= ruleIn EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1412:2: iv_ruleIn= ruleIn EOF
            {
             newCompositeNode(grammarAccess.getInRule()); 
            pushFollow(FOLLOW_ruleIn_in_entryRuleIn3043);
            iv_ruleIn=ruleIn();

            state._fsp--;

             current =iv_ruleIn; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIn3053); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIn"


    // $ANTLR start "ruleIn"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1419:1: ruleIn returns [EObject current=null] : (this_Unary_0= ruleUnary ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )* ) ;
    public final EObject ruleIn() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_Unary_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1422:28: ( (this_Unary_0= ruleUnary ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1423:1: (this_Unary_0= ruleUnary ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1423:1: (this_Unary_0= ruleUnary ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1424:5: this_Unary_0= ruleUnary ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getInAccess().getUnaryParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleUnary_in_ruleIn3100);
            this_Unary_0=ruleUnary();

            state._fsp--;

             
                    current = this_Unary_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1432:1: ( (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==43) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1432:2: (otherlv_1= 'in' () ) ( (lv_right_3_0= ruleUnary ) )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1432:2: (otherlv_1= 'in' () )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1432:4: otherlv_1= 'in' ()
            	    {
            	    otherlv_1=(Token)match(input,43,FOLLOW_43_in_ruleIn3113); 

            	        	newLeafNode(otherlv_1, grammarAccess.getInAccess().getInKeyword_1_0_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1436:1: ()
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1437:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getInAccess().getInLeftAction_1_0_1(),
            	                current);
            	        

            	    }


            	    }

            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1442:3: ( (lv_right_3_0= ruleUnary ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1443:1: (lv_right_3_0= ruleUnary )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1443:1: (lv_right_3_0= ruleUnary )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1444:3: lv_right_3_0= ruleUnary
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInAccess().getRightUnaryParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleUnary_in_ruleIn3144);
            	    lv_right_3_0=ruleUnary();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"Unary");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIn"


    // $ANTLR start "entryRuleUnary"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1468:1: entryRuleUnary returns [EObject current=null] : iv_ruleUnary= ruleUnary EOF ;
    public final EObject entryRuleUnary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnary = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1469:2: (iv_ruleUnary= ruleUnary EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1470:2: iv_ruleUnary= ruleUnary EOF
            {
             newCompositeNode(grammarAccess.getUnaryRule()); 
            pushFollow(FOLLOW_ruleUnary_in_entryRuleUnary3182);
            iv_ruleUnary=ruleUnary();

            state._fsp--;

             current =iv_ruleUnary; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnary3192); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnary"


    // $ANTLR start "ruleUnary"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1477:1: ruleUnary returns [EObject current=null] : (this_Exponential_0= ruleExponential | (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) ) | (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) ) ) ;
    public final EObject ruleUnary() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        EObject this_Exponential_0 = null;

        EObject lv_exp_3_0 = null;

        EObject lv_expr_6_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1480:28: ( (this_Exponential_0= ruleExponential | (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) ) | (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1481:1: (this_Exponential_0= ruleExponential | (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) ) | (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1481:1: (this_Exponential_0= ruleExponential | (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) ) | (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) ) )
            int alt25=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case RULE_STRING:
            case RULE_FLOAT:
            case RULE_INT:
            case 46:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
                {
                alt25=1;
                }
                break;
            case 44:
                {
                alt25=2;
                }
                break;
            case 39:
                {
                alt25=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1482:5: this_Exponential_0= ruleExponential
                    {
                     
                            newCompositeNode(grammarAccess.getUnaryAccess().getExponentialParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleExponential_in_ruleUnary3239);
                    this_Exponential_0=ruleExponential();

                    state._fsp--;

                     
                            current = this_Exponential_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1491:6: (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1491:6: (otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1491:8: otherlv_1= 'not' () ( (lv_exp_3_0= ruleUnary ) )
                    {
                    otherlv_1=(Token)match(input,44,FOLLOW_44_in_ruleUnary3257); 

                        	newLeafNode(otherlv_1, grammarAccess.getUnaryAccess().getNotKeyword_1_0());
                        
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1495:1: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1496:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getUnaryAccess().getNotAction_1_1(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1501:2: ( (lv_exp_3_0= ruleUnary ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1502:1: (lv_exp_3_0= ruleUnary )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1502:1: (lv_exp_3_0= ruleUnary )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1503:3: lv_exp_3_0= ruleUnary
                    {
                     
                    	        newCompositeNode(grammarAccess.getUnaryAccess().getExpUnaryParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleUnary_in_ruleUnary3287);
                    lv_exp_3_0=ruleUnary();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getUnaryRule());
                    	        }
                           		set(
                           			current, 
                           			"exp",
                            		lv_exp_3_0, 
                            		"Unary");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1520:6: (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1520:6: (otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1520:8: otherlv_4= '-' () ( (lv_expr_6_0= ruleUnary ) )
                    {
                    otherlv_4=(Token)match(input,39,FOLLOW_39_in_ruleUnary3307); 

                        	newLeafNode(otherlv_4, grammarAccess.getUnaryAccess().getHyphenMinusKeyword_2_0());
                        
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1524:1: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1525:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getUnaryAccess().getUnaryMinusAction_2_1(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1530:2: ( (lv_expr_6_0= ruleUnary ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1531:1: (lv_expr_6_0= ruleUnary )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1531:1: (lv_expr_6_0= ruleUnary )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1532:3: lv_expr_6_0= ruleUnary
                    {
                     
                    	        newCompositeNode(grammarAccess.getUnaryAccess().getExprUnaryParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleUnary_in_ruleUnary3337);
                    lv_expr_6_0=ruleUnary();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getUnaryRule());
                    	        }
                           		set(
                           			current, 
                           			"expr",
                            		lv_expr_6_0, 
                            		"Unary");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnary"


    // $ANTLR start "entryRuleExponential"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1556:1: entryRuleExponential returns [EObject current=null] : iv_ruleExponential= ruleExponential EOF ;
    public final EObject entryRuleExponential() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExponential = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1557:2: (iv_ruleExponential= ruleExponential EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1558:2: iv_ruleExponential= ruleExponential EOF
            {
             newCompositeNode(grammarAccess.getExponentialRule()); 
            pushFollow(FOLLOW_ruleExponential_in_entryRuleExponential3374);
            iv_ruleExponential=ruleExponential();

            state._fsp--;

             current =iv_ruleExponential; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExponential3384); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExponential"


    // $ANTLR start "ruleExponential"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1565:1: ruleExponential returns [EObject current=null] : (this_Atom_0= ruleAtom (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )? ) ;
    public final EObject ruleExponential() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_Atom_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1568:28: ( (this_Atom_0= ruleAtom (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )? ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1569:1: (this_Atom_0= ruleAtom (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )? )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1569:1: (this_Atom_0= ruleAtom (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )? )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1570:5: this_Atom_0= ruleAtom (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getExponentialAccess().getAtomParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleAtom_in_ruleExponential3431);
            this_Atom_0=ruleAtom();

            state._fsp--;

             
                    current = this_Atom_0; 
                    afterParserOrEnumRuleCall();
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1578:1: (otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==45) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1578:3: otherlv_1= '^' () ( (lv_right_3_0= ruleExponential ) )
                    {
                    otherlv_1=(Token)match(input,45,FOLLOW_45_in_ruleExponential3443); 

                        	newLeafNode(otherlv_1, grammarAccess.getExponentialAccess().getCircumflexAccentKeyword_1_0());
                        
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1582:1: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1583:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1588:2: ( (lv_right_3_0= ruleExponential ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1589:1: (lv_right_3_0= ruleExponential )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1589:1: (lv_right_3_0= ruleExponential )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1590:3: lv_right_3_0= ruleExponential
                    {
                     
                    	        newCompositeNode(grammarAccess.getExponentialAccess().getRightExponentialParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleExponential_in_ruleExponential3473);
                    lv_right_3_0=ruleExponential();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getExponentialRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"Exponential");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExponential"


    // $ANTLR start "entryRuleAtom"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1614:1: entryRuleAtom returns [EObject current=null] : iv_ruleAtom= ruleAtom EOF ;
    public final EObject entryRuleAtom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtom = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1615:2: (iv_ruleAtom= ruleAtom EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1616:2: iv_ruleAtom= ruleAtom EOF
            {
             newCompositeNode(grammarAccess.getAtomRule()); 
            pushFollow(FOLLOW_ruleAtom_in_entryRuleAtom3511);
            iv_ruleAtom=ruleAtom();

            state._fsp--;

             current =iv_ruleAtom; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtom3521); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtom"


    // $ANTLR start "ruleAtom"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1623:1: ruleAtom returns [EObject current=null] : (this_Literal_0= ruleLiteral | this_QualifiedName_1= ruleQualifiedName | this_ParsExpression_2= ruleParsExpression ) ;
    public final EObject ruleAtom() throws RecognitionException {
        EObject current = null;

        EObject this_Literal_0 = null;

        EObject this_QualifiedName_1 = null;

        EObject this_ParsExpression_2 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1626:28: ( (this_Literal_0= ruleLiteral | this_QualifiedName_1= ruleQualifiedName | this_ParsExpression_2= ruleParsExpression ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1627:1: (this_Literal_0= ruleLiteral | this_QualifiedName_1= ruleQualifiedName | this_ParsExpression_2= ruleParsExpression )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1627:1: (this_Literal_0= ruleLiteral | this_QualifiedName_1= ruleQualifiedName | this_ParsExpression_2= ruleParsExpression )
            int alt27=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_FLOAT:
            case RULE_INT:
            case 48:
            case 49:
            case 50:
                {
                alt27=1;
                }
                break;
            case RULE_ID:
            case 51:
            case 52:
                {
                alt27=2;
                }
                break;
            case 46:
                {
                alt27=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1628:5: this_Literal_0= ruleLiteral
                    {
                     
                            newCompositeNode(grammarAccess.getAtomAccess().getLiteralParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleLiteral_in_ruleAtom3568);
                    this_Literal_0=ruleLiteral();

                    state._fsp--;

                     
                            current = this_Literal_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1638:5: this_QualifiedName_1= ruleQualifiedName
                    {
                     
                            newCompositeNode(grammarAccess.getAtomAccess().getQualifiedNameParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleQualifiedName_in_ruleAtom3595);
                    this_QualifiedName_1=ruleQualifiedName();

                    state._fsp--;

                     
                            current = this_QualifiedName_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1648:5: this_ParsExpression_2= ruleParsExpression
                    {
                     
                            newCompositeNode(grammarAccess.getAtomAccess().getParsExpressionParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleParsExpression_in_ruleAtom3622);
                    this_ParsExpression_2=ruleParsExpression();

                    state._fsp--;

                     
                            current = this_ParsExpression_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtom"


    // $ANTLR start "entryRuleParsExpression"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1664:1: entryRuleParsExpression returns [EObject current=null] : iv_ruleParsExpression= ruleParsExpression EOF ;
    public final EObject entryRuleParsExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParsExpression = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1665:2: (iv_ruleParsExpression= ruleParsExpression EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1666:2: iv_ruleParsExpression= ruleParsExpression EOF
            {
             newCompositeNode(grammarAccess.getParsExpressionRule()); 
            pushFollow(FOLLOW_ruleParsExpression_in_entryRuleParsExpression3657);
            iv_ruleParsExpression=ruleParsExpression();

            state._fsp--;

             current =iv_ruleParsExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParsExpression3667); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParsExpression"


    // $ANTLR start "ruleParsExpression"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1673:1: ruleParsExpression returns [EObject current=null] : ( () otherlv_1= '(' ( (lv_expr_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleParsExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_expr_2_0 = null;

        EObject lv_expr_4_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1676:28: ( ( () otherlv_1= '(' ( (lv_expr_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1677:1: ( () otherlv_1= '(' ( (lv_expr_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1677:1: ( () otherlv_1= '(' ( (lv_expr_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1677:2: () otherlv_1= '(' ( (lv_expr_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1677:2: ()
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1678:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getParsExpressionAccess().getParsExpressionAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,46,FOLLOW_46_in_ruleParsExpression3713); 

                	newLeafNode(otherlv_1, grammarAccess.getParsExpressionAccess().getLeftParenthesisKeyword_1());
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1687:1: ( (lv_expr_2_0= ruleExpression ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1688:1: (lv_expr_2_0= ruleExpression )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1688:1: (lv_expr_2_0= ruleExpression )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1689:3: lv_expr_2_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleParsExpression3734);
            lv_expr_2_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getParsExpressionRule());
            	        }
                   		add(
                   			current, 
                   			"expr",
                    		lv_expr_2_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1705:2: (otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==13) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1705:4: otherlv_3= ',' ( (lv_expr_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleParsExpression3747); 

            	        	newLeafNode(otherlv_3, grammarAccess.getParsExpressionAccess().getCommaKeyword_3_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1709:1: ( (lv_expr_4_0= ruleExpression ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1710:1: (lv_expr_4_0= ruleExpression )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1710:1: (lv_expr_4_0= ruleExpression )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1711:3: lv_expr_4_0= ruleExpression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleExpression_in_ruleParsExpression3768);
            	    lv_expr_4_0=ruleExpression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getParsExpressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expr",
            	            		lv_expr_4_0, 
            	            		"Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            otherlv_5=(Token)match(input,47,FOLLOW_47_in_ruleParsExpression3782); 

                	newLeafNode(otherlv_5, grammarAccess.getParsExpressionAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParsExpression"


    // $ANTLR start "entryRuleLiteral"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1739:1: entryRuleLiteral returns [EObject current=null] : iv_ruleLiteral= ruleLiteral EOF ;
    public final EObject entryRuleLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteral = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1740:2: (iv_ruleLiteral= ruleLiteral EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1741:2: iv_ruleLiteral= ruleLiteral EOF
            {
             newCompositeNode(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_ruleLiteral_in_entryRuleLiteral3818);
            iv_ruleLiteral=ruleLiteral();

            state._fsp--;

             current =iv_ruleLiteral; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteral3828); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1748:1: ruleLiteral returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) | ( () ( (lv_value_3_0= RULE_FLOAT ) ) ) | ( () ( (lv_value_5_0= RULE_INT ) ) ) | ( () ( (lv_value_7_0= 'null' ) ) ) | ( () ( (lv_value_9_0= 'true' ) ) ) | ( () ( (lv_value_11_0= 'false' ) ) ) ) ;
    public final EObject ruleLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_0=null;
        Token lv_value_7_0=null;
        Token lv_value_9_0=null;
        Token lv_value_11_0=null;

         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1751:28: ( ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) | ( () ( (lv_value_3_0= RULE_FLOAT ) ) ) | ( () ( (lv_value_5_0= RULE_INT ) ) ) | ( () ( (lv_value_7_0= 'null' ) ) ) | ( () ( (lv_value_9_0= 'true' ) ) ) | ( () ( (lv_value_11_0= 'false' ) ) ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:1: ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) | ( () ( (lv_value_3_0= RULE_FLOAT ) ) ) | ( () ( (lv_value_5_0= RULE_INT ) ) ) | ( () ( (lv_value_7_0= 'null' ) ) ) | ( () ( (lv_value_9_0= 'true' ) ) ) | ( () ( (lv_value_11_0= 'false' ) ) ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:1: ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) | ( () ( (lv_value_3_0= RULE_FLOAT ) ) ) | ( () ( (lv_value_5_0= RULE_INT ) ) ) | ( () ( (lv_value_7_0= 'null' ) ) ) | ( () ( (lv_value_9_0= 'true' ) ) ) | ( () ( (lv_value_11_0= 'false' ) ) ) )
            int alt29=6;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt29=1;
                }
                break;
            case RULE_FLOAT:
                {
                alt29=2;
                }
                break;
            case RULE_INT:
                {
                alt29=3;
                }
                break;
            case 48:
                {
                alt29=4;
                }
                break;
            case 49:
                {
                alt29=5;
                }
                break;
            case 50:
                {
                alt29=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:3: () ( (lv_value_1_0= RULE_STRING ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1752:3: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1753:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getStringLiteralAction_0_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1758:2: ( (lv_value_1_0= RULE_STRING ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1759:1: (lv_value_1_0= RULE_STRING )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1759:1: (lv_value_1_0= RULE_STRING )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1760:3: lv_value_1_0= RULE_STRING
                    {
                    lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleLiteral3880); 

                    			newLeafNode(lv_value_1_0, grammarAccess.getLiteralAccess().getValueSTRINGTerminalRuleCall_0_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_1_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1777:6: ( () ( (lv_value_3_0= RULE_FLOAT ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1777:6: ( () ( (lv_value_3_0= RULE_FLOAT ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1777:7: () ( (lv_value_3_0= RULE_FLOAT ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1777:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1778:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getFloatLiteralAction_1_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1783:2: ( (lv_value_3_0= RULE_FLOAT ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1784:1: (lv_value_3_0= RULE_FLOAT )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1784:1: (lv_value_3_0= RULE_FLOAT )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1785:3: lv_value_3_0= RULE_FLOAT
                    {
                    lv_value_3_0=(Token)match(input,RULE_FLOAT,FOLLOW_RULE_FLOAT_in_ruleLiteral3919); 

                    			newLeafNode(lv_value_3_0, grammarAccess.getLiteralAccess().getValueFLOATTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_3_0, 
                            		"FLOAT");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1802:6: ( () ( (lv_value_5_0= RULE_INT ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1802:6: ( () ( (lv_value_5_0= RULE_INT ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1802:7: () ( (lv_value_5_0= RULE_INT ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1802:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1803:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getIntegerLiteralAction_2_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1808:2: ( (lv_value_5_0= RULE_INT ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1809:1: (lv_value_5_0= RULE_INT )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1809:1: (lv_value_5_0= RULE_INT )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1810:3: lv_value_5_0= RULE_INT
                    {
                    lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleLiteral3958); 

                    			newLeafNode(lv_value_5_0, grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_2_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_5_0, 
                            		"INT");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1827:6: ( () ( (lv_value_7_0= 'null' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1827:6: ( () ( (lv_value_7_0= 'null' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1827:7: () ( (lv_value_7_0= 'null' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1827:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1828:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getNullAction_3_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1833:2: ( (lv_value_7_0= 'null' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1834:1: (lv_value_7_0= 'null' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1834:1: (lv_value_7_0= 'null' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1835:3: lv_value_7_0= 'null'
                    {
                    lv_value_7_0=(Token)match(input,48,FOLLOW_48_in_ruleLiteral3998); 

                            newLeafNode(lv_value_7_0, grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_7_0, "null");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1849:6: ( () ( (lv_value_9_0= 'true' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1849:6: ( () ( (lv_value_9_0= 'true' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1849:7: () ( (lv_value_9_0= 'true' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1849:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1850:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getTrueAction_4_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1855:2: ( (lv_value_9_0= 'true' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1856:1: (lv_value_9_0= 'true' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1856:1: (lv_value_9_0= 'true' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1857:3: lv_value_9_0= 'true'
                    {
                    lv_value_9_0=(Token)match(input,49,FOLLOW_49_in_ruleLiteral4046); 

                            newLeafNode(lv_value_9_0, grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_9_0, "true");
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1871:6: ( () ( (lv_value_11_0= 'false' ) ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1871:6: ( () ( (lv_value_11_0= 'false' ) ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1871:7: () ( (lv_value_11_0= 'false' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1871:7: ()
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1872:5: 
                    {

                            current = forceCreateModelElement(
                                grammarAccess.getLiteralAccess().getFalseAction_5_0(),
                                current);
                        

                    }

                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1877:2: ( (lv_value_11_0= 'false' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1878:1: (lv_value_11_0= 'false' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1878:1: (lv_value_11_0= 'false' )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1879:3: lv_value_11_0= 'false'
                    {
                    lv_value_11_0=(Token)match(input,50,FOLLOW_50_in_ruleLiteral4094); 

                            newLeafNode(lv_value_11_0, grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLiteralRule());
                    	        }
                           		setWithLastConsumed(current, "value", lv_value_11_0, "false");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleQualifiedName"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1900:1: entryRuleQualifiedName returns [EObject current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final EObject entryRuleQualifiedName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualifiedName = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1901:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1902:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName4144);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName4154); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1909:1: ruleQualifiedName returns [EObject current=null] : ( ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )? ( (lv_qualifiers_1_0= ruleQualified ) ) (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )* ) ;
    public final EObject ruleQualifiedName() throws RecognitionException {
        EObject current = null;

        Token lv_granular_0_1=null;
        Token lv_granular_0_2=null;
        Token otherlv_2=null;
        EObject lv_qualifiers_1_0 = null;

        EObject lv_qualifiers_3_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1912:28: ( ( ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )? ( (lv_qualifiers_1_0= ruleQualified ) ) (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )* ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1913:1: ( ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )? ( (lv_qualifiers_1_0= ruleQualified ) ) (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )* )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1913:1: ( ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )? ( (lv_qualifiers_1_0= ruleQualified ) ) (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )* )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1913:2: ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )? ( (lv_qualifiers_1_0= ruleQualified ) ) (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )*
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1913:2: ( ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( ((LA31_0>=51 && LA31_0<=52)) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1914:1: ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1914:1: ( (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' ) )
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1915:1: (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' )
                    {
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1915:1: (lv_granular_0_1= 'distinct' | lv_granular_0_2= 'all' )
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==51) ) {
                        alt30=1;
                    }
                    else if ( (LA30_0==52) ) {
                        alt30=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 0, input);

                        throw nvae;
                    }
                    switch (alt30) {
                        case 1 :
                            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1916:3: lv_granular_0_1= 'distinct'
                            {
                            lv_granular_0_1=(Token)match(input,51,FOLLOW_51_in_ruleQualifiedName4199); 

                                    newLeafNode(lv_granular_0_1, grammarAccess.getQualifiedNameAccess().getGranularDistinctKeyword_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getQualifiedNameRule());
                            	        }
                                   		setWithLastConsumed(current, "granular", lv_granular_0_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1928:8: lv_granular_0_2= 'all'
                            {
                            lv_granular_0_2=(Token)match(input,52,FOLLOW_52_in_ruleQualifiedName4228); 

                                    newLeafNode(lv_granular_0_2, grammarAccess.getQualifiedNameAccess().getGranularAllKeyword_0_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getQualifiedNameRule());
                            	        }
                                   		setWithLastConsumed(current, "granular", lv_granular_0_2, null);
                            	    

                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1943:3: ( (lv_qualifiers_1_0= ruleQualified ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1944:1: (lv_qualifiers_1_0= ruleQualified )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1944:1: (lv_qualifiers_1_0= ruleQualified )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1945:3: lv_qualifiers_1_0= ruleQualified
            {
             
            	        newCompositeNode(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleQualified_in_ruleQualifiedName4266);
            lv_qualifiers_1_0=ruleQualified();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQualifiedNameRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_1_0, 
                    		"Qualified");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1961:2: (otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==28) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1961:4: otherlv_2= '.' ( (lv_qualifiers_3_0= ruleQualified ) )
            	    {
            	    otherlv_2=(Token)match(input,28,FOLLOW_28_in_ruleQualifiedName4279); 

            	        	newLeafNode(otherlv_2, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_2_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1965:1: ( (lv_qualifiers_3_0= ruleQualified ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1966:1: (lv_qualifiers_3_0= ruleQualified )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1966:1: (lv_qualifiers_3_0= ruleQualified )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1967:3: lv_qualifiers_3_0= ruleQualified
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleQualified_in_ruleQualifiedName4300);
            	    lv_qualifiers_3_0=ruleQualified();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQualifiedNameRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_3_0, 
            	            		"Qualified");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleQualified"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1991:1: entryRuleQualified returns [EObject current=null] : iv_ruleQualified= ruleQualified EOF ;
    public final EObject entryRuleQualified() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualified = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1992:2: (iv_ruleQualified= ruleQualified EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:1993:2: iv_ruleQualified= ruleQualified EOF
            {
             newCompositeNode(grammarAccess.getQualifiedRule()); 
            pushFollow(FOLLOW_ruleQualified_in_entryRuleQualified4338);
            iv_ruleQualified=ruleQualified();

            state._fsp--;

             current =iv_ruleQualified; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified4348); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualified"


    // $ANTLR start "ruleQualified"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2000:1: ruleQualified returns [EObject current=null] : this_QualifiedElement_0= ruleQualifiedElement ;
    public final EObject ruleQualified() throws RecognitionException {
        EObject current = null;

        EObject this_QualifiedElement_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2003:28: (this_QualifiedElement_0= ruleQualifiedElement )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2005:5: this_QualifiedElement_0= ruleQualifiedElement
            {
             
                    newCompositeNode(grammarAccess.getQualifiedAccess().getQualifiedElementParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleQualifiedElement_in_ruleQualified4394);
            this_QualifiedElement_0=ruleQualifiedElement();

            state._fsp--;

             
                    current = this_QualifiedElement_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualified"


    // $ANTLR start "entryRuleQualifiedElement"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2021:1: entryRuleQualifiedElement returns [EObject current=null] : iv_ruleQualifiedElement= ruleQualifiedElement EOF ;
    public final EObject entryRuleQualifiedElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualifiedElement = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2022:2: (iv_ruleQualifiedElement= ruleQualifiedElement EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2023:2: iv_ruleQualifiedElement= ruleQualifiedElement EOF
            {
             newCompositeNode(grammarAccess.getQualifiedElementRule()); 
            pushFollow(FOLLOW_ruleQualifiedElement_in_entryRuleQualifiedElement4428);
            iv_ruleQualifiedElement=ruleQualifiedElement();

            state._fsp--;

             current =iv_ruleQualifiedElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedElement4438); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedElement"


    // $ANTLR start "ruleQualifiedElement"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2030:1: ruleQualifiedElement returns [EObject current=null] : (this_Identifier_0= ruleIdentifier | this_Function_1= ruleFunction ) ;
    public final EObject ruleQualifiedElement() throws RecognitionException {
        EObject current = null;

        EObject this_Identifier_0 = null;

        EObject this_Function_1 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2033:28: ( (this_Identifier_0= ruleIdentifier | this_Function_1= ruleFunction ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2034:1: (this_Identifier_0= ruleIdentifier | this_Function_1= ruleFunction )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2034:1: (this_Identifier_0= ruleIdentifier | this_Function_1= ruleFunction )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_ID) ) {
                int LA33_1 = input.LA(2);

                if ( (LA33_1==EOF||LA33_1==13||(LA33_1>=21 && LA33_1<=24)||(LA33_1>=28 && LA33_1<=43)||LA33_1==45||LA33_1==47) ) {
                    alt33=1;
                }
                else if ( (LA33_1==46) ) {
                    alt33=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2035:5: this_Identifier_0= ruleIdentifier
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedElementAccess().getIdentifierParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleIdentifier_in_ruleQualifiedElement4485);
                    this_Identifier_0=ruleIdentifier();

                    state._fsp--;

                     
                            current = this_Identifier_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2045:5: this_Function_1= ruleFunction
                    {
                     
                            newCompositeNode(grammarAccess.getQualifiedElementAccess().getFunctionParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleFunction_in_ruleQualifiedElement4512);
                    this_Function_1=ruleFunction();

                    state._fsp--;

                     
                            current = this_Function_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedElement"


    // $ANTLR start "entryRuleFunction"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2061:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2062:2: (iv_ruleFunction= ruleFunction EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2063:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_ruleFunction_in_entryRuleFunction4547);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunction4557); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2070:1: ruleFunction returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_params_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_params_2_0 = null;

        EObject lv_params_4_0 = null;


         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2073:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_params_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2074:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_params_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2074:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_params_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2074:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( (lv_params_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2074:2: ( (lv_name_0_0= RULE_ID ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2075:1: (lv_name_0_0= RULE_ID )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2075:1: (lv_name_0_0= RULE_ID )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2076:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFunction4599); 

            			newLeafNode(lv_name_0_0, grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFunctionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,46,FOLLOW_46_in_ruleFunction4616); 

                	newLeafNode(otherlv_1, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_1());
                
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2096:1: ( (lv_params_2_0= ruleExpression ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2097:1: (lv_params_2_0= ruleExpression )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2097:1: (lv_params_2_0= ruleExpression )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2098:3: lv_params_2_0= ruleExpression
            {
             
            	        newCompositeNode(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleExpression_in_ruleFunction4637);
            lv_params_2_0=ruleExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFunctionRule());
            	        }
                   		add(
                   			current, 
                   			"params",
                    		lv_params_2_0, 
                    		"Expression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2114:2: (otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) ) )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==13) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2114:4: otherlv_3= ',' ( (lv_params_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleFunction4650); 

            	        	newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getCommaKeyword_3_0());
            	        
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2118:1: ( (lv_params_4_0= ruleExpression ) )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2119:1: (lv_params_4_0= ruleExpression )
            	    {
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2119:1: (lv_params_4_0= ruleExpression )
            	    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2120:3: lv_params_4_0= ruleExpression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleExpression_in_ruleFunction4671);
            	    lv_params_4_0=ruleExpression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFunctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"params",
            	            		lv_params_4_0, 
            	            		"Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

            otherlv_5=(Token)match(input,47,FOLLOW_47_in_ruleFunction4685); 

                	newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleIdentifier"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2148:1: entryRuleIdentifier returns [EObject current=null] : iv_ruleIdentifier= ruleIdentifier EOF ;
    public final EObject entryRuleIdentifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIdentifier = null;


        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2149:2: (iv_ruleIdentifier= ruleIdentifier EOF )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2150:2: iv_ruleIdentifier= ruleIdentifier EOF
            {
             newCompositeNode(grammarAccess.getIdentifierRule()); 
            pushFollow(FOLLOW_ruleIdentifier_in_entryRuleIdentifier4721);
            iv_ruleIdentifier=ruleIdentifier();

            state._fsp--;

             current =iv_ruleIdentifier; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIdentifier4731); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIdentifier"


    // $ANTLR start "ruleIdentifier"
    // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2157:1: ruleIdentifier returns [EObject current=null] : ( (lv_id_0_0= RULE_ID ) ) ;
    public final EObject ruleIdentifier() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;

         enterRule(); 
            
        try {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2160:28: ( ( (lv_id_0_0= RULE_ID ) ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2161:1: ( (lv_id_0_0= RULE_ID ) )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2161:1: ( (lv_id_0_0= RULE_ID ) )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2162:1: (lv_id_0_0= RULE_ID )
            {
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2162:1: (lv_id_0_0= RULE_ID )
            // ../br.ufsm.aql/src-gen/br/ufsm/parser/antlr/internal/InternalAQL.g:2163:3: lv_id_0_0= RULE_ID
            {
            lv_id_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIdentifier4772); 

            			newLeafNode(lv_id_0_0, grammarAccess.getIdentifierAccess().getIdIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIdentifierRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIdentifier"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleQuery_in_entryRuleQuery81 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuery91 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQueryStatement_in_ruleQuery140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQueryStatement_in_entryRuleQueryStatement179 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQueryStatement189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFindClause_in_ruleQueryStatement235 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_ruleWhereClause_in_ruleQueryStatement256 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_ruleReturnsClause_in_ruleQueryStatement278 = new BitSet(new long[]{0x0000000001800002L});
    public static final BitSet FOLLOW_ruleOrderByClause_in_ruleQueryStatement299 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_ruleGroupByClause_in_ruleQueryStatement321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFindClause_in_entryRuleFindClause358 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFindClause368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleFindClause411 = new BitSet(new long[]{0x00000000000FC000L});
    public static final BitSet FOLLOW_ruleBindingObject_in_ruleFindClause445 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13_in_ruleFindClause458 = new BitSet(new long[]{0x00000000000FC000L});
    public static final BitSet FOLLOW_ruleBindingObject_in_ruleFindClause479 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleBindingObject_in_entryRuleBindingObject517 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBindingObject527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectType_in_ruleBindingObject573 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBindingObject590 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleObjectType_in_entryRuleObjectType632 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleObjectType642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleObjectType695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleObjectType743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleObjectType791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleObjectType839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleObjectType887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleObjectType935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_entryRuleWhereClause985 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhereClause995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleWhereClause1038 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleWhereClause1072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReturnsClause_in_entryRuleReturnsClause1108 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReturnsClause1118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleReturnsClause1161 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleReturnsClause1195 = new BitSet(new long[]{0x0000000000402002L});
    public static final BitSet FOLLOW_22_in_ruleReturnsClause1208 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReturnsClause1225 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13_in_ruleReturnsClause1245 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleReturnsClause1266 = new BitSet(new long[]{0x0000000000402002L});
    public static final BitSet FOLLOW_22_in_ruleReturnsClause1279 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReturnsClause1296 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleGroupByClause_in_entryRuleGroupByClause1341 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGroupByClause1351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleGroupByClause1394 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_ruleGroupByClause1428 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13_in_ruleGroupByClause1441 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_ruleGroupByClause1462 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleOrderByClause_in_entryRuleOrderByClause1500 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderByClause1510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleOrderByClause1553 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_ruleOrderByClause1587 = new BitSet(new long[]{0x000000000E002002L});
    public static final BitSet FOLLOW_13_in_ruleOrderByClause1600 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_ruleOrderByClause1621 = new BitSet(new long[]{0x000000000E002002L});
    public static final BitSet FOLLOW_25_in_ruleOrderByClause1643 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_26_in_ruleOrderByClause1672 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_27_in_ruleOrderByClause1702 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleOrderByClause1723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_entryRuleSimpleQualifiedName1761 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleQualifiedName1771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_ruleSimpleQualifiedName1817 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28_in_ruleSimpleQualifiedName1830 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_ruleSimpleQualifiedName1851 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_entryRuleSimpleQualified1889 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleQualified1899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_ruleSimpleQualified1945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression1985 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression1995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_ruleExpression2045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr2083 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr2093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_ruleOr2140 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_29_in_ruleOr2153 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleAnd_in_ruleOr2184 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd2222 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd2232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_ruleAnd2279 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_ruleAnd2291 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleRelation_in_ruleAnd2321 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_ruleRelation_in_entryRuleRelation2359 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelation2369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdd_in_ruleRelation2416 = new BitSet(new long[]{0x0000003F80000002L});
    public static final BitSet FOLLOW_31_in_ruleRelation2430 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_32_in_ruleRelation2459 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_33_in_ruleRelation2488 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_34_in_ruleRelation2517 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_35_in_ruleRelation2546 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_36_in_ruleRelation2575 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_37_in_ruleRelation2604 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleAdd_in_ruleRelation2636 = new BitSet(new long[]{0x0000003F80000002L});
    public static final BitSet FOLLOW_ruleAdd_in_entryRuleAdd2674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdd2684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMult_in_ruleAdd2731 = new BitSet(new long[]{0x000000C000000002L});
    public static final BitSet FOLLOW_38_in_ruleAdd2745 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_39_in_ruleAdd2774 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleMult_in_ruleAdd2806 = new BitSet(new long[]{0x000000C000000002L});
    public static final BitSet FOLLOW_ruleMult_in_entryRuleMult2844 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMult2854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIn_in_ruleMult2901 = new BitSet(new long[]{0x0000070000000002L});
    public static final BitSet FOLLOW_40_in_ruleMult2915 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_41_in_ruleMult2944 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_42_in_ruleMult2973 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleIn_in_ruleMult3005 = new BitSet(new long[]{0x0000070000000002L});
    public static final BitSet FOLLOW_ruleIn_in_entryRuleIn3043 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIn3053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_ruleIn3100 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_43_in_ruleIn3113 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleUnary_in_ruleIn3144 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_entryRuleUnary3182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnary3192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExponential_in_ruleUnary3239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleUnary3257 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleUnary_in_ruleUnary3287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleUnary3307 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleUnary_in_ruleUnary3337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExponential_in_entryRuleExponential3374 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExponential3384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtom_in_ruleExponential3431 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_45_in_ruleExponential3443 = new BitSet(new long[]{0x001F4000000000F0L});
    public static final BitSet FOLLOW_ruleExponential_in_ruleExponential3473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtom_in_entryRuleAtom3511 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtom3521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteral_in_ruleAtom3568 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleAtom3595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParsExpression_in_ruleAtom3622 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParsExpression_in_entryRuleParsExpression3657 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParsExpression3667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleParsExpression3713 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleParsExpression3734 = new BitSet(new long[]{0x0000800000002000L});
    public static final BitSet FOLLOW_13_in_ruleParsExpression3747 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleParsExpression3768 = new BitSet(new long[]{0x0000800000002000L});
    public static final BitSet FOLLOW_47_in_ruleParsExpression3782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteral_in_entryRuleLiteral3818 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteral3828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleLiteral3880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_FLOAT_in_ruleLiteral3919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleLiteral3958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleLiteral3998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleLiteral4046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleLiteral4094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName4144 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName4154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleQualifiedName4199 = new BitSet(new long[]{0x0018000000000010L});
    public static final BitSet FOLLOW_52_in_ruleQualifiedName4228 = new BitSet(new long[]{0x0018000000000010L});
    public static final BitSet FOLLOW_ruleQualified_in_ruleQualifiedName4266 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28_in_ruleQualifiedName4279 = new BitSet(new long[]{0x0018000000000010L});
    public static final BitSet FOLLOW_ruleQualified_in_ruleQualifiedName4300 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_ruleQualified_in_entryRuleQualified4338 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified4348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedElement_in_ruleQualified4394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedElement_in_entryRuleQualifiedElement4428 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedElement4438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_ruleQualifiedElement4485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_ruleQualifiedElement4512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_entryRuleFunction4547 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunction4557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFunction4599 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_ruleFunction4616 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFunction4637 = new BitSet(new long[]{0x0000800000002000L});
    public static final BitSet FOLLOW_13_in_ruleFunction4650 = new BitSet(new long[]{0x001F5080000000F0L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleFunction4671 = new BitSet(new long[]{0x0000800000002000L});
    public static final BitSet FOLLOW_47_in_ruleFunction4685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_entryRuleIdentifier4721 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIdentifier4731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIdentifier4772 = new BitSet(new long[]{0x0000000000000002L});

}