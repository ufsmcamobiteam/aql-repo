/**
 */
package br.ufsm.aql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.QueryStatement#getFind <em>Find</em>}</li>
 *   <li>{@link br.ufsm.aql.QueryStatement#getWhere <em>Where</em>}</li>
 *   <li>{@link br.ufsm.aql.QueryStatement#getReturn <em>Return</em>}</li>
 *   <li>{@link br.ufsm.aql.QueryStatement#getOrderby <em>Orderby</em>}</li>
 *   <li>{@link br.ufsm.aql.QueryStatement#getGroupby <em>Groupby</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getQueryStatement()
 * @model
 * @generated
 */
public interface QueryStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Find</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Find</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Find</em>' containment reference.
   * @see #setFind(FindClause)
   * @see br.ufsm.aql.AqlPackage#getQueryStatement_Find()
   * @model containment="true"
   * @generated
   */
  FindClause getFind();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QueryStatement#getFind <em>Find</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Find</em>' containment reference.
   * @see #getFind()
   * @generated
   */
  void setFind(FindClause value);

  /**
   * Returns the value of the '<em><b>Where</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Where</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Where</em>' containment reference.
   * @see #setWhere(WhereClause)
   * @see br.ufsm.aql.AqlPackage#getQueryStatement_Where()
   * @model containment="true"
   * @generated
   */
  WhereClause getWhere();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QueryStatement#getWhere <em>Where</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Where</em>' containment reference.
   * @see #getWhere()
   * @generated
   */
  void setWhere(WhereClause value);

  /**
   * Returns the value of the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return</em>' containment reference.
   * @see #setReturn(ReturnsClause)
   * @see br.ufsm.aql.AqlPackage#getQueryStatement_Return()
   * @model containment="true"
   * @generated
   */
  ReturnsClause getReturn();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QueryStatement#getReturn <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return</em>' containment reference.
   * @see #getReturn()
   * @generated
   */
  void setReturn(ReturnsClause value);

  /**
   * Returns the value of the '<em><b>Orderby</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Orderby</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Orderby</em>' containment reference.
   * @see #setOrderby(OrderByClause)
   * @see br.ufsm.aql.AqlPackage#getQueryStatement_Orderby()
   * @model containment="true"
   * @generated
   */
  OrderByClause getOrderby();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QueryStatement#getOrderby <em>Orderby</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Orderby</em>' containment reference.
   * @see #getOrderby()
   * @generated
   */
  void setOrderby(OrderByClause value);

  /**
   * Returns the value of the '<em><b>Groupby</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Groupby</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Groupby</em>' containment reference.
   * @see #setGroupby(GroupByClause)
   * @see br.ufsm.aql.AqlPackage#getQueryStatement_Groupby()
   * @model containment="true"
   * @generated
   */
  GroupByClause getGroupby();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QueryStatement#getGroupby <em>Groupby</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Groupby</em>' containment reference.
   * @see #getGroupby()
   * @generated
   */
  void setGroupby(GroupByClause value);

} // QueryStatement
