/**
 */
package br.ufsm.aql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getEnum()
 * @model
 * @generated
 */
public interface Enum extends ObjectType
{
} // Enum
