/**
 */
package br.ufsm.aql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Find Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.FindClause#getClause <em>Clause</em>}</li>
 *   <li>{@link br.ufsm.aql.FindClause#getBindingObject <em>Binding Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getFindClause()
 * @model
 * @generated
 */
public interface FindClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Clause</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Clause</em>' attribute.
   * @see #setClause(String)
   * @see br.ufsm.aql.AqlPackage#getFindClause_Clause()
   * @model
   * @generated
   */
  String getClause();

  /**
   * Sets the value of the '{@link br.ufsm.aql.FindClause#getClause <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Clause</em>' attribute.
   * @see #getClause()
   * @generated
   */
  void setClause(String value);

  /**
   * Returns the value of the '<em><b>Binding Object</b></em>' containment reference list.
   * The list contents are of type {@link br.ufsm.aql.BindingObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Binding Object</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Binding Object</em>' containment reference list.
   * @see br.ufsm.aql.AqlPackage#getFindClause_BindingObject()
   * @model containment="true"
   * @generated
   */
  EList<BindingObject> getBindingObject();

} // FindClause
