/**
 */
package br.ufsm.aql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.QualifiedName#getQualifiers <em>Qualifiers</em>}</li>
 *   <li>{@link br.ufsm.aql.QualifiedName#getGranular <em>Granular</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getQualifiedName()
 * @model
 * @generated
 */
public interface QualifiedName extends Expression
{
  /**
   * Returns the value of the '<em><b>Qualifiers</b></em>' containment reference list.
   * The list contents are of type {@link br.ufsm.aql.QualifiedElement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qualifiers</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qualifiers</em>' containment reference list.
   * @see br.ufsm.aql.AqlPackage#getQualifiedName_Qualifiers()
   * @model containment="true"
   * @generated
   */
  EList<QualifiedElement> getQualifiers();

  /**
   * Returns the value of the '<em><b>Granular</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Granular</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Granular</em>' attribute.
   * @see #setGranular(String)
   * @see br.ufsm.aql.AqlPackage#getQualifiedName_Granular()
   * @model
   * @generated
   */
  String getGranular();

  /**
   * Sets the value of the '{@link br.ufsm.aql.QualifiedName#getGranular <em>Granular</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Granular</em>' attribute.
   * @see #getGranular()
   * @generated
   */
  void setGranular(String value);

} // QualifiedName
