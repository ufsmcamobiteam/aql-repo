/**
 */
package br.ufsm.aql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.Identifier#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getIdentifier()
 * @model
 * @generated
 */
public interface Identifier extends QualifiedElement
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see br.ufsm.aql.AqlPackage#getIdentifier_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link br.ufsm.aql.Identifier#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

} // Identifier
