/**
 */
package br.ufsm.aql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pars Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.ParsExpression#getExpr <em>Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getParsExpression()
 * @model
 * @generated
 */
public interface ParsExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference list.
   * The list contents are of type {@link br.ufsm.aql.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference list.
   * @see br.ufsm.aql.AqlPackage#getParsExpression_Expr()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getExpr();

} // ParsExpression
