/**
 */
package br.ufsm.aql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getQualifiedElement()
 * @model
 * @generated
 */
public interface QualifiedElement extends EObject
{
} // QualifiedElement
