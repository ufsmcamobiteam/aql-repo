/**
 */
package br.ufsm.aql.impl;

import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.Expression;
import br.ufsm.aql.OrderByClause;
import br.ufsm.aql.QualifiedName;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Order By Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link br.ufsm.aql.impl.OrderByClauseImpl#getClause <em>Clause</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.OrderByClauseImpl#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.OrderByClauseImpl#getOption <em>Option</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.OrderByClauseImpl#getHavingExpression <em>Having Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OrderByClauseImpl extends MinimalEObjectImpl.Container implements OrderByClause
{
  /**
   * The default value of the '{@link #getClause() <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClause()
   * @generated
   * @ordered
   */
  protected static final String CLAUSE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getClause() <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClause()
   * @generated
   * @ordered
   */
  protected String clause = CLAUSE_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpressions()
   * @generated
   * @ordered
   */
  protected EList<QualifiedName> expressions;

  /**
   * The default value of the '{@link #getOption() <em>Option</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOption()
   * @generated
   * @ordered
   */
  protected static final String OPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOption() <em>Option</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOption()
   * @generated
   * @ordered
   */
  protected String option = OPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getHavingExpression() <em>Having Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHavingExpression()
   * @generated
   * @ordered
   */
  protected Expression havingExpression;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OrderByClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AqlPackage.Literals.ORDER_BY_CLAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getClause()
  {
    return clause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClause(String newClause)
  {
    String oldClause = clause;
    clause = newClause;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.ORDER_BY_CLAUSE__CLAUSE, oldClause, clause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<QualifiedName> getExpressions()
  {
    if (expressions == null)
    {
      expressions = new EObjectContainmentEList<QualifiedName>(QualifiedName.class, this, AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS);
    }
    return expressions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOption()
  {
    return option;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOption(String newOption)
  {
    String oldOption = option;
    option = newOption;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.ORDER_BY_CLAUSE__OPTION, oldOption, option));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getHavingExpression()
  {
    return havingExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetHavingExpression(Expression newHavingExpression, NotificationChain msgs)
  {
    Expression oldHavingExpression = havingExpression;
    havingExpression = newHavingExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION, oldHavingExpression, newHavingExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHavingExpression(Expression newHavingExpression)
  {
    if (newHavingExpression != havingExpression)
    {
      NotificationChain msgs = null;
      if (havingExpression != null)
        msgs = ((InternalEObject)havingExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION, null, msgs);
      if (newHavingExpression != null)
        msgs = ((InternalEObject)newHavingExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION, null, msgs);
      msgs = basicSetHavingExpression(newHavingExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION, newHavingExpression, newHavingExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS:
        return ((InternalEList<?>)getExpressions()).basicRemove(otherEnd, msgs);
      case AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION:
        return basicSetHavingExpression(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AqlPackage.ORDER_BY_CLAUSE__CLAUSE:
        return getClause();
      case AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS:
        return getExpressions();
      case AqlPackage.ORDER_BY_CLAUSE__OPTION:
        return getOption();
      case AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION:
        return getHavingExpression();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AqlPackage.ORDER_BY_CLAUSE__CLAUSE:
        setClause((String)newValue);
        return;
      case AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS:
        getExpressions().clear();
        getExpressions().addAll((Collection<? extends QualifiedName>)newValue);
        return;
      case AqlPackage.ORDER_BY_CLAUSE__OPTION:
        setOption((String)newValue);
        return;
      case AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION:
        setHavingExpression((Expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.ORDER_BY_CLAUSE__CLAUSE:
        setClause(CLAUSE_EDEFAULT);
        return;
      case AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS:
        getExpressions().clear();
        return;
      case AqlPackage.ORDER_BY_CLAUSE__OPTION:
        setOption(OPTION_EDEFAULT);
        return;
      case AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION:
        setHavingExpression((Expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.ORDER_BY_CLAUSE__CLAUSE:
        return CLAUSE_EDEFAULT == null ? clause != null : !CLAUSE_EDEFAULT.equals(clause);
      case AqlPackage.ORDER_BY_CLAUSE__EXPRESSIONS:
        return expressions != null && !expressions.isEmpty();
      case AqlPackage.ORDER_BY_CLAUSE__OPTION:
        return OPTION_EDEFAULT == null ? option != null : !OPTION_EDEFAULT.equals(option);
      case AqlPackage.ORDER_BY_CLAUSE__HAVING_EXPRESSION:
        return havingExpression != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (clause: ");
    result.append(clause);
    result.append(", option: ");
    result.append(option);
    result.append(')');
    return result.toString();
  }

} //OrderByClauseImpl
