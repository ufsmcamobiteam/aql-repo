/**
 */
package br.ufsm.aql.impl;

import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.FindClause;
import br.ufsm.aql.GroupByClause;
import br.ufsm.aql.OrderByClause;
import br.ufsm.aql.QueryStatement;
import br.ufsm.aql.ReturnsClause;
import br.ufsm.aql.WhereClause;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link br.ufsm.aql.impl.QueryStatementImpl#getFind <em>Find</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.QueryStatementImpl#getWhere <em>Where</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.QueryStatementImpl#getReturn <em>Return</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.QueryStatementImpl#getOrderby <em>Orderby</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.QueryStatementImpl#getGroupby <em>Groupby</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class QueryStatementImpl extends MinimalEObjectImpl.Container implements QueryStatement
{
  /**
   * The cached value of the '{@link #getFind() <em>Find</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFind()
   * @generated
   * @ordered
   */
  protected FindClause find;

  /**
   * The cached value of the '{@link #getWhere() <em>Where</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWhere()
   * @generated
   * @ordered
   */
  protected WhereClause where;

  /**
   * The cached value of the '{@link #getReturn() <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn()
   * @generated
   * @ordered
   */
  protected ReturnsClause return_;

  /**
   * The cached value of the '{@link #getOrderby() <em>Orderby</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrderby()
   * @generated
   * @ordered
   */
  protected OrderByClause orderby;

  /**
   * The cached value of the '{@link #getGroupby() <em>Groupby</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroupby()
   * @generated
   * @ordered
   */
  protected GroupByClause groupby;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected QueryStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AqlPackage.Literals.QUERY_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FindClause getFind()
  {
    return find;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFind(FindClause newFind, NotificationChain msgs)
  {
    FindClause oldFind = find;
    find = newFind;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__FIND, oldFind, newFind);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFind(FindClause newFind)
  {
    if (newFind != find)
    {
      NotificationChain msgs = null;
      if (find != null)
        msgs = ((InternalEObject)find).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__FIND, null, msgs);
      if (newFind != null)
        msgs = ((InternalEObject)newFind).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__FIND, null, msgs);
      msgs = basicSetFind(newFind, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__FIND, newFind, newFind));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhereClause getWhere()
  {
    return where;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWhere(WhereClause newWhere, NotificationChain msgs)
  {
    WhereClause oldWhere = where;
    where = newWhere;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__WHERE, oldWhere, newWhere);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWhere(WhereClause newWhere)
  {
    if (newWhere != where)
    {
      NotificationChain msgs = null;
      if (where != null)
        msgs = ((InternalEObject)where).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__WHERE, null, msgs);
      if (newWhere != null)
        msgs = ((InternalEObject)newWhere).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__WHERE, null, msgs);
      msgs = basicSetWhere(newWhere, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__WHERE, newWhere, newWhere));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnsClause getReturn()
  {
    return return_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReturn(ReturnsClause newReturn, NotificationChain msgs)
  {
    ReturnsClause oldReturn = return_;
    return_ = newReturn;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__RETURN, oldReturn, newReturn);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturn(ReturnsClause newReturn)
  {
    if (newReturn != return_)
    {
      NotificationChain msgs = null;
      if (return_ != null)
        msgs = ((InternalEObject)return_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__RETURN, null, msgs);
      if (newReturn != null)
        msgs = ((InternalEObject)newReturn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__RETURN, null, msgs);
      msgs = basicSetReturn(newReturn, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__RETURN, newReturn, newReturn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrderByClause getOrderby()
  {
    return orderby;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOrderby(OrderByClause newOrderby, NotificationChain msgs)
  {
    OrderByClause oldOrderby = orderby;
    orderby = newOrderby;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__ORDERBY, oldOrderby, newOrderby);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOrderby(OrderByClause newOrderby)
  {
    if (newOrderby != orderby)
    {
      NotificationChain msgs = null;
      if (orderby != null)
        msgs = ((InternalEObject)orderby).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__ORDERBY, null, msgs);
      if (newOrderby != null)
        msgs = ((InternalEObject)newOrderby).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__ORDERBY, null, msgs);
      msgs = basicSetOrderby(newOrderby, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__ORDERBY, newOrderby, newOrderby));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupByClause getGroupby()
  {
    return groupby;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroupby(GroupByClause newGroupby, NotificationChain msgs)
  {
    GroupByClause oldGroupby = groupby;
    groupby = newGroupby;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__GROUPBY, oldGroupby, newGroupby);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroupby(GroupByClause newGroupby)
  {
    if (newGroupby != groupby)
    {
      NotificationChain msgs = null;
      if (groupby != null)
        msgs = ((InternalEObject)groupby).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__GROUPBY, null, msgs);
      if (newGroupby != null)
        msgs = ((InternalEObject)newGroupby).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AqlPackage.QUERY_STATEMENT__GROUPBY, null, msgs);
      msgs = basicSetGroupby(newGroupby, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUERY_STATEMENT__GROUPBY, newGroupby, newGroupby));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AqlPackage.QUERY_STATEMENT__FIND:
        return basicSetFind(null, msgs);
      case AqlPackage.QUERY_STATEMENT__WHERE:
        return basicSetWhere(null, msgs);
      case AqlPackage.QUERY_STATEMENT__RETURN:
        return basicSetReturn(null, msgs);
      case AqlPackage.QUERY_STATEMENT__ORDERBY:
        return basicSetOrderby(null, msgs);
      case AqlPackage.QUERY_STATEMENT__GROUPBY:
        return basicSetGroupby(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AqlPackage.QUERY_STATEMENT__FIND:
        return getFind();
      case AqlPackage.QUERY_STATEMENT__WHERE:
        return getWhere();
      case AqlPackage.QUERY_STATEMENT__RETURN:
        return getReturn();
      case AqlPackage.QUERY_STATEMENT__ORDERBY:
        return getOrderby();
      case AqlPackage.QUERY_STATEMENT__GROUPBY:
        return getGroupby();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AqlPackage.QUERY_STATEMENT__FIND:
        setFind((FindClause)newValue);
        return;
      case AqlPackage.QUERY_STATEMENT__WHERE:
        setWhere((WhereClause)newValue);
        return;
      case AqlPackage.QUERY_STATEMENT__RETURN:
        setReturn((ReturnsClause)newValue);
        return;
      case AqlPackage.QUERY_STATEMENT__ORDERBY:
        setOrderby((OrderByClause)newValue);
        return;
      case AqlPackage.QUERY_STATEMENT__GROUPBY:
        setGroupby((GroupByClause)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.QUERY_STATEMENT__FIND:
        setFind((FindClause)null);
        return;
      case AqlPackage.QUERY_STATEMENT__WHERE:
        setWhere((WhereClause)null);
        return;
      case AqlPackage.QUERY_STATEMENT__RETURN:
        setReturn((ReturnsClause)null);
        return;
      case AqlPackage.QUERY_STATEMENT__ORDERBY:
        setOrderby((OrderByClause)null);
        return;
      case AqlPackage.QUERY_STATEMENT__GROUPBY:
        setGroupby((GroupByClause)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.QUERY_STATEMENT__FIND:
        return find != null;
      case AqlPackage.QUERY_STATEMENT__WHERE:
        return where != null;
      case AqlPackage.QUERY_STATEMENT__RETURN:
        return return_ != null;
      case AqlPackage.QUERY_STATEMENT__ORDERBY:
        return orderby != null;
      case AqlPackage.QUERY_STATEMENT__GROUPBY:
        return groupby != null;
    }
    return super.eIsSet(featureID);
  }

} //QueryStatementImpl
