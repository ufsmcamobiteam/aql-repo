/**
 */
package br.ufsm.aql.impl;

import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.QualifiedElement;
import br.ufsm.aql.QualifiedName;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Qualified Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link br.ufsm.aql.impl.QualifiedNameImpl#getQualifiers <em>Qualifiers</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.QualifiedNameImpl#getGranular <em>Granular</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class QualifiedNameImpl extends ExpressionImpl implements QualifiedName
{
  /**
   * The cached value of the '{@link #getQualifiers() <em>Qualifiers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQualifiers()
   * @generated
   * @ordered
   */
  protected EList<QualifiedElement> qualifiers;

  /**
   * The default value of the '{@link #getGranular() <em>Granular</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGranular()
   * @generated
   * @ordered
   */
  protected static final String GRANULAR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getGranular() <em>Granular</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGranular()
   * @generated
   * @ordered
   */
  protected String granular = GRANULAR_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected QualifiedNameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AqlPackage.Literals.QUALIFIED_NAME;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<QualifiedElement> getQualifiers()
  {
    if (qualifiers == null)
    {
      qualifiers = new EObjectContainmentEList<QualifiedElement>(QualifiedElement.class, this, AqlPackage.QUALIFIED_NAME__QUALIFIERS);
    }
    return qualifiers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getGranular()
  {
    return granular;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGranular(String newGranular)
  {
    String oldGranular = granular;
    granular = newGranular;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.QUALIFIED_NAME__GRANULAR, oldGranular, granular));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AqlPackage.QUALIFIED_NAME__QUALIFIERS:
        return ((InternalEList<?>)getQualifiers()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AqlPackage.QUALIFIED_NAME__QUALIFIERS:
        return getQualifiers();
      case AqlPackage.QUALIFIED_NAME__GRANULAR:
        return getGranular();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AqlPackage.QUALIFIED_NAME__QUALIFIERS:
        getQualifiers().clear();
        getQualifiers().addAll((Collection<? extends QualifiedElement>)newValue);
        return;
      case AqlPackage.QUALIFIED_NAME__GRANULAR:
        setGranular((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.QUALIFIED_NAME__QUALIFIERS:
        getQualifiers().clear();
        return;
      case AqlPackage.QUALIFIED_NAME__GRANULAR:
        setGranular(GRANULAR_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.QUALIFIED_NAME__QUALIFIERS:
        return qualifiers != null && !qualifiers.isEmpty();
      case AqlPackage.QUALIFIED_NAME__GRANULAR:
        return GRANULAR_EDEFAULT == null ? granular != null : !GRANULAR_EDEFAULT.equals(granular);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (granular: ");
    result.append(granular);
    result.append(')');
    return result.toString();
  }

} //QualifiedNameImpl
