/**
 */
package br.ufsm.aql.impl;

import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.Expression;
import br.ufsm.aql.ReturnsClause;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Returns Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link br.ufsm.aql.impl.ReturnsClauseImpl#getClause <em>Clause</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.ReturnsClauseImpl#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link br.ufsm.aql.impl.ReturnsClauseImpl#getResultAlias <em>Result Alias</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReturnsClauseImpl extends MinimalEObjectImpl.Container implements ReturnsClause
{
  /**
   * The default value of the '{@link #getClause() <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClause()
   * @generated
   * @ordered
   */
  protected static final String CLAUSE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getClause() <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClause()
   * @generated
   * @ordered
   */
  protected String clause = CLAUSE_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpressions()
   * @generated
   * @ordered
   */
  protected EList<Expression> expressions;

  /**
   * The cached value of the '{@link #getResultAlias() <em>Result Alias</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultAlias()
   * @generated
   * @ordered
   */
  protected EList<String> resultAlias;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ReturnsClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AqlPackage.Literals.RETURNS_CLAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getClause()
  {
    return clause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClause(String newClause)
  {
    String oldClause = clause;
    clause = newClause;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AqlPackage.RETURNS_CLAUSE__CLAUSE, oldClause, clause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getExpressions()
  {
    if (expressions == null)
    {
      expressions = new EObjectContainmentEList<Expression>(Expression.class, this, AqlPackage.RETURNS_CLAUSE__EXPRESSIONS);
    }
    return expressions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getResultAlias()
  {
    if (resultAlias == null)
    {
      resultAlias = new EDataTypeEList<String>(String.class, this, AqlPackage.RETURNS_CLAUSE__RESULT_ALIAS);
    }
    return resultAlias;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AqlPackage.RETURNS_CLAUSE__EXPRESSIONS:
        return ((InternalEList<?>)getExpressions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AqlPackage.RETURNS_CLAUSE__CLAUSE:
        return getClause();
      case AqlPackage.RETURNS_CLAUSE__EXPRESSIONS:
        return getExpressions();
      case AqlPackage.RETURNS_CLAUSE__RESULT_ALIAS:
        return getResultAlias();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AqlPackage.RETURNS_CLAUSE__CLAUSE:
        setClause((String)newValue);
        return;
      case AqlPackage.RETURNS_CLAUSE__EXPRESSIONS:
        getExpressions().clear();
        getExpressions().addAll((Collection<? extends Expression>)newValue);
        return;
      case AqlPackage.RETURNS_CLAUSE__RESULT_ALIAS:
        getResultAlias().clear();
        getResultAlias().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.RETURNS_CLAUSE__CLAUSE:
        setClause(CLAUSE_EDEFAULT);
        return;
      case AqlPackage.RETURNS_CLAUSE__EXPRESSIONS:
        getExpressions().clear();
        return;
      case AqlPackage.RETURNS_CLAUSE__RESULT_ALIAS:
        getResultAlias().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AqlPackage.RETURNS_CLAUSE__CLAUSE:
        return CLAUSE_EDEFAULT == null ? clause != null : !CLAUSE_EDEFAULT.equals(clause);
      case AqlPackage.RETURNS_CLAUSE__EXPRESSIONS:
        return expressions != null && !expressions.isEmpty();
      case AqlPackage.RETURNS_CLAUSE__RESULT_ALIAS:
        return resultAlias != null && !resultAlias.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (clause: ");
    result.append(clause);
    result.append(", resultAlias: ");
    result.append(resultAlias);
    result.append(')');
    return result.toString();
  }

} //ReturnsClauseImpl
