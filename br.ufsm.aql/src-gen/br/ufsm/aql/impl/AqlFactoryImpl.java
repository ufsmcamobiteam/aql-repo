/**
 */
package br.ufsm.aql.impl;

import br.ufsm.aql.Add;
import br.ufsm.aql.And;
import br.ufsm.aql.AqlFactory;
import br.ufsm.aql.AqlPackage;
import br.ufsm.aql.Aspect;
import br.ufsm.aql.BindingObject;
import br.ufsm.aql.Div;
import br.ufsm.aql.Equals;
import br.ufsm.aql.Exponential;
import br.ufsm.aql.Expression;
import br.ufsm.aql.False;
import br.ufsm.aql.FindClause;
import br.ufsm.aql.FloatLiteral;
import br.ufsm.aql.Function;
import br.ufsm.aql.Greater;
import br.ufsm.aql.GreaterEqual;
import br.ufsm.aql.GroupByClause;
import br.ufsm.aql.Identifier;
import br.ufsm.aql.In;
import br.ufsm.aql.IntegerLiteral;
import br.ufsm.aql.Interface;
import br.ufsm.aql.Less;
import br.ufsm.aql.LessEqual;
import br.ufsm.aql.Like;
import br.ufsm.aql.Literal;
import br.ufsm.aql.Minus;
import br.ufsm.aql.Mod;
import br.ufsm.aql.Mult;
import br.ufsm.aql.Not;
import br.ufsm.aql.NotEquals;
import br.ufsm.aql.Null;
import br.ufsm.aql.ObjectType;
import br.ufsm.aql.Or;
import br.ufsm.aql.OrderByClause;
import br.ufsm.aql.ParsExpression;
import br.ufsm.aql.Project;
import br.ufsm.aql.QualifiedElement;
import br.ufsm.aql.QualifiedName;
import br.ufsm.aql.Query;
import br.ufsm.aql.QueryStatement;
import br.ufsm.aql.ReturnsClause;
import br.ufsm.aql.StringLiteral;
import br.ufsm.aql.True;
import br.ufsm.aql.UnaryMinus;
import br.ufsm.aql.WhereClause;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AqlFactoryImpl extends EFactoryImpl implements AqlFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static AqlFactory init()
  {
    try
    {
      AqlFactory theAqlFactory = (AqlFactory)EPackage.Registry.INSTANCE.getEFactory(AqlPackage.eNS_URI);
      if (theAqlFactory != null)
      {
        return theAqlFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new AqlFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AqlFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case AqlPackage.QUERY: return createQuery();
      case AqlPackage.QUERY_STATEMENT: return createQueryStatement();
      case AqlPackage.FIND_CLAUSE: return createFindClause();
      case AqlPackage.BINDING_OBJECT: return createBindingObject();
      case AqlPackage.OBJECT_TYPE: return createObjectType();
      case AqlPackage.WHERE_CLAUSE: return createWhereClause();
      case AqlPackage.RETURNS_CLAUSE: return createReturnsClause();
      case AqlPackage.GROUP_BY_CLAUSE: return createGroupByClause();
      case AqlPackage.ORDER_BY_CLAUSE: return createOrderByClause();
      case AqlPackage.QUALIFIED_NAME: return createQualifiedName();
      case AqlPackage.QUALIFIED_ELEMENT: return createQualifiedElement();
      case AqlPackage.EXPRESSION: return createExpression();
      case AqlPackage.LITERAL: return createLiteral();
      case AqlPackage.FUNCTION: return createFunction();
      case AqlPackage.IDENTIFIER: return createIdentifier();
      case AqlPackage.PROJECT: return createProject();
      case AqlPackage.PACKAGE: return createPackage();
      case AqlPackage.CLASS: return createClass();
      case AqlPackage.ASPECT: return createAspect();
      case AqlPackage.INTERFACE: return createInterface();
      case AqlPackage.ENUM: return createEnum();
      case AqlPackage.OR: return createOr();
      case AqlPackage.AND: return createAnd();
      case AqlPackage.EQUALS: return createEquals();
      case AqlPackage.GREATER: return createGreater();
      case AqlPackage.GREATER_EQUAL: return createGreaterEqual();
      case AqlPackage.LESS: return createLess();
      case AqlPackage.LESS_EQUAL: return createLessEqual();
      case AqlPackage.LIKE: return createLike();
      case AqlPackage.NOT_EQUALS: return createNotEquals();
      case AqlPackage.ADD: return createAdd();
      case AqlPackage.MINUS: return createMinus();
      case AqlPackage.MULT: return createMult();
      case AqlPackage.DIV: return createDiv();
      case AqlPackage.MOD: return createMod();
      case AqlPackage.IN: return createIn();
      case AqlPackage.NOT: return createNot();
      case AqlPackage.UNARY_MINUS: return createUnaryMinus();
      case AqlPackage.EXPONENTIAL: return createExponential();
      case AqlPackage.PARS_EXPRESSION: return createParsExpression();
      case AqlPackage.STRING_LITERAL: return createStringLiteral();
      case AqlPackage.FLOAT_LITERAL: return createFloatLiteral();
      case AqlPackage.INTEGER_LITERAL: return createIntegerLiteral();
      case AqlPackage.NULL: return createNull();
      case AqlPackage.TRUE: return createTrue();
      case AqlPackage.FALSE: return createFalse();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Query createQuery()
  {
    QueryImpl query = new QueryImpl();
    return query;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QueryStatement createQueryStatement()
  {
    QueryStatementImpl queryStatement = new QueryStatementImpl();
    return queryStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FindClause createFindClause()
  {
    FindClauseImpl findClause = new FindClauseImpl();
    return findClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BindingObject createBindingObject()
  {
    BindingObjectImpl bindingObject = new BindingObjectImpl();
    return bindingObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObjectType createObjectType()
  {
    ObjectTypeImpl objectType = new ObjectTypeImpl();
    return objectType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhereClause createWhereClause()
  {
    WhereClauseImpl whereClause = new WhereClauseImpl();
    return whereClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnsClause createReturnsClause()
  {
    ReturnsClauseImpl returnsClause = new ReturnsClauseImpl();
    return returnsClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupByClause createGroupByClause()
  {
    GroupByClauseImpl groupByClause = new GroupByClauseImpl();
    return groupByClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrderByClause createOrderByClause()
  {
    OrderByClauseImpl orderByClause = new OrderByClauseImpl();
    return orderByClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QualifiedName createQualifiedName()
  {
    QualifiedNameImpl qualifiedName = new QualifiedNameImpl();
    return qualifiedName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QualifiedElement createQualifiedElement()
  {
    QualifiedElementImpl qualifiedElement = new QualifiedElementImpl();
    return qualifiedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Literal createLiteral()
  {
    LiteralImpl literal = new LiteralImpl();
    return literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Function createFunction()
  {
    FunctionImpl function = new FunctionImpl();
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Identifier createIdentifier()
  {
    IdentifierImpl identifier = new IdentifierImpl();
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Project createProject()
  {
    ProjectImpl project = new ProjectImpl();
    return project;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public br.ufsm.aql.Package createPackage()
  {
    PackageImpl package_ = new PackageImpl();
    return package_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public br.ufsm.aql.Class createClass()
  {
    ClassImpl class_ = new ClassImpl();
    return class_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Aspect createAspect()
  {
    AspectImpl aspect = new AspectImpl();
    return aspect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Interface createInterface()
  {
    InterfaceImpl interface_ = new InterfaceImpl();
    return interface_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public br.ufsm.aql.Enum createEnum()
  {
    EnumImpl enum_ = new EnumImpl();
    return enum_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Or createOr()
  {
    OrImpl or = new OrImpl();
    return or;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public And createAnd()
  {
    AndImpl and = new AndImpl();
    return and;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Equals createEquals()
  {
    EqualsImpl equals = new EqualsImpl();
    return equals;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Greater createGreater()
  {
    GreaterImpl greater = new GreaterImpl();
    return greater;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GreaterEqual createGreaterEqual()
  {
    GreaterEqualImpl greaterEqual = new GreaterEqualImpl();
    return greaterEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Less createLess()
  {
    LessImpl less = new LessImpl();
    return less;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LessEqual createLessEqual()
  {
    LessEqualImpl lessEqual = new LessEqualImpl();
    return lessEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Like createLike()
  {
    LikeImpl like = new LikeImpl();
    return like;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotEquals createNotEquals()
  {
    NotEqualsImpl notEquals = new NotEqualsImpl();
    return notEquals;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Add createAdd()
  {
    AddImpl add = new AddImpl();
    return add;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Minus createMinus()
  {
    MinusImpl minus = new MinusImpl();
    return minus;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Mult createMult()
  {
    MultImpl mult = new MultImpl();
    return mult;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Div createDiv()
  {
    DivImpl div = new DivImpl();
    return div;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Mod createMod()
  {
    ModImpl mod = new ModImpl();
    return mod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public In createIn()
  {
    InImpl in = new InImpl();
    return in;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Not createNot()
  {
    NotImpl not = new NotImpl();
    return not;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryMinus createUnaryMinus()
  {
    UnaryMinusImpl unaryMinus = new UnaryMinusImpl();
    return unaryMinus;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Exponential createExponential()
  {
    ExponentialImpl exponential = new ExponentialImpl();
    return exponential;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParsExpression createParsExpression()
  {
    ParsExpressionImpl parsExpression = new ParsExpressionImpl();
    return parsExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringLiteral createStringLiteral()
  {
    StringLiteralImpl stringLiteral = new StringLiteralImpl();
    return stringLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FloatLiteral createFloatLiteral()
  {
    FloatLiteralImpl floatLiteral = new FloatLiteralImpl();
    return floatLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerLiteral createIntegerLiteral()
  {
    IntegerLiteralImpl integerLiteral = new IntegerLiteralImpl();
    return integerLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Null createNull()
  {
    NullImpl null_ = new NullImpl();
    return null_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public True createTrue()
  {
    TrueImpl true_ = new TrueImpl();
    return true_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public False createFalse()
  {
    FalseImpl false_ = new FalseImpl();
    return false_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AqlPackage getAqlPackage()
  {
    return (AqlPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static AqlPackage getPackage()
  {
    return AqlPackage.eINSTANCE;
  }

} //AqlFactoryImpl
