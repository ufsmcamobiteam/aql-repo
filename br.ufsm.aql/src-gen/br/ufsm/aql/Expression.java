/**
 */
package br.ufsm.aql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
} // Expression
