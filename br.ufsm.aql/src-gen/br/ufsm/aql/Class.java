/**
 */
package br.ufsm.aql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends ObjectType
{
} // Class
