/**
 */
package br.ufsm.aql;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see br.ufsm.aql.AqlFactory
 * @model kind="package"
 * @generated
 */
public interface AqlPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "aql";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.ufsm.br/AQL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "aql";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  AqlPackage eINSTANCE = br.ufsm.aql.impl.AqlPackageImpl.init();

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.QueryImpl <em>Query</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.QueryImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getQuery()
   * @generated
   */
  int QUERY = 0;

  /**
   * The feature id for the '<em><b>Query</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY__QUERY = 0;

  /**
   * The number of structural features of the '<em>Query</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.QueryStatementImpl <em>Query Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.QueryStatementImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getQueryStatement()
   * @generated
   */
  int QUERY_STATEMENT = 1;

  /**
   * The feature id for the '<em><b>Find</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT__FIND = 0;

  /**
   * The feature id for the '<em><b>Where</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT__WHERE = 1;

  /**
   * The feature id for the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT__RETURN = 2;

  /**
   * The feature id for the '<em><b>Orderby</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT__ORDERBY = 3;

  /**
   * The feature id for the '<em><b>Groupby</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT__GROUPBY = 4;

  /**
   * The number of structural features of the '<em>Query Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUERY_STATEMENT_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.FindClauseImpl <em>Find Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.FindClauseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getFindClause()
   * @generated
   */
  int FIND_CLAUSE = 2;

  /**
   * The feature id for the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIND_CLAUSE__CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Binding Object</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIND_CLAUSE__BINDING_OBJECT = 1;

  /**
   * The number of structural features of the '<em>Find Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIND_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.BindingObjectImpl <em>Binding Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.BindingObjectImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getBindingObject()
   * @generated
   */
  int BINDING_OBJECT = 3;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINDING_OBJECT__TYPE = 0;

  /**
   * The feature id for the '<em><b>Alias</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINDING_OBJECT__ALIAS = 1;

  /**
   * The number of structural features of the '<em>Binding Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BINDING_OBJECT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ObjectTypeImpl <em>Object Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ObjectTypeImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getObjectType()
   * @generated
   */
  int OBJECT_TYPE = 4;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_TYPE__VALUE = 0;

  /**
   * The number of structural features of the '<em>Object Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.WhereClauseImpl <em>Where Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.WhereClauseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getWhereClause()
   * @generated
   */
  int WHERE_CLAUSE = 5;

  /**
   * The feature id for the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE__CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE__EXPRESSION = 1;

  /**
   * The number of structural features of the '<em>Where Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHERE_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ReturnsClauseImpl <em>Returns Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ReturnsClauseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getReturnsClause()
   * @generated
   */
  int RETURNS_CLAUSE = 6;

  /**
   * The feature id for the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURNS_CLAUSE__CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURNS_CLAUSE__EXPRESSIONS = 1;

  /**
   * The feature id for the '<em><b>Result Alias</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURNS_CLAUSE__RESULT_ALIAS = 2;

  /**
   * The number of structural features of the '<em>Returns Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETURNS_CLAUSE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.GroupByClauseImpl <em>Group By Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.GroupByClauseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getGroupByClause()
   * @generated
   */
  int GROUP_BY_CLAUSE = 7;

  /**
   * The feature id for the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_BY_CLAUSE__CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_BY_CLAUSE__EXPRESSIONS = 1;

  /**
   * The number of structural features of the '<em>Group By Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GROUP_BY_CLAUSE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.OrderByClauseImpl <em>Order By Clause</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.OrderByClauseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getOrderByClause()
   * @generated
   */
  int ORDER_BY_CLAUSE = 8;

  /**
   * The feature id for the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_BY_CLAUSE__CLAUSE = 0;

  /**
   * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_BY_CLAUSE__EXPRESSIONS = 1;

  /**
   * The feature id for the '<em><b>Option</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_BY_CLAUSE__OPTION = 2;

  /**
   * The feature id for the '<em><b>Having Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_BY_CLAUSE__HAVING_EXPRESSION = 3;

  /**
   * The number of structural features of the '<em>Order By Clause</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDER_BY_CLAUSE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ExpressionImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 11;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.QualifiedNameImpl <em>Qualified Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.QualifiedNameImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getQualifiedName()
   * @generated
   */
  int QUALIFIED_NAME = 9;

  /**
   * The feature id for the '<em><b>Qualifiers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_NAME__QUALIFIERS = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Granular</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_NAME__GRANULAR = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Qualified Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_NAME_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.QualifiedElementImpl <em>Qualified Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.QualifiedElementImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getQualifiedElement()
   * @generated
   */
  int QUALIFIED_ELEMENT = 10;

  /**
   * The number of structural features of the '<em>Qualified Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUALIFIED_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.LiteralImpl <em>Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.LiteralImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getLiteral()
   * @generated
   */
  int LITERAL = 12;

  /**
   * The number of structural features of the '<em>Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.FunctionImpl <em>Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.FunctionImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getFunction()
   * @generated
   */
  int FUNCTION = 13;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__NAME = QUALIFIED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__PARAMS = QUALIFIED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FEATURE_COUNT = QUALIFIED_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.IdentifierImpl <em>Identifier</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.IdentifierImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getIdentifier()
   * @generated
   */
  int IDENTIFIER = 14;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER__ID = QUALIFIED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Identifier</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_FEATURE_COUNT = QUALIFIED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ProjectImpl <em>Project</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ProjectImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getProject()
   * @generated
   */
  int PROJECT = 15;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Project</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.PackageImpl <em>Package</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.PackageImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getPackage()
   * @generated
   */
  int PACKAGE = 16;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Package</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ClassImpl <em>Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ClassImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getClass_()
   * @generated
   */
  int CLASS = 17;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.AspectImpl <em>Aspect</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.AspectImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getAspect()
   * @generated
   */
  int ASPECT = 18;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASPECT__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Aspect</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASPECT_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.InterfaceImpl <em>Interface</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.InterfaceImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getInterface()
   * @generated
   */
  int INTERFACE = 19;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Interface</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERFACE_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.EnumImpl <em>Enum</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.EnumImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getEnum()
   * @generated
   */
  int ENUM = 20;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM__VALUE = OBJECT_TYPE__VALUE;

  /**
   * The number of structural features of the '<em>Enum</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENUM_FEATURE_COUNT = OBJECT_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.OrImpl <em>Or</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.OrImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getOr()
   * @generated
   */
  int OR = 21;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Or</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.AndImpl <em>And</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.AndImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getAnd()
   * @generated
   */
  int AND = 22;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>And</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.EqualsImpl <em>Equals</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.EqualsImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getEquals()
   * @generated
   */
  int EQUALS = 23;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALS__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALS__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Equals</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EQUALS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.GreaterImpl <em>Greater</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.GreaterImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getGreater()
   * @generated
   */
  int GREATER = 24;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Greater</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.GreaterEqualImpl <em>Greater Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.GreaterEqualImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getGreaterEqual()
   * @generated
   */
  int GREATER_EQUAL = 25;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Greater Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GREATER_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.LessImpl <em>Less</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.LessImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getLess()
   * @generated
   */
  int LESS = 26;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Less</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.LessEqualImpl <em>Less Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.LessEqualImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getLessEqual()
   * @generated
   */
  int LESS_EQUAL = 27;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Less Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LESS_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.LikeImpl <em>Like</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.LikeImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getLike()
   * @generated
   */
  int LIKE = 28;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIKE__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIKE__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Like</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIKE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.NotEqualsImpl <em>Not Equals</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.NotEqualsImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getNotEquals()
   * @generated
   */
  int NOT_EQUALS = 29;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EQUALS__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EQUALS__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Not Equals</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_EQUALS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.AddImpl <em>Add</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.AddImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getAdd()
   * @generated
   */
  int ADD = 30;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Add</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADD_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.MinusImpl <em>Minus</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.MinusImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getMinus()
   * @generated
   */
  int MINUS = 31;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINUS__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINUS__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Minus</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MINUS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.MultImpl <em>Mult</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.MultImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getMult()
   * @generated
   */
  int MULT = 32;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Mult</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.DivImpl <em>Div</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.DivImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getDiv()
   * @generated
   */
  int DIV = 33;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIV__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIV__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Div</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIV_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ModImpl <em>Mod</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ModImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getMod()
   * @generated
   */
  int MOD = 34;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Mod</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MOD_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.InImpl <em>In</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.InImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getIn()
   * @generated
   */
  int IN = 35;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>In</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IN_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.NotImpl <em>Not</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.NotImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getNot()
   * @generated
   */
  int NOT = 36;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT__EXP = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Not</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.UnaryMinusImpl <em>Unary Minus</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.UnaryMinusImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getUnaryMinus()
   * @generated
   */
  int UNARY_MINUS = 37;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS__EXPR = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Minus</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ExponentialImpl <em>Exponential</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ExponentialImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getExponential()
   * @generated
   */
  int EXPONENTIAL = 38;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENTIAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENTIAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Exponential</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPONENTIAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.ParsExpressionImpl <em>Pars Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.ParsExpressionImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getParsExpression()
   * @generated
   */
  int PARS_EXPRESSION = 39;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARS_EXPRESSION__EXPR = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Pars Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.StringLiteralImpl <em>String Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.StringLiteralImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getStringLiteral()
   * @generated
   */
  int STRING_LITERAL = 40;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_LITERAL__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>String Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_LITERAL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.FloatLiteralImpl <em>Float Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.FloatLiteralImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getFloatLiteral()
   * @generated
   */
  int FLOAT_LITERAL = 41;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLOAT_LITERAL__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Float Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FLOAT_LITERAL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.IntegerLiteralImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getIntegerLiteral()
   * @generated
   */
  int INTEGER_LITERAL = 42;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_LITERAL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.NullImpl <em>Null</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.NullImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getNull()
   * @generated
   */
  int NULL = 43;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NULL__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Null</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NULL_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.TrueImpl <em>True</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.TrueImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getTrue()
   * @generated
   */
  int TRUE = 44;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>True</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link br.ufsm.aql.impl.FalseImpl <em>False</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.ufsm.aql.impl.FalseImpl
   * @see br.ufsm.aql.impl.AqlPackageImpl#getFalse()
   * @generated
   */
  int FALSE = 45;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE__VALUE = LITERAL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>False</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_FEATURE_COUNT = LITERAL_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Query <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Query</em>'.
   * @see br.ufsm.aql.Query
   * @generated
   */
  EClass getQuery();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Query#getQuery <em>Query</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Query</em>'.
   * @see br.ufsm.aql.Query#getQuery()
   * @see #getQuery()
   * @generated
   */
  EReference getQuery_Query();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.QueryStatement <em>Query Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Query Statement</em>'.
   * @see br.ufsm.aql.QueryStatement
   * @generated
   */
  EClass getQueryStatement();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.QueryStatement#getFind <em>Find</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Find</em>'.
   * @see br.ufsm.aql.QueryStatement#getFind()
   * @see #getQueryStatement()
   * @generated
   */
  EReference getQueryStatement_Find();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.QueryStatement#getWhere <em>Where</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Where</em>'.
   * @see br.ufsm.aql.QueryStatement#getWhere()
   * @see #getQueryStatement()
   * @generated
   */
  EReference getQueryStatement_Where();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.QueryStatement#getReturn <em>Return</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return</em>'.
   * @see br.ufsm.aql.QueryStatement#getReturn()
   * @see #getQueryStatement()
   * @generated
   */
  EReference getQueryStatement_Return();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.QueryStatement#getOrderby <em>Orderby</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Orderby</em>'.
   * @see br.ufsm.aql.QueryStatement#getOrderby()
   * @see #getQueryStatement()
   * @generated
   */
  EReference getQueryStatement_Orderby();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.QueryStatement#getGroupby <em>Groupby</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Groupby</em>'.
   * @see br.ufsm.aql.QueryStatement#getGroupby()
   * @see #getQueryStatement()
   * @generated
   */
  EReference getQueryStatement_Groupby();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.FindClause <em>Find Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Find Clause</em>'.
   * @see br.ufsm.aql.FindClause
   * @generated
   */
  EClass getFindClause();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.FindClause#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clause</em>'.
   * @see br.ufsm.aql.FindClause#getClause()
   * @see #getFindClause()
   * @generated
   */
  EAttribute getFindClause_Clause();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.FindClause#getBindingObject <em>Binding Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Binding Object</em>'.
   * @see br.ufsm.aql.FindClause#getBindingObject()
   * @see #getFindClause()
   * @generated
   */
  EReference getFindClause_BindingObject();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.BindingObject <em>Binding Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Binding Object</em>'.
   * @see br.ufsm.aql.BindingObject
   * @generated
   */
  EClass getBindingObject();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.BindingObject#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see br.ufsm.aql.BindingObject#getType()
   * @see #getBindingObject()
   * @generated
   */
  EReference getBindingObject_Type();

  /**
   * Returns the meta object for the attribute list '{@link br.ufsm.aql.BindingObject#getAlias <em>Alias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Alias</em>'.
   * @see br.ufsm.aql.BindingObject#getAlias()
   * @see #getBindingObject()
   * @generated
   */
  EAttribute getBindingObject_Alias();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.ObjectType <em>Object Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Object Type</em>'.
   * @see br.ufsm.aql.ObjectType
   * @generated
   */
  EClass getObjectType();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.ObjectType#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.ObjectType#getValue()
   * @see #getObjectType()
   * @generated
   */
  EAttribute getObjectType_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.WhereClause <em>Where Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Where Clause</em>'.
   * @see br.ufsm.aql.WhereClause
   * @generated
   */
  EClass getWhereClause();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.WhereClause#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clause</em>'.
   * @see br.ufsm.aql.WhereClause#getClause()
   * @see #getWhereClause()
   * @generated
   */
  EAttribute getWhereClause_Clause();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.WhereClause#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see br.ufsm.aql.WhereClause#getExpression()
   * @see #getWhereClause()
   * @generated
   */
  EReference getWhereClause_Expression();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.ReturnsClause <em>Returns Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Returns Clause</em>'.
   * @see br.ufsm.aql.ReturnsClause
   * @generated
   */
  EClass getReturnsClause();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.ReturnsClause#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clause</em>'.
   * @see br.ufsm.aql.ReturnsClause#getClause()
   * @see #getReturnsClause()
   * @generated
   */
  EAttribute getReturnsClause_Clause();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.ReturnsClause#getExpressions <em>Expressions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expressions</em>'.
   * @see br.ufsm.aql.ReturnsClause#getExpressions()
   * @see #getReturnsClause()
   * @generated
   */
  EReference getReturnsClause_Expressions();

  /**
   * Returns the meta object for the attribute list '{@link br.ufsm.aql.ReturnsClause#getResultAlias <em>Result Alias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Result Alias</em>'.
   * @see br.ufsm.aql.ReturnsClause#getResultAlias()
   * @see #getReturnsClause()
   * @generated
   */
  EAttribute getReturnsClause_ResultAlias();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.GroupByClause <em>Group By Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Group By Clause</em>'.
   * @see br.ufsm.aql.GroupByClause
   * @generated
   */
  EClass getGroupByClause();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.GroupByClause#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clause</em>'.
   * @see br.ufsm.aql.GroupByClause#getClause()
   * @see #getGroupByClause()
   * @generated
   */
  EAttribute getGroupByClause_Clause();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.GroupByClause#getExpressions <em>Expressions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expressions</em>'.
   * @see br.ufsm.aql.GroupByClause#getExpressions()
   * @see #getGroupByClause()
   * @generated
   */
  EReference getGroupByClause_Expressions();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.OrderByClause <em>Order By Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Order By Clause</em>'.
   * @see br.ufsm.aql.OrderByClause
   * @generated
   */
  EClass getOrderByClause();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.OrderByClause#getClause <em>Clause</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Clause</em>'.
   * @see br.ufsm.aql.OrderByClause#getClause()
   * @see #getOrderByClause()
   * @generated
   */
  EAttribute getOrderByClause_Clause();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.OrderByClause#getExpressions <em>Expressions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expressions</em>'.
   * @see br.ufsm.aql.OrderByClause#getExpressions()
   * @see #getOrderByClause()
   * @generated
   */
  EReference getOrderByClause_Expressions();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.OrderByClause#getOption <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Option</em>'.
   * @see br.ufsm.aql.OrderByClause#getOption()
   * @see #getOrderByClause()
   * @generated
   */
  EAttribute getOrderByClause_Option();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.OrderByClause#getHavingExpression <em>Having Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Having Expression</em>'.
   * @see br.ufsm.aql.OrderByClause#getHavingExpression()
   * @see #getOrderByClause()
   * @generated
   */
  EReference getOrderByClause_HavingExpression();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.QualifiedName <em>Qualified Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Qualified Name</em>'.
   * @see br.ufsm.aql.QualifiedName
   * @generated
   */
  EClass getQualifiedName();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.QualifiedName#getQualifiers <em>Qualifiers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Qualifiers</em>'.
   * @see br.ufsm.aql.QualifiedName#getQualifiers()
   * @see #getQualifiedName()
   * @generated
   */
  EReference getQualifiedName_Qualifiers();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.QualifiedName#getGranular <em>Granular</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Granular</em>'.
   * @see br.ufsm.aql.QualifiedName#getGranular()
   * @see #getQualifiedName()
   * @generated
   */
  EAttribute getQualifiedName_Granular();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.QualifiedElement <em>Qualified Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Qualified Element</em>'.
   * @see br.ufsm.aql.QualifiedElement
   * @generated
   */
  EClass getQualifiedElement();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see br.ufsm.aql.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Literal <em>Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Literal</em>'.
   * @see br.ufsm.aql.Literal
   * @generated
   */
  EClass getLiteral();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function</em>'.
   * @see br.ufsm.aql.Function
   * @generated
   */
  EClass getFunction();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.Function#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see br.ufsm.aql.Function#getName()
   * @see #getFunction()
   * @generated
   */
  EAttribute getFunction_Name();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.Function#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see br.ufsm.aql.Function#getParams()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Params();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Identifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier</em>'.
   * @see br.ufsm.aql.Identifier
   * @generated
   */
  EClass getIdentifier();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.Identifier#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see br.ufsm.aql.Identifier#getId()
   * @see #getIdentifier()
   * @generated
   */
  EAttribute getIdentifier_Id();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Project <em>Project</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Project</em>'.
   * @see br.ufsm.aql.Project
   * @generated
   */
  EClass getProject();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Package <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Package</em>'.
   * @see br.ufsm.aql.Package
   * @generated
   */
  EClass getPackage();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Class <em>Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class</em>'.
   * @see br.ufsm.aql.Class
   * @generated
   */
  EClass getClass_();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Aspect <em>Aspect</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Aspect</em>'.
   * @see br.ufsm.aql.Aspect
   * @generated
   */
  EClass getAspect();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Interface <em>Interface</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interface</em>'.
   * @see br.ufsm.aql.Interface
   * @generated
   */
  EClass getInterface();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Enum <em>Enum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enum</em>'.
   * @see br.ufsm.aql.Enum
   * @generated
   */
  EClass getEnum();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Or <em>Or</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or</em>'.
   * @see br.ufsm.aql.Or
   * @generated
   */
  EClass getOr();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Or#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Or#getLeft()
   * @see #getOr()
   * @generated
   */
  EReference getOr_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Or#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Or#getRight()
   * @see #getOr()
   * @generated
   */
  EReference getOr_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.And <em>And</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And</em>'.
   * @see br.ufsm.aql.And
   * @generated
   */
  EClass getAnd();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.And#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.And#getLeft()
   * @see #getAnd()
   * @generated
   */
  EReference getAnd_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.And#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.And#getRight()
   * @see #getAnd()
   * @generated
   */
  EReference getAnd_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Equals <em>Equals</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Equals</em>'.
   * @see br.ufsm.aql.Equals
   * @generated
   */
  EClass getEquals();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Equals#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Equals#getLeft()
   * @see #getEquals()
   * @generated
   */
  EReference getEquals_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Equals#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Equals#getRight()
   * @see #getEquals()
   * @generated
   */
  EReference getEquals_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Greater <em>Greater</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Greater</em>'.
   * @see br.ufsm.aql.Greater
   * @generated
   */
  EClass getGreater();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Greater#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Greater#getLeft()
   * @see #getGreater()
   * @generated
   */
  EReference getGreater_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Greater#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Greater#getRight()
   * @see #getGreater()
   * @generated
   */
  EReference getGreater_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.GreaterEqual <em>Greater Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Greater Equal</em>'.
   * @see br.ufsm.aql.GreaterEqual
   * @generated
   */
  EClass getGreaterEqual();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.GreaterEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.GreaterEqual#getLeft()
   * @see #getGreaterEqual()
   * @generated
   */
  EReference getGreaterEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.GreaterEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.GreaterEqual#getRight()
   * @see #getGreaterEqual()
   * @generated
   */
  EReference getGreaterEqual_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Less <em>Less</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Less</em>'.
   * @see br.ufsm.aql.Less
   * @generated
   */
  EClass getLess();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Less#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Less#getLeft()
   * @see #getLess()
   * @generated
   */
  EReference getLess_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Less#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Less#getRight()
   * @see #getLess()
   * @generated
   */
  EReference getLess_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.LessEqual <em>Less Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Less Equal</em>'.
   * @see br.ufsm.aql.LessEqual
   * @generated
   */
  EClass getLessEqual();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.LessEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.LessEqual#getLeft()
   * @see #getLessEqual()
   * @generated
   */
  EReference getLessEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.LessEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.LessEqual#getRight()
   * @see #getLessEqual()
   * @generated
   */
  EReference getLessEqual_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Like <em>Like</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Like</em>'.
   * @see br.ufsm.aql.Like
   * @generated
   */
  EClass getLike();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Like#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Like#getLeft()
   * @see #getLike()
   * @generated
   */
  EReference getLike_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Like#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Like#getRight()
   * @see #getLike()
   * @generated
   */
  EReference getLike_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.NotEquals <em>Not Equals</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Equals</em>'.
   * @see br.ufsm.aql.NotEquals
   * @generated
   */
  EClass getNotEquals();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.NotEquals#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.NotEquals#getLeft()
   * @see #getNotEquals()
   * @generated
   */
  EReference getNotEquals_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.NotEquals#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.NotEquals#getRight()
   * @see #getNotEquals()
   * @generated
   */
  EReference getNotEquals_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Add <em>Add</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Add</em>'.
   * @see br.ufsm.aql.Add
   * @generated
   */
  EClass getAdd();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Add#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Add#getLeft()
   * @see #getAdd()
   * @generated
   */
  EReference getAdd_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Add#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Add#getRight()
   * @see #getAdd()
   * @generated
   */
  EReference getAdd_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Minus <em>Minus</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Minus</em>'.
   * @see br.ufsm.aql.Minus
   * @generated
   */
  EClass getMinus();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Minus#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Minus#getLeft()
   * @see #getMinus()
   * @generated
   */
  EReference getMinus_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Minus#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Minus#getRight()
   * @see #getMinus()
   * @generated
   */
  EReference getMinus_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Mult <em>Mult</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mult</em>'.
   * @see br.ufsm.aql.Mult
   * @generated
   */
  EClass getMult();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Mult#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Mult#getLeft()
   * @see #getMult()
   * @generated
   */
  EReference getMult_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Mult#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Mult#getRight()
   * @see #getMult()
   * @generated
   */
  EReference getMult_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Div <em>Div</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Div</em>'.
   * @see br.ufsm.aql.Div
   * @generated
   */
  EClass getDiv();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Div#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Div#getLeft()
   * @see #getDiv()
   * @generated
   */
  EReference getDiv_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Div#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Div#getRight()
   * @see #getDiv()
   * @generated
   */
  EReference getDiv_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Mod <em>Mod</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mod</em>'.
   * @see br.ufsm.aql.Mod
   * @generated
   */
  EClass getMod();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Mod#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Mod#getLeft()
   * @see #getMod()
   * @generated
   */
  EReference getMod_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Mod#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Mod#getRight()
   * @see #getMod()
   * @generated
   */
  EReference getMod_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.In <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>In</em>'.
   * @see br.ufsm.aql.In
   * @generated
   */
  EClass getIn();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.In#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.In#getLeft()
   * @see #getIn()
   * @generated
   */
  EReference getIn_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.In#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.In#getRight()
   * @see #getIn()
   * @generated
   */
  EReference getIn_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Not <em>Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not</em>'.
   * @see br.ufsm.aql.Not
   * @generated
   */
  EClass getNot();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Not#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see br.ufsm.aql.Not#getExp()
   * @see #getNot()
   * @generated
   */
  EReference getNot_Exp();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.UnaryMinus <em>Unary Minus</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Minus</em>'.
   * @see br.ufsm.aql.UnaryMinus
   * @generated
   */
  EClass getUnaryMinus();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.UnaryMinus#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expr</em>'.
   * @see br.ufsm.aql.UnaryMinus#getExpr()
   * @see #getUnaryMinus()
   * @generated
   */
  EReference getUnaryMinus_Expr();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Exponential <em>Exponential</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exponential</em>'.
   * @see br.ufsm.aql.Exponential
   * @generated
   */
  EClass getExponential();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Exponential#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see br.ufsm.aql.Exponential#getLeft()
   * @see #getExponential()
   * @generated
   */
  EReference getExponential_Left();

  /**
   * Returns the meta object for the containment reference '{@link br.ufsm.aql.Exponential#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see br.ufsm.aql.Exponential#getRight()
   * @see #getExponential()
   * @generated
   */
  EReference getExponential_Right();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.ParsExpression <em>Pars Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pars Expression</em>'.
   * @see br.ufsm.aql.ParsExpression
   * @generated
   */
  EClass getParsExpression();

  /**
   * Returns the meta object for the containment reference list '{@link br.ufsm.aql.ParsExpression#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see br.ufsm.aql.ParsExpression#getExpr()
   * @see #getParsExpression()
   * @generated
   */
  EReference getParsExpression_Expr();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.StringLiteral <em>String Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String Literal</em>'.
   * @see br.ufsm.aql.StringLiteral
   * @generated
   */
  EClass getStringLiteral();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.StringLiteral#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.StringLiteral#getValue()
   * @see #getStringLiteral()
   * @generated
   */
  EAttribute getStringLiteral_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.FloatLiteral <em>Float Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Float Literal</em>'.
   * @see br.ufsm.aql.FloatLiteral
   * @generated
   */
  EClass getFloatLiteral();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.FloatLiteral#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.FloatLiteral#getValue()
   * @see #getFloatLiteral()
   * @generated
   */
  EAttribute getFloatLiteral_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.IntegerLiteral <em>Integer Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Literal</em>'.
   * @see br.ufsm.aql.IntegerLiteral
   * @generated
   */
  EClass getIntegerLiteral();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.IntegerLiteral#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.IntegerLiteral#getValue()
   * @see #getIntegerLiteral()
   * @generated
   */
  EAttribute getIntegerLiteral_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.Null <em>Null</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Null</em>'.
   * @see br.ufsm.aql.Null
   * @generated
   */
  EClass getNull();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.Null#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.Null#getValue()
   * @see #getNull()
   * @generated
   */
  EAttribute getNull_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.True <em>True</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>True</em>'.
   * @see br.ufsm.aql.True
   * @generated
   */
  EClass getTrue();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.True#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.True#getValue()
   * @see #getTrue()
   * @generated
   */
  EAttribute getTrue_Value();

  /**
   * Returns the meta object for class '{@link br.ufsm.aql.False <em>False</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>False</em>'.
   * @see br.ufsm.aql.False
   * @generated
   */
  EClass getFalse();

  /**
   * Returns the meta object for the attribute '{@link br.ufsm.aql.False#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.ufsm.aql.False#getValue()
   * @see #getFalse()
   * @generated
   */
  EAttribute getFalse_Value();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  AqlFactory getAqlFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.QueryImpl <em>Query</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.QueryImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getQuery()
     * @generated
     */
    EClass QUERY = eINSTANCE.getQuery();

    /**
     * The meta object literal for the '<em><b>Query</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY__QUERY = eINSTANCE.getQuery_Query();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.QueryStatementImpl <em>Query Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.QueryStatementImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getQueryStatement()
     * @generated
     */
    EClass QUERY_STATEMENT = eINSTANCE.getQueryStatement();

    /**
     * The meta object literal for the '<em><b>Find</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_STATEMENT__FIND = eINSTANCE.getQueryStatement_Find();

    /**
     * The meta object literal for the '<em><b>Where</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_STATEMENT__WHERE = eINSTANCE.getQueryStatement_Where();

    /**
     * The meta object literal for the '<em><b>Return</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_STATEMENT__RETURN = eINSTANCE.getQueryStatement_Return();

    /**
     * The meta object literal for the '<em><b>Orderby</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_STATEMENT__ORDERBY = eINSTANCE.getQueryStatement_Orderby();

    /**
     * The meta object literal for the '<em><b>Groupby</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUERY_STATEMENT__GROUPBY = eINSTANCE.getQueryStatement_Groupby();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.FindClauseImpl <em>Find Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.FindClauseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getFindClause()
     * @generated
     */
    EClass FIND_CLAUSE = eINSTANCE.getFindClause();

    /**
     * The meta object literal for the '<em><b>Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIND_CLAUSE__CLAUSE = eINSTANCE.getFindClause_Clause();

    /**
     * The meta object literal for the '<em><b>Binding Object</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIND_CLAUSE__BINDING_OBJECT = eINSTANCE.getFindClause_BindingObject();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.BindingObjectImpl <em>Binding Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.BindingObjectImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getBindingObject()
     * @generated
     */
    EClass BINDING_OBJECT = eINSTANCE.getBindingObject();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BINDING_OBJECT__TYPE = eINSTANCE.getBindingObject_Type();

    /**
     * The meta object literal for the '<em><b>Alias</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BINDING_OBJECT__ALIAS = eINSTANCE.getBindingObject_Alias();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ObjectTypeImpl <em>Object Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ObjectTypeImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getObjectType()
     * @generated
     */
    EClass OBJECT_TYPE = eINSTANCE.getObjectType();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBJECT_TYPE__VALUE = eINSTANCE.getObjectType_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.WhereClauseImpl <em>Where Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.WhereClauseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getWhereClause()
     * @generated
     */
    EClass WHERE_CLAUSE = eINSTANCE.getWhereClause();

    /**
     * The meta object literal for the '<em><b>Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WHERE_CLAUSE__CLAUSE = eINSTANCE.getWhereClause_Clause();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHERE_CLAUSE__EXPRESSION = eINSTANCE.getWhereClause_Expression();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ReturnsClauseImpl <em>Returns Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ReturnsClauseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getReturnsClause()
     * @generated
     */
    EClass RETURNS_CLAUSE = eINSTANCE.getReturnsClause();

    /**
     * The meta object literal for the '<em><b>Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETURNS_CLAUSE__CLAUSE = eINSTANCE.getReturnsClause_Clause();

    /**
     * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RETURNS_CLAUSE__EXPRESSIONS = eINSTANCE.getReturnsClause_Expressions();

    /**
     * The meta object literal for the '<em><b>Result Alias</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETURNS_CLAUSE__RESULT_ALIAS = eINSTANCE.getReturnsClause_ResultAlias();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.GroupByClauseImpl <em>Group By Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.GroupByClauseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getGroupByClause()
     * @generated
     */
    EClass GROUP_BY_CLAUSE = eINSTANCE.getGroupByClause();

    /**
     * The meta object literal for the '<em><b>Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GROUP_BY_CLAUSE__CLAUSE = eINSTANCE.getGroupByClause_Clause();

    /**
     * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GROUP_BY_CLAUSE__EXPRESSIONS = eINSTANCE.getGroupByClause_Expressions();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.OrderByClauseImpl <em>Order By Clause</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.OrderByClauseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getOrderByClause()
     * @generated
     */
    EClass ORDER_BY_CLAUSE = eINSTANCE.getOrderByClause();

    /**
     * The meta object literal for the '<em><b>Clause</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORDER_BY_CLAUSE__CLAUSE = eINSTANCE.getOrderByClause_Clause();

    /**
     * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ORDER_BY_CLAUSE__EXPRESSIONS = eINSTANCE.getOrderByClause_Expressions();

    /**
     * The meta object literal for the '<em><b>Option</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORDER_BY_CLAUSE__OPTION = eINSTANCE.getOrderByClause_Option();

    /**
     * The meta object literal for the '<em><b>Having Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ORDER_BY_CLAUSE__HAVING_EXPRESSION = eINSTANCE.getOrderByClause_HavingExpression();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.QualifiedNameImpl <em>Qualified Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.QualifiedNameImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getQualifiedName()
     * @generated
     */
    EClass QUALIFIED_NAME = eINSTANCE.getQualifiedName();

    /**
     * The meta object literal for the '<em><b>Qualifiers</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUALIFIED_NAME__QUALIFIERS = eINSTANCE.getQualifiedName_Qualifiers();

    /**
     * The meta object literal for the '<em><b>Granular</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUALIFIED_NAME__GRANULAR = eINSTANCE.getQualifiedName_Granular();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.QualifiedElementImpl <em>Qualified Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.QualifiedElementImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getQualifiedElement()
     * @generated
     */
    EClass QUALIFIED_ELEMENT = eINSTANCE.getQualifiedElement();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ExpressionImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.LiteralImpl <em>Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.LiteralImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getLiteral()
     * @generated
     */
    EClass LITERAL = eINSTANCE.getLiteral();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.FunctionImpl <em>Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.FunctionImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getFunction()
     * @generated
     */
    EClass FUNCTION = eINSTANCE.getFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FUNCTION__NAME = eINSTANCE.getFunction_Name();

    /**
     * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__PARAMS = eINSTANCE.getFunction_Params();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.IdentifierImpl <em>Identifier</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.IdentifierImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getIdentifier()
     * @generated
     */
    EClass IDENTIFIER = eINSTANCE.getIdentifier();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIER__ID = eINSTANCE.getIdentifier_Id();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ProjectImpl <em>Project</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ProjectImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getProject()
     * @generated
     */
    EClass PROJECT = eINSTANCE.getProject();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.PackageImpl <em>Package</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.PackageImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getPackage()
     * @generated
     */
    EClass PACKAGE = eINSTANCE.getPackage();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ClassImpl <em>Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ClassImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getClass_()
     * @generated
     */
    EClass CLASS = eINSTANCE.getClass_();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.AspectImpl <em>Aspect</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.AspectImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getAspect()
     * @generated
     */
    EClass ASPECT = eINSTANCE.getAspect();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.InterfaceImpl <em>Interface</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.InterfaceImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getInterface()
     * @generated
     */
    EClass INTERFACE = eINSTANCE.getInterface();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.EnumImpl <em>Enum</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.EnumImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getEnum()
     * @generated
     */
    EClass ENUM = eINSTANCE.getEnum();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.OrImpl <em>Or</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.OrImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getOr()
     * @generated
     */
    EClass OR = eINSTANCE.getOr();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR__LEFT = eINSTANCE.getOr_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR__RIGHT = eINSTANCE.getOr_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.AndImpl <em>And</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.AndImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getAnd()
     * @generated
     */
    EClass AND = eINSTANCE.getAnd();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND__LEFT = eINSTANCE.getAnd_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND__RIGHT = eINSTANCE.getAnd_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.EqualsImpl <em>Equals</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.EqualsImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getEquals()
     * @generated
     */
    EClass EQUALS = eINSTANCE.getEquals();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUALS__LEFT = eINSTANCE.getEquals_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EQUALS__RIGHT = eINSTANCE.getEquals_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.GreaterImpl <em>Greater</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.GreaterImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getGreater()
     * @generated
     */
    EClass GREATER = eINSTANCE.getGreater();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GREATER__LEFT = eINSTANCE.getGreater_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GREATER__RIGHT = eINSTANCE.getGreater_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.GreaterEqualImpl <em>Greater Equal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.GreaterEqualImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getGreaterEqual()
     * @generated
     */
    EClass GREATER_EQUAL = eINSTANCE.getGreaterEqual();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GREATER_EQUAL__LEFT = eINSTANCE.getGreaterEqual_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GREATER_EQUAL__RIGHT = eINSTANCE.getGreaterEqual_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.LessImpl <em>Less</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.LessImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getLess()
     * @generated
     */
    EClass LESS = eINSTANCE.getLess();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LESS__LEFT = eINSTANCE.getLess_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LESS__RIGHT = eINSTANCE.getLess_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.LessEqualImpl <em>Less Equal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.LessEqualImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getLessEqual()
     * @generated
     */
    EClass LESS_EQUAL = eINSTANCE.getLessEqual();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LESS_EQUAL__LEFT = eINSTANCE.getLessEqual_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LESS_EQUAL__RIGHT = eINSTANCE.getLessEqual_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.LikeImpl <em>Like</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.LikeImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getLike()
     * @generated
     */
    EClass LIKE = eINSTANCE.getLike();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIKE__LEFT = eINSTANCE.getLike_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIKE__RIGHT = eINSTANCE.getLike_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.NotEqualsImpl <em>Not Equals</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.NotEqualsImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getNotEquals()
     * @generated
     */
    EClass NOT_EQUALS = eINSTANCE.getNotEquals();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT_EQUALS__LEFT = eINSTANCE.getNotEquals_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT_EQUALS__RIGHT = eINSTANCE.getNotEquals_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.AddImpl <em>Add</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.AddImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getAdd()
     * @generated
     */
    EClass ADD = eINSTANCE.getAdd();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD__LEFT = eINSTANCE.getAdd_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ADD__RIGHT = eINSTANCE.getAdd_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.MinusImpl <em>Minus</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.MinusImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getMinus()
     * @generated
     */
    EClass MINUS = eINSTANCE.getMinus();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MINUS__LEFT = eINSTANCE.getMinus_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MINUS__RIGHT = eINSTANCE.getMinus_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.MultImpl <em>Mult</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.MultImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getMult()
     * @generated
     */
    EClass MULT = eINSTANCE.getMult();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULT__LEFT = eINSTANCE.getMult_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULT__RIGHT = eINSTANCE.getMult_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.DivImpl <em>Div</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.DivImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getDiv()
     * @generated
     */
    EClass DIV = eINSTANCE.getDiv();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DIV__LEFT = eINSTANCE.getDiv_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DIV__RIGHT = eINSTANCE.getDiv_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ModImpl <em>Mod</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ModImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getMod()
     * @generated
     */
    EClass MOD = eINSTANCE.getMod();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MOD__LEFT = eINSTANCE.getMod_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MOD__RIGHT = eINSTANCE.getMod_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.InImpl <em>In</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.InImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getIn()
     * @generated
     */
    EClass IN = eINSTANCE.getIn();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IN__LEFT = eINSTANCE.getIn_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IN__RIGHT = eINSTANCE.getIn_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.NotImpl <em>Not</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.NotImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getNot()
     * @generated
     */
    EClass NOT = eINSTANCE.getNot();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT__EXP = eINSTANCE.getNot_Exp();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.UnaryMinusImpl <em>Unary Minus</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.UnaryMinusImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getUnaryMinus()
     * @generated
     */
    EClass UNARY_MINUS = eINSTANCE.getUnaryMinus();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNARY_MINUS__EXPR = eINSTANCE.getUnaryMinus_Expr();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ExponentialImpl <em>Exponential</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ExponentialImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getExponential()
     * @generated
     */
    EClass EXPONENTIAL = eINSTANCE.getExponential();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENTIAL__LEFT = eINSTANCE.getExponential_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPONENTIAL__RIGHT = eINSTANCE.getExponential_Right();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.ParsExpressionImpl <em>Pars Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.ParsExpressionImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getParsExpression()
     * @generated
     */
    EClass PARS_EXPRESSION = eINSTANCE.getParsExpression();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARS_EXPRESSION__EXPR = eINSTANCE.getParsExpression_Expr();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.StringLiteralImpl <em>String Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.StringLiteralImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getStringLiteral()
     * @generated
     */
    EClass STRING_LITERAL = eINSTANCE.getStringLiteral();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_LITERAL__VALUE = eINSTANCE.getStringLiteral_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.FloatLiteralImpl <em>Float Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.FloatLiteralImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getFloatLiteral()
     * @generated
     */
    EClass FLOAT_LITERAL = eINSTANCE.getFloatLiteral();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FLOAT_LITERAL__VALUE = eINSTANCE.getFloatLiteral_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.IntegerLiteralImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getIntegerLiteral()
     * @generated
     */
    EClass INTEGER_LITERAL = eINSTANCE.getIntegerLiteral();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_LITERAL__VALUE = eINSTANCE.getIntegerLiteral_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.NullImpl <em>Null</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.NullImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getNull()
     * @generated
     */
    EClass NULL = eINSTANCE.getNull();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NULL__VALUE = eINSTANCE.getNull_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.TrueImpl <em>True</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.TrueImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getTrue()
     * @generated
     */
    EClass TRUE = eINSTANCE.getTrue();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TRUE__VALUE = eINSTANCE.getTrue_Value();

    /**
     * The meta object literal for the '{@link br.ufsm.aql.impl.FalseImpl <em>False</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.ufsm.aql.impl.FalseImpl
     * @see br.ufsm.aql.impl.AqlPackageImpl#getFalse()
     * @generated
     */
    EClass FALSE = eINSTANCE.getFalse();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FALSE__VALUE = eINSTANCE.getFalse_Value();

  }

} //AqlPackage
