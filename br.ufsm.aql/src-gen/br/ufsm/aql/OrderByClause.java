/**
 */
package br.ufsm.aql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Order By Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.OrderByClause#getClause <em>Clause</em>}</li>
 *   <li>{@link br.ufsm.aql.OrderByClause#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link br.ufsm.aql.OrderByClause#getOption <em>Option</em>}</li>
 *   <li>{@link br.ufsm.aql.OrderByClause#getHavingExpression <em>Having Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getOrderByClause()
 * @model
 * @generated
 */
public interface OrderByClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Clause</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Clause</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Clause</em>' attribute.
   * @see #setClause(String)
   * @see br.ufsm.aql.AqlPackage#getOrderByClause_Clause()
   * @model
   * @generated
   */
  String getClause();

  /**
   * Sets the value of the '{@link br.ufsm.aql.OrderByClause#getClause <em>Clause</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Clause</em>' attribute.
   * @see #getClause()
   * @generated
   */
  void setClause(String value);

  /**
   * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
   * The list contents are of type {@link br.ufsm.aql.QualifiedName}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expressions</em>' containment reference list.
   * @see br.ufsm.aql.AqlPackage#getOrderByClause_Expressions()
   * @model containment="true"
   * @generated
   */
  EList<QualifiedName> getExpressions();

  /**
   * Returns the value of the '<em><b>Option</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Option</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Option</em>' attribute.
   * @see #setOption(String)
   * @see br.ufsm.aql.AqlPackage#getOrderByClause_Option()
   * @model
   * @generated
   */
  String getOption();

  /**
   * Sets the value of the '{@link br.ufsm.aql.OrderByClause#getOption <em>Option</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Option</em>' attribute.
   * @see #getOption()
   * @generated
   */
  void setOption(String value);

  /**
   * Returns the value of the '<em><b>Having Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Having Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Having Expression</em>' containment reference.
   * @see #setHavingExpression(Expression)
   * @see br.ufsm.aql.AqlPackage#getOrderByClause_HavingExpression()
   * @model containment="true"
   * @generated
   */
  Expression getHavingExpression();

  /**
   * Sets the value of the '{@link br.ufsm.aql.OrderByClause#getHavingExpression <em>Having Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Having Expression</em>' containment reference.
   * @see #getHavingExpression()
   * @generated
   */
  void setHavingExpression(Expression value);

} // OrderByClause
