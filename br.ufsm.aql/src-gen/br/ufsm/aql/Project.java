/**
 */
package br.ufsm.aql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends ObjectType
{
} // Project
