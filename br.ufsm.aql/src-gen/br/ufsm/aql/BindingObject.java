/**
 */
package br.ufsm.aql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link br.ufsm.aql.BindingObject#getType <em>Type</em>}</li>
 *   <li>{@link br.ufsm.aql.BindingObject#getAlias <em>Alias</em>}</li>
 * </ul>
 * </p>
 *
 * @see br.ufsm.aql.AqlPackage#getBindingObject()
 * @model
 * @generated
 */
public interface BindingObject extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ObjectType)
   * @see br.ufsm.aql.AqlPackage#getBindingObject_Type()
   * @model containment="true"
   * @generated
   */
  ObjectType getType();

  /**
   * Sets the value of the '{@link br.ufsm.aql.BindingObject#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ObjectType value);

  /**
   * Returns the value of the '<em><b>Alias</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alias</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alias</em>' attribute list.
   * @see br.ufsm.aql.AqlPackage#getBindingObject_Alias()
   * @model unique="false"
   * @generated
   */
  EList<String> getAlias();

} // BindingObject
