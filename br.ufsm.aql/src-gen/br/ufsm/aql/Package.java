/**
 */
package br.ufsm.aql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufsm.aql.AqlPackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends ObjectType
{
} // Package
