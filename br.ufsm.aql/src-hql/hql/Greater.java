/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Greater</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getGreater()
 * @model
 * @generated
 */
public interface Greater extends BinaryExpression {

} // Greater
