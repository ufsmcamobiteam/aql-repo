/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mult</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getMult()
 * @model
 * @generated
 */
public interface Mult extends BinaryExpression {

} // Mult
