/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Less</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getLess()
 * @model
 * @generated
 */
public interface Less extends BinaryExpression {

} // Less
