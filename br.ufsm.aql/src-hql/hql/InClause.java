/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getInClause()
 * @model
 * @generated
 */
public interface InClause extends BinaryExpression {

} // InClause
