/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Left Join</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getLeftJoin()
 * @model
 * @generated
 */
public interface LeftJoin extends BinaryFromExpression {
} // LeftJoin
