/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Add</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getAdd()
 * @model
 * @generated
 */
public interface Add extends BinaryExpression {

} // Add
