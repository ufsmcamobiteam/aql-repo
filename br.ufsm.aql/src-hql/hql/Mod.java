/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mod</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getMod()
 * @model
 * @generated
 */
public interface Mod extends BinaryExpression {

} // Mod
