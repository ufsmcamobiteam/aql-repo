/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Div</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getDiv()
 * @model
 * @generated
 */
public interface Div extends BinaryExpression {

} // Div
