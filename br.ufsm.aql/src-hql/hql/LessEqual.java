/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Less Equal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getLessEqual()
 * @model
 * @generated
 */
public interface LessEqual extends BinaryExpression {

} // LessEqual
