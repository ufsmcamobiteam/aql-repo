/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends EObject {
} // Expression
