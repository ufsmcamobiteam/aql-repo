/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.Clause#getClause <em>Clause</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getClause()
 * @model abstract="true"
 * @generated
 */
public interface Clause extends EObject {
	/**
	 * Returns the value of the '<em><b>Clause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clause</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clause</em>' attribute.
	 * @see #setClause(String)
	 * @see hql.HqlPackage#getClause_Clause()
	 * @model
	 * @generated
	 */
	String getClause();

	/**
	 * Sets the value of the '{@link hql.Clause#getClause <em>Clause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clause</em>' attribute.
	 * @see #getClause()
	 * @generated
	 */
	void setClause(String value);

} // Clause
