/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equals</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getEquals()
 * @model
 * @generated
 */
public interface Equals extends BinaryExpression {

} // Equals
