/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary From Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.BinaryFromExpression#getRight <em>Right</em>}</li>
 *   <li>{@link hql.BinaryFromExpression#getLeft <em>Left</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getBinaryFromExpression()
 * @model
 * @generated
 */
public interface BinaryFromExpression extends FromExpression {
	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(FromExpression)
	 * @see hql.HqlPackage#getBinaryFromExpression_Right()
	 * @model containment="true"
	 * @generated
	 */
	FromExpression getRight();

	/**
	 * Sets the value of the '{@link hql.BinaryFromExpression#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(FromExpression value);

	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(FromExpression)
	 * @see hql.HqlPackage#getBinaryFromExpression_Left()
	 * @model containment="true"
	 * @generated
	 */
	FromExpression getLeft();

	/**
	 * Sets the value of the '{@link hql.BinaryFromExpression#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(FromExpression value);

} // BinaryFromExpression
