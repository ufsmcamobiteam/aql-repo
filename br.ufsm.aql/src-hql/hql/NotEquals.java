/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Equals</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getNotEquals()
 * @model
 * @generated
 */
public interface NotEquals extends BinaryExpression {

} // NotEquals
