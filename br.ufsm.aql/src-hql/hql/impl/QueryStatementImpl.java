/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.FromClause;
import hql.HqlPackage;
import hql.OrderByClause;
import hql.QueryStatement;
import hql.SelectClause;
import hql.WhereClause;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hql.impl.QueryStatementImpl#getWhereClause <em>Where Clause</em>}</li>
 *   <li>{@link hql.impl.QueryStatementImpl#getSelectClause <em>Select Clause</em>}</li>
 *   <li>{@link hql.impl.QueryStatementImpl#getFromClause <em>From Clause</em>}</li>
 *   <li>{@link hql.impl.QueryStatementImpl#getOrderByClause <em>Order By Clause</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QueryStatementImpl extends EObjectImpl implements QueryStatement {
	/**
	 * The cached value of the '{@link #getWhereClause() <em>Where Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhereClause()
	 * @generated
	 * @ordered
	 */
	protected WhereClause whereClause;

	/**
	 * The cached value of the '{@link #getSelectClause() <em>Select Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectClause()
	 * @generated
	 * @ordered
	 */
	protected SelectClause selectClause;

	/**
	 * The cached value of the '{@link #getFromClause() <em>From Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromClause()
	 * @generated
	 * @ordered
	 */
	protected FromClause fromClause;

	/**
	 * The cached value of the '{@link #getOrderByClause() <em>Order By Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderByClause()
	 * @generated
	 * @ordered
	 */
	protected OrderByClause orderByClause;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueryStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.QUERY_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhereClause getWhereClause() {
		return whereClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWhereClause(WhereClause newWhereClause, NotificationChain msgs) {
		WhereClause oldWhereClause = whereClause;
		whereClause = newWhereClause;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE, oldWhereClause, newWhereClause);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhereClause(WhereClause newWhereClause) {
		if (newWhereClause != whereClause) {
			NotificationChain msgs = null;
			if (whereClause != null)
				msgs = ((InternalEObject)whereClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE, null, msgs);
			if (newWhereClause != null)
				msgs = ((InternalEObject)newWhereClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE, null, msgs);
			msgs = basicSetWhereClause(newWhereClause, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE, newWhereClause, newWhereClause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectClause getSelectClause() {
		return selectClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectClause(SelectClause newSelectClause, NotificationChain msgs) {
		SelectClause oldSelectClause = selectClause;
		selectClause = newSelectClause;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE, oldSelectClause, newSelectClause);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectClause(SelectClause newSelectClause) {
		if (newSelectClause != selectClause) {
			NotificationChain msgs = null;
			if (selectClause != null)
				msgs = ((InternalEObject)selectClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE, null, msgs);
			if (newSelectClause != null)
				msgs = ((InternalEObject)newSelectClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE, null, msgs);
			msgs = basicSetSelectClause(newSelectClause, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE, newSelectClause, newSelectClause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromClause getFromClause() {
		return fromClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFromClause(FromClause newFromClause, NotificationChain msgs) {
		FromClause oldFromClause = fromClause;
		fromClause = newFromClause;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__FROM_CLAUSE, oldFromClause, newFromClause);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromClause(FromClause newFromClause) {
		if (newFromClause != fromClause) {
			NotificationChain msgs = null;
			if (fromClause != null)
				msgs = ((InternalEObject)fromClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__FROM_CLAUSE, null, msgs);
			if (newFromClause != null)
				msgs = ((InternalEObject)newFromClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__FROM_CLAUSE, null, msgs);
			msgs = basicSetFromClause(newFromClause, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__FROM_CLAUSE, newFromClause, newFromClause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderByClause getOrderByClause() {
		return orderByClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrderByClause(OrderByClause newOrderByClause, NotificationChain msgs) {
		OrderByClause oldOrderByClause = orderByClause;
		orderByClause = newOrderByClause;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE, oldOrderByClause, newOrderByClause);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrderByClause(OrderByClause newOrderByClause) {
		if (newOrderByClause != orderByClause) {
			NotificationChain msgs = null;
			if (orderByClause != null)
				msgs = ((InternalEObject)orderByClause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE, null, msgs);
			if (newOrderByClause != null)
				msgs = ((InternalEObject)newOrderByClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE, null, msgs);
			msgs = basicSetOrderByClause(newOrderByClause, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE, newOrderByClause, newOrderByClause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE:
				return basicSetWhereClause(null, msgs);
			case HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE:
				return basicSetSelectClause(null, msgs);
			case HqlPackage.QUERY_STATEMENT__FROM_CLAUSE:
				return basicSetFromClause(null, msgs);
			case HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE:
				return basicSetOrderByClause(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE:
				return getWhereClause();
			case HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE:
				return getSelectClause();
			case HqlPackage.QUERY_STATEMENT__FROM_CLAUSE:
				return getFromClause();
			case HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE:
				return getOrderByClause();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE:
				setWhereClause((WhereClause)newValue);
				return;
			case HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE:
				setSelectClause((SelectClause)newValue);
				return;
			case HqlPackage.QUERY_STATEMENT__FROM_CLAUSE:
				setFromClause((FromClause)newValue);
				return;
			case HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE:
				setOrderByClause((OrderByClause)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE:
				setWhereClause((WhereClause)null);
				return;
			case HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE:
				setSelectClause((SelectClause)null);
				return;
			case HqlPackage.QUERY_STATEMENT__FROM_CLAUSE:
				setFromClause((FromClause)null);
				return;
			case HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE:
				setOrderByClause((OrderByClause)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HqlPackage.QUERY_STATEMENT__WHERE_CLAUSE:
				return whereClause != null;
			case HqlPackage.QUERY_STATEMENT__SELECT_CLAUSE:
				return selectClause != null;
			case HqlPackage.QUERY_STATEMENT__FROM_CLAUSE:
				return fromClause != null;
			case HqlPackage.QUERY_STATEMENT__ORDER_BY_CLAUSE:
				return orderByClause != null;
		}
		return super.eIsSet(featureID);
	}

} //QueryStatementImpl
