/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.BinaryFromExpression;
import hql.BinaryExpression;
import hql.FromClause;
import hql.FromExpression;
import hql.HqlPackage;
import hql.ObjectSource;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hql.impl.FromClauseImpl#getObjectList <em>Object List</em>}</li>
 *   <li>{@link hql.impl.FromClauseImpl#getBinaryObjectList <em>Binary Object List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FromClauseImpl extends ClauseImpl implements FromClause {
	/**
	 * The cached value of the '{@link #getObjectList() <em>Object List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectList()
	 * @generated
	 * @ordered
	 */
	protected EList<FromExpression> objectList;

	/**
	 * The cached value of the '{@link #getBinaryObjectList() <em>Binary Object List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinaryObjectList()
	 * @generated
	 * @ordered
	 */
	protected EList<BinaryFromExpression> binaryObjectList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.FROM_CLAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FromExpression> getObjectList() {
		if (objectList == null) {
			objectList = new EObjectContainmentEList<FromExpression>(FromExpression.class, this, HqlPackage.FROM_CLAUSE__OBJECT_LIST);
		}
		return objectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BinaryFromExpression> getBinaryObjectList() {
		if (binaryObjectList == null) {
			binaryObjectList = new EObjectContainmentEList<BinaryFromExpression>(BinaryFromExpression.class, this, HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST);
		}
		return binaryObjectList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HqlPackage.FROM_CLAUSE__OBJECT_LIST:
				return ((InternalEList<?>)getObjectList()).basicRemove(otherEnd, msgs);
			case HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST:
				return ((InternalEList<?>)getBinaryObjectList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HqlPackage.FROM_CLAUSE__OBJECT_LIST:
				return getObjectList();
			case HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST:
				return getBinaryObjectList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HqlPackage.FROM_CLAUSE__OBJECT_LIST:
				getObjectList().clear();
				getObjectList().addAll((Collection<? extends FromExpression>)newValue);
				return;
			case HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST:
				getBinaryObjectList().clear();
				getBinaryObjectList().addAll((Collection<? extends BinaryFromExpression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HqlPackage.FROM_CLAUSE__OBJECT_LIST:
				getObjectList().clear();
				return;
			case HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST:
				getBinaryObjectList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HqlPackage.FROM_CLAUSE__OBJECT_LIST:
				return objectList != null && !objectList.isEmpty();
			case HqlPackage.FROM_CLAUSE__BINARY_OBJECT_LIST:
				return binaryObjectList != null && !binaryObjectList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FromClauseImpl
