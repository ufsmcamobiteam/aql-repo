/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.FromExpression;
import hql.HqlPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FromExpressionImpl extends EObjectImpl implements FromExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.FROM_EXPRESSION;
	}

} //FromExpressionImpl
