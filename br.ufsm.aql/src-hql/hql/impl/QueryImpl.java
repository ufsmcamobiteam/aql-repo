/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.HqlPackage;
import hql.Query;
import hql.QueryStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hql.impl.QueryImpl#getQueryStatement <em>Query Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QueryImpl extends EObjectImpl implements Query {
	/**
	 * The cached value of the '{@link #getQueryStatement() <em>Query Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueryStatement()
	 * @generated
	 * @ordered
	 */
	protected QueryStatement queryStatement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.QUERY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueryStatement getQueryStatement() {
		return queryStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueryStatement(QueryStatement newQueryStatement, NotificationChain msgs) {
		QueryStatement oldQueryStatement = queryStatement;
		queryStatement = newQueryStatement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY__QUERY_STATEMENT, oldQueryStatement, newQueryStatement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueryStatement(QueryStatement newQueryStatement) {
		if (newQueryStatement != queryStatement) {
			NotificationChain msgs = null;
			if (queryStatement != null)
				msgs = ((InternalEObject)queryStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY__QUERY_STATEMENT, null, msgs);
			if (newQueryStatement != null)
				msgs = ((InternalEObject)newQueryStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HqlPackage.QUERY__QUERY_STATEMENT, null, msgs);
			msgs = basicSetQueryStatement(newQueryStatement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUERY__QUERY_STATEMENT, newQueryStatement, newQueryStatement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HqlPackage.QUERY__QUERY_STATEMENT:
				return basicSetQueryStatement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HqlPackage.QUERY__QUERY_STATEMENT:
				return getQueryStatement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HqlPackage.QUERY__QUERY_STATEMENT:
				setQueryStatement((QueryStatement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HqlPackage.QUERY__QUERY_STATEMENT:
				setQueryStatement((QueryStatement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HqlPackage.QUERY__QUERY_STATEMENT:
				return queryStatement != null;
		}
		return super.eIsSet(featureID);
	}

} //QueryImpl
