/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.HqlPackage;
import hql.QualifiedElement;
import hql.QualifiedName;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Qualified Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hql.impl.QualifiedNameImpl#getQualifiers <em>Qualifiers</em>}</li>
 *   <li>{@link hql.impl.QualifiedNameImpl#isResolved <em>Resolved</em>}</li>
 *   <li>{@link hql.impl.QualifiedNameImpl#getGranular <em>Granular</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QualifiedNameImpl extends ExpressionImpl implements QualifiedName {
	/**
	 * The cached value of the '{@link #getQualifiers() <em>Qualifiers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<QualifiedElement> qualifiers;
	/**
	 * The default value of the '{@link #isResolved() <em>Resolved</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResolved()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESOLVED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isResolved() <em>Resolved</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResolved()
	 * @generated
	 * @ordered
	 */
	protected boolean resolved = RESOLVED_EDEFAULT;
	/**
	 * The default value of the '{@link #getGranular() <em>Granular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGranular()
	 * @generated
	 * @ordered
	 */
	protected static final String GRANULAR_EDEFAULT = " ";
	/**
	 * The cached value of the '{@link #getGranular() <em>Granular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGranular()
	 * @generated
	 * @ordered
	 */
	protected String granular = GRANULAR_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualifiedNameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.QUALIFIED_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualifiedElement> getQualifiers() {
		if (qualifiers == null) {
			qualifiers = new EObjectContainmentEList<QualifiedElement>(QualifiedElement.class, this, HqlPackage.QUALIFIED_NAME__QUALIFIERS);
		}
		return qualifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResolved() {
		return resolved;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResolved(boolean newResolved) {
		boolean oldResolved = resolved;
		resolved = newResolved;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUALIFIED_NAME__RESOLVED, oldResolved, resolved));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGranular() {
		return granular;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGranular(String newGranular) {
		String oldGranular = granular;
		granular = newGranular;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.QUALIFIED_NAME__GRANULAR, oldGranular, granular));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HqlPackage.QUALIFIED_NAME__QUALIFIERS:
				return ((InternalEList<?>)getQualifiers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HqlPackage.QUALIFIED_NAME__QUALIFIERS:
				return getQualifiers();
			case HqlPackage.QUALIFIED_NAME__RESOLVED:
				return isResolved();
			case HqlPackage.QUALIFIED_NAME__GRANULAR:
				return getGranular();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HqlPackage.QUALIFIED_NAME__QUALIFIERS:
				getQualifiers().clear();
				getQualifiers().addAll((Collection<? extends QualifiedElement>)newValue);
				return;
			case HqlPackage.QUALIFIED_NAME__RESOLVED:
				setResolved((Boolean)newValue);
				return;
			case HqlPackage.QUALIFIED_NAME__GRANULAR:
				setGranular((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HqlPackage.QUALIFIED_NAME__QUALIFIERS:
				getQualifiers().clear();
				return;
			case HqlPackage.QUALIFIED_NAME__RESOLVED:
				setResolved(RESOLVED_EDEFAULT);
				return;
			case HqlPackage.QUALIFIED_NAME__GRANULAR:
				setGranular(GRANULAR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HqlPackage.QUALIFIED_NAME__QUALIFIERS:
				return qualifiers != null && !qualifiers.isEmpty();
			case HqlPackage.QUALIFIED_NAME__RESOLVED:
				return resolved != RESOLVED_EDEFAULT;
			case HqlPackage.QUALIFIED_NAME__GRANULAR:
				return GRANULAR_EDEFAULT == null ? granular != null : !GRANULAR_EDEFAULT.equals(granular);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (resolved: ");
		result.append(resolved);
		result.append(", granular: ");
		result.append(granular);
		result.append(')');
		return result.toString();
	}

} //QualifiedNameImpl
