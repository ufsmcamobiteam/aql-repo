/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.HqlPackage;
import hql.Like;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Like</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LikeImpl extends BinaryExpressionImpl implements Like {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LikeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.LIKE;
	}

} //LikeImpl
