/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql.impl;

import hql.HqlPackage;
import hql.ObjectSource;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Source</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hql.impl.ObjectSourceImpl#getAlias <em>Alias</em>}</li>
 *   <li>{@link hql.impl.ObjectSourceImpl#getClassName <em>Class Name</em>}</li>
 *   <li>{@link hql.impl.ObjectSourceImpl#getResolvedPath <em>Resolved Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectSourceImpl extends FromExpressionImpl implements ObjectSource {
	/**
	 * The default value of the '{@link #getAlias() <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlias()
	 * @generated
	 * @ordered
	 */
	protected static final String ALIAS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAlias() <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlias()
	 * @generated
	 * @ordered
	 */
	protected String alias = ALIAS_EDEFAULT;

	/**
	 * The default value of the '{@link #getClassName() <em>Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassName()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClassName() <em>Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassName()
	 * @generated
	 * @ordered
	 */
	protected String className = CLASS_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getResolvedPath() <em>Resolved Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResolvedPath()
	 * @generated
	 * @ordered
	 */
	protected static final String RESOLVED_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResolvedPath() <em>Resolved Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResolvedPath()
	 * @generated
	 * @ordered
	 */
	protected String resolvedPath = RESOLVED_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectSourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HqlPackage.Literals.OBJECT_SOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlias(String newAlias) {
		String oldAlias = alias;
		alias = newAlias;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.OBJECT_SOURCE__ALIAS, oldAlias, alias));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassName(String newClassName) {
		String oldClassName = className;
		className = newClassName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.OBJECT_SOURCE__CLASS_NAME, oldClassName, className));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResolvedPath() {
		return resolvedPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResolvedPath(String newResolvedPath) {
		String oldResolvedPath = resolvedPath;
		resolvedPath = newResolvedPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HqlPackage.OBJECT_SOURCE__RESOLVED_PATH, oldResolvedPath, resolvedPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HqlPackage.OBJECT_SOURCE__ALIAS:
				return getAlias();
			case HqlPackage.OBJECT_SOURCE__CLASS_NAME:
				return getClassName();
			case HqlPackage.OBJECT_SOURCE__RESOLVED_PATH:
				return getResolvedPath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HqlPackage.OBJECT_SOURCE__ALIAS:
				setAlias((String)newValue);
				return;
			case HqlPackage.OBJECT_SOURCE__CLASS_NAME:
				setClassName((String)newValue);
				return;
			case HqlPackage.OBJECT_SOURCE__RESOLVED_PATH:
				setResolvedPath((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HqlPackage.OBJECT_SOURCE__ALIAS:
				setAlias(ALIAS_EDEFAULT);
				return;
			case HqlPackage.OBJECT_SOURCE__CLASS_NAME:
				setClassName(CLASS_NAME_EDEFAULT);
				return;
			case HqlPackage.OBJECT_SOURCE__RESOLVED_PATH:
				setResolvedPath(RESOLVED_PATH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HqlPackage.OBJECT_SOURCE__ALIAS:
				return ALIAS_EDEFAULT == null ? alias != null : !ALIAS_EDEFAULT.equals(alias);
			case HqlPackage.OBJECT_SOURCE__CLASS_NAME:
				return CLASS_NAME_EDEFAULT == null ? className != null : !CLASS_NAME_EDEFAULT.equals(className);
			case HqlPackage.OBJECT_SOURCE__RESOLVED_PATH:
				return RESOLVED_PATH_EDEFAULT == null ? resolvedPath != null : !RESOLVED_PATH_EDEFAULT.equals(resolvedPath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (alias: ");
		result.append(alias);
		result.append(", className: ");
		result.append(className);
		result.append(", resolvedPath: ");
		result.append(resolvedPath);
		result.append(')');
		return result.toString();
	}

} //ObjectSourceImpl
