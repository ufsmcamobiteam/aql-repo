/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Greater Equal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getGreaterEqual()
 * @model
 * @generated
 */
public interface GreaterEqual extends BinaryExpression {

} // GreaterEqual
