/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getNot()
 * @model
 * @generated
 */
public interface Not extends UnaryMinus {

} // Not
