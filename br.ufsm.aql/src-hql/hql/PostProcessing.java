/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Post Processing</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.PostProcessing#getSourceNode <em>Source Node</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getPostProcessing()
 * @model
 * @generated
 */
public interface PostProcessing extends Expression {
	/**
	 * Returns the value of the '<em><b>Source Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Node</em>' containment reference.
	 * @see #setSourceNode(EObject)
	 * @see hql.HqlPackage#getPostProcessing_SourceNode()
	 * @model containment="true"
	 * @generated
	 */
	EObject getSourceNode();

	/**
	 * Sets the value of the '{@link hql.PostProcessing#getSourceNode <em>Source Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Node</em>' containment reference.
	 * @see #getSourceNode()
	 * @generated
	 */
	void setSourceNode(EObject value);

} // PostProcessing
