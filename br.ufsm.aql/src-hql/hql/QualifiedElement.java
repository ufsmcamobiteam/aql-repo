/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getQualifiedElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface QualifiedElement extends Expression {
} // QualifiedElement
