/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Theta Join</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getThetaJoin()
 * @model
 * @generated
 */
public interface ThetaJoin extends BinaryFromExpression {
} // ThetaJoin
