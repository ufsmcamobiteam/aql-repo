/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getOr()
 * @model
 * @generated
 */
public interface Or extends BinaryExpression {

} // Or
