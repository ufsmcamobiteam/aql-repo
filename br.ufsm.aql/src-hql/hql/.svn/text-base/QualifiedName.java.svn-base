/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link hql.QualifiedName#getQualifiers <em>Qualifiers</em>}</li>
 *   <li>{@link hql.QualifiedName#isResolved <em>Resolved</em>}</li>
 *   <li>{@link hql.QualifiedName#getGranular <em>Granular</em>}</li>
 * </ul>
 * </p>
 *
 * @see hql.HqlPackage#getQualifiedName()
 * @model
 * @generated
 */
public interface QualifiedName extends Expression {
	/**
	 * Returns the value of the '<em><b>Qualifiers</b></em>' containment reference list.
	 * The list contents are of type {@link hql.QualifiedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifiers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifiers</em>' containment reference list.
	 * @see hql.HqlPackage#getQualifiedName_Qualifiers()
	 * @model containment="true"
	 * @generated
	 */
	EList<QualifiedElement> getQualifiers();

	/**
	 * Returns the value of the '<em><b>Resolved</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolved</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolved</em>' attribute.
	 * @see #setResolved(boolean)
	 * @see hql.HqlPackage#getQualifiedName_Resolved()
	 * @model default="false"
	 * @generated
	 */
	boolean isResolved();

	/**
	 * Sets the value of the '{@link hql.QualifiedName#isResolved <em>Resolved</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resolved</em>' attribute.
	 * @see #isResolved()
	 * @generated
	 */
	void setResolved(boolean value);

	/**
	 * Returns the value of the '<em><b>Granular</b></em>' attribute.
	 * The default value is <code>" "</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Granular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Granular</em>' attribute.
	 * @see #setGranular(String)
	 * @see hql.HqlPackage#getQualifiedName_Granular()
	 * @model default=" "
	 * @generated
	 */
	String getGranular();

	/**
	 * Sets the value of the '{@link hql.QualifiedName#getGranular <em>Granular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Granular</em>' attribute.
	 * @see #getGranular()
	 * @generated
	 */
	void setGranular(String value);

} // QualifiedName
