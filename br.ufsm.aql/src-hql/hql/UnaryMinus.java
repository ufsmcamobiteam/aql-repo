/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getUnaryMinus()
 * @model
 * @generated
 */
public interface UnaryMinus extends UnaryExpression {

} // UnaryMinus
