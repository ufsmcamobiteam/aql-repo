/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exponential</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getExponential()
 * @model
 * @generated
 */
public interface Exponential extends BinaryExpression {

} // Exponential
