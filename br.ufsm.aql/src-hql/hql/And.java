/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getAnd()
 * @model
 * @generated
 */
public interface And extends BinaryExpression {

} // And
