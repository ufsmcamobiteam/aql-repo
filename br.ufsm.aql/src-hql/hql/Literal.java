/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getLiteral()
 * @model
 * @generated
 */
public interface Literal extends Expression {

} // Literal
