/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Order By Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.OrderByClause#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link hql.OrderByClause#getOption <em>Option</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getOrderByClause()
 * @model
 * @generated
 */
public interface OrderByClause extends Clause {
	/**
	 * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link hql.QualifiedName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expressions</em>' containment reference list.
	 * @see hql.HqlPackage#getOrderByClause_Expressions()
	 * @model containment="true"
	 * @generated
	 */
	EList<QualifiedName> getExpressions();

	/**
	 * Returns the value of the '<em><b>Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Option</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Option</em>' attribute.
	 * @see #setOption(char)
	 * @see hql.HqlPackage#getOrderByClause_Option()
	 * @model
	 * @generated
	 */
	char getOption();

	/**
	 * Sets the value of the '{@link hql.OrderByClause#getOption <em>Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Option</em>' attribute.
	 * @see #getOption()
	 * @generated
	 */
	void setOption(char value);

} // OrderByClause
