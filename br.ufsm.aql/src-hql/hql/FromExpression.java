/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getFromExpression()
 * @model abstract="true"
 * @generated
 */
public interface FromExpression extends EObject {
} // FromExpression
