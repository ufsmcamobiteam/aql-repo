/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Like</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getLike()
 * @model
 * @generated
 */
public interface Like extends BinaryExpression {
} // Like
