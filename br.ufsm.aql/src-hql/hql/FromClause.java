/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.FromClause#getObjectList <em>Object List</em>}</li>
 *   <li>{@link hql.FromClause#getBinaryObjectList <em>Binary Object List</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getFromClause()
 * @model
 * @generated
 */
public interface FromClause extends Clause {
	/**
	 * Returns the value of the '<em><b>Object List</b></em>' containment reference list.
	 * The list contents are of type {@link hql.FromExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object List</em>' containment reference list.
	 * @see hql.HqlPackage#getFromClause_ObjectList()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<FromExpression> getObjectList();

	/**
	 * Returns the value of the '<em><b>Binary Object List</b></em>' containment reference list.
	 * The list contents are of type {@link hql.BinaryFromExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binary Object List</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary Object List</em>' containment reference list.
	 * @see hql.HqlPackage#getFromClause_BinaryObjectList()
	 * @model containment="true"
	 * @generated
	 */
	EList<BinaryFromExpression> getBinaryObjectList();

} // FromClause
