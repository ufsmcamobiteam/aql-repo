/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see hql.HqlPackage#getMinus()
 * @model
 * @generated
 */
public interface Minus extends BinaryExpression {

} // Minus
