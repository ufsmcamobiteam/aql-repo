/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package hql;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hql.Query#getQueryStatement <em>Query Statement</em>}</li>
 * </ul>
 *
 * @see hql.HqlPackage#getQuery()
 * @model
 * @generated
 */
public interface Query extends EObject {
	/**
	 * Returns the value of the '<em><b>Query Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query Statement</em>' containment reference.
	 * @see #setQueryStatement(QueryStatement)
	 * @see hql.HqlPackage#getQuery_QueryStatement()
	 * @model containment="true"
	 * @generated
	 */
	QueryStatement getQueryStatement();

	/**
	 * Sets the value of the '{@link hql.Query#getQueryStatement <em>Query Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Query Statement</em>' containment reference.
	 * @see #getQueryStatement()
	 * @generated
	 */
	void setQueryStatement(QueryStatement value);

} // Query
