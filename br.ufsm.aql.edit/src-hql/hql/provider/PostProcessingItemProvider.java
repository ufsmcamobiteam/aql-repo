/**
 */
package hql.provider;


import hql.HqlFactory;
import hql.HqlPackage;
import hql.PostProcessing;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link hql.PostProcessing} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PostProcessingItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PostProcessingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns PostProcessing.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/PostProcessing"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_PostProcessing_type");
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(PostProcessing.class)) {
			case HqlPackage.POST_PROCESSING__SOURCE_NODE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createQuery()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createQueryStatement()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createObjectSource()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createWhereClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createFromClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createGreater()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createGreaterEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createLess()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createLessEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createNotEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createAdd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createMult()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createDiv()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createMod()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createExponential()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createQualifiedName()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createStringLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createIntegerLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createTrue()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createFalse()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createSelectClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createParsExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createFloatLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createBinaryFromExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createInnerJoin()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createAlias()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createInClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createPostProcessing()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createThetaJoin()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createLeftJoin()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createLike()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.POST_PROCESSING__SOURCE_NODE,
				 HqlFactory.eINSTANCE.createOrderByClause()));
	}

}
