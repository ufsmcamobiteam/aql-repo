/**
 */
package hql.provider;


import hql.HqlFactory;
import hql.HqlPackage;
import hql.SelectClause;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link hql.SelectClause} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SelectClauseItemProvider extends ClauseItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectClauseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAliasPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Alias feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAliasPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SelectClause_alias_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SelectClause_alias_feature", "_UI_SelectClause_type"),
				 HqlPackage.Literals.SELECT_CLAUSE__ALIAS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SelectClause.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SelectClause"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SelectClause)object).getClause();
		return label == null || label.length() == 0 ?
			getString("_UI_SelectClause_type") :
			getString("_UI_SelectClause_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SelectClause.class)) {
			case HqlPackage.SELECT_CLAUSE__ALIAS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case HqlPackage.SELECT_CLAUSE__EXPRESSIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createGreater()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createGreaterEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createLess()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createLessEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createNotEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createAdd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createMult()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createDiv()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createMod()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createExponential()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createQualifiedName()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createStringLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createIntegerLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createTrue()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createFalse()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createParsExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createFloatLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createAlias()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createInClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createPostProcessing()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.SELECT_CLAUSE__EXPRESSIONS,
				 HqlFactory.eINSTANCE.createLike()));
	}

}
