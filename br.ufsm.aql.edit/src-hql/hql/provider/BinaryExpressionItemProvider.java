/**
 */
package hql.provider;


import hql.BinaryExpression;
import hql.HqlFactory;
import hql.HqlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link hql.BinaryExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BinaryExpressionItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(HqlPackage.Literals.BINARY_EXPRESSION__LEFT);
			childrenFeatures.add(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BinaryExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BinaryExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_BinaryExpression_type");
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BinaryExpression.class)) {
			case HqlPackage.BINARY_EXPRESSION__LEFT:
			case HqlPackage.BINARY_EXPRESSION__RIGHT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createGreater()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createGreaterEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createLess()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createLessEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createNotEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createAdd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createMult()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createDiv()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createMod()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createExponential()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createQualifiedName()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createStringLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createIntegerLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createTrue()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createFalse()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createParsExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createFloatLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createAlias()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createInClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createPostProcessing()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__LEFT,
				 HqlFactory.eINSTANCE.createLike()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createGreater()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createGreaterEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createLess()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createLessEqual()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createNotEquals()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createAdd()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createMult()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createDiv()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createMod()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createExponential()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createQualifiedName()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createStringLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createIntegerLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createTrue()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createFalse()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createParsExpression()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createFloatLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createAlias()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createInClause()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createPostProcessing()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(HqlPackage.Literals.BINARY_EXPRESSION__RIGHT,
				 HqlFactory.eINSTANCE.createLike()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == HqlPackage.Literals.BINARY_EXPRESSION__LEFT ||
			childFeature == HqlPackage.Literals.BINARY_EXPRESSION__RIGHT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
