<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="aql"
            id="br.ufsm.AQL"
            name="AQL Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="br.ufsm.AQL.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="br.ufsm.AQL.validate">
         <activeWhen>
            <reference
                    definitionId="br.ufsm.AQL.Editor.opened">
            </reference>
         </activeWhen>
      	</handler>
      	<!-- copy qualified name -->
        <handler
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
            <activeWhen>
				<reference definitionId="br.ufsm.AQL.Editor.opened" />
            </activeWhen>
        </handler>
        <handler
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
            <activeWhen>
            	<and>
            		<reference definitionId="br.ufsm.AQL.XtextEditor.opened" />
	                <iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
            </activeWhen>
        </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="br.ufsm.AQL.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="br.ufsm.AQL" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
        <definition id="br.ufsm.AQL.XtextEditor.opened">
            <and>
                <reference definitionId="isXtextEditorActive"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="br.ufsm.AQL" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="br.ufsm.AQL"
            name="AQL">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
        </page>
        <page
            category="br.ufsm.AQL"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="br.ufsm.AQL.coloring"
            name="Syntax Coloring">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
        </page>
        <page
            category="br.ufsm.AQL"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="br.ufsm.AQL.templates"
            name="Templates">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="br.ufsm.AQL"
            name="AQL">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="br.ufsm.ui.keyword_AQL"
            label="AQL"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="br.ufsm.AQL.validate"
            name="Validate">
      </command>
      <!-- copy qualified name -->
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="br.ufsm.AQL.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="br.ufsm.AQL.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
         <!-- copy qualified name -->
         <menuContribution locationURI="popup:#TextEditorContext?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName" 
         		style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="br.ufsm.AQL.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="menu:edit?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            	style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="br.ufsm.AQL.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName" 
				style="push" tooltip="Copy Qualified Name">
         		<visibleWhen checkEnabled="false">
	            	<and>
	            		<reference definitionId="br.ufsm.AQL.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="br.ufsm.AQL.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="br.ufsm.AQL.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="aql">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="aql">
        </resourceServiceProvider>
    </extension>


	<!-- marker definitions for br.ufsm.AQL -->
	<extension
	        id="aql.check.fast"
	        name="AQL Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.fast"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="aql.check.normal"
	        name="AQL Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.normal"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="aql.check.expensive"
	        name="AQL Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.expensive"/>
	    <persistent value="true"/>
	</extension>

   <extension
         point="org.eclipse.xtext.builder.participant">
      <participant
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.builder.IXtextBuilderParticipant"
            fileExtensions="aql"
            >
      </participant>
   </extension>
   <extension
            point="org.eclipse.ui.preferencePages">
        <page
            category="br.ufsm.AQL"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="br.ufsm.AQL.compiler.preferencePage"
            name="Compiler">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            category="br.ufsm.AQL"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="br.ufsm.AQL.compiler.propertyPage"
            name="Compiler">
            <keywordReference id="br.ufsm.ui.keyword_AQL"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>

	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler 
			class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="br.ufsm.AQL.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="br.ufsm.AQL.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
    <!-- quickfix marker resolution generator for br.ufsm.AQL -->
    <extension
            point="org.eclipse.ui.ide.markerResolution">
        <markerResolutionGenerator
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="br.ufsm.ui.aql.check.fast">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="br.ufsm.ui.aql.check.normal">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="br.ufsm.ui.aql.check.expensive">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
    </extension>
   	<!-- Rename Refactoring -->
	<extension point="org.eclipse.ui.handlers">
		<handler 
			class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.DefaultRenameElementHandler"
			commandId="org.eclipse.xtext.ui.refactoring.RenameElement">
			<activeWhen>
				<reference
					definitionId="br.ufsm.AQL.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
    <extension point="org.eclipse.ui.menus">
         <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
         <command commandId="org.eclipse.xtext.ui.refactoring.RenameElement"
               style="push">
            <visibleWhen checkEnabled="false">
               <reference
                     definitionId="br.ufsm.AQL.Editor.opened">
               </reference>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>
   <extension point="org.eclipse.ui.preferencePages">
	    <page
	        category="br.ufsm.AQL"
	        class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.RefactoringPreferencePage"
	        id="br.ufsm.AQL.refactoring"
	        name="Refactoring">
	        <keywordReference id="br.ufsm.ui.keyword_AQL"/>
	    </page>
	</extension>

  <extension point="org.eclipse.compare.contentViewers">
    <viewer id="br.ufsm.AQL.compare.contentViewers"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="aql">
    </viewer>
  </extension>
  <extension point="org.eclipse.compare.contentMergeViewers">
    <viewer id="br.ufsm.AQL.compare.contentMergeViewers"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="aql" label="AQL Compare">
     </viewer>
  </extension>
  <extension point="org.eclipse.ui.editors.documentProviders">
    <provider id="br.ufsm.AQL.editors.documentProviders"
            class="br.ufsm.ui.AQLExecutableExtensionFactory:org.eclipse.xtext.ui.editor.model.XtextDocumentProvider"
            extensions="aql">
    </provider>
  </extension>

</plugin>
