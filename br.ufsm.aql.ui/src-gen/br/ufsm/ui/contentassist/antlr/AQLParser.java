/*
* generated by Xtext
*/
package br.ufsm.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import br.ufsm.services.AQLGrammarAccess;

public class AQLParser extends AbstractContentAssistParser {
	
	@Inject
	private AQLGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected br.ufsm.ui.contentassist.antlr.internal.InternalAQLParser createParser() {
		br.ufsm.ui.contentassist.antlr.internal.InternalAQLParser result = new br.ufsm.ui.contentassist.antlr.internal.InternalAQLParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getObjectTypeAccess().getAlternatives(), "rule__ObjectType__Alternatives");
					put(grammarAccess.getOrderByClauseAccess().getOptionAlternatives_3_0(), "rule__OrderByClause__OptionAlternatives_3_0");
					put(grammarAccess.getRelationAccess().getAlternatives_1_0(), "rule__Relation__Alternatives_1_0");
					put(grammarAccess.getAddAccess().getAlternatives_1_0(), "rule__Add__Alternatives_1_0");
					put(grammarAccess.getMultAccess().getAlternatives_1_0(), "rule__Mult__Alternatives_1_0");
					put(grammarAccess.getUnaryAccess().getAlternatives(), "rule__Unary__Alternatives");
					put(grammarAccess.getAtomAccess().getAlternatives(), "rule__Atom__Alternatives");
					put(grammarAccess.getLiteralAccess().getAlternatives(), "rule__Literal__Alternatives");
					put(grammarAccess.getQualifiedNameAccess().getGranularAlternatives_0_0(), "rule__QualifiedName__GranularAlternatives_0_0");
					put(grammarAccess.getQualifiedElementAccess().getAlternatives(), "rule__QualifiedElement__Alternatives");
					put(grammarAccess.getQueryStatementAccess().getGroup(), "rule__QueryStatement__Group__0");
					put(grammarAccess.getFindClauseAccess().getGroup(), "rule__FindClause__Group__0");
					put(grammarAccess.getFindClauseAccess().getGroup_2(), "rule__FindClause__Group_2__0");
					put(grammarAccess.getBindingObjectAccess().getGroup(), "rule__BindingObject__Group__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_0(), "rule__ObjectType__Group_0__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_1(), "rule__ObjectType__Group_1__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_2(), "rule__ObjectType__Group_2__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_3(), "rule__ObjectType__Group_3__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_4(), "rule__ObjectType__Group_4__0");
					put(grammarAccess.getObjectTypeAccess().getGroup_5(), "rule__ObjectType__Group_5__0");
					put(grammarAccess.getWhereClauseAccess().getGroup(), "rule__WhereClause__Group__0");
					put(grammarAccess.getReturnsClauseAccess().getGroup(), "rule__ReturnsClause__Group__0");
					put(grammarAccess.getReturnsClauseAccess().getGroup_2(), "rule__ReturnsClause__Group_2__0");
					put(grammarAccess.getReturnsClauseAccess().getGroup_3(), "rule__ReturnsClause__Group_3__0");
					put(grammarAccess.getReturnsClauseAccess().getGroup_3_2(), "rule__ReturnsClause__Group_3_2__0");
					put(grammarAccess.getGroupByClauseAccess().getGroup(), "rule__GroupByClause__Group__0");
					put(grammarAccess.getGroupByClauseAccess().getGroup_2(), "rule__GroupByClause__Group_2__0");
					put(grammarAccess.getOrderByClauseAccess().getGroup(), "rule__OrderByClause__Group__0");
					put(grammarAccess.getOrderByClauseAccess().getGroup_2(), "rule__OrderByClause__Group_2__0");
					put(grammarAccess.getOrderByClauseAccess().getGroup_4(), "rule__OrderByClause__Group_4__0");
					put(grammarAccess.getSimpleQualifiedNameAccess().getGroup(), "rule__SimpleQualifiedName__Group__0");
					put(grammarAccess.getSimpleQualifiedNameAccess().getGroup_1(), "rule__SimpleQualifiedName__Group_1__0");
					put(grammarAccess.getOrAccess().getGroup(), "rule__Or__Group__0");
					put(grammarAccess.getOrAccess().getGroup_1(), "rule__Or__Group_1__0");
					put(grammarAccess.getOrAccess().getGroup_1_0(), "rule__Or__Group_1_0__0");
					put(grammarAccess.getAndAccess().getGroup(), "rule__And__Group__0");
					put(grammarAccess.getAndAccess().getGroup_1(), "rule__And__Group_1__0");
					put(grammarAccess.getRelationAccess().getGroup(), "rule__Relation__Group__0");
					put(grammarAccess.getRelationAccess().getGroup_1(), "rule__Relation__Group_1__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_0(), "rule__Relation__Group_1_0_0__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_1(), "rule__Relation__Group_1_0_1__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_2(), "rule__Relation__Group_1_0_2__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_3(), "rule__Relation__Group_1_0_3__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_4(), "rule__Relation__Group_1_0_4__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_5(), "rule__Relation__Group_1_0_5__0");
					put(grammarAccess.getRelationAccess().getGroup_1_0_6(), "rule__Relation__Group_1_0_6__0");
					put(grammarAccess.getAddAccess().getGroup(), "rule__Add__Group__0");
					put(grammarAccess.getAddAccess().getGroup_1(), "rule__Add__Group_1__0");
					put(grammarAccess.getAddAccess().getGroup_1_0_0(), "rule__Add__Group_1_0_0__0");
					put(grammarAccess.getAddAccess().getGroup_1_0_1(), "rule__Add__Group_1_0_1__0");
					put(grammarAccess.getMultAccess().getGroup(), "rule__Mult__Group__0");
					put(grammarAccess.getMultAccess().getGroup_1(), "rule__Mult__Group_1__0");
					put(grammarAccess.getMultAccess().getGroup_1_0_0(), "rule__Mult__Group_1_0_0__0");
					put(grammarAccess.getMultAccess().getGroup_1_0_1(), "rule__Mult__Group_1_0_1__0");
					put(grammarAccess.getMultAccess().getGroup_1_0_2(), "rule__Mult__Group_1_0_2__0");
					put(grammarAccess.getInAccess().getGroup(), "rule__In__Group__0");
					put(grammarAccess.getInAccess().getGroup_1(), "rule__In__Group_1__0");
					put(grammarAccess.getInAccess().getGroup_1_0(), "rule__In__Group_1_0__0");
					put(grammarAccess.getUnaryAccess().getGroup_1(), "rule__Unary__Group_1__0");
					put(grammarAccess.getUnaryAccess().getGroup_2(), "rule__Unary__Group_2__0");
					put(grammarAccess.getExponentialAccess().getGroup(), "rule__Exponential__Group__0");
					put(grammarAccess.getExponentialAccess().getGroup_1(), "rule__Exponential__Group_1__0");
					put(grammarAccess.getParsExpressionAccess().getGroup(), "rule__ParsExpression__Group__0");
					put(grammarAccess.getParsExpressionAccess().getGroup_3(), "rule__ParsExpression__Group_3__0");
					put(grammarAccess.getLiteralAccess().getGroup_0(), "rule__Literal__Group_0__0");
					put(grammarAccess.getLiteralAccess().getGroup_1(), "rule__Literal__Group_1__0");
					put(grammarAccess.getLiteralAccess().getGroup_2(), "rule__Literal__Group_2__0");
					put(grammarAccess.getLiteralAccess().getGroup_3(), "rule__Literal__Group_3__0");
					put(grammarAccess.getLiteralAccess().getGroup_4(), "rule__Literal__Group_4__0");
					put(grammarAccess.getLiteralAccess().getGroup_5(), "rule__Literal__Group_5__0");
					put(grammarAccess.getQualifiedNameAccess().getGroup(), "rule__QualifiedName__Group__0");
					put(grammarAccess.getQualifiedNameAccess().getGroup_2(), "rule__QualifiedName__Group_2__0");
					put(grammarAccess.getFunctionAccess().getGroup(), "rule__Function__Group__0");
					put(grammarAccess.getFunctionAccess().getGroup_3(), "rule__Function__Group_3__0");
					put(grammarAccess.getQueryAccess().getQueryAssignment(), "rule__Query__QueryAssignment");
					put(grammarAccess.getQueryStatementAccess().getFindAssignment_0(), "rule__QueryStatement__FindAssignment_0");
					put(grammarAccess.getQueryStatementAccess().getWhereAssignment_1(), "rule__QueryStatement__WhereAssignment_1");
					put(grammarAccess.getQueryStatementAccess().getReturnAssignment_2(), "rule__QueryStatement__ReturnAssignment_2");
					put(grammarAccess.getQueryStatementAccess().getOrderbyAssignment_3(), "rule__QueryStatement__OrderbyAssignment_3");
					put(grammarAccess.getQueryStatementAccess().getGroupbyAssignment_4(), "rule__QueryStatement__GroupbyAssignment_4");
					put(grammarAccess.getFindClauseAccess().getClauseAssignment_0(), "rule__FindClause__ClauseAssignment_0");
					put(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_1(), "rule__FindClause__BindingObjectAssignment_1");
					put(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_2_1(), "rule__FindClause__BindingObjectAssignment_2_1");
					put(grammarAccess.getBindingObjectAccess().getTypeAssignment_0(), "rule__BindingObject__TypeAssignment_0");
					put(grammarAccess.getBindingObjectAccess().getAliasAssignment_1(), "rule__BindingObject__AliasAssignment_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_0_1(), "rule__ObjectType__ValueAssignment_0_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_1_1(), "rule__ObjectType__ValueAssignment_1_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_2_1(), "rule__ObjectType__ValueAssignment_2_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_3_1(), "rule__ObjectType__ValueAssignment_3_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_4_1(), "rule__ObjectType__ValueAssignment_4_1");
					put(grammarAccess.getObjectTypeAccess().getValueAssignment_5_1(), "rule__ObjectType__ValueAssignment_5_1");
					put(grammarAccess.getWhereClauseAccess().getClauseAssignment_0(), "rule__WhereClause__ClauseAssignment_0");
					put(grammarAccess.getWhereClauseAccess().getExpressionAssignment_1(), "rule__WhereClause__ExpressionAssignment_1");
					put(grammarAccess.getReturnsClauseAccess().getClauseAssignment_0(), "rule__ReturnsClause__ClauseAssignment_0");
					put(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_1(), "rule__ReturnsClause__ExpressionsAssignment_1");
					put(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_2_1(), "rule__ReturnsClause__ResultAliasAssignment_2_1");
					put(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_3_1(), "rule__ReturnsClause__ExpressionsAssignment_3_1");
					put(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_3_2_1(), "rule__ReturnsClause__ResultAliasAssignment_3_2_1");
					put(grammarAccess.getGroupByClauseAccess().getClauseAssignment_0(), "rule__GroupByClause__ClauseAssignment_0");
					put(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_1(), "rule__GroupByClause__ExpressionsAssignment_1");
					put(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_2_1(), "rule__GroupByClause__ExpressionsAssignment_2_1");
					put(grammarAccess.getOrderByClauseAccess().getClauseAssignment_0(), "rule__OrderByClause__ClauseAssignment_0");
					put(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_1(), "rule__OrderByClause__ExpressionsAssignment_1");
					put(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_2_1(), "rule__OrderByClause__ExpressionsAssignment_2_1");
					put(grammarAccess.getOrderByClauseAccess().getOptionAssignment_3(), "rule__OrderByClause__OptionAssignment_3");
					put(grammarAccess.getOrderByClauseAccess().getHavingExpressionAssignment_4_1(), "rule__OrderByClause__HavingExpressionAssignment_4_1");
					put(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_0(), "rule__SimpleQualifiedName__QualifiersAssignment_0");
					put(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_1_1(), "rule__SimpleQualifiedName__QualifiersAssignment_1_1");
					put(grammarAccess.getOrAccess().getRightAssignment_1_1(), "rule__Or__RightAssignment_1_1");
					put(grammarAccess.getAndAccess().getRightAssignment_1_2(), "rule__And__RightAssignment_1_2");
					put(grammarAccess.getRelationAccess().getRightAssignment_1_1(), "rule__Relation__RightAssignment_1_1");
					put(grammarAccess.getAddAccess().getRightAssignment_1_1(), "rule__Add__RightAssignment_1_1");
					put(grammarAccess.getMultAccess().getRightAssignment_1_1(), "rule__Mult__RightAssignment_1_1");
					put(grammarAccess.getInAccess().getRightAssignment_1_1(), "rule__In__RightAssignment_1_1");
					put(grammarAccess.getUnaryAccess().getExpAssignment_1_2(), "rule__Unary__ExpAssignment_1_2");
					put(grammarAccess.getUnaryAccess().getExprAssignment_2_2(), "rule__Unary__ExprAssignment_2_2");
					put(grammarAccess.getExponentialAccess().getRightAssignment_1_2(), "rule__Exponential__RightAssignment_1_2");
					put(grammarAccess.getParsExpressionAccess().getExprAssignment_2(), "rule__ParsExpression__ExprAssignment_2");
					put(grammarAccess.getParsExpressionAccess().getExprAssignment_3_1(), "rule__ParsExpression__ExprAssignment_3_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_0_1(), "rule__Literal__ValueAssignment_0_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_1_1(), "rule__Literal__ValueAssignment_1_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_2_1(), "rule__Literal__ValueAssignment_2_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_3_1(), "rule__Literal__ValueAssignment_3_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_4_1(), "rule__Literal__ValueAssignment_4_1");
					put(grammarAccess.getLiteralAccess().getValueAssignment_5_1(), "rule__Literal__ValueAssignment_5_1");
					put(grammarAccess.getQualifiedNameAccess().getGranularAssignment_0(), "rule__QualifiedName__GranularAssignment_0");
					put(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_1(), "rule__QualifiedName__QualifiersAssignment_1");
					put(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_2_1(), "rule__QualifiedName__QualifiersAssignment_2_1");
					put(grammarAccess.getFunctionAccess().getNameAssignment_0(), "rule__Function__NameAssignment_0");
					put(grammarAccess.getFunctionAccess().getParamsAssignment_2(), "rule__Function__ParamsAssignment_2");
					put(grammarAccess.getFunctionAccess().getParamsAssignment_3_1(), "rule__Function__ParamsAssignment_3_1");
					put(grammarAccess.getIdentifierAccess().getIdAssignment(), "rule__Identifier__IdAssignment");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			br.ufsm.ui.contentassist.antlr.internal.InternalAQLParser typedParser = (br.ufsm.ui.contentassist.antlr.internal.InternalAQLParser) parser;
			typedParser.entryRuleQuery();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public AQLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(AQLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
