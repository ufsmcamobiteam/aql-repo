package br.ufsm.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import br.ufsm.services.AQLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAQLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_FLOAT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'asc'", "'desc'", "'distinct'", "'all'", "','", "'as'", "'having'", "'.'", "'or'", "'and'", "'='", "'>'", "'>='", "'<'", "'<='", "'like'", "'!='", "'+'", "'-'", "'*'", "'/'", "'%'", "'in'", "'not'", "'^'", "'('", "')'", "'find'", "'project'", "'package'", "'class'", "'aspect'", "'interface'", "'enum'", "'where'", "'returns'", "'group by'", "'order by'", "'null'", "'true'", "'false'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=7;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_FLOAT=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=5;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=10;

    // delegates
    // delegators


        public InternalAQLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAQLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAQLParser.tokenNames; }
    public String getGrammarFileName() { return "../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g"; }


     
     	private AQLGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(AQLGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleQuery"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:60:1: entryRuleQuery : ruleQuery EOF ;
    public final void entryRuleQuery() throws RecognitionException {

        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");

        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:64:1: ( ruleQuery EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:65:1: ruleQuery EOF
            {
             before(grammarAccess.getQueryRule()); 
            pushFollow(FOLLOW_ruleQuery_in_entryRuleQuery66);
            ruleQuery();

            state._fsp--;

             after(grammarAccess.getQueryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuery73); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "entryRuleQuery"


    // $ANTLR start "ruleQuery"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:75:1: ruleQuery : ( ( rule__Query__QueryAssignment ) ) ;
    public final void ruleQuery() throws RecognitionException {

        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:80:2: ( ( ( rule__Query__QueryAssignment ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:81:1: ( ( rule__Query__QueryAssignment ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:81:1: ( ( rule__Query__QueryAssignment ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:82:1: ( rule__Query__QueryAssignment )
            {
             before(grammarAccess.getQueryAccess().getQueryAssignment()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:83:1: ( rule__Query__QueryAssignment )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:83:2: rule__Query__QueryAssignment
            {
            pushFollow(FOLLOW_rule__Query__QueryAssignment_in_ruleQuery103);
            rule__Query__QueryAssignment();

            state._fsp--;


            }

             after(grammarAccess.getQueryAccess().getQueryAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);
            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "ruleQuery"


    // $ANTLR start "entryRuleQueryStatement"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:96:1: entryRuleQueryStatement : ruleQueryStatement EOF ;
    public final void entryRuleQueryStatement() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:97:1: ( ruleQueryStatement EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:98:1: ruleQueryStatement EOF
            {
             before(grammarAccess.getQueryStatementRule()); 
            pushFollow(FOLLOW_ruleQueryStatement_in_entryRuleQueryStatement130);
            ruleQueryStatement();

            state._fsp--;

             after(grammarAccess.getQueryStatementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQueryStatement137); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQueryStatement"


    // $ANTLR start "ruleQueryStatement"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:105:1: ruleQueryStatement : ( ( rule__QueryStatement__Group__0 ) ) ;
    public final void ruleQueryStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:109:2: ( ( ( rule__QueryStatement__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:110:1: ( ( rule__QueryStatement__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:110:1: ( ( rule__QueryStatement__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:111:1: ( rule__QueryStatement__Group__0 )
            {
             before(grammarAccess.getQueryStatementAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:112:1: ( rule__QueryStatement__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:112:2: rule__QueryStatement__Group__0
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__0_in_ruleQueryStatement163);
            rule__QueryStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQueryStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQueryStatement"


    // $ANTLR start "entryRuleFindClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:124:1: entryRuleFindClause : ruleFindClause EOF ;
    public final void entryRuleFindClause() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:125:1: ( ruleFindClause EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:126:1: ruleFindClause EOF
            {
             before(grammarAccess.getFindClauseRule()); 
            pushFollow(FOLLOW_ruleFindClause_in_entryRuleFindClause190);
            ruleFindClause();

            state._fsp--;

             after(grammarAccess.getFindClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFindClause197); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFindClause"


    // $ANTLR start "ruleFindClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:133:1: ruleFindClause : ( ( rule__FindClause__Group__0 ) ) ;
    public final void ruleFindClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:137:2: ( ( ( rule__FindClause__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:138:1: ( ( rule__FindClause__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:138:1: ( ( rule__FindClause__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:139:1: ( rule__FindClause__Group__0 )
            {
             before(grammarAccess.getFindClauseAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:140:1: ( rule__FindClause__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:140:2: rule__FindClause__Group__0
            {
            pushFollow(FOLLOW_rule__FindClause__Group__0_in_ruleFindClause223);
            rule__FindClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFindClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFindClause"


    // $ANTLR start "entryRuleBindingObject"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:152:1: entryRuleBindingObject : ruleBindingObject EOF ;
    public final void entryRuleBindingObject() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:153:1: ( ruleBindingObject EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:154:1: ruleBindingObject EOF
            {
             before(grammarAccess.getBindingObjectRule()); 
            pushFollow(FOLLOW_ruleBindingObject_in_entryRuleBindingObject250);
            ruleBindingObject();

            state._fsp--;

             after(grammarAccess.getBindingObjectRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBindingObject257); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBindingObject"


    // $ANTLR start "ruleBindingObject"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:161:1: ruleBindingObject : ( ( rule__BindingObject__Group__0 ) ) ;
    public final void ruleBindingObject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:165:2: ( ( ( rule__BindingObject__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:166:1: ( ( rule__BindingObject__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:166:1: ( ( rule__BindingObject__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:167:1: ( rule__BindingObject__Group__0 )
            {
             before(grammarAccess.getBindingObjectAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:168:1: ( rule__BindingObject__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:168:2: rule__BindingObject__Group__0
            {
            pushFollow(FOLLOW_rule__BindingObject__Group__0_in_ruleBindingObject283);
            rule__BindingObject__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBindingObjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBindingObject"


    // $ANTLR start "entryRuleObjectType"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:180:1: entryRuleObjectType : ruleObjectType EOF ;
    public final void entryRuleObjectType() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:181:1: ( ruleObjectType EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:182:1: ruleObjectType EOF
            {
             before(grammarAccess.getObjectTypeRule()); 
            pushFollow(FOLLOW_ruleObjectType_in_entryRuleObjectType310);
            ruleObjectType();

            state._fsp--;

             after(grammarAccess.getObjectTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleObjectType317); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectType"


    // $ANTLR start "ruleObjectType"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:189:1: ruleObjectType : ( ( rule__ObjectType__Alternatives ) ) ;
    public final void ruleObjectType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:193:2: ( ( ( rule__ObjectType__Alternatives ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:194:1: ( ( rule__ObjectType__Alternatives ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:194:1: ( ( rule__ObjectType__Alternatives ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:195:1: ( rule__ObjectType__Alternatives )
            {
             before(grammarAccess.getObjectTypeAccess().getAlternatives()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:196:1: ( rule__ObjectType__Alternatives )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:196:2: rule__ObjectType__Alternatives
            {
            pushFollow(FOLLOW_rule__ObjectType__Alternatives_in_ruleObjectType343);
            rule__ObjectType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectType"


    // $ANTLR start "entryRuleWhereClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:208:1: entryRuleWhereClause : ruleWhereClause EOF ;
    public final void entryRuleWhereClause() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:209:1: ( ruleWhereClause EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:210:1: ruleWhereClause EOF
            {
             before(grammarAccess.getWhereClauseRule()); 
            pushFollow(FOLLOW_ruleWhereClause_in_entryRuleWhereClause370);
            ruleWhereClause();

            state._fsp--;

             after(grammarAccess.getWhereClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhereClause377); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhereClause"


    // $ANTLR start "ruleWhereClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:217:1: ruleWhereClause : ( ( rule__WhereClause__Group__0 ) ) ;
    public final void ruleWhereClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:221:2: ( ( ( rule__WhereClause__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:222:1: ( ( rule__WhereClause__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:222:1: ( ( rule__WhereClause__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:223:1: ( rule__WhereClause__Group__0 )
            {
             before(grammarAccess.getWhereClauseAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:224:1: ( rule__WhereClause__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:224:2: rule__WhereClause__Group__0
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__0_in_ruleWhereClause403);
            rule__WhereClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhereClause"


    // $ANTLR start "entryRuleReturnsClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:236:1: entryRuleReturnsClause : ruleReturnsClause EOF ;
    public final void entryRuleReturnsClause() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:237:1: ( ruleReturnsClause EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:238:1: ruleReturnsClause EOF
            {
             before(grammarAccess.getReturnsClauseRule()); 
            pushFollow(FOLLOW_ruleReturnsClause_in_entryRuleReturnsClause430);
            ruleReturnsClause();

            state._fsp--;

             after(grammarAccess.getReturnsClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReturnsClause437); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReturnsClause"


    // $ANTLR start "ruleReturnsClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:245:1: ruleReturnsClause : ( ( rule__ReturnsClause__Group__0 ) ) ;
    public final void ruleReturnsClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:249:2: ( ( ( rule__ReturnsClause__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:250:1: ( ( rule__ReturnsClause__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:250:1: ( ( rule__ReturnsClause__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:251:1: ( rule__ReturnsClause__Group__0 )
            {
             before(grammarAccess.getReturnsClauseAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:252:1: ( rule__ReturnsClause__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:252:2: rule__ReturnsClause__Group__0
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group__0_in_ruleReturnsClause463);
            rule__ReturnsClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReturnsClause"


    // $ANTLR start "entryRuleGroupByClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:264:1: entryRuleGroupByClause : ruleGroupByClause EOF ;
    public final void entryRuleGroupByClause() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:265:1: ( ruleGroupByClause EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:266:1: ruleGroupByClause EOF
            {
             before(grammarAccess.getGroupByClauseRule()); 
            pushFollow(FOLLOW_ruleGroupByClause_in_entryRuleGroupByClause490);
            ruleGroupByClause();

            state._fsp--;

             after(grammarAccess.getGroupByClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleGroupByClause497); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGroupByClause"


    // $ANTLR start "ruleGroupByClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:273:1: ruleGroupByClause : ( ( rule__GroupByClause__Group__0 ) ) ;
    public final void ruleGroupByClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:277:2: ( ( ( rule__GroupByClause__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:278:1: ( ( rule__GroupByClause__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:278:1: ( ( rule__GroupByClause__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:279:1: ( rule__GroupByClause__Group__0 )
            {
             before(grammarAccess.getGroupByClauseAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:280:1: ( rule__GroupByClause__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:280:2: rule__GroupByClause__Group__0
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group__0_in_ruleGroupByClause523);
            rule__GroupByClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGroupByClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGroupByClause"


    // $ANTLR start "entryRuleOrderByClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:292:1: entryRuleOrderByClause : ruleOrderByClause EOF ;
    public final void entryRuleOrderByClause() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:293:1: ( ruleOrderByClause EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:294:1: ruleOrderByClause EOF
            {
             before(grammarAccess.getOrderByClauseRule()); 
            pushFollow(FOLLOW_ruleOrderByClause_in_entryRuleOrderByClause550);
            ruleOrderByClause();

            state._fsp--;

             after(grammarAccess.getOrderByClauseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrderByClause557); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrderByClause"


    // $ANTLR start "ruleOrderByClause"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:301:1: ruleOrderByClause : ( ( rule__OrderByClause__Group__0 ) ) ;
    public final void ruleOrderByClause() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:305:2: ( ( ( rule__OrderByClause__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:306:1: ( ( rule__OrderByClause__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:306:1: ( ( rule__OrderByClause__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:307:1: ( rule__OrderByClause__Group__0 )
            {
             before(grammarAccess.getOrderByClauseAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:308:1: ( rule__OrderByClause__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:308:2: rule__OrderByClause__Group__0
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__0_in_ruleOrderByClause583);
            rule__OrderByClause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrderByClause"


    // $ANTLR start "entryRuleSimpleQualifiedName"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:320:1: entryRuleSimpleQualifiedName : ruleSimpleQualifiedName EOF ;
    public final void entryRuleSimpleQualifiedName() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:321:1: ( ruleSimpleQualifiedName EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:322:1: ruleSimpleQualifiedName EOF
            {
             before(grammarAccess.getSimpleQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_entryRuleSimpleQualifiedName610);
            ruleSimpleQualifiedName();

            state._fsp--;

             after(grammarAccess.getSimpleQualifiedNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleQualifiedName617); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleQualifiedName"


    // $ANTLR start "ruleSimpleQualifiedName"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:329:1: ruleSimpleQualifiedName : ( ( rule__SimpleQualifiedName__Group__0 ) ) ;
    public final void ruleSimpleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:333:2: ( ( ( rule__SimpleQualifiedName__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:334:1: ( ( rule__SimpleQualifiedName__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:334:1: ( ( rule__SimpleQualifiedName__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:335:1: ( rule__SimpleQualifiedName__Group__0 )
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:336:1: ( rule__SimpleQualifiedName__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:336:2: rule__SimpleQualifiedName__Group__0
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group__0_in_ruleSimpleQualifiedName643);
            rule__SimpleQualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleQualifiedName"


    // $ANTLR start "entryRuleSimpleQualified"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:348:1: entryRuleSimpleQualified : ruleSimpleQualified EOF ;
    public final void entryRuleSimpleQualified() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:349:1: ( ruleSimpleQualified EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:350:1: ruleSimpleQualified EOF
            {
             before(grammarAccess.getSimpleQualifiedRule()); 
            pushFollow(FOLLOW_ruleSimpleQualified_in_entryRuleSimpleQualified670);
            ruleSimpleQualified();

            state._fsp--;

             after(grammarAccess.getSimpleQualifiedRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleQualified677); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleQualified"


    // $ANTLR start "ruleSimpleQualified"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:357:1: ruleSimpleQualified : ( ruleIdentifier ) ;
    public final void ruleSimpleQualified() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:361:2: ( ( ruleIdentifier ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:362:1: ( ruleIdentifier )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:362:1: ( ruleIdentifier )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:363:1: ruleIdentifier
            {
             before(grammarAccess.getSimpleQualifiedAccess().getIdentifierParserRuleCall()); 
            pushFollow(FOLLOW_ruleIdentifier_in_ruleSimpleQualified703);
            ruleIdentifier();

            state._fsp--;

             after(grammarAccess.getSimpleQualifiedAccess().getIdentifierParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleQualified"


    // $ANTLR start "entryRuleExpression"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:376:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {

        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");

        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:380:1: ( ruleExpression EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:381:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression734);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression741); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:391:1: ruleExpression : ( ruleOr ) ;
    public final void ruleExpression() throws RecognitionException {

        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS", "RULE_SL_COMMENT", "RULE_ML_COMMENT");
        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:396:2: ( ( ruleOr ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:397:1: ( ruleOr )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:397:1: ( ruleOr )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:398:1: ruleOr
            {
             before(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 
            pushFollow(FOLLOW_ruleOr_in_ruleExpression771);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getOrParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);
            	myHiddenTokenState.restore();

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOr"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:412:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:413:1: ( ruleOr EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:414:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr797);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr804); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:421:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:425:2: ( ( ( rule__Or__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:426:1: ( ( rule__Or__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:426:1: ( ( rule__Or__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:427:1: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:428:1: ( rule__Or__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:428:2: rule__Or__Group__0
            {
            pushFollow(FOLLOW_rule__Or__Group__0_in_ruleOr830);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleAnd"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:440:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:441:1: ( ruleAnd EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:442:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd857);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd864); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:449:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:453:2: ( ( ( rule__And__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:454:1: ( ( rule__And__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:454:1: ( ( rule__And__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:455:1: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:456:1: ( rule__And__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:456:2: rule__And__Group__0
            {
            pushFollow(FOLLOW_rule__And__Group__0_in_ruleAnd890);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleRelation"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:468:1: entryRuleRelation : ruleRelation EOF ;
    public final void entryRuleRelation() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:469:1: ( ruleRelation EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:470:1: ruleRelation EOF
            {
             before(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_ruleRelation_in_entryRuleRelation917);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getRelationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelation924); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:477:1: ruleRelation : ( ( rule__Relation__Group__0 ) ) ;
    public final void ruleRelation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:481:2: ( ( ( rule__Relation__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:482:1: ( ( rule__Relation__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:482:1: ( ( rule__Relation__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:483:1: ( rule__Relation__Group__0 )
            {
             before(grammarAccess.getRelationAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:484:1: ( rule__Relation__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:484:2: rule__Relation__Group__0
            {
            pushFollow(FOLLOW_rule__Relation__Group__0_in_ruleRelation950);
            rule__Relation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleAdd"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:496:1: entryRuleAdd : ruleAdd EOF ;
    public final void entryRuleAdd() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:497:1: ( ruleAdd EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:498:1: ruleAdd EOF
            {
             before(grammarAccess.getAddRule()); 
            pushFollow(FOLLOW_ruleAdd_in_entryRuleAdd977);
            ruleAdd();

            state._fsp--;

             after(grammarAccess.getAddRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAdd984); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdd"


    // $ANTLR start "ruleAdd"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:505:1: ruleAdd : ( ( rule__Add__Group__0 ) ) ;
    public final void ruleAdd() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:509:2: ( ( ( rule__Add__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:510:1: ( ( rule__Add__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:510:1: ( ( rule__Add__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:511:1: ( rule__Add__Group__0 )
            {
             before(grammarAccess.getAddAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:512:1: ( rule__Add__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:512:2: rule__Add__Group__0
            {
            pushFollow(FOLLOW_rule__Add__Group__0_in_ruleAdd1010);
            rule__Add__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAddAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdd"


    // $ANTLR start "entryRuleMult"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:524:1: entryRuleMult : ruleMult EOF ;
    public final void entryRuleMult() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:525:1: ( ruleMult EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:526:1: ruleMult EOF
            {
             before(grammarAccess.getMultRule()); 
            pushFollow(FOLLOW_ruleMult_in_entryRuleMult1037);
            ruleMult();

            state._fsp--;

             after(grammarAccess.getMultRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMult1044); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMult"


    // $ANTLR start "ruleMult"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:533:1: ruleMult : ( ( rule__Mult__Group__0 ) ) ;
    public final void ruleMult() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:537:2: ( ( ( rule__Mult__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:538:1: ( ( rule__Mult__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:538:1: ( ( rule__Mult__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:539:1: ( rule__Mult__Group__0 )
            {
             before(grammarAccess.getMultAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:540:1: ( rule__Mult__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:540:2: rule__Mult__Group__0
            {
            pushFollow(FOLLOW_rule__Mult__Group__0_in_ruleMult1070);
            rule__Mult__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMultAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMult"


    // $ANTLR start "entryRuleIn"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:552:1: entryRuleIn : ruleIn EOF ;
    public final void entryRuleIn() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:553:1: ( ruleIn EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:554:1: ruleIn EOF
            {
             before(grammarAccess.getInRule()); 
            pushFollow(FOLLOW_ruleIn_in_entryRuleIn1097);
            ruleIn();

            state._fsp--;

             after(grammarAccess.getInRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIn1104); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIn"


    // $ANTLR start "ruleIn"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:561:1: ruleIn : ( ( rule__In__Group__0 ) ) ;
    public final void ruleIn() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:565:2: ( ( ( rule__In__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:566:1: ( ( rule__In__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:566:1: ( ( rule__In__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:567:1: ( rule__In__Group__0 )
            {
             before(grammarAccess.getInAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:568:1: ( rule__In__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:568:2: rule__In__Group__0
            {
            pushFollow(FOLLOW_rule__In__Group__0_in_ruleIn1130);
            rule__In__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIn"


    // $ANTLR start "entryRuleUnary"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:580:1: entryRuleUnary : ruleUnary EOF ;
    public final void entryRuleUnary() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:581:1: ( ruleUnary EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:582:1: ruleUnary EOF
            {
             before(grammarAccess.getUnaryRule()); 
            pushFollow(FOLLOW_ruleUnary_in_entryRuleUnary1157);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getUnaryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnary1164); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnary"


    // $ANTLR start "ruleUnary"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:589:1: ruleUnary : ( ( rule__Unary__Alternatives ) ) ;
    public final void ruleUnary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:593:2: ( ( ( rule__Unary__Alternatives ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:594:1: ( ( rule__Unary__Alternatives ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:594:1: ( ( rule__Unary__Alternatives ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:595:1: ( rule__Unary__Alternatives )
            {
             before(grammarAccess.getUnaryAccess().getAlternatives()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:596:1: ( rule__Unary__Alternatives )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:596:2: rule__Unary__Alternatives
            {
            pushFollow(FOLLOW_rule__Unary__Alternatives_in_ruleUnary1190);
            rule__Unary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnary"


    // $ANTLR start "entryRuleExponential"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:608:1: entryRuleExponential : ruleExponential EOF ;
    public final void entryRuleExponential() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:609:1: ( ruleExponential EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:610:1: ruleExponential EOF
            {
             before(grammarAccess.getExponentialRule()); 
            pushFollow(FOLLOW_ruleExponential_in_entryRuleExponential1217);
            ruleExponential();

            state._fsp--;

             after(grammarAccess.getExponentialRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExponential1224); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExponential"


    // $ANTLR start "ruleExponential"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:617:1: ruleExponential : ( ( rule__Exponential__Group__0 ) ) ;
    public final void ruleExponential() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:621:2: ( ( ( rule__Exponential__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:622:1: ( ( rule__Exponential__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:622:1: ( ( rule__Exponential__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:623:1: ( rule__Exponential__Group__0 )
            {
             before(grammarAccess.getExponentialAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:624:1: ( rule__Exponential__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:624:2: rule__Exponential__Group__0
            {
            pushFollow(FOLLOW_rule__Exponential__Group__0_in_ruleExponential1250);
            rule__Exponential__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExponentialAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExponential"


    // $ANTLR start "entryRuleAtom"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:636:1: entryRuleAtom : ruleAtom EOF ;
    public final void entryRuleAtom() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:637:1: ( ruleAtom EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:638:1: ruleAtom EOF
            {
             before(grammarAccess.getAtomRule()); 
            pushFollow(FOLLOW_ruleAtom_in_entryRuleAtom1277);
            ruleAtom();

            state._fsp--;

             after(grammarAccess.getAtomRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtom1284); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtom"


    // $ANTLR start "ruleAtom"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:645:1: ruleAtom : ( ( rule__Atom__Alternatives ) ) ;
    public final void ruleAtom() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:649:2: ( ( ( rule__Atom__Alternatives ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:650:1: ( ( rule__Atom__Alternatives ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:650:1: ( ( rule__Atom__Alternatives ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:651:1: ( rule__Atom__Alternatives )
            {
             before(grammarAccess.getAtomAccess().getAlternatives()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:652:1: ( rule__Atom__Alternatives )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:652:2: rule__Atom__Alternatives
            {
            pushFollow(FOLLOW_rule__Atom__Alternatives_in_ruleAtom1310);
            rule__Atom__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAtomAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtom"


    // $ANTLR start "entryRuleParsExpression"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:664:1: entryRuleParsExpression : ruleParsExpression EOF ;
    public final void entryRuleParsExpression() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:665:1: ( ruleParsExpression EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:666:1: ruleParsExpression EOF
            {
             before(grammarAccess.getParsExpressionRule()); 
            pushFollow(FOLLOW_ruleParsExpression_in_entryRuleParsExpression1337);
            ruleParsExpression();

            state._fsp--;

             after(grammarAccess.getParsExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleParsExpression1344); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParsExpression"


    // $ANTLR start "ruleParsExpression"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:673:1: ruleParsExpression : ( ( rule__ParsExpression__Group__0 ) ) ;
    public final void ruleParsExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:677:2: ( ( ( rule__ParsExpression__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:678:1: ( ( rule__ParsExpression__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:678:1: ( ( rule__ParsExpression__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:679:1: ( rule__ParsExpression__Group__0 )
            {
             before(grammarAccess.getParsExpressionAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:680:1: ( rule__ParsExpression__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:680:2: rule__ParsExpression__Group__0
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__0_in_ruleParsExpression1370);
            rule__ParsExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParsExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParsExpression"


    // $ANTLR start "entryRuleLiteral"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:692:1: entryRuleLiteral : ruleLiteral EOF ;
    public final void entryRuleLiteral() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:693:1: ( ruleLiteral EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:694:1: ruleLiteral EOF
            {
             before(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_ruleLiteral_in_entryRuleLiteral1397);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getLiteralRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLiteral1404); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:701:1: ruleLiteral : ( ( rule__Literal__Alternatives ) ) ;
    public final void ruleLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:705:2: ( ( ( rule__Literal__Alternatives ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:706:1: ( ( rule__Literal__Alternatives ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:706:1: ( ( rule__Literal__Alternatives ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:707:1: ( rule__Literal__Alternatives )
            {
             before(grammarAccess.getLiteralAccess().getAlternatives()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:708:1: ( rule__Literal__Alternatives )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:708:2: rule__Literal__Alternatives
            {
            pushFollow(FOLLOW_rule__Literal__Alternatives_in_ruleLiteral1430);
            rule__Literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleQualifiedName"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:720:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:721:1: ( ruleQualifiedName EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:722:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1457);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName1464); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:729:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:733:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:734:1: ( ( rule__QualifiedName__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:734:1: ( ( rule__QualifiedName__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:735:1: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:736:1: ( rule__QualifiedName__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:736:2: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1490);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleQualified"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:748:1: entryRuleQualified : ruleQualified EOF ;
    public final void entryRuleQualified() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:749:1: ( ruleQualified EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:750:1: ruleQualified EOF
            {
             before(grammarAccess.getQualifiedRule()); 
            pushFollow(FOLLOW_ruleQualified_in_entryRuleQualified1517);
            ruleQualified();

            state._fsp--;

             after(grammarAccess.getQualifiedRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified1524); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualified"


    // $ANTLR start "ruleQualified"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:757:1: ruleQualified : ( ruleQualifiedElement ) ;
    public final void ruleQualified() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:761:2: ( ( ruleQualifiedElement ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:762:1: ( ruleQualifiedElement )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:762:1: ( ruleQualifiedElement )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:763:1: ruleQualifiedElement
            {
             before(grammarAccess.getQualifiedAccess().getQualifiedElementParserRuleCall()); 
            pushFollow(FOLLOW_ruleQualifiedElement_in_ruleQualified1550);
            ruleQualifiedElement();

            state._fsp--;

             after(grammarAccess.getQualifiedAccess().getQualifiedElementParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualified"


    // $ANTLR start "entryRuleQualifiedElement"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:776:1: entryRuleQualifiedElement : ruleQualifiedElement EOF ;
    public final void entryRuleQualifiedElement() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:777:1: ( ruleQualifiedElement EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:778:1: ruleQualifiedElement EOF
            {
             before(grammarAccess.getQualifiedElementRule()); 
            pushFollow(FOLLOW_ruleQualifiedElement_in_entryRuleQualifiedElement1576);
            ruleQualifiedElement();

            state._fsp--;

             after(grammarAccess.getQualifiedElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedElement1583); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedElement"


    // $ANTLR start "ruleQualifiedElement"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:785:1: ruleQualifiedElement : ( ( rule__QualifiedElement__Alternatives ) ) ;
    public final void ruleQualifiedElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:789:2: ( ( ( rule__QualifiedElement__Alternatives ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:790:1: ( ( rule__QualifiedElement__Alternatives ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:790:1: ( ( rule__QualifiedElement__Alternatives ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:791:1: ( rule__QualifiedElement__Alternatives )
            {
             before(grammarAccess.getQualifiedElementAccess().getAlternatives()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:792:1: ( rule__QualifiedElement__Alternatives )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:792:2: rule__QualifiedElement__Alternatives
            {
            pushFollow(FOLLOW_rule__QualifiedElement__Alternatives_in_ruleQualifiedElement1609);
            rule__QualifiedElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedElement"


    // $ANTLR start "entryRuleFunction"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:804:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:805:1: ( ruleFunction EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:806:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_ruleFunction_in_entryRuleFunction1636);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFunction1643); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:813:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:817:2: ( ( ( rule__Function__Group__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:818:1: ( ( rule__Function__Group__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:818:1: ( ( rule__Function__Group__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:819:1: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:820:1: ( rule__Function__Group__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:820:2: rule__Function__Group__0
            {
            pushFollow(FOLLOW_rule__Function__Group__0_in_ruleFunction1669);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleIdentifier"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:832:1: entryRuleIdentifier : ruleIdentifier EOF ;
    public final void entryRuleIdentifier() throws RecognitionException {
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:833:1: ( ruleIdentifier EOF )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:834:1: ruleIdentifier EOF
            {
             before(grammarAccess.getIdentifierRule()); 
            pushFollow(FOLLOW_ruleIdentifier_in_entryRuleIdentifier1696);
            ruleIdentifier();

            state._fsp--;

             after(grammarAccess.getIdentifierRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIdentifier1703); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIdentifier"


    // $ANTLR start "ruleIdentifier"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:841:1: ruleIdentifier : ( ( rule__Identifier__IdAssignment ) ) ;
    public final void ruleIdentifier() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:845:2: ( ( ( rule__Identifier__IdAssignment ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:846:1: ( ( rule__Identifier__IdAssignment ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:846:1: ( ( rule__Identifier__IdAssignment ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:847:1: ( rule__Identifier__IdAssignment )
            {
             before(grammarAccess.getIdentifierAccess().getIdAssignment()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:848:1: ( rule__Identifier__IdAssignment )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:848:2: rule__Identifier__IdAssignment
            {
            pushFollow(FOLLOW_rule__Identifier__IdAssignment_in_ruleIdentifier1729);
            rule__Identifier__IdAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIdentifierAccess().getIdAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIdentifier"


    // $ANTLR start "rule__ObjectType__Alternatives"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:860:1: rule__ObjectType__Alternatives : ( ( ( rule__ObjectType__Group_0__0 ) ) | ( ( rule__ObjectType__Group_1__0 ) ) | ( ( rule__ObjectType__Group_2__0 ) ) | ( ( rule__ObjectType__Group_3__0 ) ) | ( ( rule__ObjectType__Group_4__0 ) ) | ( ( rule__ObjectType__Group_5__0 ) ) );
    public final void rule__ObjectType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:864:1: ( ( ( rule__ObjectType__Group_0__0 ) ) | ( ( rule__ObjectType__Group_1__0 ) ) | ( ( rule__ObjectType__Group_2__0 ) ) | ( ( rule__ObjectType__Group_3__0 ) ) | ( ( rule__ObjectType__Group_4__0 ) ) | ( ( rule__ObjectType__Group_5__0 ) ) )
            int alt1=6;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt1=1;
                }
                break;
            case 41:
                {
                alt1=2;
                }
                break;
            case 42:
                {
                alt1=3;
                }
                break;
            case 43:
                {
                alt1=4;
                }
                break;
            case 44:
                {
                alt1=5;
                }
                break;
            case 45:
                {
                alt1=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:865:1: ( ( rule__ObjectType__Group_0__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:865:1: ( ( rule__ObjectType__Group_0__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:866:1: ( rule__ObjectType__Group_0__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_0()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:867:1: ( rule__ObjectType__Group_0__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:867:2: rule__ObjectType__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_0__0_in_rule__ObjectType__Alternatives1765);
                    rule__ObjectType__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:871:6: ( ( rule__ObjectType__Group_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:871:6: ( ( rule__ObjectType__Group_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:872:1: ( rule__ObjectType__Group_1__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:873:1: ( rule__ObjectType__Group_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:873:2: rule__ObjectType__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_1__0_in_rule__ObjectType__Alternatives1783);
                    rule__ObjectType__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:877:6: ( ( rule__ObjectType__Group_2__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:877:6: ( ( rule__ObjectType__Group_2__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:878:1: ( rule__ObjectType__Group_2__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_2()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:879:1: ( rule__ObjectType__Group_2__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:879:2: rule__ObjectType__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_2__0_in_rule__ObjectType__Alternatives1801);
                    rule__ObjectType__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:883:6: ( ( rule__ObjectType__Group_3__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:883:6: ( ( rule__ObjectType__Group_3__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:884:1: ( rule__ObjectType__Group_3__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_3()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:885:1: ( rule__ObjectType__Group_3__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:885:2: rule__ObjectType__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_3__0_in_rule__ObjectType__Alternatives1819);
                    rule__ObjectType__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:889:6: ( ( rule__ObjectType__Group_4__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:889:6: ( ( rule__ObjectType__Group_4__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:890:1: ( rule__ObjectType__Group_4__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_4()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:891:1: ( rule__ObjectType__Group_4__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:891:2: rule__ObjectType__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_4__0_in_rule__ObjectType__Alternatives1837);
                    rule__ObjectType__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:895:6: ( ( rule__ObjectType__Group_5__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:895:6: ( ( rule__ObjectType__Group_5__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:896:1: ( rule__ObjectType__Group_5__0 )
                    {
                     before(grammarAccess.getObjectTypeAccess().getGroup_5()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:897:1: ( rule__ObjectType__Group_5__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:897:2: rule__ObjectType__Group_5__0
                    {
                    pushFollow(FOLLOW_rule__ObjectType__Group_5__0_in_rule__ObjectType__Alternatives1855);
                    rule__ObjectType__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getObjectTypeAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Alternatives"


    // $ANTLR start "rule__OrderByClause__OptionAlternatives_3_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:906:1: rule__OrderByClause__OptionAlternatives_3_0 : ( ( 'asc' ) | ( 'desc' ) );
    public final void rule__OrderByClause__OptionAlternatives_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:910:1: ( ( 'asc' ) | ( 'desc' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            else if ( (LA2_0==13) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:911:1: ( 'asc' )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:911:1: ( 'asc' )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:912:1: 'asc'
                    {
                     before(grammarAccess.getOrderByClauseAccess().getOptionAscKeyword_3_0_0()); 
                    match(input,12,FOLLOW_12_in_rule__OrderByClause__OptionAlternatives_3_01889); 
                     after(grammarAccess.getOrderByClauseAccess().getOptionAscKeyword_3_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:919:6: ( 'desc' )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:919:6: ( 'desc' )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:920:1: 'desc'
                    {
                     before(grammarAccess.getOrderByClauseAccess().getOptionDescKeyword_3_0_1()); 
                    match(input,13,FOLLOW_13_in_rule__OrderByClause__OptionAlternatives_3_01909); 
                     after(grammarAccess.getOrderByClauseAccess().getOptionDescKeyword_3_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__OptionAlternatives_3_0"


    // $ANTLR start "rule__Relation__Alternatives_1_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:932:1: rule__Relation__Alternatives_1_0 : ( ( ( rule__Relation__Group_1_0_0__0 ) ) | ( ( rule__Relation__Group_1_0_1__0 ) ) | ( ( rule__Relation__Group_1_0_2__0 ) ) | ( ( rule__Relation__Group_1_0_3__0 ) ) | ( ( rule__Relation__Group_1_0_4__0 ) ) | ( ( rule__Relation__Group_1_0_5__0 ) ) | ( ( rule__Relation__Group_1_0_6__0 ) ) );
    public final void rule__Relation__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:936:1: ( ( ( rule__Relation__Group_1_0_0__0 ) ) | ( ( rule__Relation__Group_1_0_1__0 ) ) | ( ( rule__Relation__Group_1_0_2__0 ) ) | ( ( rule__Relation__Group_1_0_3__0 ) ) | ( ( rule__Relation__Group_1_0_4__0 ) ) | ( ( rule__Relation__Group_1_0_5__0 ) ) | ( ( rule__Relation__Group_1_0_6__0 ) ) )
            int alt3=7;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt3=1;
                }
                break;
            case 23:
                {
                alt3=2;
                }
                break;
            case 24:
                {
                alt3=3;
                }
                break;
            case 25:
                {
                alt3=4;
                }
                break;
            case 26:
                {
                alt3=5;
                }
                break;
            case 27:
                {
                alt3=6;
                }
                break;
            case 28:
                {
                alt3=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:937:1: ( ( rule__Relation__Group_1_0_0__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:937:1: ( ( rule__Relation__Group_1_0_0__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:938:1: ( rule__Relation__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_0()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:939:1: ( rule__Relation__Group_1_0_0__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:939:2: rule__Relation__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_0__0_in_rule__Relation__Alternatives_1_01943);
                    rule__Relation__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:943:6: ( ( rule__Relation__Group_1_0_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:943:6: ( ( rule__Relation__Group_1_0_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:944:1: ( rule__Relation__Group_1_0_1__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:945:1: ( rule__Relation__Group_1_0_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:945:2: rule__Relation__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_1__0_in_rule__Relation__Alternatives_1_01961);
                    rule__Relation__Group_1_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:949:6: ( ( rule__Relation__Group_1_0_2__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:949:6: ( ( rule__Relation__Group_1_0_2__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:950:1: ( rule__Relation__Group_1_0_2__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_2()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:951:1: ( rule__Relation__Group_1_0_2__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:951:2: rule__Relation__Group_1_0_2__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_2__0_in_rule__Relation__Alternatives_1_01979);
                    rule__Relation__Group_1_0_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:955:6: ( ( rule__Relation__Group_1_0_3__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:955:6: ( ( rule__Relation__Group_1_0_3__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:956:1: ( rule__Relation__Group_1_0_3__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_3()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:957:1: ( rule__Relation__Group_1_0_3__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:957:2: rule__Relation__Group_1_0_3__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_3__0_in_rule__Relation__Alternatives_1_01997);
                    rule__Relation__Group_1_0_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:961:6: ( ( rule__Relation__Group_1_0_4__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:961:6: ( ( rule__Relation__Group_1_0_4__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:962:1: ( rule__Relation__Group_1_0_4__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_4()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:963:1: ( rule__Relation__Group_1_0_4__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:963:2: rule__Relation__Group_1_0_4__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_4__0_in_rule__Relation__Alternatives_1_02015);
                    rule__Relation__Group_1_0_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:967:6: ( ( rule__Relation__Group_1_0_5__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:967:6: ( ( rule__Relation__Group_1_0_5__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:968:1: ( rule__Relation__Group_1_0_5__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_5()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:969:1: ( rule__Relation__Group_1_0_5__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:969:2: rule__Relation__Group_1_0_5__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_5__0_in_rule__Relation__Alternatives_1_02033);
                    rule__Relation__Group_1_0_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:973:6: ( ( rule__Relation__Group_1_0_6__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:973:6: ( ( rule__Relation__Group_1_0_6__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:974:1: ( rule__Relation__Group_1_0_6__0 )
                    {
                     before(grammarAccess.getRelationAccess().getGroup_1_0_6()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:975:1: ( rule__Relation__Group_1_0_6__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:975:2: rule__Relation__Group_1_0_6__0
                    {
                    pushFollow(FOLLOW_rule__Relation__Group_1_0_6__0_in_rule__Relation__Alternatives_1_02051);
                    rule__Relation__Group_1_0_6__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRelationAccess().getGroup_1_0_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Alternatives_1_0"


    // $ANTLR start "rule__Add__Alternatives_1_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:984:1: rule__Add__Alternatives_1_0 : ( ( ( rule__Add__Group_1_0_0__0 ) ) | ( ( rule__Add__Group_1_0_1__0 ) ) );
    public final void rule__Add__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:988:1: ( ( ( rule__Add__Group_1_0_0__0 ) ) | ( ( rule__Add__Group_1_0_1__0 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==29) ) {
                alt4=1;
            }
            else if ( (LA4_0==30) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:989:1: ( ( rule__Add__Group_1_0_0__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:989:1: ( ( rule__Add__Group_1_0_0__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:990:1: ( rule__Add__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getAddAccess().getGroup_1_0_0()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:991:1: ( rule__Add__Group_1_0_0__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:991:2: rule__Add__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_rule__Add__Group_1_0_0__0_in_rule__Add__Alternatives_1_02084);
                    rule__Add__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAddAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:995:6: ( ( rule__Add__Group_1_0_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:995:6: ( ( rule__Add__Group_1_0_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:996:1: ( rule__Add__Group_1_0_1__0 )
                    {
                     before(grammarAccess.getAddAccess().getGroup_1_0_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:997:1: ( rule__Add__Group_1_0_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:997:2: rule__Add__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_rule__Add__Group_1_0_1__0_in_rule__Add__Alternatives_1_02102);
                    rule__Add__Group_1_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getAddAccess().getGroup_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Alternatives_1_0"


    // $ANTLR start "rule__Mult__Alternatives_1_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1006:1: rule__Mult__Alternatives_1_0 : ( ( ( rule__Mult__Group_1_0_0__0 ) ) | ( ( rule__Mult__Group_1_0_1__0 ) ) | ( ( rule__Mult__Group_1_0_2__0 ) ) );
    public final void rule__Mult__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1010:1: ( ( ( rule__Mult__Group_1_0_0__0 ) ) | ( ( rule__Mult__Group_1_0_1__0 ) ) | ( ( rule__Mult__Group_1_0_2__0 ) ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt5=1;
                }
                break;
            case 32:
                {
                alt5=2;
                }
                break;
            case 33:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1011:1: ( ( rule__Mult__Group_1_0_0__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1011:1: ( ( rule__Mult__Group_1_0_0__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1012:1: ( rule__Mult__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getMultAccess().getGroup_1_0_0()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1013:1: ( rule__Mult__Group_1_0_0__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1013:2: rule__Mult__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_rule__Mult__Group_1_0_0__0_in_rule__Mult__Alternatives_1_02135);
                    rule__Mult__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1017:6: ( ( rule__Mult__Group_1_0_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1017:6: ( ( rule__Mult__Group_1_0_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1018:1: ( rule__Mult__Group_1_0_1__0 )
                    {
                     before(grammarAccess.getMultAccess().getGroup_1_0_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1019:1: ( rule__Mult__Group_1_0_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1019:2: rule__Mult__Group_1_0_1__0
                    {
                    pushFollow(FOLLOW_rule__Mult__Group_1_0_1__0_in_rule__Mult__Alternatives_1_02153);
                    rule__Mult__Group_1_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultAccess().getGroup_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1023:6: ( ( rule__Mult__Group_1_0_2__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1023:6: ( ( rule__Mult__Group_1_0_2__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1024:1: ( rule__Mult__Group_1_0_2__0 )
                    {
                     before(grammarAccess.getMultAccess().getGroup_1_0_2()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1025:1: ( rule__Mult__Group_1_0_2__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1025:2: rule__Mult__Group_1_0_2__0
                    {
                    pushFollow(FOLLOW_rule__Mult__Group_1_0_2__0_in_rule__Mult__Alternatives_1_02171);
                    rule__Mult__Group_1_0_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMultAccess().getGroup_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Alternatives_1_0"


    // $ANTLR start "rule__Unary__Alternatives"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1034:1: rule__Unary__Alternatives : ( ( ruleExponential ) | ( ( rule__Unary__Group_1__0 ) ) | ( ( rule__Unary__Group_2__0 ) ) );
    public final void rule__Unary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1038:1: ( ( ruleExponential ) | ( ( rule__Unary__Group_1__0 ) ) | ( ( rule__Unary__Group_2__0 ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case RULE_STRING:
            case RULE_FLOAT:
            case RULE_INT:
            case 14:
            case 15:
            case 37:
            case 50:
            case 51:
            case 52:
                {
                alt6=1;
                }
                break;
            case 35:
                {
                alt6=2;
                }
                break;
            case 30:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1039:1: ( ruleExponential )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1039:1: ( ruleExponential )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1040:1: ruleExponential
                    {
                     before(grammarAccess.getUnaryAccess().getExponentialParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleExponential_in_rule__Unary__Alternatives2204);
                    ruleExponential();

                    state._fsp--;

                     after(grammarAccess.getUnaryAccess().getExponentialParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1045:6: ( ( rule__Unary__Group_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1045:6: ( ( rule__Unary__Group_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1046:1: ( rule__Unary__Group_1__0 )
                    {
                     before(grammarAccess.getUnaryAccess().getGroup_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1047:1: ( rule__Unary__Group_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1047:2: rule__Unary__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Unary__Group_1__0_in_rule__Unary__Alternatives2221);
                    rule__Unary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1051:6: ( ( rule__Unary__Group_2__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1051:6: ( ( rule__Unary__Group_2__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1052:1: ( rule__Unary__Group_2__0 )
                    {
                     before(grammarAccess.getUnaryAccess().getGroup_2()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1053:1: ( rule__Unary__Group_2__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1053:2: rule__Unary__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Unary__Group_2__0_in_rule__Unary__Alternatives2239);
                    rule__Unary__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getUnaryAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Alternatives"


    // $ANTLR start "rule__Atom__Alternatives"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1062:1: rule__Atom__Alternatives : ( ( ruleLiteral ) | ( ruleQualifiedName ) | ( ruleParsExpression ) );
    public final void rule__Atom__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1066:1: ( ( ruleLiteral ) | ( ruleQualifiedName ) | ( ruleParsExpression ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_FLOAT:
            case RULE_INT:
            case 50:
            case 51:
            case 52:
                {
                alt7=1;
                }
                break;
            case RULE_ID:
            case 14:
            case 15:
                {
                alt7=2;
                }
                break;
            case 37:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1067:1: ( ruleLiteral )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1067:1: ( ruleLiteral )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1068:1: ruleLiteral
                    {
                     before(grammarAccess.getAtomAccess().getLiteralParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleLiteral_in_rule__Atom__Alternatives2272);
                    ruleLiteral();

                    state._fsp--;

                     after(grammarAccess.getAtomAccess().getLiteralParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1073:6: ( ruleQualifiedName )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1073:6: ( ruleQualifiedName )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1074:1: ruleQualifiedName
                    {
                     before(grammarAccess.getAtomAccess().getQualifiedNameParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleQualifiedName_in_rule__Atom__Alternatives2289);
                    ruleQualifiedName();

                    state._fsp--;

                     after(grammarAccess.getAtomAccess().getQualifiedNameParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1079:6: ( ruleParsExpression )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1079:6: ( ruleParsExpression )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1080:1: ruleParsExpression
                    {
                     before(grammarAccess.getAtomAccess().getParsExpressionParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleParsExpression_in_rule__Atom__Alternatives2306);
                    ruleParsExpression();

                    state._fsp--;

                     after(grammarAccess.getAtomAccess().getParsExpressionParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Atom__Alternatives"


    // $ANTLR start "rule__Literal__Alternatives"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1090:1: rule__Literal__Alternatives : ( ( ( rule__Literal__Group_0__0 ) ) | ( ( rule__Literal__Group_1__0 ) ) | ( ( rule__Literal__Group_2__0 ) ) | ( ( rule__Literal__Group_3__0 ) ) | ( ( rule__Literal__Group_4__0 ) ) | ( ( rule__Literal__Group_5__0 ) ) );
    public final void rule__Literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1094:1: ( ( ( rule__Literal__Group_0__0 ) ) | ( ( rule__Literal__Group_1__0 ) ) | ( ( rule__Literal__Group_2__0 ) ) | ( ( rule__Literal__Group_3__0 ) ) | ( ( rule__Literal__Group_4__0 ) ) | ( ( rule__Literal__Group_5__0 ) ) )
            int alt8=6;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt8=1;
                }
                break;
            case RULE_FLOAT:
                {
                alt8=2;
                }
                break;
            case RULE_INT:
                {
                alt8=3;
                }
                break;
            case 50:
                {
                alt8=4;
                }
                break;
            case 51:
                {
                alt8=5;
                }
                break;
            case 52:
                {
                alt8=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1095:1: ( ( rule__Literal__Group_0__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1095:1: ( ( rule__Literal__Group_0__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1096:1: ( rule__Literal__Group_0__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_0()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1097:1: ( rule__Literal__Group_0__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1097:2: rule__Literal__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_0__0_in_rule__Literal__Alternatives2338);
                    rule__Literal__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1101:6: ( ( rule__Literal__Group_1__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1101:6: ( ( rule__Literal__Group_1__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1102:1: ( rule__Literal__Group_1__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_1()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1103:1: ( rule__Literal__Group_1__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1103:2: rule__Literal__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_1__0_in_rule__Literal__Alternatives2356);
                    rule__Literal__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1107:6: ( ( rule__Literal__Group_2__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1107:6: ( ( rule__Literal__Group_2__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1108:1: ( rule__Literal__Group_2__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_2()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1109:1: ( rule__Literal__Group_2__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1109:2: rule__Literal__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_2__0_in_rule__Literal__Alternatives2374);
                    rule__Literal__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1113:6: ( ( rule__Literal__Group_3__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1113:6: ( ( rule__Literal__Group_3__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1114:1: ( rule__Literal__Group_3__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_3()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1115:1: ( rule__Literal__Group_3__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1115:2: rule__Literal__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_3__0_in_rule__Literal__Alternatives2392);
                    rule__Literal__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1119:6: ( ( rule__Literal__Group_4__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1119:6: ( ( rule__Literal__Group_4__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1120:1: ( rule__Literal__Group_4__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_4()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1121:1: ( rule__Literal__Group_4__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1121:2: rule__Literal__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_4__0_in_rule__Literal__Alternatives2410);
                    rule__Literal__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1125:6: ( ( rule__Literal__Group_5__0 ) )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1125:6: ( ( rule__Literal__Group_5__0 ) )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1126:1: ( rule__Literal__Group_5__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_5()); 
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1127:1: ( rule__Literal__Group_5__0 )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1127:2: rule__Literal__Group_5__0
                    {
                    pushFollow(FOLLOW_rule__Literal__Group_5__0_in_rule__Literal__Alternatives2428);
                    rule__Literal__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Alternatives"


    // $ANTLR start "rule__QualifiedName__GranularAlternatives_0_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1136:1: rule__QualifiedName__GranularAlternatives_0_0 : ( ( 'distinct' ) | ( 'all' ) );
    public final void rule__QualifiedName__GranularAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1140:1: ( ( 'distinct' ) | ( 'all' ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==14) ) {
                alt9=1;
            }
            else if ( (LA9_0==15) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1141:1: ( 'distinct' )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1141:1: ( 'distinct' )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1142:1: 'distinct'
                    {
                     before(grammarAccess.getQualifiedNameAccess().getGranularDistinctKeyword_0_0_0()); 
                    match(input,14,FOLLOW_14_in_rule__QualifiedName__GranularAlternatives_0_02462); 
                     after(grammarAccess.getQualifiedNameAccess().getGranularDistinctKeyword_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1149:6: ( 'all' )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1149:6: ( 'all' )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1150:1: 'all'
                    {
                     before(grammarAccess.getQualifiedNameAccess().getGranularAllKeyword_0_0_1()); 
                    match(input,15,FOLLOW_15_in_rule__QualifiedName__GranularAlternatives_0_02482); 
                     after(grammarAccess.getQualifiedNameAccess().getGranularAllKeyword_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__GranularAlternatives_0_0"


    // $ANTLR start "rule__QualifiedElement__Alternatives"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1162:1: rule__QualifiedElement__Alternatives : ( ( ruleIdentifier ) | ( ruleFunction ) );
    public final void rule__QualifiedElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1166:1: ( ( ruleIdentifier ) | ( ruleFunction ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==37) ) {
                    alt10=2;
                }
                else if ( (LA10_1==EOF||(LA10_1>=16 && LA10_1<=17)||(LA10_1>=19 && LA10_1<=34)||LA10_1==36||LA10_1==38||(LA10_1>=47 && LA10_1<=49)) ) {
                    alt10=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1167:1: ( ruleIdentifier )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1167:1: ( ruleIdentifier )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1168:1: ruleIdentifier
                    {
                     before(grammarAccess.getQualifiedElementAccess().getIdentifierParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleIdentifier_in_rule__QualifiedElement__Alternatives2516);
                    ruleIdentifier();

                    state._fsp--;

                     after(grammarAccess.getQualifiedElementAccess().getIdentifierParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1173:6: ( ruleFunction )
                    {
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1173:6: ( ruleFunction )
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1174:1: ruleFunction
                    {
                     before(grammarAccess.getQualifiedElementAccess().getFunctionParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleFunction_in_rule__QualifiedElement__Alternatives2533);
                    ruleFunction();

                    state._fsp--;

                     after(grammarAccess.getQualifiedElementAccess().getFunctionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedElement__Alternatives"


    // $ANTLR start "rule__QueryStatement__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1186:1: rule__QueryStatement__Group__0 : rule__QueryStatement__Group__0__Impl rule__QueryStatement__Group__1 ;
    public final void rule__QueryStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1190:1: ( rule__QueryStatement__Group__0__Impl rule__QueryStatement__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1191:2: rule__QueryStatement__Group__0__Impl rule__QueryStatement__Group__1
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__0__Impl_in_rule__QueryStatement__Group__02563);
            rule__QueryStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QueryStatement__Group__1_in_rule__QueryStatement__Group__02566);
            rule__QueryStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__0"


    // $ANTLR start "rule__QueryStatement__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1198:1: rule__QueryStatement__Group__0__Impl : ( ( rule__QueryStatement__FindAssignment_0 ) ) ;
    public final void rule__QueryStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1202:1: ( ( ( rule__QueryStatement__FindAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1203:1: ( ( rule__QueryStatement__FindAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1203:1: ( ( rule__QueryStatement__FindAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1204:1: ( rule__QueryStatement__FindAssignment_0 )
            {
             before(grammarAccess.getQueryStatementAccess().getFindAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1205:1: ( rule__QueryStatement__FindAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1205:2: rule__QueryStatement__FindAssignment_0
            {
            pushFollow(FOLLOW_rule__QueryStatement__FindAssignment_0_in_rule__QueryStatement__Group__0__Impl2593);
            rule__QueryStatement__FindAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQueryStatementAccess().getFindAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__0__Impl"


    // $ANTLR start "rule__QueryStatement__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1215:1: rule__QueryStatement__Group__1 : rule__QueryStatement__Group__1__Impl rule__QueryStatement__Group__2 ;
    public final void rule__QueryStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1219:1: ( rule__QueryStatement__Group__1__Impl rule__QueryStatement__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1220:2: rule__QueryStatement__Group__1__Impl rule__QueryStatement__Group__2
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__1__Impl_in_rule__QueryStatement__Group__12623);
            rule__QueryStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QueryStatement__Group__2_in_rule__QueryStatement__Group__12626);
            rule__QueryStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__1"


    // $ANTLR start "rule__QueryStatement__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1227:1: rule__QueryStatement__Group__1__Impl : ( ( rule__QueryStatement__WhereAssignment_1 )? ) ;
    public final void rule__QueryStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1231:1: ( ( ( rule__QueryStatement__WhereAssignment_1 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1232:1: ( ( rule__QueryStatement__WhereAssignment_1 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1232:1: ( ( rule__QueryStatement__WhereAssignment_1 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1233:1: ( rule__QueryStatement__WhereAssignment_1 )?
            {
             before(grammarAccess.getQueryStatementAccess().getWhereAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1234:1: ( rule__QueryStatement__WhereAssignment_1 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==46) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1234:2: rule__QueryStatement__WhereAssignment_1
                    {
                    pushFollow(FOLLOW_rule__QueryStatement__WhereAssignment_1_in_rule__QueryStatement__Group__1__Impl2653);
                    rule__QueryStatement__WhereAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryStatementAccess().getWhereAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__1__Impl"


    // $ANTLR start "rule__QueryStatement__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1244:1: rule__QueryStatement__Group__2 : rule__QueryStatement__Group__2__Impl rule__QueryStatement__Group__3 ;
    public final void rule__QueryStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1248:1: ( rule__QueryStatement__Group__2__Impl rule__QueryStatement__Group__3 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1249:2: rule__QueryStatement__Group__2__Impl rule__QueryStatement__Group__3
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__2__Impl_in_rule__QueryStatement__Group__22684);
            rule__QueryStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QueryStatement__Group__3_in_rule__QueryStatement__Group__22687);
            rule__QueryStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__2"


    // $ANTLR start "rule__QueryStatement__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1256:1: rule__QueryStatement__Group__2__Impl : ( ( rule__QueryStatement__ReturnAssignment_2 ) ) ;
    public final void rule__QueryStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1260:1: ( ( ( rule__QueryStatement__ReturnAssignment_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1261:1: ( ( rule__QueryStatement__ReturnAssignment_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1261:1: ( ( rule__QueryStatement__ReturnAssignment_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1262:1: ( rule__QueryStatement__ReturnAssignment_2 )
            {
             before(grammarAccess.getQueryStatementAccess().getReturnAssignment_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1263:1: ( rule__QueryStatement__ReturnAssignment_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1263:2: rule__QueryStatement__ReturnAssignment_2
            {
            pushFollow(FOLLOW_rule__QueryStatement__ReturnAssignment_2_in_rule__QueryStatement__Group__2__Impl2714);
            rule__QueryStatement__ReturnAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQueryStatementAccess().getReturnAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__2__Impl"


    // $ANTLR start "rule__QueryStatement__Group__3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1273:1: rule__QueryStatement__Group__3 : rule__QueryStatement__Group__3__Impl rule__QueryStatement__Group__4 ;
    public final void rule__QueryStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1277:1: ( rule__QueryStatement__Group__3__Impl rule__QueryStatement__Group__4 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1278:2: rule__QueryStatement__Group__3__Impl rule__QueryStatement__Group__4
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__3__Impl_in_rule__QueryStatement__Group__32744);
            rule__QueryStatement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QueryStatement__Group__4_in_rule__QueryStatement__Group__32747);
            rule__QueryStatement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__3"


    // $ANTLR start "rule__QueryStatement__Group__3__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1285:1: rule__QueryStatement__Group__3__Impl : ( ( rule__QueryStatement__OrderbyAssignment_3 )? ) ;
    public final void rule__QueryStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1289:1: ( ( ( rule__QueryStatement__OrderbyAssignment_3 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1290:1: ( ( rule__QueryStatement__OrderbyAssignment_3 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1290:1: ( ( rule__QueryStatement__OrderbyAssignment_3 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1291:1: ( rule__QueryStatement__OrderbyAssignment_3 )?
            {
             before(grammarAccess.getQueryStatementAccess().getOrderbyAssignment_3()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1292:1: ( rule__QueryStatement__OrderbyAssignment_3 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==49) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1292:2: rule__QueryStatement__OrderbyAssignment_3
                    {
                    pushFollow(FOLLOW_rule__QueryStatement__OrderbyAssignment_3_in_rule__QueryStatement__Group__3__Impl2774);
                    rule__QueryStatement__OrderbyAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryStatementAccess().getOrderbyAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__3__Impl"


    // $ANTLR start "rule__QueryStatement__Group__4"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1302:1: rule__QueryStatement__Group__4 : rule__QueryStatement__Group__4__Impl ;
    public final void rule__QueryStatement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1306:1: ( rule__QueryStatement__Group__4__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1307:2: rule__QueryStatement__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__QueryStatement__Group__4__Impl_in_rule__QueryStatement__Group__42805);
            rule__QueryStatement__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__4"


    // $ANTLR start "rule__QueryStatement__Group__4__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1313:1: rule__QueryStatement__Group__4__Impl : ( ( rule__QueryStatement__GroupbyAssignment_4 )? ) ;
    public final void rule__QueryStatement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1317:1: ( ( ( rule__QueryStatement__GroupbyAssignment_4 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1318:1: ( ( rule__QueryStatement__GroupbyAssignment_4 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1318:1: ( ( rule__QueryStatement__GroupbyAssignment_4 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1319:1: ( rule__QueryStatement__GroupbyAssignment_4 )?
            {
             before(grammarAccess.getQueryStatementAccess().getGroupbyAssignment_4()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1320:1: ( rule__QueryStatement__GroupbyAssignment_4 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==48) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1320:2: rule__QueryStatement__GroupbyAssignment_4
                    {
                    pushFollow(FOLLOW_rule__QueryStatement__GroupbyAssignment_4_in_rule__QueryStatement__Group__4__Impl2832);
                    rule__QueryStatement__GroupbyAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQueryStatementAccess().getGroupbyAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__Group__4__Impl"


    // $ANTLR start "rule__FindClause__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1340:1: rule__FindClause__Group__0 : rule__FindClause__Group__0__Impl rule__FindClause__Group__1 ;
    public final void rule__FindClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1344:1: ( rule__FindClause__Group__0__Impl rule__FindClause__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1345:2: rule__FindClause__Group__0__Impl rule__FindClause__Group__1
            {
            pushFollow(FOLLOW_rule__FindClause__Group__0__Impl_in_rule__FindClause__Group__02873);
            rule__FindClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__FindClause__Group__1_in_rule__FindClause__Group__02876);
            rule__FindClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__0"


    // $ANTLR start "rule__FindClause__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1352:1: rule__FindClause__Group__0__Impl : ( ( rule__FindClause__ClauseAssignment_0 ) ) ;
    public final void rule__FindClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1356:1: ( ( ( rule__FindClause__ClauseAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1357:1: ( ( rule__FindClause__ClauseAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1357:1: ( ( rule__FindClause__ClauseAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1358:1: ( rule__FindClause__ClauseAssignment_0 )
            {
             before(grammarAccess.getFindClauseAccess().getClauseAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1359:1: ( rule__FindClause__ClauseAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1359:2: rule__FindClause__ClauseAssignment_0
            {
            pushFollow(FOLLOW_rule__FindClause__ClauseAssignment_0_in_rule__FindClause__Group__0__Impl2903);
            rule__FindClause__ClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFindClauseAccess().getClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__0__Impl"


    // $ANTLR start "rule__FindClause__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1369:1: rule__FindClause__Group__1 : rule__FindClause__Group__1__Impl rule__FindClause__Group__2 ;
    public final void rule__FindClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1373:1: ( rule__FindClause__Group__1__Impl rule__FindClause__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1374:2: rule__FindClause__Group__1__Impl rule__FindClause__Group__2
            {
            pushFollow(FOLLOW_rule__FindClause__Group__1__Impl_in_rule__FindClause__Group__12933);
            rule__FindClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__FindClause__Group__2_in_rule__FindClause__Group__12936);
            rule__FindClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__1"


    // $ANTLR start "rule__FindClause__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1381:1: rule__FindClause__Group__1__Impl : ( ( rule__FindClause__BindingObjectAssignment_1 ) ) ;
    public final void rule__FindClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1385:1: ( ( ( rule__FindClause__BindingObjectAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1386:1: ( ( rule__FindClause__BindingObjectAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1386:1: ( ( rule__FindClause__BindingObjectAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1387:1: ( rule__FindClause__BindingObjectAssignment_1 )
            {
             before(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1388:1: ( rule__FindClause__BindingObjectAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1388:2: rule__FindClause__BindingObjectAssignment_1
            {
            pushFollow(FOLLOW_rule__FindClause__BindingObjectAssignment_1_in_rule__FindClause__Group__1__Impl2963);
            rule__FindClause__BindingObjectAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__1__Impl"


    // $ANTLR start "rule__FindClause__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1398:1: rule__FindClause__Group__2 : rule__FindClause__Group__2__Impl ;
    public final void rule__FindClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1402:1: ( rule__FindClause__Group__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1403:2: rule__FindClause__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__FindClause__Group__2__Impl_in_rule__FindClause__Group__22993);
            rule__FindClause__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__2"


    // $ANTLR start "rule__FindClause__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1409:1: rule__FindClause__Group__2__Impl : ( ( rule__FindClause__Group_2__0 )* ) ;
    public final void rule__FindClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1413:1: ( ( ( rule__FindClause__Group_2__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1414:1: ( ( rule__FindClause__Group_2__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1414:1: ( ( rule__FindClause__Group_2__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1415:1: ( rule__FindClause__Group_2__0 )*
            {
             before(grammarAccess.getFindClauseAccess().getGroup_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1416:1: ( rule__FindClause__Group_2__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==16) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1416:2: rule__FindClause__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__FindClause__Group_2__0_in_rule__FindClause__Group__2__Impl3020);
            	    rule__FindClause__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getFindClauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group__2__Impl"


    // $ANTLR start "rule__FindClause__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1432:1: rule__FindClause__Group_2__0 : rule__FindClause__Group_2__0__Impl rule__FindClause__Group_2__1 ;
    public final void rule__FindClause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1436:1: ( rule__FindClause__Group_2__0__Impl rule__FindClause__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1437:2: rule__FindClause__Group_2__0__Impl rule__FindClause__Group_2__1
            {
            pushFollow(FOLLOW_rule__FindClause__Group_2__0__Impl_in_rule__FindClause__Group_2__03057);
            rule__FindClause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__FindClause__Group_2__1_in_rule__FindClause__Group_2__03060);
            rule__FindClause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group_2__0"


    // $ANTLR start "rule__FindClause__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1444:1: rule__FindClause__Group_2__0__Impl : ( ',' ) ;
    public final void rule__FindClause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1448:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1449:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1449:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1450:1: ','
            {
             before(grammarAccess.getFindClauseAccess().getCommaKeyword_2_0()); 
            match(input,16,FOLLOW_16_in_rule__FindClause__Group_2__0__Impl3088); 
             after(grammarAccess.getFindClauseAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group_2__0__Impl"


    // $ANTLR start "rule__FindClause__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1463:1: rule__FindClause__Group_2__1 : rule__FindClause__Group_2__1__Impl ;
    public final void rule__FindClause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1467:1: ( rule__FindClause__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1468:2: rule__FindClause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__FindClause__Group_2__1__Impl_in_rule__FindClause__Group_2__13119);
            rule__FindClause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group_2__1"


    // $ANTLR start "rule__FindClause__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1474:1: rule__FindClause__Group_2__1__Impl : ( ( rule__FindClause__BindingObjectAssignment_2_1 ) ) ;
    public final void rule__FindClause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1478:1: ( ( ( rule__FindClause__BindingObjectAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1479:1: ( ( rule__FindClause__BindingObjectAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1479:1: ( ( rule__FindClause__BindingObjectAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1480:1: ( rule__FindClause__BindingObjectAssignment_2_1 )
            {
             before(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1481:1: ( rule__FindClause__BindingObjectAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1481:2: rule__FindClause__BindingObjectAssignment_2_1
            {
            pushFollow(FOLLOW_rule__FindClause__BindingObjectAssignment_2_1_in_rule__FindClause__Group_2__1__Impl3146);
            rule__FindClause__BindingObjectAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getFindClauseAccess().getBindingObjectAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__Group_2__1__Impl"


    // $ANTLR start "rule__BindingObject__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1495:1: rule__BindingObject__Group__0 : rule__BindingObject__Group__0__Impl rule__BindingObject__Group__1 ;
    public final void rule__BindingObject__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1499:1: ( rule__BindingObject__Group__0__Impl rule__BindingObject__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1500:2: rule__BindingObject__Group__0__Impl rule__BindingObject__Group__1
            {
            pushFollow(FOLLOW_rule__BindingObject__Group__0__Impl_in_rule__BindingObject__Group__03180);
            rule__BindingObject__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__BindingObject__Group__1_in_rule__BindingObject__Group__03183);
            rule__BindingObject__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__Group__0"


    // $ANTLR start "rule__BindingObject__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1507:1: rule__BindingObject__Group__0__Impl : ( ( rule__BindingObject__TypeAssignment_0 ) ) ;
    public final void rule__BindingObject__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1511:1: ( ( ( rule__BindingObject__TypeAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1512:1: ( ( rule__BindingObject__TypeAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1512:1: ( ( rule__BindingObject__TypeAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1513:1: ( rule__BindingObject__TypeAssignment_0 )
            {
             before(grammarAccess.getBindingObjectAccess().getTypeAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1514:1: ( rule__BindingObject__TypeAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1514:2: rule__BindingObject__TypeAssignment_0
            {
            pushFollow(FOLLOW_rule__BindingObject__TypeAssignment_0_in_rule__BindingObject__Group__0__Impl3210);
            rule__BindingObject__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBindingObjectAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__Group__0__Impl"


    // $ANTLR start "rule__BindingObject__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1524:1: rule__BindingObject__Group__1 : rule__BindingObject__Group__1__Impl ;
    public final void rule__BindingObject__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1528:1: ( rule__BindingObject__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1529:2: rule__BindingObject__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BindingObject__Group__1__Impl_in_rule__BindingObject__Group__13240);
            rule__BindingObject__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__Group__1"


    // $ANTLR start "rule__BindingObject__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1535:1: rule__BindingObject__Group__1__Impl : ( ( ( rule__BindingObject__AliasAssignment_1 ) ) ( ( rule__BindingObject__AliasAssignment_1 )* ) ) ;
    public final void rule__BindingObject__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1539:1: ( ( ( ( rule__BindingObject__AliasAssignment_1 ) ) ( ( rule__BindingObject__AliasAssignment_1 )* ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1540:1: ( ( ( rule__BindingObject__AliasAssignment_1 ) ) ( ( rule__BindingObject__AliasAssignment_1 )* ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1540:1: ( ( ( rule__BindingObject__AliasAssignment_1 ) ) ( ( rule__BindingObject__AliasAssignment_1 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1541:1: ( ( rule__BindingObject__AliasAssignment_1 ) ) ( ( rule__BindingObject__AliasAssignment_1 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1541:1: ( ( rule__BindingObject__AliasAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1542:1: ( rule__BindingObject__AliasAssignment_1 )
            {
             before(grammarAccess.getBindingObjectAccess().getAliasAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1543:1: ( rule__BindingObject__AliasAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1543:2: rule__BindingObject__AliasAssignment_1
            {
            pushFollow(FOLLOW_rule__BindingObject__AliasAssignment_1_in_rule__BindingObject__Group__1__Impl3269);
            rule__BindingObject__AliasAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBindingObjectAccess().getAliasAssignment_1()); 

            }

            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1546:1: ( ( rule__BindingObject__AliasAssignment_1 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1547:1: ( rule__BindingObject__AliasAssignment_1 )*
            {
             before(grammarAccess.getBindingObjectAccess().getAliasAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1548:1: ( rule__BindingObject__AliasAssignment_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1548:2: rule__BindingObject__AliasAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__BindingObject__AliasAssignment_1_in_rule__BindingObject__Group__1__Impl3281);
            	    rule__BindingObject__AliasAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getBindingObjectAccess().getAliasAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__Group__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1563:1: rule__ObjectType__Group_0__0 : rule__ObjectType__Group_0__0__Impl rule__ObjectType__Group_0__1 ;
    public final void rule__ObjectType__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1567:1: ( rule__ObjectType__Group_0__0__Impl rule__ObjectType__Group_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1568:2: rule__ObjectType__Group_0__0__Impl rule__ObjectType__Group_0__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_0__0__Impl_in_rule__ObjectType__Group_0__03318);
            rule__ObjectType__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_0__1_in_rule__ObjectType__Group_0__03321);
            rule__ObjectType__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_0__0"


    // $ANTLR start "rule__ObjectType__Group_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1575:1: rule__ObjectType__Group_0__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1579:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1580:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1580:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1581:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getProjectAction_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1582:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1584:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getProjectAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_0__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1594:1: rule__ObjectType__Group_0__1 : rule__ObjectType__Group_0__1__Impl ;
    public final void rule__ObjectType__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1598:1: ( rule__ObjectType__Group_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1599:2: rule__ObjectType__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_0__1__Impl_in_rule__ObjectType__Group_0__13379);
            rule__ObjectType__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_0__1"


    // $ANTLR start "rule__ObjectType__Group_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1605:1: rule__ObjectType__Group_0__1__Impl : ( ( rule__ObjectType__ValueAssignment_0_1 ) ) ;
    public final void rule__ObjectType__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1609:1: ( ( ( rule__ObjectType__ValueAssignment_0_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1610:1: ( ( rule__ObjectType__ValueAssignment_0_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1610:1: ( ( rule__ObjectType__ValueAssignment_0_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1611:1: ( rule__ObjectType__ValueAssignment_0_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1612:1: ( rule__ObjectType__ValueAssignment_0_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1612:2: rule__ObjectType__ValueAssignment_0_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_0_1_in_rule__ObjectType__Group_0__1__Impl3406);
            rule__ObjectType__ValueAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_0__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1626:1: rule__ObjectType__Group_1__0 : rule__ObjectType__Group_1__0__Impl rule__ObjectType__Group_1__1 ;
    public final void rule__ObjectType__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1630:1: ( rule__ObjectType__Group_1__0__Impl rule__ObjectType__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1631:2: rule__ObjectType__Group_1__0__Impl rule__ObjectType__Group_1__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_1__0__Impl_in_rule__ObjectType__Group_1__03440);
            rule__ObjectType__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_1__1_in_rule__ObjectType__Group_1__03443);
            rule__ObjectType__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_1__0"


    // $ANTLR start "rule__ObjectType__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1638:1: rule__ObjectType__Group_1__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1642:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1643:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1643:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1644:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getPackageAction_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1645:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1647:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getPackageAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_1__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1657:1: rule__ObjectType__Group_1__1 : rule__ObjectType__Group_1__1__Impl ;
    public final void rule__ObjectType__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1661:1: ( rule__ObjectType__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1662:2: rule__ObjectType__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_1__1__Impl_in_rule__ObjectType__Group_1__13501);
            rule__ObjectType__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_1__1"


    // $ANTLR start "rule__ObjectType__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1668:1: rule__ObjectType__Group_1__1__Impl : ( ( rule__ObjectType__ValueAssignment_1_1 ) ) ;
    public final void rule__ObjectType__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1672:1: ( ( ( rule__ObjectType__ValueAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1673:1: ( ( rule__ObjectType__ValueAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1673:1: ( ( rule__ObjectType__ValueAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1674:1: ( rule__ObjectType__ValueAssignment_1_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1675:1: ( rule__ObjectType__ValueAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1675:2: rule__ObjectType__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_1_1_in_rule__ObjectType__Group_1__1__Impl3528);
            rule__ObjectType__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_1__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1689:1: rule__ObjectType__Group_2__0 : rule__ObjectType__Group_2__0__Impl rule__ObjectType__Group_2__1 ;
    public final void rule__ObjectType__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1693:1: ( rule__ObjectType__Group_2__0__Impl rule__ObjectType__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1694:2: rule__ObjectType__Group_2__0__Impl rule__ObjectType__Group_2__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_2__0__Impl_in_rule__ObjectType__Group_2__03562);
            rule__ObjectType__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_2__1_in_rule__ObjectType__Group_2__03565);
            rule__ObjectType__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_2__0"


    // $ANTLR start "rule__ObjectType__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1701:1: rule__ObjectType__Group_2__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1705:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1706:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1706:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1707:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getClassAction_2_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1708:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1710:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getClassAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_2__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1720:1: rule__ObjectType__Group_2__1 : rule__ObjectType__Group_2__1__Impl ;
    public final void rule__ObjectType__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1724:1: ( rule__ObjectType__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1725:2: rule__ObjectType__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_2__1__Impl_in_rule__ObjectType__Group_2__13623);
            rule__ObjectType__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_2__1"


    // $ANTLR start "rule__ObjectType__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1731:1: rule__ObjectType__Group_2__1__Impl : ( ( rule__ObjectType__ValueAssignment_2_1 ) ) ;
    public final void rule__ObjectType__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1735:1: ( ( ( rule__ObjectType__ValueAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1736:1: ( ( rule__ObjectType__ValueAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1736:1: ( ( rule__ObjectType__ValueAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1737:1: ( rule__ObjectType__ValueAssignment_2_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1738:1: ( rule__ObjectType__ValueAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1738:2: rule__ObjectType__ValueAssignment_2_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_2_1_in_rule__ObjectType__Group_2__1__Impl3650);
            rule__ObjectType__ValueAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_2__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1752:1: rule__ObjectType__Group_3__0 : rule__ObjectType__Group_3__0__Impl rule__ObjectType__Group_3__1 ;
    public final void rule__ObjectType__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1756:1: ( rule__ObjectType__Group_3__0__Impl rule__ObjectType__Group_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1757:2: rule__ObjectType__Group_3__0__Impl rule__ObjectType__Group_3__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_3__0__Impl_in_rule__ObjectType__Group_3__03684);
            rule__ObjectType__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_3__1_in_rule__ObjectType__Group_3__03687);
            rule__ObjectType__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_3__0"


    // $ANTLR start "rule__ObjectType__Group_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1764:1: rule__ObjectType__Group_3__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1768:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1769:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1769:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1770:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getAspectAction_3_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1771:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1773:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getAspectAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_3__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1783:1: rule__ObjectType__Group_3__1 : rule__ObjectType__Group_3__1__Impl ;
    public final void rule__ObjectType__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1787:1: ( rule__ObjectType__Group_3__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1788:2: rule__ObjectType__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_3__1__Impl_in_rule__ObjectType__Group_3__13745);
            rule__ObjectType__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_3__1"


    // $ANTLR start "rule__ObjectType__Group_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1794:1: rule__ObjectType__Group_3__1__Impl : ( ( rule__ObjectType__ValueAssignment_3_1 ) ) ;
    public final void rule__ObjectType__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1798:1: ( ( ( rule__ObjectType__ValueAssignment_3_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1799:1: ( ( rule__ObjectType__ValueAssignment_3_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1799:1: ( ( rule__ObjectType__ValueAssignment_3_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1800:1: ( rule__ObjectType__ValueAssignment_3_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1801:1: ( rule__ObjectType__ValueAssignment_3_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1801:2: rule__ObjectType__ValueAssignment_3_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_3_1_in_rule__ObjectType__Group_3__1__Impl3772);
            rule__ObjectType__ValueAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_3__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_4__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1815:1: rule__ObjectType__Group_4__0 : rule__ObjectType__Group_4__0__Impl rule__ObjectType__Group_4__1 ;
    public final void rule__ObjectType__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1819:1: ( rule__ObjectType__Group_4__0__Impl rule__ObjectType__Group_4__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1820:2: rule__ObjectType__Group_4__0__Impl rule__ObjectType__Group_4__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_4__0__Impl_in_rule__ObjectType__Group_4__03806);
            rule__ObjectType__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_4__1_in_rule__ObjectType__Group_4__03809);
            rule__ObjectType__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_4__0"


    // $ANTLR start "rule__ObjectType__Group_4__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1827:1: rule__ObjectType__Group_4__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1831:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1832:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1832:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1833:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getInterfaceAction_4_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1834:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1836:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getInterfaceAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_4__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_4__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1846:1: rule__ObjectType__Group_4__1 : rule__ObjectType__Group_4__1__Impl ;
    public final void rule__ObjectType__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1850:1: ( rule__ObjectType__Group_4__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1851:2: rule__ObjectType__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_4__1__Impl_in_rule__ObjectType__Group_4__13867);
            rule__ObjectType__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_4__1"


    // $ANTLR start "rule__ObjectType__Group_4__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1857:1: rule__ObjectType__Group_4__1__Impl : ( ( rule__ObjectType__ValueAssignment_4_1 ) ) ;
    public final void rule__ObjectType__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1861:1: ( ( ( rule__ObjectType__ValueAssignment_4_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1862:1: ( ( rule__ObjectType__ValueAssignment_4_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1862:1: ( ( rule__ObjectType__ValueAssignment_4_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1863:1: ( rule__ObjectType__ValueAssignment_4_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_4_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1864:1: ( rule__ObjectType__ValueAssignment_4_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1864:2: rule__ObjectType__ValueAssignment_4_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_4_1_in_rule__ObjectType__Group_4__1__Impl3894);
            rule__ObjectType__ValueAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_4__1__Impl"


    // $ANTLR start "rule__ObjectType__Group_5__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1878:1: rule__ObjectType__Group_5__0 : rule__ObjectType__Group_5__0__Impl rule__ObjectType__Group_5__1 ;
    public final void rule__ObjectType__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1882:1: ( rule__ObjectType__Group_5__0__Impl rule__ObjectType__Group_5__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1883:2: rule__ObjectType__Group_5__0__Impl rule__ObjectType__Group_5__1
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_5__0__Impl_in_rule__ObjectType__Group_5__03928);
            rule__ObjectType__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ObjectType__Group_5__1_in_rule__ObjectType__Group_5__03931);
            rule__ObjectType__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_5__0"


    // $ANTLR start "rule__ObjectType__Group_5__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1890:1: rule__ObjectType__Group_5__0__Impl : ( () ) ;
    public final void rule__ObjectType__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1894:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1895:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1895:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1896:1: ()
            {
             before(grammarAccess.getObjectTypeAccess().getEnumAction_5_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1897:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1899:1: 
            {
            }

             after(grammarAccess.getObjectTypeAccess().getEnumAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_5__0__Impl"


    // $ANTLR start "rule__ObjectType__Group_5__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1909:1: rule__ObjectType__Group_5__1 : rule__ObjectType__Group_5__1__Impl ;
    public final void rule__ObjectType__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1913:1: ( rule__ObjectType__Group_5__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1914:2: rule__ObjectType__Group_5__1__Impl
            {
            pushFollow(FOLLOW_rule__ObjectType__Group_5__1__Impl_in_rule__ObjectType__Group_5__13989);
            rule__ObjectType__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_5__1"


    // $ANTLR start "rule__ObjectType__Group_5__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1920:1: rule__ObjectType__Group_5__1__Impl : ( ( rule__ObjectType__ValueAssignment_5_1 ) ) ;
    public final void rule__ObjectType__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1924:1: ( ( ( rule__ObjectType__ValueAssignment_5_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1925:1: ( ( rule__ObjectType__ValueAssignment_5_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1925:1: ( ( rule__ObjectType__ValueAssignment_5_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1926:1: ( rule__ObjectType__ValueAssignment_5_1 )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAssignment_5_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1927:1: ( rule__ObjectType__ValueAssignment_5_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1927:2: rule__ObjectType__ValueAssignment_5_1
            {
            pushFollow(FOLLOW_rule__ObjectType__ValueAssignment_5_1_in_rule__ObjectType__Group_5__1__Impl4016);
            rule__ObjectType__ValueAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectTypeAccess().getValueAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__Group_5__1__Impl"


    // $ANTLR start "rule__WhereClause__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1941:1: rule__WhereClause__Group__0 : rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1 ;
    public final void rule__WhereClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1945:1: ( rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1946:2: rule__WhereClause__Group__0__Impl rule__WhereClause__Group__1
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__0__Impl_in_rule__WhereClause__Group__04050);
            rule__WhereClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WhereClause__Group__1_in_rule__WhereClause__Group__04053);
            rule__WhereClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__0"


    // $ANTLR start "rule__WhereClause__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1953:1: rule__WhereClause__Group__0__Impl : ( ( rule__WhereClause__ClauseAssignment_0 ) ) ;
    public final void rule__WhereClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1957:1: ( ( ( rule__WhereClause__ClauseAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1958:1: ( ( rule__WhereClause__ClauseAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1958:1: ( ( rule__WhereClause__ClauseAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1959:1: ( rule__WhereClause__ClauseAssignment_0 )
            {
             before(grammarAccess.getWhereClauseAccess().getClauseAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1960:1: ( rule__WhereClause__ClauseAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1960:2: rule__WhereClause__ClauseAssignment_0
            {
            pushFollow(FOLLOW_rule__WhereClause__ClauseAssignment_0_in_rule__WhereClause__Group__0__Impl4080);
            rule__WhereClause__ClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__0__Impl"


    // $ANTLR start "rule__WhereClause__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1970:1: rule__WhereClause__Group__1 : rule__WhereClause__Group__1__Impl ;
    public final void rule__WhereClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1974:1: ( rule__WhereClause__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1975:2: rule__WhereClause__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WhereClause__Group__1__Impl_in_rule__WhereClause__Group__14110);
            rule__WhereClause__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__1"


    // $ANTLR start "rule__WhereClause__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1981:1: rule__WhereClause__Group__1__Impl : ( ( rule__WhereClause__ExpressionAssignment_1 ) ) ;
    public final void rule__WhereClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1985:1: ( ( ( rule__WhereClause__ExpressionAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1986:1: ( ( rule__WhereClause__ExpressionAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1986:1: ( ( rule__WhereClause__ExpressionAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1987:1: ( rule__WhereClause__ExpressionAssignment_1 )
            {
             before(grammarAccess.getWhereClauseAccess().getExpressionAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1988:1: ( rule__WhereClause__ExpressionAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:1988:2: rule__WhereClause__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_rule__WhereClause__ExpressionAssignment_1_in_rule__WhereClause__Group__1__Impl4137);
            rule__WhereClause__ExpressionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWhereClauseAccess().getExpressionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__Group__1__Impl"


    // $ANTLR start "rule__ReturnsClause__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2002:1: rule__ReturnsClause__Group__0 : rule__ReturnsClause__Group__0__Impl rule__ReturnsClause__Group__1 ;
    public final void rule__ReturnsClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2006:1: ( rule__ReturnsClause__Group__0__Impl rule__ReturnsClause__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2007:2: rule__ReturnsClause__Group__0__Impl rule__ReturnsClause__Group__1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group__0__Impl_in_rule__ReturnsClause__Group__04171);
            rule__ReturnsClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group__1_in_rule__ReturnsClause__Group__04174);
            rule__ReturnsClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__0"


    // $ANTLR start "rule__ReturnsClause__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2014:1: rule__ReturnsClause__Group__0__Impl : ( ( rule__ReturnsClause__ClauseAssignment_0 ) ) ;
    public final void rule__ReturnsClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2018:1: ( ( ( rule__ReturnsClause__ClauseAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2019:1: ( ( rule__ReturnsClause__ClauseAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2019:1: ( ( rule__ReturnsClause__ClauseAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2020:1: ( rule__ReturnsClause__ClauseAssignment_0 )
            {
             before(grammarAccess.getReturnsClauseAccess().getClauseAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2021:1: ( rule__ReturnsClause__ClauseAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2021:2: rule__ReturnsClause__ClauseAssignment_0
            {
            pushFollow(FOLLOW_rule__ReturnsClause__ClauseAssignment_0_in_rule__ReturnsClause__Group__0__Impl4201);
            rule__ReturnsClause__ClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__0__Impl"


    // $ANTLR start "rule__ReturnsClause__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2031:1: rule__ReturnsClause__Group__1 : rule__ReturnsClause__Group__1__Impl rule__ReturnsClause__Group__2 ;
    public final void rule__ReturnsClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2035:1: ( rule__ReturnsClause__Group__1__Impl rule__ReturnsClause__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2036:2: rule__ReturnsClause__Group__1__Impl rule__ReturnsClause__Group__2
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group__1__Impl_in_rule__ReturnsClause__Group__14231);
            rule__ReturnsClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group__2_in_rule__ReturnsClause__Group__14234);
            rule__ReturnsClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__1"


    // $ANTLR start "rule__ReturnsClause__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2043:1: rule__ReturnsClause__Group__1__Impl : ( ( rule__ReturnsClause__ExpressionsAssignment_1 ) ) ;
    public final void rule__ReturnsClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2047:1: ( ( ( rule__ReturnsClause__ExpressionsAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2048:1: ( ( rule__ReturnsClause__ExpressionsAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2048:1: ( ( rule__ReturnsClause__ExpressionsAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2049:1: ( rule__ReturnsClause__ExpressionsAssignment_1 )
            {
             before(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2050:1: ( rule__ReturnsClause__ExpressionsAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2050:2: rule__ReturnsClause__ExpressionsAssignment_1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__ExpressionsAssignment_1_in_rule__ReturnsClause__Group__1__Impl4261);
            rule__ReturnsClause__ExpressionsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__1__Impl"


    // $ANTLR start "rule__ReturnsClause__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2060:1: rule__ReturnsClause__Group__2 : rule__ReturnsClause__Group__2__Impl rule__ReturnsClause__Group__3 ;
    public final void rule__ReturnsClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2064:1: ( rule__ReturnsClause__Group__2__Impl rule__ReturnsClause__Group__3 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2065:2: rule__ReturnsClause__Group__2__Impl rule__ReturnsClause__Group__3
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group__2__Impl_in_rule__ReturnsClause__Group__24291);
            rule__ReturnsClause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group__3_in_rule__ReturnsClause__Group__24294);
            rule__ReturnsClause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__2"


    // $ANTLR start "rule__ReturnsClause__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2072:1: rule__ReturnsClause__Group__2__Impl : ( ( rule__ReturnsClause__Group_2__0 )? ) ;
    public final void rule__ReturnsClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2076:1: ( ( ( rule__ReturnsClause__Group_2__0 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2077:1: ( ( rule__ReturnsClause__Group_2__0 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2077:1: ( ( rule__ReturnsClause__Group_2__0 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2078:1: ( rule__ReturnsClause__Group_2__0 )?
            {
             before(grammarAccess.getReturnsClauseAccess().getGroup_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2079:1: ( rule__ReturnsClause__Group_2__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==17) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2079:2: rule__ReturnsClause__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__ReturnsClause__Group_2__0_in_rule__ReturnsClause__Group__2__Impl4321);
                    rule__ReturnsClause__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReturnsClauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__2__Impl"


    // $ANTLR start "rule__ReturnsClause__Group__3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2089:1: rule__ReturnsClause__Group__3 : rule__ReturnsClause__Group__3__Impl ;
    public final void rule__ReturnsClause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2093:1: ( rule__ReturnsClause__Group__3__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2094:2: rule__ReturnsClause__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group__3__Impl_in_rule__ReturnsClause__Group__34352);
            rule__ReturnsClause__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__3"


    // $ANTLR start "rule__ReturnsClause__Group__3__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2100:1: rule__ReturnsClause__Group__3__Impl : ( ( rule__ReturnsClause__Group_3__0 )* ) ;
    public final void rule__ReturnsClause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2104:1: ( ( ( rule__ReturnsClause__Group_3__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2105:1: ( ( rule__ReturnsClause__Group_3__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2105:1: ( ( rule__ReturnsClause__Group_3__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2106:1: ( rule__ReturnsClause__Group_3__0 )*
            {
             before(grammarAccess.getReturnsClauseAccess().getGroup_3()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2107:1: ( rule__ReturnsClause__Group_3__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==16) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2107:2: rule__ReturnsClause__Group_3__0
            	    {
            	    pushFollow(FOLLOW_rule__ReturnsClause__Group_3__0_in_rule__ReturnsClause__Group__3__Impl4379);
            	    rule__ReturnsClause__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getReturnsClauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group__3__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2125:1: rule__ReturnsClause__Group_2__0 : rule__ReturnsClause__Group_2__0__Impl rule__ReturnsClause__Group_2__1 ;
    public final void rule__ReturnsClause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2129:1: ( rule__ReturnsClause__Group_2__0__Impl rule__ReturnsClause__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2130:2: rule__ReturnsClause__Group_2__0__Impl rule__ReturnsClause__Group_2__1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_2__0__Impl_in_rule__ReturnsClause__Group_2__04418);
            rule__ReturnsClause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group_2__1_in_rule__ReturnsClause__Group_2__04421);
            rule__ReturnsClause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_2__0"


    // $ANTLR start "rule__ReturnsClause__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2137:1: rule__ReturnsClause__Group_2__0__Impl : ( 'as' ) ;
    public final void rule__ReturnsClause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2141:1: ( ( 'as' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2142:1: ( 'as' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2142:1: ( 'as' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2143:1: 'as'
            {
             before(grammarAccess.getReturnsClauseAccess().getAsKeyword_2_0()); 
            match(input,17,FOLLOW_17_in_rule__ReturnsClause__Group_2__0__Impl4449); 
             after(grammarAccess.getReturnsClauseAccess().getAsKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_2__0__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2156:1: rule__ReturnsClause__Group_2__1 : rule__ReturnsClause__Group_2__1__Impl ;
    public final void rule__ReturnsClause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2160:1: ( rule__ReturnsClause__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2161:2: rule__ReturnsClause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_2__1__Impl_in_rule__ReturnsClause__Group_2__14480);
            rule__ReturnsClause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_2__1"


    // $ANTLR start "rule__ReturnsClause__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2167:1: rule__ReturnsClause__Group_2__1__Impl : ( ( rule__ReturnsClause__ResultAliasAssignment_2_1 ) ) ;
    public final void rule__ReturnsClause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2171:1: ( ( ( rule__ReturnsClause__ResultAliasAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2172:1: ( ( rule__ReturnsClause__ResultAliasAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2172:1: ( ( rule__ReturnsClause__ResultAliasAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2173:1: ( rule__ReturnsClause__ResultAliasAssignment_2_1 )
            {
             before(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2174:1: ( rule__ReturnsClause__ResultAliasAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2174:2: rule__ReturnsClause__ResultAliasAssignment_2_1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__ResultAliasAssignment_2_1_in_rule__ReturnsClause__Group_2__1__Impl4507);
            rule__ReturnsClause__ResultAliasAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_2__1__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2188:1: rule__ReturnsClause__Group_3__0 : rule__ReturnsClause__Group_3__0__Impl rule__ReturnsClause__Group_3__1 ;
    public final void rule__ReturnsClause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2192:1: ( rule__ReturnsClause__Group_3__0__Impl rule__ReturnsClause__Group_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2193:2: rule__ReturnsClause__Group_3__0__Impl rule__ReturnsClause__Group_3__1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_3__0__Impl_in_rule__ReturnsClause__Group_3__04541);
            rule__ReturnsClause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group_3__1_in_rule__ReturnsClause__Group_3__04544);
            rule__ReturnsClause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__0"


    // $ANTLR start "rule__ReturnsClause__Group_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2200:1: rule__ReturnsClause__Group_3__0__Impl : ( ',' ) ;
    public final void rule__ReturnsClause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2204:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2205:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2205:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2206:1: ','
            {
             before(grammarAccess.getReturnsClauseAccess().getCommaKeyword_3_0()); 
            match(input,16,FOLLOW_16_in_rule__ReturnsClause__Group_3__0__Impl4572); 
             after(grammarAccess.getReturnsClauseAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__0__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2219:1: rule__ReturnsClause__Group_3__1 : rule__ReturnsClause__Group_3__1__Impl rule__ReturnsClause__Group_3__2 ;
    public final void rule__ReturnsClause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2223:1: ( rule__ReturnsClause__Group_3__1__Impl rule__ReturnsClause__Group_3__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2224:2: rule__ReturnsClause__Group_3__1__Impl rule__ReturnsClause__Group_3__2
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_3__1__Impl_in_rule__ReturnsClause__Group_3__14603);
            rule__ReturnsClause__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group_3__2_in_rule__ReturnsClause__Group_3__14606);
            rule__ReturnsClause__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__1"


    // $ANTLR start "rule__ReturnsClause__Group_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2231:1: rule__ReturnsClause__Group_3__1__Impl : ( ( rule__ReturnsClause__ExpressionsAssignment_3_1 ) ) ;
    public final void rule__ReturnsClause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2235:1: ( ( ( rule__ReturnsClause__ExpressionsAssignment_3_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2236:1: ( ( rule__ReturnsClause__ExpressionsAssignment_3_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2236:1: ( ( rule__ReturnsClause__ExpressionsAssignment_3_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2237:1: ( rule__ReturnsClause__ExpressionsAssignment_3_1 )
            {
             before(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2238:1: ( rule__ReturnsClause__ExpressionsAssignment_3_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2238:2: rule__ReturnsClause__ExpressionsAssignment_3_1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__ExpressionsAssignment_3_1_in_rule__ReturnsClause__Group_3__1__Impl4633);
            rule__ReturnsClause__ExpressionsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getExpressionsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__1__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_3__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2248:1: rule__ReturnsClause__Group_3__2 : rule__ReturnsClause__Group_3__2__Impl ;
    public final void rule__ReturnsClause__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2252:1: ( rule__ReturnsClause__Group_3__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2253:2: rule__ReturnsClause__Group_3__2__Impl
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_3__2__Impl_in_rule__ReturnsClause__Group_3__24663);
            rule__ReturnsClause__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__2"


    // $ANTLR start "rule__ReturnsClause__Group_3__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2259:1: rule__ReturnsClause__Group_3__2__Impl : ( ( rule__ReturnsClause__Group_3_2__0 )? ) ;
    public final void rule__ReturnsClause__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2263:1: ( ( ( rule__ReturnsClause__Group_3_2__0 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2264:1: ( ( rule__ReturnsClause__Group_3_2__0 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2264:1: ( ( rule__ReturnsClause__Group_3_2__0 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2265:1: ( rule__ReturnsClause__Group_3_2__0 )?
            {
             before(grammarAccess.getReturnsClauseAccess().getGroup_3_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2266:1: ( rule__ReturnsClause__Group_3_2__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==17) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2266:2: rule__ReturnsClause__Group_3_2__0
                    {
                    pushFollow(FOLLOW_rule__ReturnsClause__Group_3_2__0_in_rule__ReturnsClause__Group_3__2__Impl4690);
                    rule__ReturnsClause__Group_3_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReturnsClauseAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3__2__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_3_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2282:1: rule__ReturnsClause__Group_3_2__0 : rule__ReturnsClause__Group_3_2__0__Impl rule__ReturnsClause__Group_3_2__1 ;
    public final void rule__ReturnsClause__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2286:1: ( rule__ReturnsClause__Group_3_2__0__Impl rule__ReturnsClause__Group_3_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2287:2: rule__ReturnsClause__Group_3_2__0__Impl rule__ReturnsClause__Group_3_2__1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_3_2__0__Impl_in_rule__ReturnsClause__Group_3_2__04727);
            rule__ReturnsClause__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReturnsClause__Group_3_2__1_in_rule__ReturnsClause__Group_3_2__04730);
            rule__ReturnsClause__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3_2__0"


    // $ANTLR start "rule__ReturnsClause__Group_3_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2294:1: rule__ReturnsClause__Group_3_2__0__Impl : ( 'as' ) ;
    public final void rule__ReturnsClause__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2298:1: ( ( 'as' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2299:1: ( 'as' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2299:1: ( 'as' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2300:1: 'as'
            {
             before(grammarAccess.getReturnsClauseAccess().getAsKeyword_3_2_0()); 
            match(input,17,FOLLOW_17_in_rule__ReturnsClause__Group_3_2__0__Impl4758); 
             after(grammarAccess.getReturnsClauseAccess().getAsKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3_2__0__Impl"


    // $ANTLR start "rule__ReturnsClause__Group_3_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2313:1: rule__ReturnsClause__Group_3_2__1 : rule__ReturnsClause__Group_3_2__1__Impl ;
    public final void rule__ReturnsClause__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2317:1: ( rule__ReturnsClause__Group_3_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2318:2: rule__ReturnsClause__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_rule__ReturnsClause__Group_3_2__1__Impl_in_rule__ReturnsClause__Group_3_2__14789);
            rule__ReturnsClause__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3_2__1"


    // $ANTLR start "rule__ReturnsClause__Group_3_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2324:1: rule__ReturnsClause__Group_3_2__1__Impl : ( ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 ) ) ;
    public final void rule__ReturnsClause__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2328:1: ( ( ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2329:1: ( ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2329:1: ( ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2330:1: ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 )
            {
             before(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_3_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2331:1: ( rule__ReturnsClause__ResultAliasAssignment_3_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2331:2: rule__ReturnsClause__ResultAliasAssignment_3_2_1
            {
            pushFollow(FOLLOW_rule__ReturnsClause__ResultAliasAssignment_3_2_1_in_rule__ReturnsClause__Group_3_2__1__Impl4816);
            rule__ReturnsClause__ResultAliasAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getReturnsClauseAccess().getResultAliasAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__Group_3_2__1__Impl"


    // $ANTLR start "rule__GroupByClause__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2345:1: rule__GroupByClause__Group__0 : rule__GroupByClause__Group__0__Impl rule__GroupByClause__Group__1 ;
    public final void rule__GroupByClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2349:1: ( rule__GroupByClause__Group__0__Impl rule__GroupByClause__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2350:2: rule__GroupByClause__Group__0__Impl rule__GroupByClause__Group__1
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group__0__Impl_in_rule__GroupByClause__Group__04850);
            rule__GroupByClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__GroupByClause__Group__1_in_rule__GroupByClause__Group__04853);
            rule__GroupByClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__0"


    // $ANTLR start "rule__GroupByClause__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2357:1: rule__GroupByClause__Group__0__Impl : ( ( rule__GroupByClause__ClauseAssignment_0 ) ) ;
    public final void rule__GroupByClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2361:1: ( ( ( rule__GroupByClause__ClauseAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2362:1: ( ( rule__GroupByClause__ClauseAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2362:1: ( ( rule__GroupByClause__ClauseAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2363:1: ( rule__GroupByClause__ClauseAssignment_0 )
            {
             before(grammarAccess.getGroupByClauseAccess().getClauseAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2364:1: ( rule__GroupByClause__ClauseAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2364:2: rule__GroupByClause__ClauseAssignment_0
            {
            pushFollow(FOLLOW_rule__GroupByClause__ClauseAssignment_0_in_rule__GroupByClause__Group__0__Impl4880);
            rule__GroupByClause__ClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getGroupByClauseAccess().getClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__0__Impl"


    // $ANTLR start "rule__GroupByClause__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2374:1: rule__GroupByClause__Group__1 : rule__GroupByClause__Group__1__Impl rule__GroupByClause__Group__2 ;
    public final void rule__GroupByClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2378:1: ( rule__GroupByClause__Group__1__Impl rule__GroupByClause__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2379:2: rule__GroupByClause__Group__1__Impl rule__GroupByClause__Group__2
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group__1__Impl_in_rule__GroupByClause__Group__14910);
            rule__GroupByClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__GroupByClause__Group__2_in_rule__GroupByClause__Group__14913);
            rule__GroupByClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__1"


    // $ANTLR start "rule__GroupByClause__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2386:1: rule__GroupByClause__Group__1__Impl : ( ( rule__GroupByClause__ExpressionsAssignment_1 ) ) ;
    public final void rule__GroupByClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2390:1: ( ( ( rule__GroupByClause__ExpressionsAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2391:1: ( ( rule__GroupByClause__ExpressionsAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2391:1: ( ( rule__GroupByClause__ExpressionsAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2392:1: ( rule__GroupByClause__ExpressionsAssignment_1 )
            {
             before(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2393:1: ( rule__GroupByClause__ExpressionsAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2393:2: rule__GroupByClause__ExpressionsAssignment_1
            {
            pushFollow(FOLLOW_rule__GroupByClause__ExpressionsAssignment_1_in_rule__GroupByClause__Group__1__Impl4940);
            rule__GroupByClause__ExpressionsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__1__Impl"


    // $ANTLR start "rule__GroupByClause__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2403:1: rule__GroupByClause__Group__2 : rule__GroupByClause__Group__2__Impl ;
    public final void rule__GroupByClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2407:1: ( rule__GroupByClause__Group__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2408:2: rule__GroupByClause__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group__2__Impl_in_rule__GroupByClause__Group__24970);
            rule__GroupByClause__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__2"


    // $ANTLR start "rule__GroupByClause__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2414:1: rule__GroupByClause__Group__2__Impl : ( ( rule__GroupByClause__Group_2__0 )* ) ;
    public final void rule__GroupByClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2418:1: ( ( ( rule__GroupByClause__Group_2__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2419:1: ( ( rule__GroupByClause__Group_2__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2419:1: ( ( rule__GroupByClause__Group_2__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2420:1: ( rule__GroupByClause__Group_2__0 )*
            {
             before(grammarAccess.getGroupByClauseAccess().getGroup_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2421:1: ( rule__GroupByClause__Group_2__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==16) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2421:2: rule__GroupByClause__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__GroupByClause__Group_2__0_in_rule__GroupByClause__Group__2__Impl4997);
            	    rule__GroupByClause__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getGroupByClauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group__2__Impl"


    // $ANTLR start "rule__GroupByClause__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2437:1: rule__GroupByClause__Group_2__0 : rule__GroupByClause__Group_2__0__Impl rule__GroupByClause__Group_2__1 ;
    public final void rule__GroupByClause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2441:1: ( rule__GroupByClause__Group_2__0__Impl rule__GroupByClause__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2442:2: rule__GroupByClause__Group_2__0__Impl rule__GroupByClause__Group_2__1
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group_2__0__Impl_in_rule__GroupByClause__Group_2__05034);
            rule__GroupByClause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__GroupByClause__Group_2__1_in_rule__GroupByClause__Group_2__05037);
            rule__GroupByClause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group_2__0"


    // $ANTLR start "rule__GroupByClause__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2449:1: rule__GroupByClause__Group_2__0__Impl : ( ',' ) ;
    public final void rule__GroupByClause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2453:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2454:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2454:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2455:1: ','
            {
             before(grammarAccess.getGroupByClauseAccess().getCommaKeyword_2_0()); 
            match(input,16,FOLLOW_16_in_rule__GroupByClause__Group_2__0__Impl5065); 
             after(grammarAccess.getGroupByClauseAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group_2__0__Impl"


    // $ANTLR start "rule__GroupByClause__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2468:1: rule__GroupByClause__Group_2__1 : rule__GroupByClause__Group_2__1__Impl ;
    public final void rule__GroupByClause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2472:1: ( rule__GroupByClause__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2473:2: rule__GroupByClause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__GroupByClause__Group_2__1__Impl_in_rule__GroupByClause__Group_2__15096);
            rule__GroupByClause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group_2__1"


    // $ANTLR start "rule__GroupByClause__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2479:1: rule__GroupByClause__Group_2__1__Impl : ( ( rule__GroupByClause__ExpressionsAssignment_2_1 ) ) ;
    public final void rule__GroupByClause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2483:1: ( ( ( rule__GroupByClause__ExpressionsAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2484:1: ( ( rule__GroupByClause__ExpressionsAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2484:1: ( ( rule__GroupByClause__ExpressionsAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2485:1: ( rule__GroupByClause__ExpressionsAssignment_2_1 )
            {
             before(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2486:1: ( rule__GroupByClause__ExpressionsAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2486:2: rule__GroupByClause__ExpressionsAssignment_2_1
            {
            pushFollow(FOLLOW_rule__GroupByClause__ExpressionsAssignment_2_1_in_rule__GroupByClause__Group_2__1__Impl5123);
            rule__GroupByClause__ExpressionsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getGroupByClauseAccess().getExpressionsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__Group_2__1__Impl"


    // $ANTLR start "rule__OrderByClause__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2500:1: rule__OrderByClause__Group__0 : rule__OrderByClause__Group__0__Impl rule__OrderByClause__Group__1 ;
    public final void rule__OrderByClause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2504:1: ( rule__OrderByClause__Group__0__Impl rule__OrderByClause__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2505:2: rule__OrderByClause__Group__0__Impl rule__OrderByClause__Group__1
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__0__Impl_in_rule__OrderByClause__Group__05157);
            rule__OrderByClause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group__1_in_rule__OrderByClause__Group__05160);
            rule__OrderByClause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__0"


    // $ANTLR start "rule__OrderByClause__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2512:1: rule__OrderByClause__Group__0__Impl : ( ( rule__OrderByClause__ClauseAssignment_0 ) ) ;
    public final void rule__OrderByClause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2516:1: ( ( ( rule__OrderByClause__ClauseAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2517:1: ( ( rule__OrderByClause__ClauseAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2517:1: ( ( rule__OrderByClause__ClauseAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2518:1: ( rule__OrderByClause__ClauseAssignment_0 )
            {
             before(grammarAccess.getOrderByClauseAccess().getClauseAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2519:1: ( rule__OrderByClause__ClauseAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2519:2: rule__OrderByClause__ClauseAssignment_0
            {
            pushFollow(FOLLOW_rule__OrderByClause__ClauseAssignment_0_in_rule__OrderByClause__Group__0__Impl5187);
            rule__OrderByClause__ClauseAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getClauseAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__0__Impl"


    // $ANTLR start "rule__OrderByClause__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2529:1: rule__OrderByClause__Group__1 : rule__OrderByClause__Group__1__Impl rule__OrderByClause__Group__2 ;
    public final void rule__OrderByClause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2533:1: ( rule__OrderByClause__Group__1__Impl rule__OrderByClause__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2534:2: rule__OrderByClause__Group__1__Impl rule__OrderByClause__Group__2
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__1__Impl_in_rule__OrderByClause__Group__15217);
            rule__OrderByClause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group__2_in_rule__OrderByClause__Group__15220);
            rule__OrderByClause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__1"


    // $ANTLR start "rule__OrderByClause__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2541:1: rule__OrderByClause__Group__1__Impl : ( ( rule__OrderByClause__ExpressionsAssignment_1 ) ) ;
    public final void rule__OrderByClause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2545:1: ( ( ( rule__OrderByClause__ExpressionsAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2546:1: ( ( rule__OrderByClause__ExpressionsAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2546:1: ( ( rule__OrderByClause__ExpressionsAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2547:1: ( rule__OrderByClause__ExpressionsAssignment_1 )
            {
             before(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2548:1: ( rule__OrderByClause__ExpressionsAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2548:2: rule__OrderByClause__ExpressionsAssignment_1
            {
            pushFollow(FOLLOW_rule__OrderByClause__ExpressionsAssignment_1_in_rule__OrderByClause__Group__1__Impl5247);
            rule__OrderByClause__ExpressionsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__1__Impl"


    // $ANTLR start "rule__OrderByClause__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2558:1: rule__OrderByClause__Group__2 : rule__OrderByClause__Group__2__Impl rule__OrderByClause__Group__3 ;
    public final void rule__OrderByClause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2562:1: ( rule__OrderByClause__Group__2__Impl rule__OrderByClause__Group__3 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2563:2: rule__OrderByClause__Group__2__Impl rule__OrderByClause__Group__3
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__2__Impl_in_rule__OrderByClause__Group__25277);
            rule__OrderByClause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group__3_in_rule__OrderByClause__Group__25280);
            rule__OrderByClause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__2"


    // $ANTLR start "rule__OrderByClause__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2570:1: rule__OrderByClause__Group__2__Impl : ( ( rule__OrderByClause__Group_2__0 )* ) ;
    public final void rule__OrderByClause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2574:1: ( ( ( rule__OrderByClause__Group_2__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2575:1: ( ( rule__OrderByClause__Group_2__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2575:1: ( ( rule__OrderByClause__Group_2__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2576:1: ( rule__OrderByClause__Group_2__0 )*
            {
             before(grammarAccess.getOrderByClauseAccess().getGroup_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2577:1: ( rule__OrderByClause__Group_2__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==16) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2577:2: rule__OrderByClause__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__OrderByClause__Group_2__0_in_rule__OrderByClause__Group__2__Impl5307);
            	    rule__OrderByClause__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getOrderByClauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__2__Impl"


    // $ANTLR start "rule__OrderByClause__Group__3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2587:1: rule__OrderByClause__Group__3 : rule__OrderByClause__Group__3__Impl rule__OrderByClause__Group__4 ;
    public final void rule__OrderByClause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2591:1: ( rule__OrderByClause__Group__3__Impl rule__OrderByClause__Group__4 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2592:2: rule__OrderByClause__Group__3__Impl rule__OrderByClause__Group__4
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__3__Impl_in_rule__OrderByClause__Group__35338);
            rule__OrderByClause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group__4_in_rule__OrderByClause__Group__35341);
            rule__OrderByClause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__3"


    // $ANTLR start "rule__OrderByClause__Group__3__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2599:1: rule__OrderByClause__Group__3__Impl : ( ( rule__OrderByClause__OptionAssignment_3 )? ) ;
    public final void rule__OrderByClause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2603:1: ( ( ( rule__OrderByClause__OptionAssignment_3 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2604:1: ( ( rule__OrderByClause__OptionAssignment_3 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2604:1: ( ( rule__OrderByClause__OptionAssignment_3 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2605:1: ( rule__OrderByClause__OptionAssignment_3 )?
            {
             before(grammarAccess.getOrderByClauseAccess().getOptionAssignment_3()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2606:1: ( rule__OrderByClause__OptionAssignment_3 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=12 && LA21_0<=13)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2606:2: rule__OrderByClause__OptionAssignment_3
                    {
                    pushFollow(FOLLOW_rule__OrderByClause__OptionAssignment_3_in_rule__OrderByClause__Group__3__Impl5368);
                    rule__OrderByClause__OptionAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOrderByClauseAccess().getOptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__3__Impl"


    // $ANTLR start "rule__OrderByClause__Group__4"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2616:1: rule__OrderByClause__Group__4 : rule__OrderByClause__Group__4__Impl ;
    public final void rule__OrderByClause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2620:1: ( rule__OrderByClause__Group__4__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2621:2: rule__OrderByClause__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group__4__Impl_in_rule__OrderByClause__Group__45399);
            rule__OrderByClause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__4"


    // $ANTLR start "rule__OrderByClause__Group__4__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2627:1: rule__OrderByClause__Group__4__Impl : ( ( rule__OrderByClause__Group_4__0 )? ) ;
    public final void rule__OrderByClause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2631:1: ( ( ( rule__OrderByClause__Group_4__0 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2632:1: ( ( rule__OrderByClause__Group_4__0 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2632:1: ( ( rule__OrderByClause__Group_4__0 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2633:1: ( rule__OrderByClause__Group_4__0 )?
            {
             before(grammarAccess.getOrderByClauseAccess().getGroup_4()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2634:1: ( rule__OrderByClause__Group_4__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==18) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2634:2: rule__OrderByClause__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__OrderByClause__Group_4__0_in_rule__OrderByClause__Group__4__Impl5426);
                    rule__OrderByClause__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOrderByClauseAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group__4__Impl"


    // $ANTLR start "rule__OrderByClause__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2654:1: rule__OrderByClause__Group_2__0 : rule__OrderByClause__Group_2__0__Impl rule__OrderByClause__Group_2__1 ;
    public final void rule__OrderByClause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2658:1: ( rule__OrderByClause__Group_2__0__Impl rule__OrderByClause__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2659:2: rule__OrderByClause__Group_2__0__Impl rule__OrderByClause__Group_2__1
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group_2__0__Impl_in_rule__OrderByClause__Group_2__05467);
            rule__OrderByClause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group_2__1_in_rule__OrderByClause__Group_2__05470);
            rule__OrderByClause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_2__0"


    // $ANTLR start "rule__OrderByClause__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2666:1: rule__OrderByClause__Group_2__0__Impl : ( ',' ) ;
    public final void rule__OrderByClause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2670:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2671:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2671:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2672:1: ','
            {
             before(grammarAccess.getOrderByClauseAccess().getCommaKeyword_2_0()); 
            match(input,16,FOLLOW_16_in_rule__OrderByClause__Group_2__0__Impl5498); 
             after(grammarAccess.getOrderByClauseAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_2__0__Impl"


    // $ANTLR start "rule__OrderByClause__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2685:1: rule__OrderByClause__Group_2__1 : rule__OrderByClause__Group_2__1__Impl ;
    public final void rule__OrderByClause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2689:1: ( rule__OrderByClause__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2690:2: rule__OrderByClause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group_2__1__Impl_in_rule__OrderByClause__Group_2__15529);
            rule__OrderByClause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_2__1"


    // $ANTLR start "rule__OrderByClause__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2696:1: rule__OrderByClause__Group_2__1__Impl : ( ( rule__OrderByClause__ExpressionsAssignment_2_1 ) ) ;
    public final void rule__OrderByClause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2700:1: ( ( ( rule__OrderByClause__ExpressionsAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2701:1: ( ( rule__OrderByClause__ExpressionsAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2701:1: ( ( rule__OrderByClause__ExpressionsAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2702:1: ( rule__OrderByClause__ExpressionsAssignment_2_1 )
            {
             before(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2703:1: ( rule__OrderByClause__ExpressionsAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2703:2: rule__OrderByClause__ExpressionsAssignment_2_1
            {
            pushFollow(FOLLOW_rule__OrderByClause__ExpressionsAssignment_2_1_in_rule__OrderByClause__Group_2__1__Impl5556);
            rule__OrderByClause__ExpressionsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getExpressionsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_2__1__Impl"


    // $ANTLR start "rule__OrderByClause__Group_4__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2717:1: rule__OrderByClause__Group_4__0 : rule__OrderByClause__Group_4__0__Impl rule__OrderByClause__Group_4__1 ;
    public final void rule__OrderByClause__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2721:1: ( rule__OrderByClause__Group_4__0__Impl rule__OrderByClause__Group_4__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2722:2: rule__OrderByClause__Group_4__0__Impl rule__OrderByClause__Group_4__1
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group_4__0__Impl_in_rule__OrderByClause__Group_4__05590);
            rule__OrderByClause__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OrderByClause__Group_4__1_in_rule__OrderByClause__Group_4__05593);
            rule__OrderByClause__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_4__0"


    // $ANTLR start "rule__OrderByClause__Group_4__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2729:1: rule__OrderByClause__Group_4__0__Impl : ( 'having' ) ;
    public final void rule__OrderByClause__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2733:1: ( ( 'having' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2734:1: ( 'having' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2734:1: ( 'having' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2735:1: 'having'
            {
             before(grammarAccess.getOrderByClauseAccess().getHavingKeyword_4_0()); 
            match(input,18,FOLLOW_18_in_rule__OrderByClause__Group_4__0__Impl5621); 
             after(grammarAccess.getOrderByClauseAccess().getHavingKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_4__0__Impl"


    // $ANTLR start "rule__OrderByClause__Group_4__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2748:1: rule__OrderByClause__Group_4__1 : rule__OrderByClause__Group_4__1__Impl ;
    public final void rule__OrderByClause__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2752:1: ( rule__OrderByClause__Group_4__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2753:2: rule__OrderByClause__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__OrderByClause__Group_4__1__Impl_in_rule__OrderByClause__Group_4__15652);
            rule__OrderByClause__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_4__1"


    // $ANTLR start "rule__OrderByClause__Group_4__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2759:1: rule__OrderByClause__Group_4__1__Impl : ( ( rule__OrderByClause__HavingExpressionAssignment_4_1 ) ) ;
    public final void rule__OrderByClause__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2763:1: ( ( ( rule__OrderByClause__HavingExpressionAssignment_4_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2764:1: ( ( rule__OrderByClause__HavingExpressionAssignment_4_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2764:1: ( ( rule__OrderByClause__HavingExpressionAssignment_4_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2765:1: ( rule__OrderByClause__HavingExpressionAssignment_4_1 )
            {
             before(grammarAccess.getOrderByClauseAccess().getHavingExpressionAssignment_4_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2766:1: ( rule__OrderByClause__HavingExpressionAssignment_4_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2766:2: rule__OrderByClause__HavingExpressionAssignment_4_1
            {
            pushFollow(FOLLOW_rule__OrderByClause__HavingExpressionAssignment_4_1_in_rule__OrderByClause__Group_4__1__Impl5679);
            rule__OrderByClause__HavingExpressionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getHavingExpressionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__Group_4__1__Impl"


    // $ANTLR start "rule__SimpleQualifiedName__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2780:1: rule__SimpleQualifiedName__Group__0 : rule__SimpleQualifiedName__Group__0__Impl rule__SimpleQualifiedName__Group__1 ;
    public final void rule__SimpleQualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2784:1: ( rule__SimpleQualifiedName__Group__0__Impl rule__SimpleQualifiedName__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2785:2: rule__SimpleQualifiedName__Group__0__Impl rule__SimpleQualifiedName__Group__1
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group__0__Impl_in_rule__SimpleQualifiedName__Group__05713);
            rule__SimpleQualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group__1_in_rule__SimpleQualifiedName__Group__05716);
            rule__SimpleQualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group__0"


    // $ANTLR start "rule__SimpleQualifiedName__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2792:1: rule__SimpleQualifiedName__Group__0__Impl : ( ( rule__SimpleQualifiedName__QualifiersAssignment_0 ) ) ;
    public final void rule__SimpleQualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2796:1: ( ( ( rule__SimpleQualifiedName__QualifiersAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2797:1: ( ( rule__SimpleQualifiedName__QualifiersAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2797:1: ( ( rule__SimpleQualifiedName__QualifiersAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2798:1: ( rule__SimpleQualifiedName__QualifiersAssignment_0 )
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2799:1: ( rule__SimpleQualifiedName__QualifiersAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2799:2: rule__SimpleQualifiedName__QualifiersAssignment_0
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__QualifiersAssignment_0_in_rule__SimpleQualifiedName__Group__0__Impl5743);
            rule__SimpleQualifiedName__QualifiersAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group__0__Impl"


    // $ANTLR start "rule__SimpleQualifiedName__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2809:1: rule__SimpleQualifiedName__Group__1 : rule__SimpleQualifiedName__Group__1__Impl ;
    public final void rule__SimpleQualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2813:1: ( rule__SimpleQualifiedName__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2814:2: rule__SimpleQualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group__1__Impl_in_rule__SimpleQualifiedName__Group__15773);
            rule__SimpleQualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group__1"


    // $ANTLR start "rule__SimpleQualifiedName__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2820:1: rule__SimpleQualifiedName__Group__1__Impl : ( ( rule__SimpleQualifiedName__Group_1__0 )* ) ;
    public final void rule__SimpleQualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2824:1: ( ( ( rule__SimpleQualifiedName__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2825:1: ( ( rule__SimpleQualifiedName__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2825:1: ( ( rule__SimpleQualifiedName__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2826:1: ( rule__SimpleQualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2827:1: ( rule__SimpleQualifiedName__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==19) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2827:2: rule__SimpleQualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__SimpleQualifiedName__Group_1__0_in_rule__SimpleQualifiedName__Group__1__Impl5800);
            	    rule__SimpleQualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getSimpleQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group__1__Impl"


    // $ANTLR start "rule__SimpleQualifiedName__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2841:1: rule__SimpleQualifiedName__Group_1__0 : rule__SimpleQualifiedName__Group_1__0__Impl rule__SimpleQualifiedName__Group_1__1 ;
    public final void rule__SimpleQualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2845:1: ( rule__SimpleQualifiedName__Group_1__0__Impl rule__SimpleQualifiedName__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2846:2: rule__SimpleQualifiedName__Group_1__0__Impl rule__SimpleQualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group_1__0__Impl_in_rule__SimpleQualifiedName__Group_1__05835);
            rule__SimpleQualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group_1__1_in_rule__SimpleQualifiedName__Group_1__05838);
            rule__SimpleQualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group_1__0"


    // $ANTLR start "rule__SimpleQualifiedName__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2853:1: rule__SimpleQualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__SimpleQualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2857:1: ( ( '.' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2858:1: ( '.' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2858:1: ( '.' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2859:1: '.'
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,19,FOLLOW_19_in_rule__SimpleQualifiedName__Group_1__0__Impl5866); 
             after(grammarAccess.getSimpleQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__SimpleQualifiedName__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2872:1: rule__SimpleQualifiedName__Group_1__1 : rule__SimpleQualifiedName__Group_1__1__Impl ;
    public final void rule__SimpleQualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2876:1: ( rule__SimpleQualifiedName__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2877:2: rule__SimpleQualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__Group_1__1__Impl_in_rule__SimpleQualifiedName__Group_1__15897);
            rule__SimpleQualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group_1__1"


    // $ANTLR start "rule__SimpleQualifiedName__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2883:1: rule__SimpleQualifiedName__Group_1__1__Impl : ( ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 ) ) ;
    public final void rule__SimpleQualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2887:1: ( ( ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2888:1: ( ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2888:1: ( ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2889:1: ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 )
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2890:1: ( rule__SimpleQualifiedName__QualifiersAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2890:2: rule__SimpleQualifiedName__QualifiersAssignment_1_1
            {
            pushFollow(FOLLOW_rule__SimpleQualifiedName__QualifiersAssignment_1_1_in_rule__SimpleQualifiedName__Group_1__1__Impl5924);
            rule__SimpleQualifiedName__QualifiersAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2904:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2908:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2909:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__05958);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group__1_in_rule__Or__Group__05961);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2916:1: rule__Or__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2920:1: ( ( ruleAnd ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2921:1: ( ruleAnd )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2921:1: ( ruleAnd )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2922:1: ruleAnd
            {
             before(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleAnd_in_rule__Or__Group__0__Impl5988);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2933:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2937:1: ( rule__Or__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2938:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__16017);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2944:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2948:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2949:1: ( ( rule__Or__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2949:1: ( ( rule__Or__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2950:1: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2951:1: ( rule__Or__Group_1__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==20) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2951:2: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Or__Group_1__0_in_rule__Or__Group__1__Impl6044);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2965:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2969:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2970:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_rule__Or__Group_1__0__Impl_in_rule__Or__Group_1__06079);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group_1__1_in_rule__Or__Group_1__06082);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2977:1: rule__Or__Group_1__0__Impl : ( ( rule__Or__Group_1_0__0 ) ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2981:1: ( ( ( rule__Or__Group_1_0__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2982:1: ( ( rule__Or__Group_1_0__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2982:1: ( ( rule__Or__Group_1_0__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2983:1: ( rule__Or__Group_1_0__0 )
            {
             before(grammarAccess.getOrAccess().getGroup_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2984:1: ( rule__Or__Group_1_0__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2984:2: rule__Or__Group_1_0__0
            {
            pushFollow(FOLLOW_rule__Or__Group_1_0__0_in_rule__Or__Group_1__0__Impl6109);
            rule__Or__Group_1_0__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2994:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2998:1: ( rule__Or__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:2999:2: rule__Or__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group_1__1__Impl_in_rule__Or__Group_1__16139);
            rule__Or__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3005:1: rule__Or__Group_1__1__Impl : ( ( rule__Or__RightAssignment_1_1 ) ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3009:1: ( ( ( rule__Or__RightAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3010:1: ( ( rule__Or__RightAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3010:1: ( ( rule__Or__RightAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3011:1: ( rule__Or__RightAssignment_1_1 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3012:1: ( rule__Or__RightAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3012:2: rule__Or__RightAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Or__RightAssignment_1_1_in_rule__Or__Group_1__1__Impl6166);
            rule__Or__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3026:1: rule__Or__Group_1_0__0 : rule__Or__Group_1_0__0__Impl rule__Or__Group_1_0__1 ;
    public final void rule__Or__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3030:1: ( rule__Or__Group_1_0__0__Impl rule__Or__Group_1_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3031:2: rule__Or__Group_1_0__0__Impl rule__Or__Group_1_0__1
            {
            pushFollow(FOLLOW_rule__Or__Group_1_0__0__Impl_in_rule__Or__Group_1_0__06200);
            rule__Or__Group_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Or__Group_1_0__1_in_rule__Or__Group_1_0__06203);
            rule__Or__Group_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1_0__0"


    // $ANTLR start "rule__Or__Group_1_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3038:1: rule__Or__Group_1_0__0__Impl : ( 'or' ) ;
    public final void rule__Or__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3042:1: ( ( 'or' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3043:1: ( 'or' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3043:1: ( 'or' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3044:1: 'or'
            {
             before(grammarAccess.getOrAccess().getOrKeyword_1_0_0()); 
            match(input,20,FOLLOW_20_in_rule__Or__Group_1_0__0__Impl6231); 
             after(grammarAccess.getOrAccess().getOrKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1_0__0__Impl"


    // $ANTLR start "rule__Or__Group_1_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3057:1: rule__Or__Group_1_0__1 : rule__Or__Group_1_0__1__Impl ;
    public final void rule__Or__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3061:1: ( rule__Or__Group_1_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3062:2: rule__Or__Group_1_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group_1_0__1__Impl_in_rule__Or__Group_1_0__16262);
            rule__Or__Group_1_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1_0__1"


    // $ANTLR start "rule__Or__Group_1_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3068:1: rule__Or__Group_1_0__1__Impl : ( () ) ;
    public final void rule__Or__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3072:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3073:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3073:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3074:1: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3075:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3077:1: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1_0__1__Impl"


    // $ANTLR start "rule__And__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3091:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3095:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3096:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__06324);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group__1_in_rule__And__Group__06327);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3103:1: rule__And__Group__0__Impl : ( ruleRelation ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3107:1: ( ( ruleRelation ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3108:1: ( ruleRelation )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3108:1: ( ruleRelation )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3109:1: ruleRelation
            {
             before(grammarAccess.getAndAccess().getRelationParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleRelation_in_rule__And__Group__0__Impl6354);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRelationParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3120:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3124:1: ( rule__And__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3125:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__16383);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3131:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3135:1: ( ( ( rule__And__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3136:1: ( ( rule__And__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3136:1: ( ( rule__And__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3137:1: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3138:1: ( rule__And__Group_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==21) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3138:2: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__And__Group_1__0_in_rule__And__Group__1__Impl6410);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3152:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3156:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3157:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_rule__And__Group_1__0__Impl_in_rule__And__Group_1__06445);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group_1__1_in_rule__And__Group_1__06448);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3164:1: rule__And__Group_1__0__Impl : ( 'and' ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3168:1: ( ( 'and' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3169:1: ( 'and' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3169:1: ( 'and' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3170:1: 'and'
            {
             before(grammarAccess.getAndAccess().getAndKeyword_1_0()); 
            match(input,21,FOLLOW_21_in_rule__And__Group_1__0__Impl6476); 
             after(grammarAccess.getAndAccess().getAndKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3183:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3187:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3188:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_rule__And__Group_1__1__Impl_in_rule__And__Group_1__16507);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__And__Group_1__2_in_rule__And__Group_1__16510);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3195:1: rule__And__Group_1__1__Impl : ( () ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3199:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3200:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3200:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3201:1: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3202:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3204:1: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3214:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3218:1: ( rule__And__Group_1__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3219:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__And__Group_1__2__Impl_in_rule__And__Group_1__26568);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3225:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3229:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3230:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3230:1: ( ( rule__And__RightAssignment_1_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3231:1: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3232:1: ( rule__And__RightAssignment_1_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3232:2: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_rule__And__RightAssignment_1_2_in_rule__And__Group_1__2__Impl6595);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Relation__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3248:1: rule__Relation__Group__0 : rule__Relation__Group__0__Impl rule__Relation__Group__1 ;
    public final void rule__Relation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3252:1: ( rule__Relation__Group__0__Impl rule__Relation__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3253:2: rule__Relation__Group__0__Impl rule__Relation__Group__1
            {
            pushFollow(FOLLOW_rule__Relation__Group__0__Impl_in_rule__Relation__Group__06631);
            rule__Relation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group__1_in_rule__Relation__Group__06634);
            rule__Relation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__0"


    // $ANTLR start "rule__Relation__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3260:1: rule__Relation__Group__0__Impl : ( ruleAdd ) ;
    public final void rule__Relation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3264:1: ( ( ruleAdd ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3265:1: ( ruleAdd )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3265:1: ( ruleAdd )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3266:1: ruleAdd
            {
             before(grammarAccess.getRelationAccess().getAddParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleAdd_in_rule__Relation__Group__0__Impl6661);
            ruleAdd();

            state._fsp--;

             after(grammarAccess.getRelationAccess().getAddParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__0__Impl"


    // $ANTLR start "rule__Relation__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3277:1: rule__Relation__Group__1 : rule__Relation__Group__1__Impl ;
    public final void rule__Relation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3281:1: ( rule__Relation__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3282:2: rule__Relation__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group__1__Impl_in_rule__Relation__Group__16690);
            rule__Relation__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__1"


    // $ANTLR start "rule__Relation__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3288:1: rule__Relation__Group__1__Impl : ( ( rule__Relation__Group_1__0 )* ) ;
    public final void rule__Relation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3292:1: ( ( ( rule__Relation__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3293:1: ( ( rule__Relation__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3293:1: ( ( rule__Relation__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3294:1: ( rule__Relation__Group_1__0 )*
            {
             before(grammarAccess.getRelationAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3295:1: ( rule__Relation__Group_1__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=22 && LA26_0<=28)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3295:2: rule__Relation__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Relation__Group_1__0_in_rule__Relation__Group__1__Impl6717);
            	    rule__Relation__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getRelationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group__1__Impl"


    // $ANTLR start "rule__Relation__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3309:1: rule__Relation__Group_1__0 : rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1 ;
    public final void rule__Relation__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3313:1: ( rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3314:2: rule__Relation__Group_1__0__Impl rule__Relation__Group_1__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1__0__Impl_in_rule__Relation__Group_1__06752);
            rule__Relation__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1__1_in_rule__Relation__Group_1__06755);
            rule__Relation__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__0"


    // $ANTLR start "rule__Relation__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3321:1: rule__Relation__Group_1__0__Impl : ( ( rule__Relation__Alternatives_1_0 ) ) ;
    public final void rule__Relation__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3325:1: ( ( ( rule__Relation__Alternatives_1_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3326:1: ( ( rule__Relation__Alternatives_1_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3326:1: ( ( rule__Relation__Alternatives_1_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3327:1: ( rule__Relation__Alternatives_1_0 )
            {
             before(grammarAccess.getRelationAccess().getAlternatives_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3328:1: ( rule__Relation__Alternatives_1_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3328:2: rule__Relation__Alternatives_1_0
            {
            pushFollow(FOLLOW_rule__Relation__Alternatives_1_0_in_rule__Relation__Group_1__0__Impl6782);
            rule__Relation__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__0__Impl"


    // $ANTLR start "rule__Relation__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3338:1: rule__Relation__Group_1__1 : rule__Relation__Group_1__1__Impl ;
    public final void rule__Relation__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3342:1: ( rule__Relation__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3343:2: rule__Relation__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1__1__Impl_in_rule__Relation__Group_1__16812);
            rule__Relation__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__1"


    // $ANTLR start "rule__Relation__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3349:1: rule__Relation__Group_1__1__Impl : ( ( rule__Relation__RightAssignment_1_1 ) ) ;
    public final void rule__Relation__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3353:1: ( ( ( rule__Relation__RightAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3354:1: ( ( rule__Relation__RightAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3354:1: ( ( rule__Relation__RightAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3355:1: ( rule__Relation__RightAssignment_1_1 )
            {
             before(grammarAccess.getRelationAccess().getRightAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3356:1: ( rule__Relation__RightAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3356:2: rule__Relation__RightAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Relation__RightAssignment_1_1_in_rule__Relation__Group_1__1__Impl6839);
            rule__Relation__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getRelationAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3370:1: rule__Relation__Group_1_0_0__0 : rule__Relation__Group_1_0_0__0__Impl rule__Relation__Group_1_0_0__1 ;
    public final void rule__Relation__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3374:1: ( rule__Relation__Group_1_0_0__0__Impl rule__Relation__Group_1_0_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3375:2: rule__Relation__Group_1_0_0__0__Impl rule__Relation__Group_1_0_0__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_0__0__Impl_in_rule__Relation__Group_1_0_0__06873);
            rule__Relation__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_0__1_in_rule__Relation__Group_1_0_0__06876);
            rule__Relation__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_0__0"


    // $ANTLR start "rule__Relation__Group_1_0_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3382:1: rule__Relation__Group_1_0_0__0__Impl : ( '=' ) ;
    public final void rule__Relation__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3386:1: ( ( '=' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3387:1: ( '=' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3387:1: ( '=' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3388:1: '='
            {
             before(grammarAccess.getRelationAccess().getEqualsSignKeyword_1_0_0_0()); 
            match(input,22,FOLLOW_22_in_rule__Relation__Group_1_0_0__0__Impl6904); 
             after(grammarAccess.getRelationAccess().getEqualsSignKeyword_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3401:1: rule__Relation__Group_1_0_0__1 : rule__Relation__Group_1_0_0__1__Impl ;
    public final void rule__Relation__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3405:1: ( rule__Relation__Group_1_0_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3406:2: rule__Relation__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_0__1__Impl_in_rule__Relation__Group_1_0_0__16935);
            rule__Relation__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_0__1"


    // $ANTLR start "rule__Relation__Group_1_0_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3412:1: rule__Relation__Group_1_0_0__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3416:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3417:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3417:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3418:1: ()
            {
             before(grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3419:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3421:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getEqualsLeftAction_1_0_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3435:1: rule__Relation__Group_1_0_1__0 : rule__Relation__Group_1_0_1__0__Impl rule__Relation__Group_1_0_1__1 ;
    public final void rule__Relation__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3439:1: ( rule__Relation__Group_1_0_1__0__Impl rule__Relation__Group_1_0_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3440:2: rule__Relation__Group_1_0_1__0__Impl rule__Relation__Group_1_0_1__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_1__0__Impl_in_rule__Relation__Group_1_0_1__06997);
            rule__Relation__Group_1_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_1__1_in_rule__Relation__Group_1_0_1__07000);
            rule__Relation__Group_1_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_1__0"


    // $ANTLR start "rule__Relation__Group_1_0_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3447:1: rule__Relation__Group_1_0_1__0__Impl : ( '>' ) ;
    public final void rule__Relation__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3451:1: ( ( '>' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3452:1: ( '>' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3452:1: ( '>' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3453:1: '>'
            {
             before(grammarAccess.getRelationAccess().getGreaterThanSignKeyword_1_0_1_0()); 
            match(input,23,FOLLOW_23_in_rule__Relation__Group_1_0_1__0__Impl7028); 
             after(grammarAccess.getRelationAccess().getGreaterThanSignKeyword_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3466:1: rule__Relation__Group_1_0_1__1 : rule__Relation__Group_1_0_1__1__Impl ;
    public final void rule__Relation__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3470:1: ( rule__Relation__Group_1_0_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3471:2: rule__Relation__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_1__1__Impl_in_rule__Relation__Group_1_0_1__17059);
            rule__Relation__Group_1_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_1__1"


    // $ANTLR start "rule__Relation__Group_1_0_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3477:1: rule__Relation__Group_1_0_1__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3481:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3482:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3482:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3483:1: ()
            {
             before(grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3484:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3486:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getGreaterLeftAction_1_0_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3500:1: rule__Relation__Group_1_0_2__0 : rule__Relation__Group_1_0_2__0__Impl rule__Relation__Group_1_0_2__1 ;
    public final void rule__Relation__Group_1_0_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3504:1: ( rule__Relation__Group_1_0_2__0__Impl rule__Relation__Group_1_0_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3505:2: rule__Relation__Group_1_0_2__0__Impl rule__Relation__Group_1_0_2__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_2__0__Impl_in_rule__Relation__Group_1_0_2__07121);
            rule__Relation__Group_1_0_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_2__1_in_rule__Relation__Group_1_0_2__07124);
            rule__Relation__Group_1_0_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_2__0"


    // $ANTLR start "rule__Relation__Group_1_0_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3512:1: rule__Relation__Group_1_0_2__0__Impl : ( '>=' ) ;
    public final void rule__Relation__Group_1_0_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3516:1: ( ( '>=' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3517:1: ( '>=' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3517:1: ( '>=' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3518:1: '>='
            {
             before(grammarAccess.getRelationAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_0()); 
            match(input,24,FOLLOW_24_in_rule__Relation__Group_1_0_2__0__Impl7152); 
             after(grammarAccess.getRelationAccess().getGreaterThanSignEqualsSignKeyword_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_2__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3531:1: rule__Relation__Group_1_0_2__1 : rule__Relation__Group_1_0_2__1__Impl ;
    public final void rule__Relation__Group_1_0_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3535:1: ( rule__Relation__Group_1_0_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3536:2: rule__Relation__Group_1_0_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_2__1__Impl_in_rule__Relation__Group_1_0_2__17183);
            rule__Relation__Group_1_0_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_2__1"


    // $ANTLR start "rule__Relation__Group_1_0_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3542:1: rule__Relation__Group_1_0_2__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3546:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3547:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3547:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3548:1: ()
            {
             before(grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3549:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3551:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getGreaterEqualLeftAction_1_0_2_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_2__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3565:1: rule__Relation__Group_1_0_3__0 : rule__Relation__Group_1_0_3__0__Impl rule__Relation__Group_1_0_3__1 ;
    public final void rule__Relation__Group_1_0_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3569:1: ( rule__Relation__Group_1_0_3__0__Impl rule__Relation__Group_1_0_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3570:2: rule__Relation__Group_1_0_3__0__Impl rule__Relation__Group_1_0_3__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_3__0__Impl_in_rule__Relation__Group_1_0_3__07245);
            rule__Relation__Group_1_0_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_3__1_in_rule__Relation__Group_1_0_3__07248);
            rule__Relation__Group_1_0_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_3__0"


    // $ANTLR start "rule__Relation__Group_1_0_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3577:1: rule__Relation__Group_1_0_3__0__Impl : ( '<' ) ;
    public final void rule__Relation__Group_1_0_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3581:1: ( ( '<' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3582:1: ( '<' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3582:1: ( '<' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3583:1: '<'
            {
             before(grammarAccess.getRelationAccess().getLessThanSignKeyword_1_0_3_0()); 
            match(input,25,FOLLOW_25_in_rule__Relation__Group_1_0_3__0__Impl7276); 
             after(grammarAccess.getRelationAccess().getLessThanSignKeyword_1_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_3__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3596:1: rule__Relation__Group_1_0_3__1 : rule__Relation__Group_1_0_3__1__Impl ;
    public final void rule__Relation__Group_1_0_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3600:1: ( rule__Relation__Group_1_0_3__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3601:2: rule__Relation__Group_1_0_3__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_3__1__Impl_in_rule__Relation__Group_1_0_3__17307);
            rule__Relation__Group_1_0_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_3__1"


    // $ANTLR start "rule__Relation__Group_1_0_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3607:1: rule__Relation__Group_1_0_3__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3611:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3612:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3612:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3613:1: ()
            {
             before(grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3614:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3616:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getLessLeftAction_1_0_3_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_3__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_4__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3630:1: rule__Relation__Group_1_0_4__0 : rule__Relation__Group_1_0_4__0__Impl rule__Relation__Group_1_0_4__1 ;
    public final void rule__Relation__Group_1_0_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3634:1: ( rule__Relation__Group_1_0_4__0__Impl rule__Relation__Group_1_0_4__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3635:2: rule__Relation__Group_1_0_4__0__Impl rule__Relation__Group_1_0_4__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_4__0__Impl_in_rule__Relation__Group_1_0_4__07369);
            rule__Relation__Group_1_0_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_4__1_in_rule__Relation__Group_1_0_4__07372);
            rule__Relation__Group_1_0_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_4__0"


    // $ANTLR start "rule__Relation__Group_1_0_4__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3642:1: rule__Relation__Group_1_0_4__0__Impl : ( '<=' ) ;
    public final void rule__Relation__Group_1_0_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3646:1: ( ( '<=' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3647:1: ( '<=' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3647:1: ( '<=' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3648:1: '<='
            {
             before(grammarAccess.getRelationAccess().getLessThanSignEqualsSignKeyword_1_0_4_0()); 
            match(input,26,FOLLOW_26_in_rule__Relation__Group_1_0_4__0__Impl7400); 
             after(grammarAccess.getRelationAccess().getLessThanSignEqualsSignKeyword_1_0_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_4__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_4__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3661:1: rule__Relation__Group_1_0_4__1 : rule__Relation__Group_1_0_4__1__Impl ;
    public final void rule__Relation__Group_1_0_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3665:1: ( rule__Relation__Group_1_0_4__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3666:2: rule__Relation__Group_1_0_4__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_4__1__Impl_in_rule__Relation__Group_1_0_4__17431);
            rule__Relation__Group_1_0_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_4__1"


    // $ANTLR start "rule__Relation__Group_1_0_4__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3672:1: rule__Relation__Group_1_0_4__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3676:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3677:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3677:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3678:1: ()
            {
             before(grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3679:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3681:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getLessEqualLeftAction_1_0_4_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_4__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_5__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3695:1: rule__Relation__Group_1_0_5__0 : rule__Relation__Group_1_0_5__0__Impl rule__Relation__Group_1_0_5__1 ;
    public final void rule__Relation__Group_1_0_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3699:1: ( rule__Relation__Group_1_0_5__0__Impl rule__Relation__Group_1_0_5__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3700:2: rule__Relation__Group_1_0_5__0__Impl rule__Relation__Group_1_0_5__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_5__0__Impl_in_rule__Relation__Group_1_0_5__07493);
            rule__Relation__Group_1_0_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_5__1_in_rule__Relation__Group_1_0_5__07496);
            rule__Relation__Group_1_0_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_5__0"


    // $ANTLR start "rule__Relation__Group_1_0_5__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3707:1: rule__Relation__Group_1_0_5__0__Impl : ( 'like' ) ;
    public final void rule__Relation__Group_1_0_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3711:1: ( ( 'like' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3712:1: ( 'like' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3712:1: ( 'like' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3713:1: 'like'
            {
             before(grammarAccess.getRelationAccess().getLikeKeyword_1_0_5_0()); 
            match(input,27,FOLLOW_27_in_rule__Relation__Group_1_0_5__0__Impl7524); 
             after(grammarAccess.getRelationAccess().getLikeKeyword_1_0_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_5__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_5__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3726:1: rule__Relation__Group_1_0_5__1 : rule__Relation__Group_1_0_5__1__Impl ;
    public final void rule__Relation__Group_1_0_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3730:1: ( rule__Relation__Group_1_0_5__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3731:2: rule__Relation__Group_1_0_5__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_5__1__Impl_in_rule__Relation__Group_1_0_5__17555);
            rule__Relation__Group_1_0_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_5__1"


    // $ANTLR start "rule__Relation__Group_1_0_5__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3737:1: rule__Relation__Group_1_0_5__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3741:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3742:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3742:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3743:1: ()
            {
             before(grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3744:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3746:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getLikeLeftAction_1_0_5_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_5__1__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_6__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3760:1: rule__Relation__Group_1_0_6__0 : rule__Relation__Group_1_0_6__0__Impl rule__Relation__Group_1_0_6__1 ;
    public final void rule__Relation__Group_1_0_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3764:1: ( rule__Relation__Group_1_0_6__0__Impl rule__Relation__Group_1_0_6__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3765:2: rule__Relation__Group_1_0_6__0__Impl rule__Relation__Group_1_0_6__1
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_6__0__Impl_in_rule__Relation__Group_1_0_6__07617);
            rule__Relation__Group_1_0_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Relation__Group_1_0_6__1_in_rule__Relation__Group_1_0_6__07620);
            rule__Relation__Group_1_0_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_6__0"


    // $ANTLR start "rule__Relation__Group_1_0_6__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3772:1: rule__Relation__Group_1_0_6__0__Impl : ( '!=' ) ;
    public final void rule__Relation__Group_1_0_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3776:1: ( ( '!=' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3777:1: ( '!=' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3777:1: ( '!=' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3778:1: '!='
            {
             before(grammarAccess.getRelationAccess().getExclamationMarkEqualsSignKeyword_1_0_6_0()); 
            match(input,28,FOLLOW_28_in_rule__Relation__Group_1_0_6__0__Impl7648); 
             after(grammarAccess.getRelationAccess().getExclamationMarkEqualsSignKeyword_1_0_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_6__0__Impl"


    // $ANTLR start "rule__Relation__Group_1_0_6__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3791:1: rule__Relation__Group_1_0_6__1 : rule__Relation__Group_1_0_6__1__Impl ;
    public final void rule__Relation__Group_1_0_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3795:1: ( rule__Relation__Group_1_0_6__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3796:2: rule__Relation__Group_1_0_6__1__Impl
            {
            pushFollow(FOLLOW_rule__Relation__Group_1_0_6__1__Impl_in_rule__Relation__Group_1_0_6__17679);
            rule__Relation__Group_1_0_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_6__1"


    // $ANTLR start "rule__Relation__Group_1_0_6__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3802:1: rule__Relation__Group_1_0_6__1__Impl : ( () ) ;
    public final void rule__Relation__Group_1_0_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3806:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3807:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3807:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3808:1: ()
            {
             before(grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3809:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3811:1: 
            {
            }

             after(grammarAccess.getRelationAccess().getNotEqualsLeftAction_1_0_6_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__Group_1_0_6__1__Impl"


    // $ANTLR start "rule__Add__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3825:1: rule__Add__Group__0 : rule__Add__Group__0__Impl rule__Add__Group__1 ;
    public final void rule__Add__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3829:1: ( rule__Add__Group__0__Impl rule__Add__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3830:2: rule__Add__Group__0__Impl rule__Add__Group__1
            {
            pushFollow(FOLLOW_rule__Add__Group__0__Impl_in_rule__Add__Group__07741);
            rule__Add__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Add__Group__1_in_rule__Add__Group__07744);
            rule__Add__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group__0"


    // $ANTLR start "rule__Add__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3837:1: rule__Add__Group__0__Impl : ( ruleMult ) ;
    public final void rule__Add__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3841:1: ( ( ruleMult ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3842:1: ( ruleMult )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3842:1: ( ruleMult )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3843:1: ruleMult
            {
             before(grammarAccess.getAddAccess().getMultParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleMult_in_rule__Add__Group__0__Impl7771);
            ruleMult();

            state._fsp--;

             after(grammarAccess.getAddAccess().getMultParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group__0__Impl"


    // $ANTLR start "rule__Add__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3854:1: rule__Add__Group__1 : rule__Add__Group__1__Impl ;
    public final void rule__Add__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3858:1: ( rule__Add__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3859:2: rule__Add__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Add__Group__1__Impl_in_rule__Add__Group__17800);
            rule__Add__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group__1"


    // $ANTLR start "rule__Add__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3865:1: rule__Add__Group__1__Impl : ( ( rule__Add__Group_1__0 )* ) ;
    public final void rule__Add__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3869:1: ( ( ( rule__Add__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3870:1: ( ( rule__Add__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3870:1: ( ( rule__Add__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3871:1: ( rule__Add__Group_1__0 )*
            {
             before(grammarAccess.getAddAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3872:1: ( rule__Add__Group_1__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>=29 && LA27_0<=30)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3872:2: rule__Add__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Add__Group_1__0_in_rule__Add__Group__1__Impl7827);
            	    rule__Add__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAddAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group__1__Impl"


    // $ANTLR start "rule__Add__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3886:1: rule__Add__Group_1__0 : rule__Add__Group_1__0__Impl rule__Add__Group_1__1 ;
    public final void rule__Add__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3890:1: ( rule__Add__Group_1__0__Impl rule__Add__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3891:2: rule__Add__Group_1__0__Impl rule__Add__Group_1__1
            {
            pushFollow(FOLLOW_rule__Add__Group_1__0__Impl_in_rule__Add__Group_1__07862);
            rule__Add__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Add__Group_1__1_in_rule__Add__Group_1__07865);
            rule__Add__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1__0"


    // $ANTLR start "rule__Add__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3898:1: rule__Add__Group_1__0__Impl : ( ( rule__Add__Alternatives_1_0 ) ) ;
    public final void rule__Add__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3902:1: ( ( ( rule__Add__Alternatives_1_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3903:1: ( ( rule__Add__Alternatives_1_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3903:1: ( ( rule__Add__Alternatives_1_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3904:1: ( rule__Add__Alternatives_1_0 )
            {
             before(grammarAccess.getAddAccess().getAlternatives_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3905:1: ( rule__Add__Alternatives_1_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3905:2: rule__Add__Alternatives_1_0
            {
            pushFollow(FOLLOW_rule__Add__Alternatives_1_0_in_rule__Add__Group_1__0__Impl7892);
            rule__Add__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAddAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1__0__Impl"


    // $ANTLR start "rule__Add__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3915:1: rule__Add__Group_1__1 : rule__Add__Group_1__1__Impl ;
    public final void rule__Add__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3919:1: ( rule__Add__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3920:2: rule__Add__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Add__Group_1__1__Impl_in_rule__Add__Group_1__17922);
            rule__Add__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1__1"


    // $ANTLR start "rule__Add__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3926:1: rule__Add__Group_1__1__Impl : ( ( rule__Add__RightAssignment_1_1 ) ) ;
    public final void rule__Add__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3930:1: ( ( ( rule__Add__RightAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3931:1: ( ( rule__Add__RightAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3931:1: ( ( rule__Add__RightAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3932:1: ( rule__Add__RightAssignment_1_1 )
            {
             before(grammarAccess.getAddAccess().getRightAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3933:1: ( rule__Add__RightAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3933:2: rule__Add__RightAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Add__RightAssignment_1_1_in_rule__Add__Group_1__1__Impl7949);
            rule__Add__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAddAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1__1__Impl"


    // $ANTLR start "rule__Add__Group_1_0_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3947:1: rule__Add__Group_1_0_0__0 : rule__Add__Group_1_0_0__0__Impl rule__Add__Group_1_0_0__1 ;
    public final void rule__Add__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3951:1: ( rule__Add__Group_1_0_0__0__Impl rule__Add__Group_1_0_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3952:2: rule__Add__Group_1_0_0__0__Impl rule__Add__Group_1_0_0__1
            {
            pushFollow(FOLLOW_rule__Add__Group_1_0_0__0__Impl_in_rule__Add__Group_1_0_0__07983);
            rule__Add__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Add__Group_1_0_0__1_in_rule__Add__Group_1_0_0__07986);
            rule__Add__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_0__0"


    // $ANTLR start "rule__Add__Group_1_0_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3959:1: rule__Add__Group_1_0_0__0__Impl : ( '+' ) ;
    public final void rule__Add__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3963:1: ( ( '+' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3964:1: ( '+' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3964:1: ( '+' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3965:1: '+'
            {
             before(grammarAccess.getAddAccess().getPlusSignKeyword_1_0_0_0()); 
            match(input,29,FOLLOW_29_in_rule__Add__Group_1_0_0__0__Impl8014); 
             after(grammarAccess.getAddAccess().getPlusSignKeyword_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__Add__Group_1_0_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3978:1: rule__Add__Group_1_0_0__1 : rule__Add__Group_1_0_0__1__Impl ;
    public final void rule__Add__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3982:1: ( rule__Add__Group_1_0_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3983:2: rule__Add__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Add__Group_1_0_0__1__Impl_in_rule__Add__Group_1_0_0__18045);
            rule__Add__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_0__1"


    // $ANTLR start "rule__Add__Group_1_0_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3989:1: rule__Add__Group_1_0_0__1__Impl : ( () ) ;
    public final void rule__Add__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3993:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3994:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3994:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3995:1: ()
            {
             before(grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3996:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:3998:1: 
            {
            }

             after(grammarAccess.getAddAccess().getAddLeftAction_1_0_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__Add__Group_1_0_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4012:1: rule__Add__Group_1_0_1__0 : rule__Add__Group_1_0_1__0__Impl rule__Add__Group_1_0_1__1 ;
    public final void rule__Add__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4016:1: ( rule__Add__Group_1_0_1__0__Impl rule__Add__Group_1_0_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4017:2: rule__Add__Group_1_0_1__0__Impl rule__Add__Group_1_0_1__1
            {
            pushFollow(FOLLOW_rule__Add__Group_1_0_1__0__Impl_in_rule__Add__Group_1_0_1__08107);
            rule__Add__Group_1_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Add__Group_1_0_1__1_in_rule__Add__Group_1_0_1__08110);
            rule__Add__Group_1_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_1__0"


    // $ANTLR start "rule__Add__Group_1_0_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4024:1: rule__Add__Group_1_0_1__0__Impl : ( '-' ) ;
    public final void rule__Add__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4028:1: ( ( '-' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4029:1: ( '-' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4029:1: ( '-' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4030:1: '-'
            {
             before(grammarAccess.getAddAccess().getHyphenMinusKeyword_1_0_1_0()); 
            match(input,30,FOLLOW_30_in_rule__Add__Group_1_0_1__0__Impl8138); 
             after(grammarAccess.getAddAccess().getHyphenMinusKeyword_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__Add__Group_1_0_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4043:1: rule__Add__Group_1_0_1__1 : rule__Add__Group_1_0_1__1__Impl ;
    public final void rule__Add__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4047:1: ( rule__Add__Group_1_0_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4048:2: rule__Add__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Add__Group_1_0_1__1__Impl_in_rule__Add__Group_1_0_1__18169);
            rule__Add__Group_1_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_1__1"


    // $ANTLR start "rule__Add__Group_1_0_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4054:1: rule__Add__Group_1_0_1__1__Impl : ( () ) ;
    public final void rule__Add__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4058:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4059:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4059:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4060:1: ()
            {
             before(grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4061:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4063:1: 
            {
            }

             after(grammarAccess.getAddAccess().getMinusLeftAction_1_0_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__Mult__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4077:1: rule__Mult__Group__0 : rule__Mult__Group__0__Impl rule__Mult__Group__1 ;
    public final void rule__Mult__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4081:1: ( rule__Mult__Group__0__Impl rule__Mult__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4082:2: rule__Mult__Group__0__Impl rule__Mult__Group__1
            {
            pushFollow(FOLLOW_rule__Mult__Group__0__Impl_in_rule__Mult__Group__08231);
            rule__Mult__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mult__Group__1_in_rule__Mult__Group__08234);
            rule__Mult__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group__0"


    // $ANTLR start "rule__Mult__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4089:1: rule__Mult__Group__0__Impl : ( ruleIn ) ;
    public final void rule__Mult__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4093:1: ( ( ruleIn ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4094:1: ( ruleIn )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4094:1: ( ruleIn )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4095:1: ruleIn
            {
             before(grammarAccess.getMultAccess().getInParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleIn_in_rule__Mult__Group__0__Impl8261);
            ruleIn();

            state._fsp--;

             after(grammarAccess.getMultAccess().getInParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group__0__Impl"


    // $ANTLR start "rule__Mult__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4106:1: rule__Mult__Group__1 : rule__Mult__Group__1__Impl ;
    public final void rule__Mult__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4110:1: ( rule__Mult__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4111:2: rule__Mult__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Mult__Group__1__Impl_in_rule__Mult__Group__18290);
            rule__Mult__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group__1"


    // $ANTLR start "rule__Mult__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4117:1: rule__Mult__Group__1__Impl : ( ( rule__Mult__Group_1__0 )* ) ;
    public final void rule__Mult__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4121:1: ( ( ( rule__Mult__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4122:1: ( ( rule__Mult__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4122:1: ( ( rule__Mult__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4123:1: ( rule__Mult__Group_1__0 )*
            {
             before(grammarAccess.getMultAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4124:1: ( rule__Mult__Group_1__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( ((LA28_0>=31 && LA28_0<=33)) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4124:2: rule__Mult__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Mult__Group_1__0_in_rule__Mult__Group__1__Impl8317);
            	    rule__Mult__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getMultAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group__1__Impl"


    // $ANTLR start "rule__Mult__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4138:1: rule__Mult__Group_1__0 : rule__Mult__Group_1__0__Impl rule__Mult__Group_1__1 ;
    public final void rule__Mult__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4142:1: ( rule__Mult__Group_1__0__Impl rule__Mult__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4143:2: rule__Mult__Group_1__0__Impl rule__Mult__Group_1__1
            {
            pushFollow(FOLLOW_rule__Mult__Group_1__0__Impl_in_rule__Mult__Group_1__08352);
            rule__Mult__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mult__Group_1__1_in_rule__Mult__Group_1__08355);
            rule__Mult__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1__0"


    // $ANTLR start "rule__Mult__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4150:1: rule__Mult__Group_1__0__Impl : ( ( rule__Mult__Alternatives_1_0 ) ) ;
    public final void rule__Mult__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4154:1: ( ( ( rule__Mult__Alternatives_1_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4155:1: ( ( rule__Mult__Alternatives_1_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4155:1: ( ( rule__Mult__Alternatives_1_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4156:1: ( rule__Mult__Alternatives_1_0 )
            {
             before(grammarAccess.getMultAccess().getAlternatives_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4157:1: ( rule__Mult__Alternatives_1_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4157:2: rule__Mult__Alternatives_1_0
            {
            pushFollow(FOLLOW_rule__Mult__Alternatives_1_0_in_rule__Mult__Group_1__0__Impl8382);
            rule__Mult__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getMultAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1__0__Impl"


    // $ANTLR start "rule__Mult__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4167:1: rule__Mult__Group_1__1 : rule__Mult__Group_1__1__Impl ;
    public final void rule__Mult__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4171:1: ( rule__Mult__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4172:2: rule__Mult__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Mult__Group_1__1__Impl_in_rule__Mult__Group_1__18412);
            rule__Mult__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1__1"


    // $ANTLR start "rule__Mult__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4178:1: rule__Mult__Group_1__1__Impl : ( ( rule__Mult__RightAssignment_1_1 ) ) ;
    public final void rule__Mult__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4182:1: ( ( ( rule__Mult__RightAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4183:1: ( ( rule__Mult__RightAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4183:1: ( ( rule__Mult__RightAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4184:1: ( rule__Mult__RightAssignment_1_1 )
            {
             before(grammarAccess.getMultAccess().getRightAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4185:1: ( rule__Mult__RightAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4185:2: rule__Mult__RightAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Mult__RightAssignment_1_1_in_rule__Mult__Group_1__1__Impl8439);
            rule__Mult__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMultAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1__1__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4199:1: rule__Mult__Group_1_0_0__0 : rule__Mult__Group_1_0_0__0__Impl rule__Mult__Group_1_0_0__1 ;
    public final void rule__Mult__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4203:1: ( rule__Mult__Group_1_0_0__0__Impl rule__Mult__Group_1_0_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4204:2: rule__Mult__Group_1_0_0__0__Impl rule__Mult__Group_1_0_0__1
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_0__0__Impl_in_rule__Mult__Group_1_0_0__08473);
            rule__Mult__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mult__Group_1_0_0__1_in_rule__Mult__Group_1_0_0__08476);
            rule__Mult__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_0__0"


    // $ANTLR start "rule__Mult__Group_1_0_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4211:1: rule__Mult__Group_1_0_0__0__Impl : ( '*' ) ;
    public final void rule__Mult__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4215:1: ( ( '*' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4216:1: ( '*' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4216:1: ( '*' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4217:1: '*'
            {
             before(grammarAccess.getMultAccess().getAsteriskKeyword_1_0_0_0()); 
            match(input,31,FOLLOW_31_in_rule__Mult__Group_1_0_0__0__Impl8504); 
             after(grammarAccess.getMultAccess().getAsteriskKeyword_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4230:1: rule__Mult__Group_1_0_0__1 : rule__Mult__Group_1_0_0__1__Impl ;
    public final void rule__Mult__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4234:1: ( rule__Mult__Group_1_0_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4235:2: rule__Mult__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_0__1__Impl_in_rule__Mult__Group_1_0_0__18535);
            rule__Mult__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_0__1"


    // $ANTLR start "rule__Mult__Group_1_0_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4241:1: rule__Mult__Group_1_0_0__1__Impl : ( () ) ;
    public final void rule__Mult__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4245:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4246:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4246:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4247:1: ()
            {
             before(grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4248:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4250:1: 
            {
            }

             after(grammarAccess.getMultAccess().getMultLeftAction_1_0_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4264:1: rule__Mult__Group_1_0_1__0 : rule__Mult__Group_1_0_1__0__Impl rule__Mult__Group_1_0_1__1 ;
    public final void rule__Mult__Group_1_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4268:1: ( rule__Mult__Group_1_0_1__0__Impl rule__Mult__Group_1_0_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4269:2: rule__Mult__Group_1_0_1__0__Impl rule__Mult__Group_1_0_1__1
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_1__0__Impl_in_rule__Mult__Group_1_0_1__08597);
            rule__Mult__Group_1_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mult__Group_1_0_1__1_in_rule__Mult__Group_1_0_1__08600);
            rule__Mult__Group_1_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_1__0"


    // $ANTLR start "rule__Mult__Group_1_0_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4276:1: rule__Mult__Group_1_0_1__0__Impl : ( '/' ) ;
    public final void rule__Mult__Group_1_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4280:1: ( ( '/' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4281:1: ( '/' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4281:1: ( '/' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4282:1: '/'
            {
             before(grammarAccess.getMultAccess().getSolidusKeyword_1_0_1_0()); 
            match(input,32,FOLLOW_32_in_rule__Mult__Group_1_0_1__0__Impl8628); 
             after(grammarAccess.getMultAccess().getSolidusKeyword_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_1__0__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4295:1: rule__Mult__Group_1_0_1__1 : rule__Mult__Group_1_0_1__1__Impl ;
    public final void rule__Mult__Group_1_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4299:1: ( rule__Mult__Group_1_0_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4300:2: rule__Mult__Group_1_0_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_1__1__Impl_in_rule__Mult__Group_1_0_1__18659);
            rule__Mult__Group_1_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_1__1"


    // $ANTLR start "rule__Mult__Group_1_0_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4306:1: rule__Mult__Group_1_0_1__1__Impl : ( () ) ;
    public final void rule__Mult__Group_1_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4310:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4311:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4311:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4312:1: ()
            {
             before(grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4313:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4315:1: 
            {
            }

             after(grammarAccess.getMultAccess().getDivLeftAction_1_0_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_1__1__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4329:1: rule__Mult__Group_1_0_2__0 : rule__Mult__Group_1_0_2__0__Impl rule__Mult__Group_1_0_2__1 ;
    public final void rule__Mult__Group_1_0_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4333:1: ( rule__Mult__Group_1_0_2__0__Impl rule__Mult__Group_1_0_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4334:2: rule__Mult__Group_1_0_2__0__Impl rule__Mult__Group_1_0_2__1
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_2__0__Impl_in_rule__Mult__Group_1_0_2__08721);
            rule__Mult__Group_1_0_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mult__Group_1_0_2__1_in_rule__Mult__Group_1_0_2__08724);
            rule__Mult__Group_1_0_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_2__0"


    // $ANTLR start "rule__Mult__Group_1_0_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4341:1: rule__Mult__Group_1_0_2__0__Impl : ( '%' ) ;
    public final void rule__Mult__Group_1_0_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4345:1: ( ( '%' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4346:1: ( '%' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4346:1: ( '%' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4347:1: '%'
            {
             before(grammarAccess.getMultAccess().getPercentSignKeyword_1_0_2_0()); 
            match(input,33,FOLLOW_33_in_rule__Mult__Group_1_0_2__0__Impl8752); 
             after(grammarAccess.getMultAccess().getPercentSignKeyword_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_2__0__Impl"


    // $ANTLR start "rule__Mult__Group_1_0_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4360:1: rule__Mult__Group_1_0_2__1 : rule__Mult__Group_1_0_2__1__Impl ;
    public final void rule__Mult__Group_1_0_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4364:1: ( rule__Mult__Group_1_0_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4365:2: rule__Mult__Group_1_0_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Mult__Group_1_0_2__1__Impl_in_rule__Mult__Group_1_0_2__18783);
            rule__Mult__Group_1_0_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_2__1"


    // $ANTLR start "rule__Mult__Group_1_0_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4371:1: rule__Mult__Group_1_0_2__1__Impl : ( () ) ;
    public final void rule__Mult__Group_1_0_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4375:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4376:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4376:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4377:1: ()
            {
             before(grammarAccess.getMultAccess().getModLeftAction_1_0_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4378:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4380:1: 
            {
            }

             after(grammarAccess.getMultAccess().getModLeftAction_1_0_2_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__Group_1_0_2__1__Impl"


    // $ANTLR start "rule__In__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4394:1: rule__In__Group__0 : rule__In__Group__0__Impl rule__In__Group__1 ;
    public final void rule__In__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4398:1: ( rule__In__Group__0__Impl rule__In__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4399:2: rule__In__Group__0__Impl rule__In__Group__1
            {
            pushFollow(FOLLOW_rule__In__Group__0__Impl_in_rule__In__Group__08845);
            rule__In__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__In__Group__1_in_rule__In__Group__08848);
            rule__In__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group__0"


    // $ANTLR start "rule__In__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4406:1: rule__In__Group__0__Impl : ( ruleUnary ) ;
    public final void rule__In__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4410:1: ( ( ruleUnary ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4411:1: ( ruleUnary )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4411:1: ( ruleUnary )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4412:1: ruleUnary
            {
             before(grammarAccess.getInAccess().getUnaryParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleUnary_in_rule__In__Group__0__Impl8875);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getInAccess().getUnaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group__0__Impl"


    // $ANTLR start "rule__In__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4423:1: rule__In__Group__1 : rule__In__Group__1__Impl ;
    public final void rule__In__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4427:1: ( rule__In__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4428:2: rule__In__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__In__Group__1__Impl_in_rule__In__Group__18904);
            rule__In__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group__1"


    // $ANTLR start "rule__In__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4434:1: rule__In__Group__1__Impl : ( ( rule__In__Group_1__0 )* ) ;
    public final void rule__In__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4438:1: ( ( ( rule__In__Group_1__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4439:1: ( ( rule__In__Group_1__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4439:1: ( ( rule__In__Group_1__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4440:1: ( rule__In__Group_1__0 )*
            {
             before(grammarAccess.getInAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4441:1: ( rule__In__Group_1__0 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==34) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4441:2: rule__In__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__In__Group_1__0_in_rule__In__Group__1__Impl8931);
            	    rule__In__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getInAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group__1__Impl"


    // $ANTLR start "rule__In__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4455:1: rule__In__Group_1__0 : rule__In__Group_1__0__Impl rule__In__Group_1__1 ;
    public final void rule__In__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4459:1: ( rule__In__Group_1__0__Impl rule__In__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4460:2: rule__In__Group_1__0__Impl rule__In__Group_1__1
            {
            pushFollow(FOLLOW_rule__In__Group_1__0__Impl_in_rule__In__Group_1__08966);
            rule__In__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__In__Group_1__1_in_rule__In__Group_1__08969);
            rule__In__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1__0"


    // $ANTLR start "rule__In__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4467:1: rule__In__Group_1__0__Impl : ( ( rule__In__Group_1_0__0 ) ) ;
    public final void rule__In__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4471:1: ( ( ( rule__In__Group_1_0__0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4472:1: ( ( rule__In__Group_1_0__0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4472:1: ( ( rule__In__Group_1_0__0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4473:1: ( rule__In__Group_1_0__0 )
            {
             before(grammarAccess.getInAccess().getGroup_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4474:1: ( rule__In__Group_1_0__0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4474:2: rule__In__Group_1_0__0
            {
            pushFollow(FOLLOW_rule__In__Group_1_0__0_in_rule__In__Group_1__0__Impl8996);
            rule__In__Group_1_0__0();

            state._fsp--;


            }

             after(grammarAccess.getInAccess().getGroup_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1__0__Impl"


    // $ANTLR start "rule__In__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4484:1: rule__In__Group_1__1 : rule__In__Group_1__1__Impl ;
    public final void rule__In__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4488:1: ( rule__In__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4489:2: rule__In__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__In__Group_1__1__Impl_in_rule__In__Group_1__19026);
            rule__In__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1__1"


    // $ANTLR start "rule__In__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4495:1: rule__In__Group_1__1__Impl : ( ( rule__In__RightAssignment_1_1 ) ) ;
    public final void rule__In__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4499:1: ( ( ( rule__In__RightAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4500:1: ( ( rule__In__RightAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4500:1: ( ( rule__In__RightAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4501:1: ( rule__In__RightAssignment_1_1 )
            {
             before(grammarAccess.getInAccess().getRightAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4502:1: ( rule__In__RightAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4502:2: rule__In__RightAssignment_1_1
            {
            pushFollow(FOLLOW_rule__In__RightAssignment_1_1_in_rule__In__Group_1__1__Impl9053);
            rule__In__RightAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getInAccess().getRightAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1__1__Impl"


    // $ANTLR start "rule__In__Group_1_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4516:1: rule__In__Group_1_0__0 : rule__In__Group_1_0__0__Impl rule__In__Group_1_0__1 ;
    public final void rule__In__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4520:1: ( rule__In__Group_1_0__0__Impl rule__In__Group_1_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4521:2: rule__In__Group_1_0__0__Impl rule__In__Group_1_0__1
            {
            pushFollow(FOLLOW_rule__In__Group_1_0__0__Impl_in_rule__In__Group_1_0__09087);
            rule__In__Group_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__In__Group_1_0__1_in_rule__In__Group_1_0__09090);
            rule__In__Group_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1_0__0"


    // $ANTLR start "rule__In__Group_1_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4528:1: rule__In__Group_1_0__0__Impl : ( 'in' ) ;
    public final void rule__In__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4532:1: ( ( 'in' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4533:1: ( 'in' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4533:1: ( 'in' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4534:1: 'in'
            {
             before(grammarAccess.getInAccess().getInKeyword_1_0_0()); 
            match(input,34,FOLLOW_34_in_rule__In__Group_1_0__0__Impl9118); 
             after(grammarAccess.getInAccess().getInKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1_0__0__Impl"


    // $ANTLR start "rule__In__Group_1_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4547:1: rule__In__Group_1_0__1 : rule__In__Group_1_0__1__Impl ;
    public final void rule__In__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4551:1: ( rule__In__Group_1_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4552:2: rule__In__Group_1_0__1__Impl
            {
            pushFollow(FOLLOW_rule__In__Group_1_0__1__Impl_in_rule__In__Group_1_0__19149);
            rule__In__Group_1_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1_0__1"


    // $ANTLR start "rule__In__Group_1_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4558:1: rule__In__Group_1_0__1__Impl : ( () ) ;
    public final void rule__In__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4562:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4563:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4563:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4564:1: ()
            {
             before(grammarAccess.getInAccess().getInLeftAction_1_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4565:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4567:1: 
            {
            }

             after(grammarAccess.getInAccess().getInLeftAction_1_0_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__Group_1_0__1__Impl"


    // $ANTLR start "rule__Unary__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4581:1: rule__Unary__Group_1__0 : rule__Unary__Group_1__0__Impl rule__Unary__Group_1__1 ;
    public final void rule__Unary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4585:1: ( rule__Unary__Group_1__0__Impl rule__Unary__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4586:2: rule__Unary__Group_1__0__Impl rule__Unary__Group_1__1
            {
            pushFollow(FOLLOW_rule__Unary__Group_1__0__Impl_in_rule__Unary__Group_1__09211);
            rule__Unary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Unary__Group_1__1_in_rule__Unary__Group_1__09214);
            rule__Unary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__0"


    // $ANTLR start "rule__Unary__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4593:1: rule__Unary__Group_1__0__Impl : ( 'not' ) ;
    public final void rule__Unary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4597:1: ( ( 'not' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4598:1: ( 'not' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4598:1: ( 'not' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4599:1: 'not'
            {
             before(grammarAccess.getUnaryAccess().getNotKeyword_1_0()); 
            match(input,35,FOLLOW_35_in_rule__Unary__Group_1__0__Impl9242); 
             after(grammarAccess.getUnaryAccess().getNotKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__0__Impl"


    // $ANTLR start "rule__Unary__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4612:1: rule__Unary__Group_1__1 : rule__Unary__Group_1__1__Impl rule__Unary__Group_1__2 ;
    public final void rule__Unary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4616:1: ( rule__Unary__Group_1__1__Impl rule__Unary__Group_1__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4617:2: rule__Unary__Group_1__1__Impl rule__Unary__Group_1__2
            {
            pushFollow(FOLLOW_rule__Unary__Group_1__1__Impl_in_rule__Unary__Group_1__19273);
            rule__Unary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Unary__Group_1__2_in_rule__Unary__Group_1__19276);
            rule__Unary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__1"


    // $ANTLR start "rule__Unary__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4624:1: rule__Unary__Group_1__1__Impl : ( () ) ;
    public final void rule__Unary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4628:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4629:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4629:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4630:1: ()
            {
             before(grammarAccess.getUnaryAccess().getNotAction_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4631:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4633:1: 
            {
            }

             after(grammarAccess.getUnaryAccess().getNotAction_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__1__Impl"


    // $ANTLR start "rule__Unary__Group_1__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4643:1: rule__Unary__Group_1__2 : rule__Unary__Group_1__2__Impl ;
    public final void rule__Unary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4647:1: ( rule__Unary__Group_1__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4648:2: rule__Unary__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__Unary__Group_1__2__Impl_in_rule__Unary__Group_1__29334);
            rule__Unary__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__2"


    // $ANTLR start "rule__Unary__Group_1__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4654:1: rule__Unary__Group_1__2__Impl : ( ( rule__Unary__ExpAssignment_1_2 ) ) ;
    public final void rule__Unary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4658:1: ( ( ( rule__Unary__ExpAssignment_1_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4659:1: ( ( rule__Unary__ExpAssignment_1_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4659:1: ( ( rule__Unary__ExpAssignment_1_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4660:1: ( rule__Unary__ExpAssignment_1_2 )
            {
             before(grammarAccess.getUnaryAccess().getExpAssignment_1_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4661:1: ( rule__Unary__ExpAssignment_1_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4661:2: rule__Unary__ExpAssignment_1_2
            {
            pushFollow(FOLLOW_rule__Unary__ExpAssignment_1_2_in_rule__Unary__Group_1__2__Impl9361);
            rule__Unary__ExpAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getExpAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_1__2__Impl"


    // $ANTLR start "rule__Unary__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4677:1: rule__Unary__Group_2__0 : rule__Unary__Group_2__0__Impl rule__Unary__Group_2__1 ;
    public final void rule__Unary__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4681:1: ( rule__Unary__Group_2__0__Impl rule__Unary__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4682:2: rule__Unary__Group_2__0__Impl rule__Unary__Group_2__1
            {
            pushFollow(FOLLOW_rule__Unary__Group_2__0__Impl_in_rule__Unary__Group_2__09397);
            rule__Unary__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Unary__Group_2__1_in_rule__Unary__Group_2__09400);
            rule__Unary__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__0"


    // $ANTLR start "rule__Unary__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4689:1: rule__Unary__Group_2__0__Impl : ( '-' ) ;
    public final void rule__Unary__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4693:1: ( ( '-' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4694:1: ( '-' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4694:1: ( '-' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4695:1: '-'
            {
             before(grammarAccess.getUnaryAccess().getHyphenMinusKeyword_2_0()); 
            match(input,30,FOLLOW_30_in_rule__Unary__Group_2__0__Impl9428); 
             after(grammarAccess.getUnaryAccess().getHyphenMinusKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__0__Impl"


    // $ANTLR start "rule__Unary__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4708:1: rule__Unary__Group_2__1 : rule__Unary__Group_2__1__Impl rule__Unary__Group_2__2 ;
    public final void rule__Unary__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4712:1: ( rule__Unary__Group_2__1__Impl rule__Unary__Group_2__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4713:2: rule__Unary__Group_2__1__Impl rule__Unary__Group_2__2
            {
            pushFollow(FOLLOW_rule__Unary__Group_2__1__Impl_in_rule__Unary__Group_2__19459);
            rule__Unary__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Unary__Group_2__2_in_rule__Unary__Group_2__19462);
            rule__Unary__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__1"


    // $ANTLR start "rule__Unary__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4720:1: rule__Unary__Group_2__1__Impl : ( () ) ;
    public final void rule__Unary__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4724:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4725:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4725:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4726:1: ()
            {
             before(grammarAccess.getUnaryAccess().getUnaryMinusAction_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4727:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4729:1: 
            {
            }

             after(grammarAccess.getUnaryAccess().getUnaryMinusAction_2_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__1__Impl"


    // $ANTLR start "rule__Unary__Group_2__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4739:1: rule__Unary__Group_2__2 : rule__Unary__Group_2__2__Impl ;
    public final void rule__Unary__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4743:1: ( rule__Unary__Group_2__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4744:2: rule__Unary__Group_2__2__Impl
            {
            pushFollow(FOLLOW_rule__Unary__Group_2__2__Impl_in_rule__Unary__Group_2__29520);
            rule__Unary__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__2"


    // $ANTLR start "rule__Unary__Group_2__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4750:1: rule__Unary__Group_2__2__Impl : ( ( rule__Unary__ExprAssignment_2_2 ) ) ;
    public final void rule__Unary__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4754:1: ( ( ( rule__Unary__ExprAssignment_2_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4755:1: ( ( rule__Unary__ExprAssignment_2_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4755:1: ( ( rule__Unary__ExprAssignment_2_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4756:1: ( rule__Unary__ExprAssignment_2_2 )
            {
             before(grammarAccess.getUnaryAccess().getExprAssignment_2_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4757:1: ( rule__Unary__ExprAssignment_2_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4757:2: rule__Unary__ExprAssignment_2_2
            {
            pushFollow(FOLLOW_rule__Unary__ExprAssignment_2_2_in_rule__Unary__Group_2__2__Impl9547);
            rule__Unary__ExprAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getUnaryAccess().getExprAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__Group_2__2__Impl"


    // $ANTLR start "rule__Exponential__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4773:1: rule__Exponential__Group__0 : rule__Exponential__Group__0__Impl rule__Exponential__Group__1 ;
    public final void rule__Exponential__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4777:1: ( rule__Exponential__Group__0__Impl rule__Exponential__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4778:2: rule__Exponential__Group__0__Impl rule__Exponential__Group__1
            {
            pushFollow(FOLLOW_rule__Exponential__Group__0__Impl_in_rule__Exponential__Group__09583);
            rule__Exponential__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponential__Group__1_in_rule__Exponential__Group__09586);
            rule__Exponential__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group__0"


    // $ANTLR start "rule__Exponential__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4785:1: rule__Exponential__Group__0__Impl : ( ruleAtom ) ;
    public final void rule__Exponential__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4789:1: ( ( ruleAtom ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4790:1: ( ruleAtom )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4790:1: ( ruleAtom )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4791:1: ruleAtom
            {
             before(grammarAccess.getExponentialAccess().getAtomParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleAtom_in_rule__Exponential__Group__0__Impl9613);
            ruleAtom();

            state._fsp--;

             after(grammarAccess.getExponentialAccess().getAtomParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group__0__Impl"


    // $ANTLR start "rule__Exponential__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4802:1: rule__Exponential__Group__1 : rule__Exponential__Group__1__Impl ;
    public final void rule__Exponential__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4806:1: ( rule__Exponential__Group__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4807:2: rule__Exponential__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Exponential__Group__1__Impl_in_rule__Exponential__Group__19642);
            rule__Exponential__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group__1"


    // $ANTLR start "rule__Exponential__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4813:1: rule__Exponential__Group__1__Impl : ( ( rule__Exponential__Group_1__0 )? ) ;
    public final void rule__Exponential__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4817:1: ( ( ( rule__Exponential__Group_1__0 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4818:1: ( ( rule__Exponential__Group_1__0 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4818:1: ( ( rule__Exponential__Group_1__0 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4819:1: ( rule__Exponential__Group_1__0 )?
            {
             before(grammarAccess.getExponentialAccess().getGroup_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4820:1: ( rule__Exponential__Group_1__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==36) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4820:2: rule__Exponential__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Exponential__Group_1__0_in_rule__Exponential__Group__1__Impl9669);
                    rule__Exponential__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExponentialAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group__1__Impl"


    // $ANTLR start "rule__Exponential__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4834:1: rule__Exponential__Group_1__0 : rule__Exponential__Group_1__0__Impl rule__Exponential__Group_1__1 ;
    public final void rule__Exponential__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4838:1: ( rule__Exponential__Group_1__0__Impl rule__Exponential__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4839:2: rule__Exponential__Group_1__0__Impl rule__Exponential__Group_1__1
            {
            pushFollow(FOLLOW_rule__Exponential__Group_1__0__Impl_in_rule__Exponential__Group_1__09704);
            rule__Exponential__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponential__Group_1__1_in_rule__Exponential__Group_1__09707);
            rule__Exponential__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__0"


    // $ANTLR start "rule__Exponential__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4846:1: rule__Exponential__Group_1__0__Impl : ( '^' ) ;
    public final void rule__Exponential__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4850:1: ( ( '^' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4851:1: ( '^' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4851:1: ( '^' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4852:1: '^'
            {
             before(grammarAccess.getExponentialAccess().getCircumflexAccentKeyword_1_0()); 
            match(input,36,FOLLOW_36_in_rule__Exponential__Group_1__0__Impl9735); 
             after(grammarAccess.getExponentialAccess().getCircumflexAccentKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__0__Impl"


    // $ANTLR start "rule__Exponential__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4865:1: rule__Exponential__Group_1__1 : rule__Exponential__Group_1__1__Impl rule__Exponential__Group_1__2 ;
    public final void rule__Exponential__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4869:1: ( rule__Exponential__Group_1__1__Impl rule__Exponential__Group_1__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4870:2: rule__Exponential__Group_1__1__Impl rule__Exponential__Group_1__2
            {
            pushFollow(FOLLOW_rule__Exponential__Group_1__1__Impl_in_rule__Exponential__Group_1__19766);
            rule__Exponential__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Exponential__Group_1__2_in_rule__Exponential__Group_1__19769);
            rule__Exponential__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__1"


    // $ANTLR start "rule__Exponential__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4877:1: rule__Exponential__Group_1__1__Impl : ( () ) ;
    public final void rule__Exponential__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4881:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4882:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4882:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4883:1: ()
            {
             before(grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4884:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4886:1: 
            {
            }

             after(grammarAccess.getExponentialAccess().getExponentialLeftAction_1_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__1__Impl"


    // $ANTLR start "rule__Exponential__Group_1__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4896:1: rule__Exponential__Group_1__2 : rule__Exponential__Group_1__2__Impl ;
    public final void rule__Exponential__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4900:1: ( rule__Exponential__Group_1__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4901:2: rule__Exponential__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__Exponential__Group_1__2__Impl_in_rule__Exponential__Group_1__29827);
            rule__Exponential__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__2"


    // $ANTLR start "rule__Exponential__Group_1__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4907:1: rule__Exponential__Group_1__2__Impl : ( ( rule__Exponential__RightAssignment_1_2 ) ) ;
    public final void rule__Exponential__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4911:1: ( ( ( rule__Exponential__RightAssignment_1_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4912:1: ( ( rule__Exponential__RightAssignment_1_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4912:1: ( ( rule__Exponential__RightAssignment_1_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4913:1: ( rule__Exponential__RightAssignment_1_2 )
            {
             before(grammarAccess.getExponentialAccess().getRightAssignment_1_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4914:1: ( rule__Exponential__RightAssignment_1_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4914:2: rule__Exponential__RightAssignment_1_2
            {
            pushFollow(FOLLOW_rule__Exponential__RightAssignment_1_2_in_rule__Exponential__Group_1__2__Impl9854);
            rule__Exponential__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExponentialAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__Group_1__2__Impl"


    // $ANTLR start "rule__ParsExpression__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4930:1: rule__ParsExpression__Group__0 : rule__ParsExpression__Group__0__Impl rule__ParsExpression__Group__1 ;
    public final void rule__ParsExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4934:1: ( rule__ParsExpression__Group__0__Impl rule__ParsExpression__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4935:2: rule__ParsExpression__Group__0__Impl rule__ParsExpression__Group__1
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__0__Impl_in_rule__ParsExpression__Group__09890);
            rule__ParsExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ParsExpression__Group__1_in_rule__ParsExpression__Group__09893);
            rule__ParsExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__0"


    // $ANTLR start "rule__ParsExpression__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4942:1: rule__ParsExpression__Group__0__Impl : ( () ) ;
    public final void rule__ParsExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4946:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4947:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4947:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4948:1: ()
            {
             before(grammarAccess.getParsExpressionAccess().getParsExpressionAction_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4949:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4951:1: 
            {
            }

             after(grammarAccess.getParsExpressionAccess().getParsExpressionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__0__Impl"


    // $ANTLR start "rule__ParsExpression__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4961:1: rule__ParsExpression__Group__1 : rule__ParsExpression__Group__1__Impl rule__ParsExpression__Group__2 ;
    public final void rule__ParsExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4965:1: ( rule__ParsExpression__Group__1__Impl rule__ParsExpression__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4966:2: rule__ParsExpression__Group__1__Impl rule__ParsExpression__Group__2
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__1__Impl_in_rule__ParsExpression__Group__19951);
            rule__ParsExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ParsExpression__Group__2_in_rule__ParsExpression__Group__19954);
            rule__ParsExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__1"


    // $ANTLR start "rule__ParsExpression__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4973:1: rule__ParsExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__ParsExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4977:1: ( ( '(' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4978:1: ( '(' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4978:1: ( '(' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4979:1: '('
            {
             before(grammarAccess.getParsExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,37,FOLLOW_37_in_rule__ParsExpression__Group__1__Impl9982); 
             after(grammarAccess.getParsExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__1__Impl"


    // $ANTLR start "rule__ParsExpression__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4992:1: rule__ParsExpression__Group__2 : rule__ParsExpression__Group__2__Impl rule__ParsExpression__Group__3 ;
    public final void rule__ParsExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4996:1: ( rule__ParsExpression__Group__2__Impl rule__ParsExpression__Group__3 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:4997:2: rule__ParsExpression__Group__2__Impl rule__ParsExpression__Group__3
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__2__Impl_in_rule__ParsExpression__Group__210013);
            rule__ParsExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ParsExpression__Group__3_in_rule__ParsExpression__Group__210016);
            rule__ParsExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__2"


    // $ANTLR start "rule__ParsExpression__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5004:1: rule__ParsExpression__Group__2__Impl : ( ( rule__ParsExpression__ExprAssignment_2 ) ) ;
    public final void rule__ParsExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5008:1: ( ( ( rule__ParsExpression__ExprAssignment_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5009:1: ( ( rule__ParsExpression__ExprAssignment_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5009:1: ( ( rule__ParsExpression__ExprAssignment_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5010:1: ( rule__ParsExpression__ExprAssignment_2 )
            {
             before(grammarAccess.getParsExpressionAccess().getExprAssignment_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5011:1: ( rule__ParsExpression__ExprAssignment_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5011:2: rule__ParsExpression__ExprAssignment_2
            {
            pushFollow(FOLLOW_rule__ParsExpression__ExprAssignment_2_in_rule__ParsExpression__Group__2__Impl10043);
            rule__ParsExpression__ExprAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getParsExpressionAccess().getExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__2__Impl"


    // $ANTLR start "rule__ParsExpression__Group__3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5021:1: rule__ParsExpression__Group__3 : rule__ParsExpression__Group__3__Impl rule__ParsExpression__Group__4 ;
    public final void rule__ParsExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5025:1: ( rule__ParsExpression__Group__3__Impl rule__ParsExpression__Group__4 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5026:2: rule__ParsExpression__Group__3__Impl rule__ParsExpression__Group__4
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__3__Impl_in_rule__ParsExpression__Group__310073);
            rule__ParsExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ParsExpression__Group__4_in_rule__ParsExpression__Group__310076);
            rule__ParsExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__3"


    // $ANTLR start "rule__ParsExpression__Group__3__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5033:1: rule__ParsExpression__Group__3__Impl : ( ( rule__ParsExpression__Group_3__0 )* ) ;
    public final void rule__ParsExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5037:1: ( ( ( rule__ParsExpression__Group_3__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5038:1: ( ( rule__ParsExpression__Group_3__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5038:1: ( ( rule__ParsExpression__Group_3__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5039:1: ( rule__ParsExpression__Group_3__0 )*
            {
             before(grammarAccess.getParsExpressionAccess().getGroup_3()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5040:1: ( rule__ParsExpression__Group_3__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==16) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5040:2: rule__ParsExpression__Group_3__0
            	    {
            	    pushFollow(FOLLOW_rule__ParsExpression__Group_3__0_in_rule__ParsExpression__Group__3__Impl10103);
            	    rule__ParsExpression__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getParsExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__3__Impl"


    // $ANTLR start "rule__ParsExpression__Group__4"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5050:1: rule__ParsExpression__Group__4 : rule__ParsExpression__Group__4__Impl ;
    public final void rule__ParsExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5054:1: ( rule__ParsExpression__Group__4__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5055:2: rule__ParsExpression__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group__4__Impl_in_rule__ParsExpression__Group__410134);
            rule__ParsExpression__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__4"


    // $ANTLR start "rule__ParsExpression__Group__4__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5061:1: rule__ParsExpression__Group__4__Impl : ( ')' ) ;
    public final void rule__ParsExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5065:1: ( ( ')' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5066:1: ( ')' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5066:1: ( ')' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5067:1: ')'
            {
             before(grammarAccess.getParsExpressionAccess().getRightParenthesisKeyword_4()); 
            match(input,38,FOLLOW_38_in_rule__ParsExpression__Group__4__Impl10162); 
             after(grammarAccess.getParsExpressionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group__4__Impl"


    // $ANTLR start "rule__ParsExpression__Group_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5090:1: rule__ParsExpression__Group_3__0 : rule__ParsExpression__Group_3__0__Impl rule__ParsExpression__Group_3__1 ;
    public final void rule__ParsExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5094:1: ( rule__ParsExpression__Group_3__0__Impl rule__ParsExpression__Group_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5095:2: rule__ParsExpression__Group_3__0__Impl rule__ParsExpression__Group_3__1
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group_3__0__Impl_in_rule__ParsExpression__Group_3__010203);
            rule__ParsExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ParsExpression__Group_3__1_in_rule__ParsExpression__Group_3__010206);
            rule__ParsExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group_3__0"


    // $ANTLR start "rule__ParsExpression__Group_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5102:1: rule__ParsExpression__Group_3__0__Impl : ( ',' ) ;
    public final void rule__ParsExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5106:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5107:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5107:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5108:1: ','
            {
             before(grammarAccess.getParsExpressionAccess().getCommaKeyword_3_0()); 
            match(input,16,FOLLOW_16_in_rule__ParsExpression__Group_3__0__Impl10234); 
             after(grammarAccess.getParsExpressionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group_3__0__Impl"


    // $ANTLR start "rule__ParsExpression__Group_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5121:1: rule__ParsExpression__Group_3__1 : rule__ParsExpression__Group_3__1__Impl ;
    public final void rule__ParsExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5125:1: ( rule__ParsExpression__Group_3__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5126:2: rule__ParsExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__ParsExpression__Group_3__1__Impl_in_rule__ParsExpression__Group_3__110265);
            rule__ParsExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group_3__1"


    // $ANTLR start "rule__ParsExpression__Group_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5132:1: rule__ParsExpression__Group_3__1__Impl : ( ( rule__ParsExpression__ExprAssignment_3_1 ) ) ;
    public final void rule__ParsExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5136:1: ( ( ( rule__ParsExpression__ExprAssignment_3_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5137:1: ( ( rule__ParsExpression__ExprAssignment_3_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5137:1: ( ( rule__ParsExpression__ExprAssignment_3_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5138:1: ( rule__ParsExpression__ExprAssignment_3_1 )
            {
             before(grammarAccess.getParsExpressionAccess().getExprAssignment_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5139:1: ( rule__ParsExpression__ExprAssignment_3_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5139:2: rule__ParsExpression__ExprAssignment_3_1
            {
            pushFollow(FOLLOW_rule__ParsExpression__ExprAssignment_3_1_in_rule__ParsExpression__Group_3__1__Impl10292);
            rule__ParsExpression__ExprAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getParsExpressionAccess().getExprAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__Group_3__1__Impl"


    // $ANTLR start "rule__Literal__Group_0__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5153:1: rule__Literal__Group_0__0 : rule__Literal__Group_0__0__Impl rule__Literal__Group_0__1 ;
    public final void rule__Literal__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5157:1: ( rule__Literal__Group_0__0__Impl rule__Literal__Group_0__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5158:2: rule__Literal__Group_0__0__Impl rule__Literal__Group_0__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_0__0__Impl_in_rule__Literal__Group_0__010326);
            rule__Literal__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_0__1_in_rule__Literal__Group_0__010329);
            rule__Literal__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_0__0"


    // $ANTLR start "rule__Literal__Group_0__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5165:1: rule__Literal__Group_0__0__Impl : ( () ) ;
    public final void rule__Literal__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5169:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5170:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5170:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5171:1: ()
            {
             before(grammarAccess.getLiteralAccess().getStringLiteralAction_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5172:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5174:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getStringLiteralAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_0__0__Impl"


    // $ANTLR start "rule__Literal__Group_0__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5184:1: rule__Literal__Group_0__1 : rule__Literal__Group_0__1__Impl ;
    public final void rule__Literal__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5188:1: ( rule__Literal__Group_0__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5189:2: rule__Literal__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_0__1__Impl_in_rule__Literal__Group_0__110387);
            rule__Literal__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_0__1"


    // $ANTLR start "rule__Literal__Group_0__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5195:1: rule__Literal__Group_0__1__Impl : ( ( rule__Literal__ValueAssignment_0_1 ) ) ;
    public final void rule__Literal__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5199:1: ( ( ( rule__Literal__ValueAssignment_0_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5200:1: ( ( rule__Literal__ValueAssignment_0_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5200:1: ( ( rule__Literal__ValueAssignment_0_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5201:1: ( rule__Literal__ValueAssignment_0_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_0_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5202:1: ( rule__Literal__ValueAssignment_0_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5202:2: rule__Literal__ValueAssignment_0_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_0_1_in_rule__Literal__Group_0__1__Impl10414);
            rule__Literal__ValueAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_0__1__Impl"


    // $ANTLR start "rule__Literal__Group_1__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5216:1: rule__Literal__Group_1__0 : rule__Literal__Group_1__0__Impl rule__Literal__Group_1__1 ;
    public final void rule__Literal__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5220:1: ( rule__Literal__Group_1__0__Impl rule__Literal__Group_1__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5221:2: rule__Literal__Group_1__0__Impl rule__Literal__Group_1__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_1__0__Impl_in_rule__Literal__Group_1__010448);
            rule__Literal__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_1__1_in_rule__Literal__Group_1__010451);
            rule__Literal__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_1__0"


    // $ANTLR start "rule__Literal__Group_1__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5228:1: rule__Literal__Group_1__0__Impl : ( () ) ;
    public final void rule__Literal__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5232:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5233:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5233:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5234:1: ()
            {
             before(grammarAccess.getLiteralAccess().getFloatLiteralAction_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5235:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5237:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getFloatLiteralAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_1__0__Impl"


    // $ANTLR start "rule__Literal__Group_1__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5247:1: rule__Literal__Group_1__1 : rule__Literal__Group_1__1__Impl ;
    public final void rule__Literal__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5251:1: ( rule__Literal__Group_1__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5252:2: rule__Literal__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_1__1__Impl_in_rule__Literal__Group_1__110509);
            rule__Literal__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_1__1"


    // $ANTLR start "rule__Literal__Group_1__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5258:1: rule__Literal__Group_1__1__Impl : ( ( rule__Literal__ValueAssignment_1_1 ) ) ;
    public final void rule__Literal__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5262:1: ( ( ( rule__Literal__ValueAssignment_1_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5263:1: ( ( rule__Literal__ValueAssignment_1_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5263:1: ( ( rule__Literal__ValueAssignment_1_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5264:1: ( rule__Literal__ValueAssignment_1_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_1_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5265:1: ( rule__Literal__ValueAssignment_1_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5265:2: rule__Literal__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_1_1_in_rule__Literal__Group_1__1__Impl10536);
            rule__Literal__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_1__1__Impl"


    // $ANTLR start "rule__Literal__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5279:1: rule__Literal__Group_2__0 : rule__Literal__Group_2__0__Impl rule__Literal__Group_2__1 ;
    public final void rule__Literal__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5283:1: ( rule__Literal__Group_2__0__Impl rule__Literal__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5284:2: rule__Literal__Group_2__0__Impl rule__Literal__Group_2__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_2__0__Impl_in_rule__Literal__Group_2__010570);
            rule__Literal__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_2__1_in_rule__Literal__Group_2__010573);
            rule__Literal__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_2__0"


    // $ANTLR start "rule__Literal__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5291:1: rule__Literal__Group_2__0__Impl : ( () ) ;
    public final void rule__Literal__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5295:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5296:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5296:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5297:1: ()
            {
             before(grammarAccess.getLiteralAccess().getIntegerLiteralAction_2_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5298:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5300:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getIntegerLiteralAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_2__0__Impl"


    // $ANTLR start "rule__Literal__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5310:1: rule__Literal__Group_2__1 : rule__Literal__Group_2__1__Impl ;
    public final void rule__Literal__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5314:1: ( rule__Literal__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5315:2: rule__Literal__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_2__1__Impl_in_rule__Literal__Group_2__110631);
            rule__Literal__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_2__1"


    // $ANTLR start "rule__Literal__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5321:1: rule__Literal__Group_2__1__Impl : ( ( rule__Literal__ValueAssignment_2_1 ) ) ;
    public final void rule__Literal__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5325:1: ( ( ( rule__Literal__ValueAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5326:1: ( ( rule__Literal__ValueAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5326:1: ( ( rule__Literal__ValueAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5327:1: ( rule__Literal__ValueAssignment_2_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5328:1: ( rule__Literal__ValueAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5328:2: rule__Literal__ValueAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_2_1_in_rule__Literal__Group_2__1__Impl10658);
            rule__Literal__ValueAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_2__1__Impl"


    // $ANTLR start "rule__Literal__Group_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5342:1: rule__Literal__Group_3__0 : rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1 ;
    public final void rule__Literal__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5346:1: ( rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5347:2: rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_3__0__Impl_in_rule__Literal__Group_3__010692);
            rule__Literal__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_3__1_in_rule__Literal__Group_3__010695);
            rule__Literal__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__0"


    // $ANTLR start "rule__Literal__Group_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5354:1: rule__Literal__Group_3__0__Impl : ( () ) ;
    public final void rule__Literal__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5358:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5359:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5359:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5360:1: ()
            {
             before(grammarAccess.getLiteralAccess().getNullAction_3_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5361:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5363:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getNullAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__0__Impl"


    // $ANTLR start "rule__Literal__Group_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5373:1: rule__Literal__Group_3__1 : rule__Literal__Group_3__1__Impl ;
    public final void rule__Literal__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5377:1: ( rule__Literal__Group_3__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5378:2: rule__Literal__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_3__1__Impl_in_rule__Literal__Group_3__110753);
            rule__Literal__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__1"


    // $ANTLR start "rule__Literal__Group_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5384:1: rule__Literal__Group_3__1__Impl : ( ( rule__Literal__ValueAssignment_3_1 ) ) ;
    public final void rule__Literal__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5388:1: ( ( ( rule__Literal__ValueAssignment_3_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5389:1: ( ( rule__Literal__ValueAssignment_3_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5389:1: ( ( rule__Literal__ValueAssignment_3_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5390:1: ( rule__Literal__ValueAssignment_3_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5391:1: ( rule__Literal__ValueAssignment_3_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5391:2: rule__Literal__ValueAssignment_3_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_3_1_in_rule__Literal__Group_3__1__Impl10780);
            rule__Literal__ValueAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__1__Impl"


    // $ANTLR start "rule__Literal__Group_4__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5405:1: rule__Literal__Group_4__0 : rule__Literal__Group_4__0__Impl rule__Literal__Group_4__1 ;
    public final void rule__Literal__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5409:1: ( rule__Literal__Group_4__0__Impl rule__Literal__Group_4__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5410:2: rule__Literal__Group_4__0__Impl rule__Literal__Group_4__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_4__0__Impl_in_rule__Literal__Group_4__010814);
            rule__Literal__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_4__1_in_rule__Literal__Group_4__010817);
            rule__Literal__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_4__0"


    // $ANTLR start "rule__Literal__Group_4__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5417:1: rule__Literal__Group_4__0__Impl : ( () ) ;
    public final void rule__Literal__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5421:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5422:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5422:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5423:1: ()
            {
             before(grammarAccess.getLiteralAccess().getTrueAction_4_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5424:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5426:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getTrueAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_4__0__Impl"


    // $ANTLR start "rule__Literal__Group_4__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5436:1: rule__Literal__Group_4__1 : rule__Literal__Group_4__1__Impl ;
    public final void rule__Literal__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5440:1: ( rule__Literal__Group_4__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5441:2: rule__Literal__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_4__1__Impl_in_rule__Literal__Group_4__110875);
            rule__Literal__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_4__1"


    // $ANTLR start "rule__Literal__Group_4__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5447:1: rule__Literal__Group_4__1__Impl : ( ( rule__Literal__ValueAssignment_4_1 ) ) ;
    public final void rule__Literal__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5451:1: ( ( ( rule__Literal__ValueAssignment_4_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5452:1: ( ( rule__Literal__ValueAssignment_4_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5452:1: ( ( rule__Literal__ValueAssignment_4_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5453:1: ( rule__Literal__ValueAssignment_4_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_4_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5454:1: ( rule__Literal__ValueAssignment_4_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5454:2: rule__Literal__ValueAssignment_4_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_4_1_in_rule__Literal__Group_4__1__Impl10902);
            rule__Literal__ValueAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_4__1__Impl"


    // $ANTLR start "rule__Literal__Group_5__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5468:1: rule__Literal__Group_5__0 : rule__Literal__Group_5__0__Impl rule__Literal__Group_5__1 ;
    public final void rule__Literal__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5472:1: ( rule__Literal__Group_5__0__Impl rule__Literal__Group_5__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5473:2: rule__Literal__Group_5__0__Impl rule__Literal__Group_5__1
            {
            pushFollow(FOLLOW_rule__Literal__Group_5__0__Impl_in_rule__Literal__Group_5__010936);
            rule__Literal__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Literal__Group_5__1_in_rule__Literal__Group_5__010939);
            rule__Literal__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_5__0"


    // $ANTLR start "rule__Literal__Group_5__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5480:1: rule__Literal__Group_5__0__Impl : ( () ) ;
    public final void rule__Literal__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5484:1: ( ( () ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5485:1: ( () )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5485:1: ( () )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5486:1: ()
            {
             before(grammarAccess.getLiteralAccess().getFalseAction_5_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5487:1: ()
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5489:1: 
            {
            }

             after(grammarAccess.getLiteralAccess().getFalseAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_5__0__Impl"


    // $ANTLR start "rule__Literal__Group_5__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5499:1: rule__Literal__Group_5__1 : rule__Literal__Group_5__1__Impl ;
    public final void rule__Literal__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5503:1: ( rule__Literal__Group_5__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5504:2: rule__Literal__Group_5__1__Impl
            {
            pushFollow(FOLLOW_rule__Literal__Group_5__1__Impl_in_rule__Literal__Group_5__110997);
            rule__Literal__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_5__1"


    // $ANTLR start "rule__Literal__Group_5__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5510:1: rule__Literal__Group_5__1__Impl : ( ( rule__Literal__ValueAssignment_5_1 ) ) ;
    public final void rule__Literal__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5514:1: ( ( ( rule__Literal__ValueAssignment_5_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5515:1: ( ( rule__Literal__ValueAssignment_5_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5515:1: ( ( rule__Literal__ValueAssignment_5_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5516:1: ( rule__Literal__ValueAssignment_5_1 )
            {
             before(grammarAccess.getLiteralAccess().getValueAssignment_5_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5517:1: ( rule__Literal__ValueAssignment_5_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5517:2: rule__Literal__ValueAssignment_5_1
            {
            pushFollow(FOLLOW_rule__Literal__ValueAssignment_5_1_in_rule__Literal__Group_5__1__Impl11024);
            rule__Literal__ValueAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getValueAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_5__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5531:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5535:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5536:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__011058);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__011061);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5543:1: rule__QualifiedName__Group__0__Impl : ( ( rule__QualifiedName__GranularAssignment_0 )? ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5547:1: ( ( ( rule__QualifiedName__GranularAssignment_0 )? ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5548:1: ( ( rule__QualifiedName__GranularAssignment_0 )? )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5548:1: ( ( rule__QualifiedName__GranularAssignment_0 )? )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5549:1: ( rule__QualifiedName__GranularAssignment_0 )?
            {
             before(grammarAccess.getQualifiedNameAccess().getGranularAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5550:1: ( rule__QualifiedName__GranularAssignment_0 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( ((LA32_0>=14 && LA32_0<=15)) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5550:2: rule__QualifiedName__GranularAssignment_0
                    {
                    pushFollow(FOLLOW_rule__QualifiedName__GranularAssignment_0_in_rule__QualifiedName__Group__0__Impl11088);
                    rule__QualifiedName__GranularAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameAccess().getGranularAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5560:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2 ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5564:1: ( rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5565:2: rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__111119);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group__2_in_rule__QualifiedName__Group__111122);
            rule__QualifiedName__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5572:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__QualifiersAssignment_1 ) ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5576:1: ( ( ( rule__QualifiedName__QualifiersAssignment_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5577:1: ( ( rule__QualifiedName__QualifiersAssignment_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5577:1: ( ( rule__QualifiedName__QualifiersAssignment_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5578:1: ( rule__QualifiedName__QualifiersAssignment_1 )
            {
             before(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5579:1: ( rule__QualifiedName__QualifiersAssignment_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5579:2: rule__QualifiedName__QualifiersAssignment_1
            {
            pushFollow(FOLLOW_rule__QualifiedName__QualifiersAssignment_1_in_rule__QualifiedName__Group__1__Impl11149);
            rule__QualifiedName__QualifiersAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5589:1: rule__QualifiedName__Group__2 : rule__QualifiedName__Group__2__Impl ;
    public final void rule__QualifiedName__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5593:1: ( rule__QualifiedName__Group__2__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5594:2: rule__QualifiedName__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group__2__Impl_in_rule__QualifiedName__Group__211179);
            rule__QualifiedName__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__2"


    // $ANTLR start "rule__QualifiedName__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5600:1: rule__QualifiedName__Group__2__Impl : ( ( rule__QualifiedName__Group_2__0 )* ) ;
    public final void rule__QualifiedName__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5604:1: ( ( ( rule__QualifiedName__Group_2__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5605:1: ( ( rule__QualifiedName__Group_2__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5605:1: ( ( rule__QualifiedName__Group_2__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5606:1: ( rule__QualifiedName__Group_2__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5607:1: ( rule__QualifiedName__Group_2__0 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==19) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5607:2: rule__QualifiedName__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__QualifiedName__Group_2__0_in_rule__QualifiedName__Group__2__Impl11206);
            	    rule__QualifiedName__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__2__Impl"


    // $ANTLR start "rule__QualifiedName__Group_2__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5623:1: rule__QualifiedName__Group_2__0 : rule__QualifiedName__Group_2__0__Impl rule__QualifiedName__Group_2__1 ;
    public final void rule__QualifiedName__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5627:1: ( rule__QualifiedName__Group_2__0__Impl rule__QualifiedName__Group_2__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5628:2: rule__QualifiedName__Group_2__0__Impl rule__QualifiedName__Group_2__1
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_2__0__Impl_in_rule__QualifiedName__Group_2__011243);
            rule__QualifiedName__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__QualifiedName__Group_2__1_in_rule__QualifiedName__Group_2__011246);
            rule__QualifiedName__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_2__0"


    // $ANTLR start "rule__QualifiedName__Group_2__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5635:1: rule__QualifiedName__Group_2__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5639:1: ( ( '.' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5640:1: ( '.' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5640:1: ( '.' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5641:1: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_2_0()); 
            match(input,19,FOLLOW_19_in_rule__QualifiedName__Group_2__0__Impl11274); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_2__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_2__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5654:1: rule__QualifiedName__Group_2__1 : rule__QualifiedName__Group_2__1__Impl ;
    public final void rule__QualifiedName__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5658:1: ( rule__QualifiedName__Group_2__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5659:2: rule__QualifiedName__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__QualifiedName__Group_2__1__Impl_in_rule__QualifiedName__Group_2__111305);
            rule__QualifiedName__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_2__1"


    // $ANTLR start "rule__QualifiedName__Group_2__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5665:1: rule__QualifiedName__Group_2__1__Impl : ( ( rule__QualifiedName__QualifiersAssignment_2_1 ) ) ;
    public final void rule__QualifiedName__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5669:1: ( ( ( rule__QualifiedName__QualifiersAssignment_2_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5670:1: ( ( rule__QualifiedName__QualifiersAssignment_2_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5670:1: ( ( rule__QualifiedName__QualifiersAssignment_2_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5671:1: ( rule__QualifiedName__QualifiersAssignment_2_1 )
            {
             before(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_2_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5672:1: ( rule__QualifiedName__QualifiersAssignment_2_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5672:2: rule__QualifiedName__QualifiersAssignment_2_1
            {
            pushFollow(FOLLOW_rule__QualifiedName__QualifiersAssignment_2_1_in_rule__QualifiedName__Group_2__1__Impl11332);
            rule__QualifiedName__QualifiersAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getQualifiersAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_2__1__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5686:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5690:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5691:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_rule__Function__Group__0__Impl_in_rule__Function__Group__011366);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__1_in_rule__Function__Group__011369);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5698:1: rule__Function__Group__0__Impl : ( ( rule__Function__NameAssignment_0 ) ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5702:1: ( ( ( rule__Function__NameAssignment_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5703:1: ( ( rule__Function__NameAssignment_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5703:1: ( ( rule__Function__NameAssignment_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5704:1: ( rule__Function__NameAssignment_0 )
            {
             before(grammarAccess.getFunctionAccess().getNameAssignment_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5705:1: ( rule__Function__NameAssignment_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5705:2: rule__Function__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__Function__NameAssignment_0_in_rule__Function__Group__0__Impl11396);
            rule__Function__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5715:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5719:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5720:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_rule__Function__Group__1__Impl_in_rule__Function__Group__111426);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__2_in_rule__Function__Group__111429);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5727:1: rule__Function__Group__1__Impl : ( '(' ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5731:1: ( ( '(' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5732:1: ( '(' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5732:1: ( '(' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5733:1: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_1()); 
            match(input,37,FOLLOW_37_in_rule__Function__Group__1__Impl11457); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5746:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5750:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5751:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_rule__Function__Group__2__Impl_in_rule__Function__Group__211488);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__3_in_rule__Function__Group__211491);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5758:1: rule__Function__Group__2__Impl : ( ( rule__Function__ParamsAssignment_2 ) ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5762:1: ( ( ( rule__Function__ParamsAssignment_2 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5763:1: ( ( rule__Function__ParamsAssignment_2 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5763:1: ( ( rule__Function__ParamsAssignment_2 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5764:1: ( rule__Function__ParamsAssignment_2 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_2()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5765:1: ( rule__Function__ParamsAssignment_2 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5765:2: rule__Function__ParamsAssignment_2
            {
            pushFollow(FOLLOW_rule__Function__ParamsAssignment_2_in_rule__Function__Group__2__Impl11518);
            rule__Function__ParamsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5775:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5779:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5780:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_rule__Function__Group__3__Impl_in_rule__Function__Group__311548);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group__4_in_rule__Function__Group__311551);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5787:1: rule__Function__Group__3__Impl : ( ( rule__Function__Group_3__0 )* ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5791:1: ( ( ( rule__Function__Group_3__0 )* ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5792:1: ( ( rule__Function__Group_3__0 )* )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5792:1: ( ( rule__Function__Group_3__0 )* )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5793:1: ( rule__Function__Group_3__0 )*
            {
             before(grammarAccess.getFunctionAccess().getGroup_3()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5794:1: ( rule__Function__Group_3__0 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==16) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5794:2: rule__Function__Group_3__0
            	    {
            	    pushFollow(FOLLOW_rule__Function__Group_3__0_in_rule__Function__Group__3__Impl11578);
            	    rule__Function__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5804:1: rule__Function__Group__4 : rule__Function__Group__4__Impl ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5808:1: ( rule__Function__Group__4__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5809:2: rule__Function__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Function__Group__4__Impl_in_rule__Function__Group__411609);
            rule__Function__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5815:1: rule__Function__Group__4__Impl : ( ')' ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5819:1: ( ( ')' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5820:1: ( ')' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5820:1: ( ')' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5821:1: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 
            match(input,38,FOLLOW_38_in_rule__Function__Group__4__Impl11637); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group_3__0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5844:1: rule__Function__Group_3__0 : rule__Function__Group_3__0__Impl rule__Function__Group_3__1 ;
    public final void rule__Function__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5848:1: ( rule__Function__Group_3__0__Impl rule__Function__Group_3__1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5849:2: rule__Function__Group_3__0__Impl rule__Function__Group_3__1
            {
            pushFollow(FOLLOW_rule__Function__Group_3__0__Impl_in_rule__Function__Group_3__011678);
            rule__Function__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Function__Group_3__1_in_rule__Function__Group_3__011681);
            rule__Function__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__0"


    // $ANTLR start "rule__Function__Group_3__0__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5856:1: rule__Function__Group_3__0__Impl : ( ',' ) ;
    public final void rule__Function__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5860:1: ( ( ',' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5861:1: ( ',' )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5861:1: ( ',' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5862:1: ','
            {
             before(grammarAccess.getFunctionAccess().getCommaKeyword_3_0()); 
            match(input,16,FOLLOW_16_in_rule__Function__Group_3__0__Impl11709); 
             after(grammarAccess.getFunctionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__0__Impl"


    // $ANTLR start "rule__Function__Group_3__1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5875:1: rule__Function__Group_3__1 : rule__Function__Group_3__1__Impl ;
    public final void rule__Function__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5879:1: ( rule__Function__Group_3__1__Impl )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5880:2: rule__Function__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__Function__Group_3__1__Impl_in_rule__Function__Group_3__111740);
            rule__Function__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__1"


    // $ANTLR start "rule__Function__Group_3__1__Impl"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5886:1: rule__Function__Group_3__1__Impl : ( ( rule__Function__ParamsAssignment_3_1 ) ) ;
    public final void rule__Function__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5890:1: ( ( ( rule__Function__ParamsAssignment_3_1 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5891:1: ( ( rule__Function__ParamsAssignment_3_1 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5891:1: ( ( rule__Function__ParamsAssignment_3_1 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5892:1: ( rule__Function__ParamsAssignment_3_1 )
            {
             before(grammarAccess.getFunctionAccess().getParamsAssignment_3_1()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5893:1: ( rule__Function__ParamsAssignment_3_1 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5893:2: rule__Function__ParamsAssignment_3_1
            {
            pushFollow(FOLLOW_rule__Function__ParamsAssignment_3_1_in_rule__Function__Group_3__1__Impl11767);
            rule__Function__ParamsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParamsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__1__Impl"


    // $ANTLR start "rule__Query__QueryAssignment"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5908:1: rule__Query__QueryAssignment : ( ruleQueryStatement ) ;
    public final void rule__Query__QueryAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5912:1: ( ( ruleQueryStatement ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5913:1: ( ruleQueryStatement )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5913:1: ( ruleQueryStatement )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5914:1: ruleQueryStatement
            {
             before(grammarAccess.getQueryAccess().getQueryQueryStatementParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleQueryStatement_in_rule__Query__QueryAssignment11806);
            ruleQueryStatement();

            state._fsp--;

             after(grammarAccess.getQueryAccess().getQueryQueryStatementParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Query__QueryAssignment"


    // $ANTLR start "rule__QueryStatement__FindAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5923:1: rule__QueryStatement__FindAssignment_0 : ( ruleFindClause ) ;
    public final void rule__QueryStatement__FindAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5927:1: ( ( ruleFindClause ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5928:1: ( ruleFindClause )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5928:1: ( ruleFindClause )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5929:1: ruleFindClause
            {
             before(grammarAccess.getQueryStatementAccess().getFindFindClauseParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleFindClause_in_rule__QueryStatement__FindAssignment_011837);
            ruleFindClause();

            state._fsp--;

             after(grammarAccess.getQueryStatementAccess().getFindFindClauseParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__FindAssignment_0"


    // $ANTLR start "rule__QueryStatement__WhereAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5938:1: rule__QueryStatement__WhereAssignment_1 : ( ruleWhereClause ) ;
    public final void rule__QueryStatement__WhereAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5942:1: ( ( ruleWhereClause ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5943:1: ( ruleWhereClause )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5943:1: ( ruleWhereClause )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5944:1: ruleWhereClause
            {
             before(grammarAccess.getQueryStatementAccess().getWhereWhereClauseParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleWhereClause_in_rule__QueryStatement__WhereAssignment_111868);
            ruleWhereClause();

            state._fsp--;

             after(grammarAccess.getQueryStatementAccess().getWhereWhereClauseParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__WhereAssignment_1"


    // $ANTLR start "rule__QueryStatement__ReturnAssignment_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5953:1: rule__QueryStatement__ReturnAssignment_2 : ( ruleReturnsClause ) ;
    public final void rule__QueryStatement__ReturnAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5957:1: ( ( ruleReturnsClause ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5958:1: ( ruleReturnsClause )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5958:1: ( ruleReturnsClause )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5959:1: ruleReturnsClause
            {
             before(grammarAccess.getQueryStatementAccess().getReturnReturnsClauseParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleReturnsClause_in_rule__QueryStatement__ReturnAssignment_211899);
            ruleReturnsClause();

            state._fsp--;

             after(grammarAccess.getQueryStatementAccess().getReturnReturnsClauseParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__ReturnAssignment_2"


    // $ANTLR start "rule__QueryStatement__OrderbyAssignment_3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5968:1: rule__QueryStatement__OrderbyAssignment_3 : ( ruleOrderByClause ) ;
    public final void rule__QueryStatement__OrderbyAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5972:1: ( ( ruleOrderByClause ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5973:1: ( ruleOrderByClause )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5973:1: ( ruleOrderByClause )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5974:1: ruleOrderByClause
            {
             before(grammarAccess.getQueryStatementAccess().getOrderbyOrderByClauseParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleOrderByClause_in_rule__QueryStatement__OrderbyAssignment_311930);
            ruleOrderByClause();

            state._fsp--;

             after(grammarAccess.getQueryStatementAccess().getOrderbyOrderByClauseParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__OrderbyAssignment_3"


    // $ANTLR start "rule__QueryStatement__GroupbyAssignment_4"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5983:1: rule__QueryStatement__GroupbyAssignment_4 : ( ruleGroupByClause ) ;
    public final void rule__QueryStatement__GroupbyAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5987:1: ( ( ruleGroupByClause ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5988:1: ( ruleGroupByClause )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5988:1: ( ruleGroupByClause )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5989:1: ruleGroupByClause
            {
             before(grammarAccess.getQueryStatementAccess().getGroupbyGroupByClauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleGroupByClause_in_rule__QueryStatement__GroupbyAssignment_411961);
            ruleGroupByClause();

            state._fsp--;

             after(grammarAccess.getQueryStatementAccess().getGroupbyGroupByClauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QueryStatement__GroupbyAssignment_4"


    // $ANTLR start "rule__FindClause__ClauseAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:5998:1: rule__FindClause__ClauseAssignment_0 : ( ( 'find' ) ) ;
    public final void rule__FindClause__ClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6002:1: ( ( ( 'find' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6003:1: ( ( 'find' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6003:1: ( ( 'find' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6004:1: ( 'find' )
            {
             before(grammarAccess.getFindClauseAccess().getClauseFindKeyword_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6005:1: ( 'find' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6006:1: 'find'
            {
             before(grammarAccess.getFindClauseAccess().getClauseFindKeyword_0_0()); 
            match(input,39,FOLLOW_39_in_rule__FindClause__ClauseAssignment_011997); 
             after(grammarAccess.getFindClauseAccess().getClauseFindKeyword_0_0()); 

            }

             after(grammarAccess.getFindClauseAccess().getClauseFindKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__ClauseAssignment_0"


    // $ANTLR start "rule__FindClause__BindingObjectAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6021:1: rule__FindClause__BindingObjectAssignment_1 : ( ruleBindingObject ) ;
    public final void rule__FindClause__BindingObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6025:1: ( ( ruleBindingObject ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6026:1: ( ruleBindingObject )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6026:1: ( ruleBindingObject )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6027:1: ruleBindingObject
            {
             before(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleBindingObject_in_rule__FindClause__BindingObjectAssignment_112036);
            ruleBindingObject();

            state._fsp--;

             after(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__BindingObjectAssignment_1"


    // $ANTLR start "rule__FindClause__BindingObjectAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6036:1: rule__FindClause__BindingObjectAssignment_2_1 : ( ruleBindingObject ) ;
    public final void rule__FindClause__BindingObjectAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6040:1: ( ( ruleBindingObject ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6041:1: ( ruleBindingObject )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6041:1: ( ruleBindingObject )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6042:1: ruleBindingObject
            {
             before(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleBindingObject_in_rule__FindClause__BindingObjectAssignment_2_112067);
            ruleBindingObject();

            state._fsp--;

             after(grammarAccess.getFindClauseAccess().getBindingObjectBindingObjectParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FindClause__BindingObjectAssignment_2_1"


    // $ANTLR start "rule__BindingObject__TypeAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6051:1: rule__BindingObject__TypeAssignment_0 : ( ruleObjectType ) ;
    public final void rule__BindingObject__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6055:1: ( ( ruleObjectType ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6056:1: ( ruleObjectType )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6056:1: ( ruleObjectType )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6057:1: ruleObjectType
            {
             before(grammarAccess.getBindingObjectAccess().getTypeObjectTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleObjectType_in_rule__BindingObject__TypeAssignment_012098);
            ruleObjectType();

            state._fsp--;

             after(grammarAccess.getBindingObjectAccess().getTypeObjectTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__TypeAssignment_0"


    // $ANTLR start "rule__BindingObject__AliasAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6066:1: rule__BindingObject__AliasAssignment_1 : ( RULE_ID ) ;
    public final void rule__BindingObject__AliasAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6070:1: ( ( RULE_ID ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6071:1: ( RULE_ID )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6071:1: ( RULE_ID )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6072:1: RULE_ID
            {
             before(grammarAccess.getBindingObjectAccess().getAliasIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BindingObject__AliasAssignment_112129); 
             after(grammarAccess.getBindingObjectAccess().getAliasIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BindingObject__AliasAssignment_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_0_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6081:1: rule__ObjectType__ValueAssignment_0_1 : ( ( 'project' ) ) ;
    public final void rule__ObjectType__ValueAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6085:1: ( ( ( 'project' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6086:1: ( ( 'project' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6086:1: ( ( 'project' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6087:1: ( 'project' )
            {
             before(grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6088:1: ( 'project' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6089:1: 'project'
            {
             before(grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0()); 
            match(input,40,FOLLOW_40_in_rule__ObjectType__ValueAssignment_0_112165); 
             after(grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValueProjectKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_0_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6104:1: rule__ObjectType__ValueAssignment_1_1 : ( ( 'package' ) ) ;
    public final void rule__ObjectType__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6108:1: ( ( ( 'package' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6109:1: ( ( 'package' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6109:1: ( ( 'package' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6110:1: ( 'package' )
            {
             before(grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6111:1: ( 'package' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6112:1: 'package'
            {
             before(grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0()); 
            match(input,41,FOLLOW_41_in_rule__ObjectType__ValueAssignment_1_112209); 
             after(grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValuePackageKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_1_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6127:1: rule__ObjectType__ValueAssignment_2_1 : ( ( 'class' ) ) ;
    public final void rule__ObjectType__ValueAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6131:1: ( ( ( 'class' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6132:1: ( ( 'class' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6132:1: ( ( 'class' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6133:1: ( 'class' )
            {
             before(grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6134:1: ( 'class' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6135:1: 'class'
            {
             before(grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0()); 
            match(input,42,FOLLOW_42_in_rule__ObjectType__ValueAssignment_2_112253); 
             after(grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValueClassKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_2_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_3_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6150:1: rule__ObjectType__ValueAssignment_3_1 : ( ( 'aspect' ) ) ;
    public final void rule__ObjectType__ValueAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6154:1: ( ( ( 'aspect' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6155:1: ( ( 'aspect' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6155:1: ( ( 'aspect' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6156:1: ( 'aspect' )
            {
             before(grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6157:1: ( 'aspect' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6158:1: 'aspect'
            {
             before(grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0()); 
            match(input,43,FOLLOW_43_in_rule__ObjectType__ValueAssignment_3_112297); 
             after(grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValueAspectKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_3_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_4_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6173:1: rule__ObjectType__ValueAssignment_4_1 : ( ( 'interface' ) ) ;
    public final void rule__ObjectType__ValueAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6177:1: ( ( ( 'interface' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6178:1: ( ( 'interface' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6178:1: ( ( 'interface' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6179:1: ( 'interface' )
            {
             before(grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6180:1: ( 'interface' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6181:1: 'interface'
            {
             before(grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0()); 
            match(input,44,FOLLOW_44_in_rule__ObjectType__ValueAssignment_4_112341); 
             after(grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValueInterfaceKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_4_1"


    // $ANTLR start "rule__ObjectType__ValueAssignment_5_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6196:1: rule__ObjectType__ValueAssignment_5_1 : ( ( 'enum' ) ) ;
    public final void rule__ObjectType__ValueAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6200:1: ( ( ( 'enum' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6201:1: ( ( 'enum' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6201:1: ( ( 'enum' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6202:1: ( 'enum' )
            {
             before(grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6203:1: ( 'enum' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6204:1: 'enum'
            {
             before(grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0()); 
            match(input,45,FOLLOW_45_in_rule__ObjectType__ValueAssignment_5_112385); 
             after(grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0()); 

            }

             after(grammarAccess.getObjectTypeAccess().getValueEnumKeyword_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectType__ValueAssignment_5_1"


    // $ANTLR start "rule__WhereClause__ClauseAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6219:1: rule__WhereClause__ClauseAssignment_0 : ( ( 'where' ) ) ;
    public final void rule__WhereClause__ClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6223:1: ( ( ( 'where' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6224:1: ( ( 'where' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6224:1: ( ( 'where' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6225:1: ( 'where' )
            {
             before(grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6226:1: ( 'where' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6227:1: 'where'
            {
             before(grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0()); 
            match(input,46,FOLLOW_46_in_rule__WhereClause__ClauseAssignment_012429); 
             after(grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0()); 

            }

             after(grammarAccess.getWhereClauseAccess().getClauseWhereKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__ClauseAssignment_0"


    // $ANTLR start "rule__WhereClause__ExpressionAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6242:1: rule__WhereClause__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__WhereClause__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6246:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6247:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6247:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6248:1: ruleExpression
            {
             before(grammarAccess.getWhereClauseAccess().getExpressionExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__WhereClause__ExpressionAssignment_112468);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getWhereClauseAccess().getExpressionExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhereClause__ExpressionAssignment_1"


    // $ANTLR start "rule__ReturnsClause__ClauseAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6257:1: rule__ReturnsClause__ClauseAssignment_0 : ( ( 'returns' ) ) ;
    public final void rule__ReturnsClause__ClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6261:1: ( ( ( 'returns' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6262:1: ( ( 'returns' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6262:1: ( ( 'returns' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6263:1: ( 'returns' )
            {
             before(grammarAccess.getReturnsClauseAccess().getClauseReturnsKeyword_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6264:1: ( 'returns' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6265:1: 'returns'
            {
             before(grammarAccess.getReturnsClauseAccess().getClauseReturnsKeyword_0_0()); 
            match(input,47,FOLLOW_47_in_rule__ReturnsClause__ClauseAssignment_012504); 
             after(grammarAccess.getReturnsClauseAccess().getClauseReturnsKeyword_0_0()); 

            }

             after(grammarAccess.getReturnsClauseAccess().getClauseReturnsKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__ClauseAssignment_0"


    // $ANTLR start "rule__ReturnsClause__ExpressionsAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6280:1: rule__ReturnsClause__ExpressionsAssignment_1 : ( ruleExpression ) ;
    public final void rule__ReturnsClause__ExpressionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6284:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6285:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6285:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6286:1: ruleExpression
            {
             before(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__ReturnsClause__ExpressionsAssignment_112543);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__ExpressionsAssignment_1"


    // $ANTLR start "rule__ReturnsClause__ResultAliasAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6295:1: rule__ReturnsClause__ResultAliasAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__ReturnsClause__ResultAliasAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6299:1: ( ( RULE_ID ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6300:1: ( RULE_ID )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6300:1: ( RULE_ID )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6301:1: RULE_ID
            {
             before(grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ReturnsClause__ResultAliasAssignment_2_112574); 
             after(grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__ResultAliasAssignment_2_1"


    // $ANTLR start "rule__ReturnsClause__ExpressionsAssignment_3_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6310:1: rule__ReturnsClause__ExpressionsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__ReturnsClause__ExpressionsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6314:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6315:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6315:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6316:1: ruleExpression
            {
             before(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__ReturnsClause__ExpressionsAssignment_3_112605);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getReturnsClauseAccess().getExpressionsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__ExpressionsAssignment_3_1"


    // $ANTLR start "rule__ReturnsClause__ResultAliasAssignment_3_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6325:1: rule__ReturnsClause__ResultAliasAssignment_3_2_1 : ( RULE_ID ) ;
    public final void rule__ReturnsClause__ResultAliasAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6329:1: ( ( RULE_ID ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6330:1: ( RULE_ID )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6330:1: ( RULE_ID )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6331:1: RULE_ID
            {
             before(grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_3_2_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ReturnsClause__ResultAliasAssignment_3_2_112636); 
             after(grammarAccess.getReturnsClauseAccess().getResultAliasIDTerminalRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnsClause__ResultAliasAssignment_3_2_1"


    // $ANTLR start "rule__GroupByClause__ClauseAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6340:1: rule__GroupByClause__ClauseAssignment_0 : ( ( 'group by' ) ) ;
    public final void rule__GroupByClause__ClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6344:1: ( ( ( 'group by' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6345:1: ( ( 'group by' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6345:1: ( ( 'group by' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6346:1: ( 'group by' )
            {
             before(grammarAccess.getGroupByClauseAccess().getClauseGroupByKeyword_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6347:1: ( 'group by' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6348:1: 'group by'
            {
             before(grammarAccess.getGroupByClauseAccess().getClauseGroupByKeyword_0_0()); 
            match(input,48,FOLLOW_48_in_rule__GroupByClause__ClauseAssignment_012672); 
             after(grammarAccess.getGroupByClauseAccess().getClauseGroupByKeyword_0_0()); 

            }

             after(grammarAccess.getGroupByClauseAccess().getClauseGroupByKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__ClauseAssignment_0"


    // $ANTLR start "rule__GroupByClause__ExpressionsAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6363:1: rule__GroupByClause__ExpressionsAssignment_1 : ( ruleSimpleQualifiedName ) ;
    public final void rule__GroupByClause__ExpressionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6367:1: ( ( ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6368:1: ( ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6368:1: ( ruleSimpleQualifiedName )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6369:1: ruleSimpleQualifiedName
            {
             before(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_rule__GroupByClause__ExpressionsAssignment_112711);
            ruleSimpleQualifiedName();

            state._fsp--;

             after(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__ExpressionsAssignment_1"


    // $ANTLR start "rule__GroupByClause__ExpressionsAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6378:1: rule__GroupByClause__ExpressionsAssignment_2_1 : ( ruleSimpleQualifiedName ) ;
    public final void rule__GroupByClause__ExpressionsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6382:1: ( ( ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6383:1: ( ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6383:1: ( ruleSimpleQualifiedName )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6384:1: ruleSimpleQualifiedName
            {
             before(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_rule__GroupByClause__ExpressionsAssignment_2_112742);
            ruleSimpleQualifiedName();

            state._fsp--;

             after(grammarAccess.getGroupByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GroupByClause__ExpressionsAssignment_2_1"


    // $ANTLR start "rule__OrderByClause__ClauseAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6393:1: rule__OrderByClause__ClauseAssignment_0 : ( ( 'order by' ) ) ;
    public final void rule__OrderByClause__ClauseAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6397:1: ( ( ( 'order by' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6398:1: ( ( 'order by' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6398:1: ( ( 'order by' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6399:1: ( 'order by' )
            {
             before(grammarAccess.getOrderByClauseAccess().getClauseOrderByKeyword_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6400:1: ( 'order by' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6401:1: 'order by'
            {
             before(grammarAccess.getOrderByClauseAccess().getClauseOrderByKeyword_0_0()); 
            match(input,49,FOLLOW_49_in_rule__OrderByClause__ClauseAssignment_012778); 
             after(grammarAccess.getOrderByClauseAccess().getClauseOrderByKeyword_0_0()); 

            }

             after(grammarAccess.getOrderByClauseAccess().getClauseOrderByKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__ClauseAssignment_0"


    // $ANTLR start "rule__OrderByClause__ExpressionsAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6416:1: rule__OrderByClause__ExpressionsAssignment_1 : ( ruleSimpleQualifiedName ) ;
    public final void rule__OrderByClause__ExpressionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6420:1: ( ( ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6421:1: ( ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6421:1: ( ruleSimpleQualifiedName )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6422:1: ruleSimpleQualifiedName
            {
             before(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_rule__OrderByClause__ExpressionsAssignment_112817);
            ruleSimpleQualifiedName();

            state._fsp--;

             after(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__ExpressionsAssignment_1"


    // $ANTLR start "rule__OrderByClause__ExpressionsAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6431:1: rule__OrderByClause__ExpressionsAssignment_2_1 : ( ruleSimpleQualifiedName ) ;
    public final void rule__OrderByClause__ExpressionsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6435:1: ( ( ruleSimpleQualifiedName ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6436:1: ( ruleSimpleQualifiedName )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6436:1: ( ruleSimpleQualifiedName )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6437:1: ruleSimpleQualifiedName
            {
             before(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleSimpleQualifiedName_in_rule__OrderByClause__ExpressionsAssignment_2_112848);
            ruleSimpleQualifiedName();

            state._fsp--;

             after(grammarAccess.getOrderByClauseAccess().getExpressionsSimpleQualifiedNameParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__ExpressionsAssignment_2_1"


    // $ANTLR start "rule__OrderByClause__OptionAssignment_3"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6446:1: rule__OrderByClause__OptionAssignment_3 : ( ( rule__OrderByClause__OptionAlternatives_3_0 ) ) ;
    public final void rule__OrderByClause__OptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6450:1: ( ( ( rule__OrderByClause__OptionAlternatives_3_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6451:1: ( ( rule__OrderByClause__OptionAlternatives_3_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6451:1: ( ( rule__OrderByClause__OptionAlternatives_3_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6452:1: ( rule__OrderByClause__OptionAlternatives_3_0 )
            {
             before(grammarAccess.getOrderByClauseAccess().getOptionAlternatives_3_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6453:1: ( rule__OrderByClause__OptionAlternatives_3_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6453:2: rule__OrderByClause__OptionAlternatives_3_0
            {
            pushFollow(FOLLOW_rule__OrderByClause__OptionAlternatives_3_0_in_rule__OrderByClause__OptionAssignment_312879);
            rule__OrderByClause__OptionAlternatives_3_0();

            state._fsp--;


            }

             after(grammarAccess.getOrderByClauseAccess().getOptionAlternatives_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__OptionAssignment_3"


    // $ANTLR start "rule__OrderByClause__HavingExpressionAssignment_4_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6462:1: rule__OrderByClause__HavingExpressionAssignment_4_1 : ( ruleExpression ) ;
    public final void rule__OrderByClause__HavingExpressionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6466:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6467:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6467:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6468:1: ruleExpression
            {
             before(grammarAccess.getOrderByClauseAccess().getHavingExpressionExpressionParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__OrderByClause__HavingExpressionAssignment_4_112912);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getOrderByClauseAccess().getHavingExpressionExpressionParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrderByClause__HavingExpressionAssignment_4_1"


    // $ANTLR start "rule__SimpleQualifiedName__QualifiersAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6477:1: rule__SimpleQualifiedName__QualifiersAssignment_0 : ( ruleSimpleQualified ) ;
    public final void rule__SimpleQualifiedName__QualifiersAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6481:1: ( ( ruleSimpleQualified ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6482:1: ( ruleSimpleQualified )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6482:1: ( ruleSimpleQualified )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6483:1: ruleSimpleQualified
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleSimpleQualified_in_rule__SimpleQualifiedName__QualifiersAssignment_012943);
            ruleSimpleQualified();

            state._fsp--;

             after(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__QualifiersAssignment_0"


    // $ANTLR start "rule__SimpleQualifiedName__QualifiersAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6492:1: rule__SimpleQualifiedName__QualifiersAssignment_1_1 : ( ruleSimpleQualified ) ;
    public final void rule__SimpleQualifiedName__QualifiersAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6496:1: ( ( ruleSimpleQualified ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6497:1: ( ruleSimpleQualified )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6497:1: ( ruleSimpleQualified )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6498:1: ruleSimpleQualified
            {
             before(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleSimpleQualified_in_rule__SimpleQualifiedName__QualifiersAssignment_1_112974);
            ruleSimpleQualified();

            state._fsp--;

             after(grammarAccess.getSimpleQualifiedNameAccess().getQualifiersSimpleQualifiedParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleQualifiedName__QualifiersAssignment_1_1"


    // $ANTLR start "rule__Or__RightAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6507:1: rule__Or__RightAssignment_1_1 : ( ruleAnd ) ;
    public final void rule__Or__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6511:1: ( ( ruleAnd ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6512:1: ( ruleAnd )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6512:1: ( ruleAnd )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6513:1: ruleAnd
            {
             before(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleAnd_in_rule__Or__RightAssignment_1_113005);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightAndParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_1"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6522:1: rule__And__RightAssignment_1_2 : ( ruleRelation ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6526:1: ( ( ruleRelation ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6527:1: ( ruleRelation )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6527:1: ( ruleRelation )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6528:1: ruleRelation
            {
             before(grammarAccess.getAndAccess().getRightRelationParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleRelation_in_rule__And__RightAssignment_1_213036);
            ruleRelation();

            state._fsp--;

             after(grammarAccess.getAndAccess().getRightRelationParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Relation__RightAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6537:1: rule__Relation__RightAssignment_1_1 : ( ruleAdd ) ;
    public final void rule__Relation__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6541:1: ( ( ruleAdd ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6542:1: ( ruleAdd )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6542:1: ( ruleAdd )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6543:1: ruleAdd
            {
             before(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleAdd_in_rule__Relation__RightAssignment_1_113067);
            ruleAdd();

            state._fsp--;

             after(grammarAccess.getRelationAccess().getRightAddParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relation__RightAssignment_1_1"


    // $ANTLR start "rule__Add__RightAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6552:1: rule__Add__RightAssignment_1_1 : ( ruleMult ) ;
    public final void rule__Add__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6556:1: ( ( ruleMult ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6557:1: ( ruleMult )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6557:1: ( ruleMult )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6558:1: ruleMult
            {
             before(grammarAccess.getAddAccess().getRightMultParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleMult_in_rule__Add__RightAssignment_1_113098);
            ruleMult();

            state._fsp--;

             after(grammarAccess.getAddAccess().getRightMultParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Add__RightAssignment_1_1"


    // $ANTLR start "rule__Mult__RightAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6567:1: rule__Mult__RightAssignment_1_1 : ( ruleIn ) ;
    public final void rule__Mult__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6571:1: ( ( ruleIn ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6572:1: ( ruleIn )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6572:1: ( ruleIn )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6573:1: ruleIn
            {
             before(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleIn_in_rule__Mult__RightAssignment_1_113129);
            ruleIn();

            state._fsp--;

             after(grammarAccess.getMultAccess().getRightInParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mult__RightAssignment_1_1"


    // $ANTLR start "rule__In__RightAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6582:1: rule__In__RightAssignment_1_1 : ( ruleUnary ) ;
    public final void rule__In__RightAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6586:1: ( ( ruleUnary ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6587:1: ( ruleUnary )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6587:1: ( ruleUnary )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6588:1: ruleUnary
            {
             before(grammarAccess.getInAccess().getRightUnaryParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_ruleUnary_in_rule__In__RightAssignment_1_113160);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getInAccess().getRightUnaryParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__In__RightAssignment_1_1"


    // $ANTLR start "rule__Unary__ExpAssignment_1_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6597:1: rule__Unary__ExpAssignment_1_2 : ( ruleUnary ) ;
    public final void rule__Unary__ExpAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6601:1: ( ( ruleUnary ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6602:1: ( ruleUnary )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6602:1: ( ruleUnary )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6603:1: ruleUnary
            {
             before(grammarAccess.getUnaryAccess().getExpUnaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleUnary_in_rule__Unary__ExpAssignment_1_213191);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getUnaryAccess().getExpUnaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__ExpAssignment_1_2"


    // $ANTLR start "rule__Unary__ExprAssignment_2_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6612:1: rule__Unary__ExprAssignment_2_2 : ( ruleUnary ) ;
    public final void rule__Unary__ExprAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6616:1: ( ( ruleUnary ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6617:1: ( ruleUnary )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6617:1: ( ruleUnary )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6618:1: ruleUnary
            {
             before(grammarAccess.getUnaryAccess().getExprUnaryParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_ruleUnary_in_rule__Unary__ExprAssignment_2_213222);
            ruleUnary();

            state._fsp--;

             after(grammarAccess.getUnaryAccess().getExprUnaryParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Unary__ExprAssignment_2_2"


    // $ANTLR start "rule__Exponential__RightAssignment_1_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6627:1: rule__Exponential__RightAssignment_1_2 : ( ruleExponential ) ;
    public final void rule__Exponential__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6631:1: ( ( ruleExponential ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6632:1: ( ruleExponential )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6632:1: ( ruleExponential )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6633:1: ruleExponential
            {
             before(grammarAccess.getExponentialAccess().getRightExponentialParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_ruleExponential_in_rule__Exponential__RightAssignment_1_213253);
            ruleExponential();

            state._fsp--;

             after(grammarAccess.getExponentialAccess().getRightExponentialParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Exponential__RightAssignment_1_2"


    // $ANTLR start "rule__ParsExpression__ExprAssignment_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6642:1: rule__ParsExpression__ExprAssignment_2 : ( ruleExpression ) ;
    public final void rule__ParsExpression__ExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6646:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6647:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6647:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6648:1: ruleExpression
            {
             before(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__ParsExpression__ExprAssignment_213284);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__ExprAssignment_2"


    // $ANTLR start "rule__ParsExpression__ExprAssignment_3_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6657:1: rule__ParsExpression__ExprAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__ParsExpression__ExprAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6661:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6662:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6662:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6663:1: ruleExpression
            {
             before(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__ParsExpression__ExprAssignment_3_113315);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getParsExpressionAccess().getExprExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParsExpression__ExprAssignment_3_1"


    // $ANTLR start "rule__Literal__ValueAssignment_0_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6672:1: rule__Literal__ValueAssignment_0_1 : ( RULE_STRING ) ;
    public final void rule__Literal__ValueAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6676:1: ( ( RULE_STRING ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6677:1: ( RULE_STRING )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6677:1: ( RULE_STRING )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6678:1: RULE_STRING
            {
             before(grammarAccess.getLiteralAccess().getValueSTRINGTerminalRuleCall_0_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Literal__ValueAssignment_0_113346); 
             after(grammarAccess.getLiteralAccess().getValueSTRINGTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_0_1"


    // $ANTLR start "rule__Literal__ValueAssignment_1_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6687:1: rule__Literal__ValueAssignment_1_1 : ( RULE_FLOAT ) ;
    public final void rule__Literal__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6691:1: ( ( RULE_FLOAT ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6692:1: ( RULE_FLOAT )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6692:1: ( RULE_FLOAT )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6693:1: RULE_FLOAT
            {
             before(grammarAccess.getLiteralAccess().getValueFLOATTerminalRuleCall_1_1_0()); 
            match(input,RULE_FLOAT,FOLLOW_RULE_FLOAT_in_rule__Literal__ValueAssignment_1_113377); 
             after(grammarAccess.getLiteralAccess().getValueFLOATTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_1_1"


    // $ANTLR start "rule__Literal__ValueAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6702:1: rule__Literal__ValueAssignment_2_1 : ( RULE_INT ) ;
    public final void rule__Literal__ValueAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6706:1: ( ( RULE_INT ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6707:1: ( RULE_INT )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6707:1: ( RULE_INT )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6708:1: RULE_INT
            {
             before(grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_2_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Literal__ValueAssignment_2_113408); 
             after(grammarAccess.getLiteralAccess().getValueINTTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_2_1"


    // $ANTLR start "rule__Literal__ValueAssignment_3_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6717:1: rule__Literal__ValueAssignment_3_1 : ( ( 'null' ) ) ;
    public final void rule__Literal__ValueAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6721:1: ( ( ( 'null' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6722:1: ( ( 'null' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6722:1: ( ( 'null' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6723:1: ( 'null' )
            {
             before(grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6724:1: ( 'null' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6725:1: 'null'
            {
             before(grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0()); 
            match(input,50,FOLLOW_50_in_rule__Literal__ValueAssignment_3_113444); 
             after(grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0()); 

            }

             after(grammarAccess.getLiteralAccess().getValueNullKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_3_1"


    // $ANTLR start "rule__Literal__ValueAssignment_4_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6740:1: rule__Literal__ValueAssignment_4_1 : ( ( 'true' ) ) ;
    public final void rule__Literal__ValueAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6744:1: ( ( ( 'true' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6745:1: ( ( 'true' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6745:1: ( ( 'true' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6746:1: ( 'true' )
            {
             before(grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6747:1: ( 'true' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6748:1: 'true'
            {
             before(grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0()); 
            match(input,51,FOLLOW_51_in_rule__Literal__ValueAssignment_4_113488); 
             after(grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0()); 

            }

             after(grammarAccess.getLiteralAccess().getValueTrueKeyword_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_4_1"


    // $ANTLR start "rule__Literal__ValueAssignment_5_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6763:1: rule__Literal__ValueAssignment_5_1 : ( ( 'false' ) ) ;
    public final void rule__Literal__ValueAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6767:1: ( ( ( 'false' ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6768:1: ( ( 'false' ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6768:1: ( ( 'false' ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6769:1: ( 'false' )
            {
             before(grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6770:1: ( 'false' )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6771:1: 'false'
            {
             before(grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0()); 
            match(input,52,FOLLOW_52_in_rule__Literal__ValueAssignment_5_113532); 
             after(grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0()); 

            }

             after(grammarAccess.getLiteralAccess().getValueFalseKeyword_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__ValueAssignment_5_1"


    // $ANTLR start "rule__QualifiedName__GranularAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6786:1: rule__QualifiedName__GranularAssignment_0 : ( ( rule__QualifiedName__GranularAlternatives_0_0 ) ) ;
    public final void rule__QualifiedName__GranularAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6790:1: ( ( ( rule__QualifiedName__GranularAlternatives_0_0 ) ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6791:1: ( ( rule__QualifiedName__GranularAlternatives_0_0 ) )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6791:1: ( ( rule__QualifiedName__GranularAlternatives_0_0 ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6792:1: ( rule__QualifiedName__GranularAlternatives_0_0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGranularAlternatives_0_0()); 
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6793:1: ( rule__QualifiedName__GranularAlternatives_0_0 )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6793:2: rule__QualifiedName__GranularAlternatives_0_0
            {
            pushFollow(FOLLOW_rule__QualifiedName__GranularAlternatives_0_0_in_rule__QualifiedName__GranularAssignment_013571);
            rule__QualifiedName__GranularAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGranularAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__GranularAssignment_0"


    // $ANTLR start "rule__QualifiedName__QualifiersAssignment_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6802:1: rule__QualifiedName__QualifiersAssignment_1 : ( ruleQualified ) ;
    public final void rule__QualifiedName__QualifiersAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6806:1: ( ( ruleQualified ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6807:1: ( ruleQualified )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6807:1: ( ruleQualified )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6808:1: ruleQualified
            {
             before(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleQualified_in_rule__QualifiedName__QualifiersAssignment_113604);
            ruleQualified();

            state._fsp--;

             after(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__QualifiersAssignment_1"


    // $ANTLR start "rule__QualifiedName__QualifiersAssignment_2_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6817:1: rule__QualifiedName__QualifiersAssignment_2_1 : ( ruleQualified ) ;
    public final void rule__QualifiedName__QualifiersAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6821:1: ( ( ruleQualified ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6822:1: ( ruleQualified )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6822:1: ( ruleQualified )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6823:1: ruleQualified
            {
             before(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleQualified_in_rule__QualifiedName__QualifiersAssignment_2_113635);
            ruleQualified();

            state._fsp--;

             after(grammarAccess.getQualifiedNameAccess().getQualifiersQualifiedParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__QualifiersAssignment_2_1"


    // $ANTLR start "rule__Function__NameAssignment_0"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6832:1: rule__Function__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Function__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6836:1: ( ( RULE_ID ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6837:1: ( RULE_ID )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6837:1: ( RULE_ID )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6838:1: RULE_ID
            {
             before(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Function__NameAssignment_013666); 
             after(grammarAccess.getFunctionAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__NameAssignment_0"


    // $ANTLR start "rule__Function__ParamsAssignment_2"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6847:1: rule__Function__ParamsAssignment_2 : ( ruleExpression ) ;
    public final void rule__Function__ParamsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6851:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6852:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6852:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6853:1: ruleExpression
            {
             before(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Function__ParamsAssignment_213697);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_2"


    // $ANTLR start "rule__Function__ParamsAssignment_3_1"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6862:1: rule__Function__ParamsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__Function__ParamsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6866:1: ( ( ruleExpression ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6867:1: ( ruleExpression )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6867:1: ( ruleExpression )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6868:1: ruleExpression
            {
             before(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Function__ParamsAssignment_3_113728);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getParamsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParamsAssignment_3_1"


    // $ANTLR start "rule__Identifier__IdAssignment"
    // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6877:1: rule__Identifier__IdAssignment : ( RULE_ID ) ;
    public final void rule__Identifier__IdAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6881:1: ( ( RULE_ID ) )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6882:1: ( RULE_ID )
            {
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6882:1: ( RULE_ID )
            // ../br.ufsm.aql.ui/src-gen/br/ufsm/ui/contentassist/antlr/internal/InternalAQL.g:6883:1: RULE_ID
            {
             before(grammarAccess.getIdentifierAccess().getIdIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Identifier__IdAssignment13759); 
             after(grammarAccess.getIdentifierAccess().getIdIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Identifier__IdAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleQuery_in_entryRuleQuery66 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuery73 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Query__QueryAssignment_in_ruleQuery103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQueryStatement_in_entryRuleQueryStatement130 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQueryStatement137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__0_in_ruleQueryStatement163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFindClause_in_entryRuleFindClause190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFindClause197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group__0_in_ruleFindClause223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBindingObject_in_entryRuleBindingObject250 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBindingObject257 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BindingObject__Group__0_in_ruleBindingObject283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectType_in_entryRuleObjectType310 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleObjectType317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Alternatives_in_ruleObjectType343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_entryRuleWhereClause370 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhereClause377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__0_in_ruleWhereClause403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReturnsClause_in_entryRuleReturnsClause430 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReturnsClause437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__0_in_ruleReturnsClause463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGroupByClause_in_entryRuleGroupByClause490 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGroupByClause497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__0_in_ruleGroupByClause523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderByClause_in_entryRuleOrderByClause550 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrderByClause557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__0_in_ruleOrderByClause583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_entryRuleSimpleQualifiedName610 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleQualifiedName617 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group__0_in_ruleSimpleQualifiedName643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_entryRuleSimpleQualified670 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleQualified677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_ruleSimpleQualified703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression734 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_ruleExpression771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr797 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0_in_ruleOr830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd857 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0_in_ruleAnd890 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_entryRuleRelation917 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelation924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__0_in_ruleRelation950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdd_in_entryRuleAdd977 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAdd984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group__0_in_ruleAdd1010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMult_in_entryRuleMult1037 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMult1044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group__0_in_ruleMult1070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIn_in_entryRuleIn1097 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIn1104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group__0_in_ruleIn1130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_entryRuleUnary1157 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnary1164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Alternatives_in_ruleUnary1190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExponential_in_entryRuleExponential1217 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExponential1224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group__0_in_ruleExponential1250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtom_in_entryRuleAtom1277 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtom1284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Atom__Alternatives_in_ruleAtom1310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParsExpression_in_entryRuleParsExpression1337 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParsExpression1344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__0_in_ruleParsExpression1370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteral_in_entryRuleLiteral1397 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLiteral1404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Alternatives_in_ruleLiteral1430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName1457 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName1464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0_in_ruleQualifiedName1490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified_in_entryRuleQualified1517 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified1524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedElement_in_ruleQualified1550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedElement_in_entryRuleQualifiedElement1576 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedElement1583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedElement__Alternatives_in_ruleQualifiedElement1609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_entryRuleFunction1636 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFunction1643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__0_in_ruleFunction1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_entryRuleIdentifier1696 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIdentifier1703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Identifier__IdAssignment_in_ruleIdentifier1729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_0__0_in_rule__ObjectType__Alternatives1765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_1__0_in_rule__ObjectType__Alternatives1783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_2__0_in_rule__ObjectType__Alternatives1801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_3__0_in_rule__ObjectType__Alternatives1819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_4__0_in_rule__ObjectType__Alternatives1837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_5__0_in_rule__ObjectType__Alternatives1855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__OrderByClause__OptionAlternatives_3_01889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__OrderByClause__OptionAlternatives_3_01909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_0__0_in_rule__Relation__Alternatives_1_01943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_1__0_in_rule__Relation__Alternatives_1_01961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_2__0_in_rule__Relation__Alternatives_1_01979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_3__0_in_rule__Relation__Alternatives_1_01997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_4__0_in_rule__Relation__Alternatives_1_02015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_5__0_in_rule__Relation__Alternatives_1_02033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_6__0_in_rule__Relation__Alternatives_1_02051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_0__0_in_rule__Add__Alternatives_1_02084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_1__0_in_rule__Add__Alternatives_1_02102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_0__0_in_rule__Mult__Alternatives_1_02135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_1__0_in_rule__Mult__Alternatives_1_02153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_2__0_in_rule__Mult__Alternatives_1_02171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExponential_in_rule__Unary__Alternatives2204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__0_in_rule__Unary__Alternatives2221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__0_in_rule__Unary__Alternatives2239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLiteral_in_rule__Atom__Alternatives2272 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_rule__Atom__Alternatives2289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParsExpression_in_rule__Atom__Alternatives2306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_0__0_in_rule__Literal__Alternatives2338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_1__0_in_rule__Literal__Alternatives2356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_2__0_in_rule__Literal__Alternatives2374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_3__0_in_rule__Literal__Alternatives2392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_4__0_in_rule__Literal__Alternatives2410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_5__0_in_rule__Literal__Alternatives2428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__QualifiedName__GranularAlternatives_0_02462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__QualifiedName__GranularAlternatives_0_02482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdentifier_in_rule__QualifiedElement__Alternatives2516 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFunction_in_rule__QualifiedElement__Alternatives2533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__0__Impl_in_rule__QueryStatement__Group__02563 = new BitSet(new long[]{0x0000C00000000000L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__1_in_rule__QueryStatement__Group__02566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__FindAssignment_0_in_rule__QueryStatement__Group__0__Impl2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__1__Impl_in_rule__QueryStatement__Group__12623 = new BitSet(new long[]{0x0000C00000000000L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__2_in_rule__QueryStatement__Group__12626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__WhereAssignment_1_in_rule__QueryStatement__Group__1__Impl2653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__2__Impl_in_rule__QueryStatement__Group__22684 = new BitSet(new long[]{0x0003000000000000L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__3_in_rule__QueryStatement__Group__22687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__ReturnAssignment_2_in_rule__QueryStatement__Group__2__Impl2714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__3__Impl_in_rule__QueryStatement__Group__32744 = new BitSet(new long[]{0x0003000000000000L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__4_in_rule__QueryStatement__Group__32747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__OrderbyAssignment_3_in_rule__QueryStatement__Group__3__Impl2774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__Group__4__Impl_in_rule__QueryStatement__Group__42805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QueryStatement__GroupbyAssignment_4_in_rule__QueryStatement__Group__4__Impl2832 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group__0__Impl_in_rule__FindClause__Group__02873 = new BitSet(new long[]{0x00003F0000000000L});
    public static final BitSet FOLLOW_rule__FindClause__Group__1_in_rule__FindClause__Group__02876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__ClauseAssignment_0_in_rule__FindClause__Group__0__Impl2903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group__1__Impl_in_rule__FindClause__Group__12933 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__FindClause__Group__2_in_rule__FindClause__Group__12936 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__BindingObjectAssignment_1_in_rule__FindClause__Group__1__Impl2963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group__2__Impl_in_rule__FindClause__Group__22993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group_2__0_in_rule__FindClause__Group__2__Impl3020 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__FindClause__Group_2__0__Impl_in_rule__FindClause__Group_2__03057 = new BitSet(new long[]{0x00003F0000000000L});
    public static final BitSet FOLLOW_rule__FindClause__Group_2__1_in_rule__FindClause__Group_2__03060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__FindClause__Group_2__0__Impl3088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__Group_2__1__Impl_in_rule__FindClause__Group_2__13119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FindClause__BindingObjectAssignment_2_1_in_rule__FindClause__Group_2__1__Impl3146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BindingObject__Group__0__Impl_in_rule__BindingObject__Group__03180 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__BindingObject__Group__1_in_rule__BindingObject__Group__03183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BindingObject__TypeAssignment_0_in_rule__BindingObject__Group__0__Impl3210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BindingObject__Group__1__Impl_in_rule__BindingObject__Group__13240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BindingObject__AliasAssignment_1_in_rule__BindingObject__Group__1__Impl3269 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__BindingObject__AliasAssignment_1_in_rule__BindingObject__Group__1__Impl3281 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_0__0__Impl_in_rule__ObjectType__Group_0__03318 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_0__1_in_rule__ObjectType__Group_0__03321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_0__1__Impl_in_rule__ObjectType__Group_0__13379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_0_1_in_rule__ObjectType__Group_0__1__Impl3406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_1__0__Impl_in_rule__ObjectType__Group_1__03440 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_1__1_in_rule__ObjectType__Group_1__03443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_1__1__Impl_in_rule__ObjectType__Group_1__13501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_1_1_in_rule__ObjectType__Group_1__1__Impl3528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_2__0__Impl_in_rule__ObjectType__Group_2__03562 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_2__1_in_rule__ObjectType__Group_2__03565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_2__1__Impl_in_rule__ObjectType__Group_2__13623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_2_1_in_rule__ObjectType__Group_2__1__Impl3650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_3__0__Impl_in_rule__ObjectType__Group_3__03684 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_3__1_in_rule__ObjectType__Group_3__03687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_3__1__Impl_in_rule__ObjectType__Group_3__13745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_3_1_in_rule__ObjectType__Group_3__1__Impl3772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_4__0__Impl_in_rule__ObjectType__Group_4__03806 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_4__1_in_rule__ObjectType__Group_4__03809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_4__1__Impl_in_rule__ObjectType__Group_4__13867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_4_1_in_rule__ObjectType__Group_4__1__Impl3894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_5__0__Impl_in_rule__ObjectType__Group_5__03928 = new BitSet(new long[]{0x00003F0000000000L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_5__1_in_rule__ObjectType__Group_5__03931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__Group_5__1__Impl_in_rule__ObjectType__Group_5__13989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectType__ValueAssignment_5_1_in_rule__ObjectType__Group_5__1__Impl4016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__0__Impl_in_rule__WhereClause__Group__04050 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__1_in_rule__WhereClause__Group__04053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__ClauseAssignment_0_in_rule__WhereClause__Group__0__Impl4080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__Group__1__Impl_in_rule__WhereClause__Group__14110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhereClause__ExpressionAssignment_1_in_rule__WhereClause__Group__1__Impl4137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__0__Impl_in_rule__ReturnsClause__Group__04171 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__1_in_rule__ReturnsClause__Group__04174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__ClauseAssignment_0_in_rule__ReturnsClause__Group__0__Impl4201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__1__Impl_in_rule__ReturnsClause__Group__14231 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__2_in_rule__ReturnsClause__Group__14234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__ExpressionsAssignment_1_in_rule__ReturnsClause__Group__1__Impl4261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__2__Impl_in_rule__ReturnsClause__Group__24291 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__3_in_rule__ReturnsClause__Group__24294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_2__0_in_rule__ReturnsClause__Group__2__Impl4321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group__3__Impl_in_rule__ReturnsClause__Group__34352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__0_in_rule__ReturnsClause__Group__3__Impl4379 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_2__0__Impl_in_rule__ReturnsClause__Group_2__04418 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_2__1_in_rule__ReturnsClause__Group_2__04421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__ReturnsClause__Group_2__0__Impl4449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_2__1__Impl_in_rule__ReturnsClause__Group_2__14480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__ResultAliasAssignment_2_1_in_rule__ReturnsClause__Group_2__1__Impl4507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__0__Impl_in_rule__ReturnsClause__Group_3__04541 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__1_in_rule__ReturnsClause__Group_3__04544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__ReturnsClause__Group_3__0__Impl4572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__1__Impl_in_rule__ReturnsClause__Group_3__14603 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__2_in_rule__ReturnsClause__Group_3__14606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__ExpressionsAssignment_3_1_in_rule__ReturnsClause__Group_3__1__Impl4633 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3__2__Impl_in_rule__ReturnsClause__Group_3__24663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3_2__0_in_rule__ReturnsClause__Group_3__2__Impl4690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3_2__0__Impl_in_rule__ReturnsClause__Group_3_2__04727 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3_2__1_in_rule__ReturnsClause__Group_3_2__04730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__ReturnsClause__Group_3_2__0__Impl4758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__Group_3_2__1__Impl_in_rule__ReturnsClause__Group_3_2__14789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReturnsClause__ResultAliasAssignment_3_2_1_in_rule__ReturnsClause__Group_3_2__1__Impl4816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__0__Impl_in_rule__GroupByClause__Group__04850 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__1_in_rule__GroupByClause__Group__04853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__ClauseAssignment_0_in_rule__GroupByClause__Group__0__Impl4880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__1__Impl_in_rule__GroupByClause__Group__14910 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__2_in_rule__GroupByClause__Group__14913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__ExpressionsAssignment_1_in_rule__GroupByClause__Group__1__Impl4940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group__2__Impl_in_rule__GroupByClause__Group__24970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group_2__0_in_rule__GroupByClause__Group__2__Impl4997 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group_2__0__Impl_in_rule__GroupByClause__Group_2__05034 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group_2__1_in_rule__GroupByClause__Group_2__05037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__GroupByClause__Group_2__0__Impl5065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__Group_2__1__Impl_in_rule__GroupByClause__Group_2__15096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GroupByClause__ExpressionsAssignment_2_1_in_rule__GroupByClause__Group_2__1__Impl5123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__0__Impl_in_rule__OrderByClause__Group__05157 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__1_in_rule__OrderByClause__Group__05160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__ClauseAssignment_0_in_rule__OrderByClause__Group__0__Impl5187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__1__Impl_in_rule__OrderByClause__Group__15217 = new BitSet(new long[]{0x0000000000053000L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__2_in_rule__OrderByClause__Group__15220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__ExpressionsAssignment_1_in_rule__OrderByClause__Group__1__Impl5247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__2__Impl_in_rule__OrderByClause__Group__25277 = new BitSet(new long[]{0x0000000000053000L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__3_in_rule__OrderByClause__Group__25280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_2__0_in_rule__OrderByClause__Group__2__Impl5307 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__3__Impl_in_rule__OrderByClause__Group__35338 = new BitSet(new long[]{0x0000000000053000L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__4_in_rule__OrderByClause__Group__35341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__OptionAssignment_3_in_rule__OrderByClause__Group__3__Impl5368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group__4__Impl_in_rule__OrderByClause__Group__45399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_4__0_in_rule__OrderByClause__Group__4__Impl5426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_2__0__Impl_in_rule__OrderByClause__Group_2__05467 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_2__1_in_rule__OrderByClause__Group_2__05470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__OrderByClause__Group_2__0__Impl5498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_2__1__Impl_in_rule__OrderByClause__Group_2__15529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__ExpressionsAssignment_2_1_in_rule__OrderByClause__Group_2__1__Impl5556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_4__0__Impl_in_rule__OrderByClause__Group_4__05590 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_4__1_in_rule__OrderByClause__Group_4__05593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__OrderByClause__Group_4__0__Impl5621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__Group_4__1__Impl_in_rule__OrderByClause__Group_4__15652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__HavingExpressionAssignment_4_1_in_rule__OrderByClause__Group_4__1__Impl5679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group__0__Impl_in_rule__SimpleQualifiedName__Group__05713 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group__1_in_rule__SimpleQualifiedName__Group__05716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__QualifiersAssignment_0_in_rule__SimpleQualifiedName__Group__0__Impl5743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group__1__Impl_in_rule__SimpleQualifiedName__Group__15773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group_1__0_in_rule__SimpleQualifiedName__Group__1__Impl5800 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group_1__0__Impl_in_rule__SimpleQualifiedName__Group_1__05835 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group_1__1_in_rule__SimpleQualifiedName__Group_1__05838 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__SimpleQualifiedName__Group_1__0__Impl5866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__Group_1__1__Impl_in_rule__SimpleQualifiedName__Group_1__15897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SimpleQualifiedName__QualifiersAssignment_1_1_in_rule__SimpleQualifiedName__Group_1__1__Impl5924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__05958 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Or__Group__1_in_rule__Or__Group__05961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_rule__Or__Group__0__Impl5988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__16017 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__0_in_rule__Or__Group__1__Impl6044 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__0__Impl_in_rule__Or__Group_1__06079 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Or__Group_1__1_in_rule__Or__Group_1__06082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1_0__0_in_rule__Or__Group_1__0__Impl6109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1__1__Impl_in_rule__Or__Group_1__16139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__RightAssignment_1_1_in_rule__Or__Group_1__1__Impl6166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1_0__0__Impl_in_rule__Or__Group_1_0__06200 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Or__Group_1_0__1_in_rule__Or__Group_1_0__06203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Or__Group_1_0__0__Impl6231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group_1_0__1__Impl_in_rule__Or__Group_1_0__16262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__06324 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__And__Group__1_in_rule__And__Group__06327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_rule__And__Group__0__Impl6354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__16383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__0_in_rule__And__Group__1__Impl6410 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_rule__And__Group_1__0__Impl_in_rule__And__Group_1__06445 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__And__Group_1__1_in_rule__And__Group_1__06448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__And__Group_1__0__Impl6476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__1__Impl_in_rule__And__Group_1__16507 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__And__Group_1__2_in_rule__And__Group_1__16510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group_1__2__Impl_in_rule__And__Group_1__26568 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__RightAssignment_1_2_in_rule__And__Group_1__2__Impl6595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__0__Impl_in_rule__Relation__Group__06631 = new BitSet(new long[]{0x000000001FC00000L});
    public static final BitSet FOLLOW_rule__Relation__Group__1_in_rule__Relation__Group__06634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdd_in_rule__Relation__Group__0__Impl6661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group__1__Impl_in_rule__Relation__Group__16690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__0_in_rule__Relation__Group__1__Impl6717 = new BitSet(new long[]{0x000000001FC00002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__0__Impl_in_rule__Relation__Group_1__06752 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__1_in_rule__Relation__Group_1__06755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Alternatives_1_0_in_rule__Relation__Group_1__0__Impl6782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1__1__Impl_in_rule__Relation__Group_1__16812 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__RightAssignment_1_1_in_rule__Relation__Group_1__1__Impl6839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_0__0__Impl_in_rule__Relation__Group_1_0_0__06873 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_0__1_in_rule__Relation__Group_1_0_0__06876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Relation__Group_1_0_0__0__Impl6904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_0__1__Impl_in_rule__Relation__Group_1_0_0__16935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_1__0__Impl_in_rule__Relation__Group_1_0_1__06997 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_1__1_in_rule__Relation__Group_1_0_1__07000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Relation__Group_1_0_1__0__Impl7028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_1__1__Impl_in_rule__Relation__Group_1_0_1__17059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_2__0__Impl_in_rule__Relation__Group_1_0_2__07121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_2__1_in_rule__Relation__Group_1_0_2__07124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Relation__Group_1_0_2__0__Impl7152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_2__1__Impl_in_rule__Relation__Group_1_0_2__17183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_3__0__Impl_in_rule__Relation__Group_1_0_3__07245 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_3__1_in_rule__Relation__Group_1_0_3__07248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Relation__Group_1_0_3__0__Impl7276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_3__1__Impl_in_rule__Relation__Group_1_0_3__17307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_4__0__Impl_in_rule__Relation__Group_1_0_4__07369 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_4__1_in_rule__Relation__Group_1_0_4__07372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Relation__Group_1_0_4__0__Impl7400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_4__1__Impl_in_rule__Relation__Group_1_0_4__17431 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_5__0__Impl_in_rule__Relation__Group_1_0_5__07493 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_5__1_in_rule__Relation__Group_1_0_5__07496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Relation__Group_1_0_5__0__Impl7524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_5__1__Impl_in_rule__Relation__Group_1_0_5__17555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_6__0__Impl_in_rule__Relation__Group_1_0_6__07617 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_6__1_in_rule__Relation__Group_1_0_6__07620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Relation__Group_1_0_6__0__Impl7648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Relation__Group_1_0_6__1__Impl_in_rule__Relation__Group_1_0_6__17679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group__0__Impl_in_rule__Add__Group__07741 = new BitSet(new long[]{0x0000000060000000L});
    public static final BitSet FOLLOW_rule__Add__Group__1_in_rule__Add__Group__07744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMult_in_rule__Add__Group__0__Impl7771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group__1__Impl_in_rule__Add__Group__17800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1__0_in_rule__Add__Group__1__Impl7827 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1__0__Impl_in_rule__Add__Group_1__07862 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Add__Group_1__1_in_rule__Add__Group_1__07865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Alternatives_1_0_in_rule__Add__Group_1__0__Impl7892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1__1__Impl_in_rule__Add__Group_1__17922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__RightAssignment_1_1_in_rule__Add__Group_1__1__Impl7949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_0__0__Impl_in_rule__Add__Group_1_0_0__07983 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_0__1_in_rule__Add__Group_1_0_0__07986 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Add__Group_1_0_0__0__Impl8014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_0__1__Impl_in_rule__Add__Group_1_0_0__18045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_1__0__Impl_in_rule__Add__Group_1_0_1__08107 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_1__1_in_rule__Add__Group_1_0_1__08110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Add__Group_1_0_1__0__Impl8138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Add__Group_1_0_1__1__Impl_in_rule__Add__Group_1_0_1__18169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group__0__Impl_in_rule__Mult__Group__08231 = new BitSet(new long[]{0x0000000380000000L});
    public static final BitSet FOLLOW_rule__Mult__Group__1_in_rule__Mult__Group__08234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIn_in_rule__Mult__Group__0__Impl8261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group__1__Impl_in_rule__Mult__Group__18290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1__0_in_rule__Mult__Group__1__Impl8317 = new BitSet(new long[]{0x0000000380000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1__0__Impl_in_rule__Mult__Group_1__08352 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Mult__Group_1__1_in_rule__Mult__Group_1__08355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Alternatives_1_0_in_rule__Mult__Group_1__0__Impl8382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1__1__Impl_in_rule__Mult__Group_1__18412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__RightAssignment_1_1_in_rule__Mult__Group_1__1__Impl8439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_0__0__Impl_in_rule__Mult__Group_1_0_0__08473 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_0__1_in_rule__Mult__Group_1_0_0__08476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Mult__Group_1_0_0__0__Impl8504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_0__1__Impl_in_rule__Mult__Group_1_0_0__18535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_1__0__Impl_in_rule__Mult__Group_1_0_1__08597 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_1__1_in_rule__Mult__Group_1_0_1__08600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Mult__Group_1_0_1__0__Impl8628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_1__1__Impl_in_rule__Mult__Group_1_0_1__18659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_2__0__Impl_in_rule__Mult__Group_1_0_2__08721 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_2__1_in_rule__Mult__Group_1_0_2__08724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__Mult__Group_1_0_2__0__Impl8752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mult__Group_1_0_2__1__Impl_in_rule__Mult__Group_1_0_2__18783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group__0__Impl_in_rule__In__Group__08845 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__In__Group__1_in_rule__In__Group__08848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_rule__In__Group__0__Impl8875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group__1__Impl_in_rule__In__Group__18904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group_1__0_in_rule__In__Group__1__Impl8931 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_rule__In__Group_1__0__Impl_in_rule__In__Group_1__08966 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__In__Group_1__1_in_rule__In__Group_1__08969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group_1_0__0_in_rule__In__Group_1__0__Impl8996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group_1__1__Impl_in_rule__In__Group_1__19026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__RightAssignment_1_1_in_rule__In__Group_1__1__Impl9053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group_1_0__0__Impl_in_rule__In__Group_1_0__09087 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__In__Group_1_0__1_in_rule__In__Group_1_0__09090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__In__Group_1_0__0__Impl9118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__In__Group_1_0__1__Impl_in_rule__In__Group_1_0__19149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__0__Impl_in_rule__Unary__Group_1__09211 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__1_in_rule__Unary__Group_1__09214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__Unary__Group_1__0__Impl9242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__1__Impl_in_rule__Unary__Group_1__19273 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__2_in_rule__Unary__Group_1__19276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_1__2__Impl_in_rule__Unary__Group_1__29334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__ExpAssignment_1_2_in_rule__Unary__Group_1__2__Impl9361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__0__Impl_in_rule__Unary__Group_2__09397 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__1_in_rule__Unary__Group_2__09400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Unary__Group_2__0__Impl9428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__1__Impl_in_rule__Unary__Group_2__19459 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__2_in_rule__Unary__Group_2__19462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__Group_2__2__Impl_in_rule__Unary__Group_2__29520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Unary__ExprAssignment_2_2_in_rule__Unary__Group_2__2__Impl9547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group__0__Impl_in_rule__Exponential__Group__09583 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_rule__Exponential__Group__1_in_rule__Exponential__Group__09586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtom_in_rule__Exponential__Group__0__Impl9613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group__1__Impl_in_rule__Exponential__Group__19642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__0_in_rule__Exponential__Group__1__Impl9669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__0__Impl_in_rule__Exponential__Group_1__09704 = new BitSet(new long[]{0x001C00200000C0F0L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__1_in_rule__Exponential__Group_1__09707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__Exponential__Group_1__0__Impl9735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__1__Impl_in_rule__Exponential__Group_1__19766 = new BitSet(new long[]{0x001C00200000C0F0L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__2_in_rule__Exponential__Group_1__19769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__Group_1__2__Impl_in_rule__Exponential__Group_1__29827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Exponential__RightAssignment_1_2_in_rule__Exponential__Group_1__2__Impl9854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__0__Impl_in_rule__ParsExpression__Group__09890 = new BitSet(new long[]{0x001C00200000C0F0L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__1_in_rule__ParsExpression__Group__09893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__1__Impl_in_rule__ParsExpression__Group__19951 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__2_in_rule__ParsExpression__Group__19954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__ParsExpression__Group__1__Impl9982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__2__Impl_in_rule__ParsExpression__Group__210013 = new BitSet(new long[]{0x0000004000010000L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__3_in_rule__ParsExpression__Group__210016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__ExprAssignment_2_in_rule__ParsExpression__Group__2__Impl10043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__3__Impl_in_rule__ParsExpression__Group__310073 = new BitSet(new long[]{0x0000004000010000L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__4_in_rule__ParsExpression__Group__310076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group_3__0_in_rule__ParsExpression__Group__3__Impl10103 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group__4__Impl_in_rule__ParsExpression__Group__410134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__ParsExpression__Group__4__Impl10162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group_3__0__Impl_in_rule__ParsExpression__Group_3__010203 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group_3__1_in_rule__ParsExpression__Group_3__010206 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__ParsExpression__Group_3__0__Impl10234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__Group_3__1__Impl_in_rule__ParsExpression__Group_3__110265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ParsExpression__ExprAssignment_3_1_in_rule__ParsExpression__Group_3__1__Impl10292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_0__0__Impl_in_rule__Literal__Group_0__010326 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Literal__Group_0__1_in_rule__Literal__Group_0__010329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_0__1__Impl_in_rule__Literal__Group_0__110387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_0_1_in_rule__Literal__Group_0__1__Impl10414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_1__0__Impl_in_rule__Literal__Group_1__010448 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Literal__Group_1__1_in_rule__Literal__Group_1__010451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_1__1__Impl_in_rule__Literal__Group_1__110509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_1_1_in_rule__Literal__Group_1__1__Impl10536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_2__0__Impl_in_rule__Literal__Group_2__010570 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_rule__Literal__Group_2__1_in_rule__Literal__Group_2__010573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_2__1__Impl_in_rule__Literal__Group_2__110631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_2_1_in_rule__Literal__Group_2__1__Impl10658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_3__0__Impl_in_rule__Literal__Group_3__010692 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_rule__Literal__Group_3__1_in_rule__Literal__Group_3__010695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_3__1__Impl_in_rule__Literal__Group_3__110753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_3_1_in_rule__Literal__Group_3__1__Impl10780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_4__0__Impl_in_rule__Literal__Group_4__010814 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_rule__Literal__Group_4__1_in_rule__Literal__Group_4__010817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_4__1__Impl_in_rule__Literal__Group_4__110875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_4_1_in_rule__Literal__Group_4__1__Impl10902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_5__0__Impl_in_rule__Literal__Group_5__010936 = new BitSet(new long[]{0x001C0000000000E0L});
    public static final BitSet FOLLOW_rule__Literal__Group_5__1_in_rule__Literal__Group_5__010939 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__Group_5__1__Impl_in_rule__Literal__Group_5__110997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Literal__ValueAssignment_5_1_in_rule__Literal__Group_5__1__Impl11024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__0__Impl_in_rule__QualifiedName__Group__011058 = new BitSet(new long[]{0x000000000000C010L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1_in_rule__QualifiedName__Group__011061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__GranularAssignment_0_in_rule__QualifiedName__Group__0__Impl11088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__1__Impl_in_rule__QualifiedName__Group__111119 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__2_in_rule__QualifiedName__Group__111122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__QualifiersAssignment_1_in_rule__QualifiedName__Group__1__Impl11149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group__2__Impl_in_rule__QualifiedName__Group__211179 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_2__0_in_rule__QualifiedName__Group__2__Impl11206 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_2__0__Impl_in_rule__QualifiedName__Group_2__011243 = new BitSet(new long[]{0x000000000000C010L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_2__1_in_rule__QualifiedName__Group_2__011246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__QualifiedName__Group_2__0__Impl11274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__Group_2__1__Impl_in_rule__QualifiedName__Group_2__111305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__QualifiersAssignment_2_1_in_rule__QualifiedName__Group_2__1__Impl11332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__0__Impl_in_rule__Function__Group__011366 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_rule__Function__Group__1_in_rule__Function__Group__011369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__NameAssignment_0_in_rule__Function__Group__0__Impl11396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__1__Impl_in_rule__Function__Group__111426 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Function__Group__2_in_rule__Function__Group__111429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Function__Group__1__Impl11457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__2__Impl_in_rule__Function__Group__211488 = new BitSet(new long[]{0x0000004000010000L});
    public static final BitSet FOLLOW_rule__Function__Group__3_in_rule__Function__Group__211491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__ParamsAssignment_2_in_rule__Function__Group__2__Impl11518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group__3__Impl_in_rule__Function__Group__311548 = new BitSet(new long[]{0x0000004000010000L});
    public static final BitSet FOLLOW_rule__Function__Group__4_in_rule__Function__Group__311551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group_3__0_in_rule__Function__Group__3__Impl11578 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Function__Group__4__Impl_in_rule__Function__Group__411609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__Function__Group__4__Impl11637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group_3__0__Impl_in_rule__Function__Group_3__011678 = new BitSet(new long[]{0x001C00284000C0F0L});
    public static final BitSet FOLLOW_rule__Function__Group_3__1_in_rule__Function__Group_3__011681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Function__Group_3__0__Impl11709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__Group_3__1__Impl_in_rule__Function__Group_3__111740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Function__ParamsAssignment_3_1_in_rule__Function__Group_3__1__Impl11767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQueryStatement_in_rule__Query__QueryAssignment11806 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFindClause_in_rule__QueryStatement__FindAssignment_011837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhereClause_in_rule__QueryStatement__WhereAssignment_111868 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReturnsClause_in_rule__QueryStatement__ReturnAssignment_211899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrderByClause_in_rule__QueryStatement__OrderbyAssignment_311930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGroupByClause_in_rule__QueryStatement__GroupbyAssignment_411961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__FindClause__ClauseAssignment_011997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBindingObject_in_rule__FindClause__BindingObjectAssignment_112036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBindingObject_in_rule__FindClause__BindingObjectAssignment_2_112067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectType_in_rule__BindingObject__TypeAssignment_012098 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BindingObject__AliasAssignment_112129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__ObjectType__ValueAssignment_0_112165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__ObjectType__ValueAssignment_1_112209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__ObjectType__ValueAssignment_2_112253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__ObjectType__ValueAssignment_3_112297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__ObjectType__ValueAssignment_4_112341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rule__ObjectType__ValueAssignment_5_112385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_rule__WhereClause__ClauseAssignment_012429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__WhereClause__ExpressionAssignment_112468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule__ReturnsClause__ClauseAssignment_012504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__ReturnsClause__ExpressionsAssignment_112543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ReturnsClause__ResultAliasAssignment_2_112574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__ReturnsClause__ExpressionsAssignment_3_112605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ReturnsClause__ResultAliasAssignment_3_2_112636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__GroupByClause__ClauseAssignment_012672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_rule__GroupByClause__ExpressionsAssignment_112711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_rule__GroupByClause__ExpressionsAssignment_2_112742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rule__OrderByClause__ClauseAssignment_012778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_rule__OrderByClause__ExpressionsAssignment_112817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualifiedName_in_rule__OrderByClause__ExpressionsAssignment_2_112848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OrderByClause__OptionAlternatives_3_0_in_rule__OrderByClause__OptionAssignment_312879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__OrderByClause__HavingExpressionAssignment_4_112912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_rule__SimpleQualifiedName__QualifiersAssignment_012943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleQualified_in_rule__SimpleQualifiedName__QualifiersAssignment_1_112974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_rule__Or__RightAssignment_1_113005 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_rule__And__RightAssignment_1_213036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAdd_in_rule__Relation__RightAssignment_1_113067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMult_in_rule__Add__RightAssignment_1_113098 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIn_in_rule__Mult__RightAssignment_1_113129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_rule__In__RightAssignment_1_113160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_rule__Unary__ExpAssignment_1_213191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnary_in_rule__Unary__ExprAssignment_2_213222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExponential_in_rule__Exponential__RightAssignment_1_213253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__ParsExpression__ExprAssignment_213284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__ParsExpression__ExprAssignment_3_113315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Literal__ValueAssignment_0_113346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_FLOAT_in_rule__Literal__ValueAssignment_1_113377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Literal__ValueAssignment_2_113408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rule__Literal__ValueAssignment_3_113444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_rule__Literal__ValueAssignment_4_113488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_rule__Literal__ValueAssignment_5_113532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__QualifiedName__GranularAlternatives_0_0_in_rule__QualifiedName__GranularAssignment_013571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified_in_rule__QualifiedName__QualifiersAssignment_113604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified_in_rule__QualifiedName__QualifiersAssignment_2_113635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Function__NameAssignment_013666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Function__ParamsAssignment_213697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Function__ParamsAssignment_3_113728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Identifier__IdAssignment13759 = new BitSet(new long[]{0x0000000000000002L});

}