/**
 */
package hql.tests;

import hql.QualifiedElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Qualified Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class QualifiedElementTest extends ExpressionTest {

	/**
	 * Constructs a new Qualified Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifiedElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Qualified Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected QualifiedElement getFixture() {
		return (QualifiedElement)fixture;
	}

} //QualifiedElementTest
