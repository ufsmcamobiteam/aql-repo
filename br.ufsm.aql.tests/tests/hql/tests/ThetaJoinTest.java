/**
 */
package hql.tests;

import hql.HqlFactory;
import hql.ThetaJoin;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Theta Join</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThetaJoinTest extends BinaryFromExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ThetaJoinTest.class);
	}

	/**
	 * Constructs a new Theta Join test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThetaJoinTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Theta Join test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ThetaJoin getFixture() {
		return (ThetaJoin)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(HqlFactory.eINSTANCE.createThetaJoin());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ThetaJoinTest
