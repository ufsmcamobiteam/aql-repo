/**
 */
package hql.tests;

import hql.FromExpression;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>From Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FromExpressionTest extends TestCase {

	/**
	 * The fixture for this From Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromExpression fixture = null;

	/**
	 * Constructs a new From Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromExpressionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this From Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FromExpression fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this From Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromExpression getFixture() {
		return fixture;
	}

} //FromExpressionTest
