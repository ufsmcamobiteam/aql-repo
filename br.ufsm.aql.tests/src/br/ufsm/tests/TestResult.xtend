package br.ufsm.tests

import org.eclipse.xtend.lib.Property
import java.util.StringTokenizer

class TestResult {
	@Property private String testIn; 
	@Property private String testOut;
	@Property private long elapsedTimeTransformation;

	def int getNumCharIn() { 
		return testIn.length;
	}
	 
	def int getNumCharOut() { 
		return testOut.length;
	}
	
	def private String noiseLess (String str) {
		var result = str.trim()
		result = result.replace(". ", ".")	
		result = result.replace(",", " ")	
		result = result.replace("'", " ")	
		result = result.replace(";", " ")	
		result = result.replace("(", " ")	
		result = result.replace(")", " ")	
		return result
	} 
	
	def int getNumTermsIn() {
		val trimmed = testIn.noiseLess;
		if (trimmed.isEmpty) 
			return 0;
		return trimmed.count			
	}

	def private int count (String str) {
		var lexemCounter = 0;
		var tokens = new StringTokenizer(str);
		while (tokens.hasMoreElements)  {
			lexemCounter = lexemCounter + 1
			tokens.nextElement	
		}
		return lexemCounter	
	}

	def int getNumTermsOut() {
		val trimmed = testOut.noiseLess;
		if (trimmed.isEmpty) 
			return 0;
		return trimmed.count
	} 
}