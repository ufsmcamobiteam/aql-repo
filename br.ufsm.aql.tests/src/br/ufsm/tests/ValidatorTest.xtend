package br.ufsm.tests

import br.ufsm.AQLStandaloneSetup
import br.ufsm.aql.AqlFactory
import br.ufsm.aql.BindingObject
import br.ufsm.aql.FindClause
import br.ufsm.aql.GroupByClause
import br.ufsm.aql.Identifier
import br.ufsm.aql.ObjectType
import br.ufsm.aql.OrderByClause
import br.ufsm.aql.QualifiedName
import br.ufsm.aql.Query
import br.ufsm.aql.QueryStatement
import br.ufsm.aql.ReturnsClause
import br.ufsm.aql.WhereClause
import br.ufsm.validation.AQLJavaValidator
import br.ufsm.validation.ErrorList
import org.eclipse.xtext.junit4.AbstractXtextTests
import org.eclipse.xtext.junit4.validation.ValidatorTester
import org.junit.Test

class ValidatorTest extends AbstractXtextTests {
	private ValidatorTester<AQLJavaValidator> tester;

	private Query query = AqlFactory::eINSTANCE.createQuery
	private QueryStatement queryStatement = AqlFactory::eINSTANCE.createQueryStatement
	private FindClause find = AqlFactory::eINSTANCE.createFindClause
	private	ReturnsClause returns = AqlFactory::eINSTANCE.createReturnsClause
	private	WhereClause where = AqlFactory::eINSTANCE.createWhereClause
	private	OrderByClause orderby = AqlFactory::eINSTANCE.createOrderByClause
	private	GroupByClause groupby = AqlFactory::eINSTANCE.createGroupByClause
	
	override public void setUp() {
		super.setUp();
		with(typeof(AQLStandaloneSetup));
		var AQLJavaValidator validator = get(typeof(AQLJavaValidator));
		tester = new ValidatorTester<AQLJavaValidator>(validator, getInjector);
	}
	
	@Test
	def public void test1() {
		var QualifiedName returnsExpression = AqlFactory::eINSTANCE.createQualifiedName
		var Identifier returnsIdentifier = AqlFactory::eINSTANCE.createIdentifier
		var BindingObject bindObject = AqlFactory::eINSTANCE.createBindingObject
		var ObjectType objectType = AqlFactory::eINSTANCE.createObjectType
		query.setQuery(queryStatement)
		queryStatement.setFind(find)
		queryStatement.setReturn(returns)
		bindObject.setType(objectType)
		find.bindingObject.add(bindObject)
		returns.expressions.add(returnsExpression)
		find.setClause("find")
		objectType.setValue("aspect")
		bindObject.alias.add("a")
		bindObject.alias.add("a")
		returnsIdentifier.setId("a")
		returnsExpression.qualifiers.add(returnsIdentifier)
		tester.validator.checkDuplicatesBindingAlias(find);
		tester.diagnose().assertErrorContains(ErrorList::DUPLICATE_BINDING.getDescription());
	}
	
	@Test
	def public void test2() {
		var QualifiedName returnsExpression = AqlFactory::eINSTANCE.createQualifiedName
		var Identifier returnsIdentifier = AqlFactory::eINSTANCE.createIdentifier
		var BindingObject bindObject = AqlFactory::eINSTANCE.createBindingObject
		var ObjectType objectType = AqlFactory::eINSTANCE.createObjectType
		query.setQuery(queryStatement)
		queryStatement.setFind(find)
		queryStatement.setReturn(returns)
		bindObject.setType(objectType)
		find.bindingObject.add(bindObject)
		returns.expressions.add(returnsExpression)
		find.setClause("find")
		objectType.setValue("aspect")
		bindObject.alias.add("a")
		bindObject.alias.add("b")
		bindObject.alias.add("a")
		returnsIdentifier.setId("a")
		returnsExpression.qualifiers.add(returnsIdentifier)
		tester.validator.checkDuplicatesBindingAlias(find);
		tester.diagnose().assertErrorContains(ErrorList::DUPLICATE_BINDING.getDescription());
	}
	
	@Test
	def public void test3() {
		var QualifiedName returnsExpression = AqlFactory::eINSTANCE.createQualifiedName
		var Identifier returnsIdentifier = AqlFactory::eINSTANCE.createIdentifier
		var BindingObject bindObject = AqlFactory::eINSTANCE.createBindingObject
		var ObjectType objectType = AqlFactory::eINSTANCE.createObjectType
		query.setQuery(queryStatement)
		queryStatement.setFind(find)
		queryStatement.setReturn(returns)
		bindObject.setType(objectType)
		find.bindingObject.add(bindObject)
		returns.expressions.add(returnsExpression)
		find.setClause("find")
		objectType.setValue("aspect")
		bindObject.alias.add("a")
		bindObject.alias.add("b")
		bindObject.alias.add("a")
		returnsIdentifier.setId("a")
		returnsExpression.qualifiers.add(returnsIdentifier)
		tester.validator.checkDuplicatesBindingAlias(find);
		tester.diagnose().assertErrorContains(ErrorList::DUPLICATE_BINDING.getDescription());
		
		
		var BindingObject bindObject1 = AqlFactory::eINSTANCE.createBindingObject
		var BindingObject bindObject2 = AqlFactory::eINSTANCE.createBindingObject
		var ObjectType objectType1 = AqlFactory::eINSTANCE.createObjectType
		var ObjectType objectType2 = AqlFactory::eINSTANCE.createObjectType
		objectType1.setValue("aspect")
		objectType2.setValue("aspect")
		bindObject1.alias.add("a")
		bindObject1.alias.add("b")
		bindObject2.alias.add("a")
		bindObject1.setType(objectType1)
		bindObject2.setType(objectType2)
		find.bindingObject.add(bindObject1)
		find.bindingObject.add(bindObject2)
		tester.validator.checkDuplicatesBindingAlias(find);
		tester.diagnose().assertError(ErrorList::DUPLICATE_BINDING.getCode());
	}		
	
	@Test
	def public void test4() {
		var FindClause find = AqlFactory::eINSTANCE.createFindClause
		var BindingObject bindObject1 = AqlFactory::eINSTANCE.createBindingObject
		var BindingObject bindObject2 = AqlFactory::eINSTANCE.createBindingObject
		var ObjectType objectType1 = AqlFactory::eINSTANCE.createObjectType
		var ObjectType objectType2 = AqlFactory::eINSTANCE.createObjectType
		objectType1.setValue("aspect")
		objectType2.setValue("class")
		bindObject1.alias.add("a")
		bindObject1.alias.add("b")
		bindObject2.alias.add("a")
		bindObject1.setType(objectType1)
		bindObject2.setType(objectType2)
		find.setClause("Find")
		find.bindingObject.add(bindObject1)
		find.bindingObject.add(bindObject2)
		tester.validator.checkDuplicatesBindingAlias(find);
		tester.diagnose().assertError(ErrorList::DUPLICATE_BINDING.getCode());
	}	
}