package br.ufsm.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.xtext.generator.IGenerator;
import org.junit.AfterClass;


public aspect TestProfile {
	private long start;
	private long end;
	private TestResult testCase;
	private InternalTestResult result = new InternalTestResult();
	 
	pointcut testCases() : call (* IGenerator.doGenerate(..));
	pointcut runTest (CharSequence in, CharSequence out) : 
			execution (* GeneratorTest.runTest(CharSequence, CharSequence))
		&&  args (in, out);	
	pointcut endOfTest() : execution (@AfterClass * GeneratorTest.*(..));
	
	before() : testCases() { 
		start = System.nanoTime();
	}
	
	after() : testCases() {
		end = System.nanoTime();
		long elapsedTime = end - start;
		if (null != testCase)
			testCase.setElapsedTimeTransformation(elapsedTime);
	}
	
	before (CharSequence in, CharSequence out) : runTest (in, out) {
		testCase = new TestResult();
		testCase.setTestIn(in.toString());
		testCase.setTestOut(out.toString());
		result.testMap.add(testCase);
	}
	
	after() : endOfTest() {
		result.calculate();
		result.print();
	}
}

class InternalTestResult {
	List<Integer> distribution = new ArrayList<Integer>(Arrays.asList(0,0,0,0,0,0,0,0,0,0));
	List<TestResult>testMap = new ArrayList<TestResult>();
	double averageSize;
	double averageTimeCompilation;
	int maxLexems = 0;
	int minLexems = 999999;
	
	public void calculate() {
		averageSize = 0.0;
		averageTimeCompilation = 0.0;
		for (TestResult testCase : testMap) {		
			averageTimeCompilation += testCase.getElapsedTimeTransformation()/1000000000f;
			double base = (double)testCase.getNumTermsIn() / (double)testCase.getNumTermsOut();
			averageSize += (1 - base) * 100;
			int index = getRange(testCase.getNumTermsIn(), testCase.getNumTermsOut());
			int current =  0;
			current = distribution.get(index);
			distribution.set(index, current+1);
			minLexems = minLexems <= testCase.getNumTermsIn() ? minLexems : testCase.getNumTermsIn();
			maxLexems = maxLexems > testCase.getNumTermsIn() ? maxLexems : testCase.getNumTermsIn();
		}
		averageTimeCompilation = averageTimeCompilation / testMap.size();
		averageSize = averageSize / testMap.size();
	}	
		
	private int getRange (int in, int out) {
		double perc[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110};
		double ind = in;
		double outd = out;
		double base = (1.0d - (ind / outd)) * 100.0d;
		int i = 0;
		for (i = 0; i <= perc.length; i++)
			if (base <= perc[i])
				return i;
		return perc.length;		
	}		

	public void print() {
		System.out.println("---- Final Report ----");
		System.out.println("Number of test cases : " + testMap.size());
		System.out.println("Max number of lexems : " + maxLexems);
		System.out.println("Min number of lexems : " + minLexems);
		System.out.println("Average time compilation : " + averageTimeCompilation);
		int i = 1;
		for (Integer d : distribution) 
			System.out.println( i++*10 + "% improved : " + d);
		System.out.println("Average size advantage : " + averageSize);
		
		printTestCases();
	}
	
	public void printTestCases() {
		System.out.println("ajql;chars;words;hql;chars;words;elapsdtimeTransformation");
		for (TestResult testCase : testMap) {
			System.out.println(
					String.format("%s;%s;%s;%s;%s;%s;%s", 
					testCase.getTestIn(), testCase.getNumCharIn(), 
					testCase.getNumTermsIn(), testCase.getTestOut(), testCase.getNumCharOut(),
					testCase.getNumTermsOut(), testCase.getElapsedTimeTransformation()/1000000000f));
		}	
	}
}