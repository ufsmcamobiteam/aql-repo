package br.ufsm.tests

import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import br.ufsm.AQLInjectorProvider
import br.ufsm.aql.resource.Setting
import org.junit.Test
import java.util.Map
import static org.junit.Assert.*

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AQLInjectorProvider))

class SettingsTest {
	@Test 
    def void test1() {
		val Setting settings = Setting::getINSTANCE("conf/")
		val Map<String, String> map = settings.resolverMap
		print (map.toString)
        assertTrue(map.size > 0)
        assertEquals("AOJAspectDeclaration", (map.get("ASPECT")))
    }
}