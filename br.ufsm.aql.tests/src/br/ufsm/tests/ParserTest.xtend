package br.ufsm.tests

import br.ufsm.AQLInjectorProvider
import com.google.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.AbstractXtextTests

@InjectWith(typeof(AQLInjectorProvider))
@RunWith(typeof(XtextRunner))

class ParserTest extends AbstractXtextTests {
  @Inject
  private ParseHelper<br.ufsm.aql.Query> parser;
   
	@Test
	def void parseQuery() {
		val model = parser.parse("find aspect a returns a")
//		val s = model.query.^return as br.ufsm.aql.ReturnsClause
		val f = model.query.find as br.ufsm.aql.FindClause
//		val w = model.query.where as br.ufsm.aql.WhereClause

		for (br.ufsm.aql.BindingObject o : f.bindingObject) {
			println(o.type.value)
			for (String a : o.alias)
				println(a)
		}

//		// Check Select
//		assertEquals("Select", s.clause)
//		val es = model.query.select.expressions.head as Expression
//		if (es instanceof QualifiedName)
//			assertEquals ( (es as QualifiedName).toString(), "a" )
//
//		// Check From
//		assertEquals("From", f.clause)
//		val ef = model.query.from.bindingObject.head as BindingObject
//		if (e instanceof QualifiedName)
//			assertEquals ( (e as QualifiedName).toString(), "a" )
//		
//
//		
//		println(selectClause)
//		println(query.from.bindingObject.head)
//		//println(query.where.clause)
//		
//		assertEquals("returns", selectClause)
//		assertTrue(query.select.expressions.head instanceof ObjectQualifier)
		
	}
}