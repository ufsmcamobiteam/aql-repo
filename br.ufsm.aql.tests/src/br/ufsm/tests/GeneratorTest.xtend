package br.ufsm.tests

import br.ufsm.AQLInjectorProvider 
import br.ufsm.aql.Query
import com.google.inject.Inject
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*
import org.junit.AfterClass
import org.junit.BeforeClass
import java.io.FileReader
import java.io.BufferedReader
import org.eclipse.xtend.lib.annotations.Accessors

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AQLInjectorProvider))
 
class GeneratorTest {
	@Inject IGenerator generator
    @Inject ParseHelper<Query> parseHelper
    @Accessors private CharSequence in;
    @Accessors private CharSequence expectedOut;
    @Accessors private CharSequence foundOut;
    private static String fileNameTest = 'conf/InputPositiveM2MTest.txt';
    private static BufferedReader reader; 
    
    private InMemoryFileSystemAccess fsa
	@AfterClass
	def static void release() {
		// Explicit JP for aspect processing.
	}

	@BeforeClass
	def static void start() {
		reader = new BufferedReader (new FileReader(fileNameTest));
	}

    @Before
    def void init() {
		fsa = new InMemoryFileSystemAccess()
    }
    
    def asQueryComparable (CharSequence charSequence) {
    	return charSequence.toString.toUpperCase.replace(' ', '')
    }
    
    def void runTest (CharSequence in, CharSequence expectedOut) {
		val queryIn = parseHelper.parse(in)	    	
        generator.doGenerate(queryIn.eResource, fsa)
        foundOut = fsa.files.get(IFileSystemAccess::DEFAULT_OUTPUT+"query.hql")
        assertEquals(expectedOut.asQueryComparable, foundOut.asQueryComparable)
    }
    
	@Test 
    def void PositiveTests() {
    	var line = reader.readLine;
    	while (line != null) { 
    		if (! line.startsWith("#") && (line.length > 0)) {
    			println(line);
    			in = line.substring(0, line.indexOf(";"))
    			expectedOut = line.substring(line.indexOf(";") + 1, line.length)
    			runTest (in, expectedOut)
    		}
    		line = reader.readLine;    		
    	} 
    }
    
	//@Test
    def void t1() { 
        in = '''find aspect a where a.modifier(public) returns a.name''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }    
    
	//@Test
    def void t2() { 
        in = '''find class c1, class c2 where c1.extends(c2) returns c1.fullQualifiedName, c2.fullQualifiedName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }    
    
	//@Test  
    def void t3() { 
        in = '''find class c, interface i where c.implements(i) returns c.fullQualifiedName, i.fullQualifiedName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }    
   
   	//@Test 
    def void t4() { 
        in = '''find aspect a, class c where a.affects(c) returns a.fullQualifiedName, c.fullQualifiedName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }  	
    
   	//@Test 
    def void t5() { 
        in = '''find aspect a, class c where c.affectedBy(a) returns a.fullQualifiedName, c.fullQualifiedName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }
    
   	//@Test 
    def void t6() {  
        in = '''find aspect a returns a.name as nomeDoAspecto, a.fullQualifiedName as fullName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }    
    
   	//@Test 
    def void t7() {  
        in = '''find aspect a 
				where a.metrics.numberOfOutAffects = 0 
				and a.metrics.numberOfChildren = 0 
				and a.modifier(abstract)
				returns a.fullQualifiedName''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }    
    
//   	@Test 
    def void t8() {  
        in = '''find aspect a, class c where a.advice.pointcut.code like concat ('%', ' ', c.pointcut.name, ' ', '%') returns count (a.fullQualifiedName), c.fullQualifiedName,  a.fullQualifiedName, c.pointcut.name group by c.fullQualifiedName, a.fullQualifiedName, c.pointcut.name''' 
		expectedOut = '''SELECT a.name from AOJAspectDeclaration a'''
    	runTest (in, expectedOut)
    }
          
}