package br.ufsm.tests

import br.ufsm.AQLInjectorProvider
import br.ufsm.aql.resource.Qualified
import hql.HqlFactory 
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*
import java.util.Map

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AQLInjectorProvider))

class QualifiedStringTest {
    @Test
    def void isValid1() {
    	assertTrue (Qualified::isValidQualifier("a"))
    	assertTrue (Qualified::isValidQualifier("a.b"))
    	assertTrue (Qualified::isValidQualifier("_a.b"))
    	assertFalse (Qualified::isValidQualifier(""))
    	assertFalse (Qualified::isValidQualifier("a."))
    	assertFalse (Qualified::isValidQualifier("a.!"))
    	assertFalse (Qualified::isValidQualifier("a.b@"))
    	assertFalse (Qualified::isValidQualifier("a.#b"))
    	assertFalse (Qualified::isValidQualifier("a.$b"))
    	assertFalse (Qualified::isValidQualifier("a.%b"))
    	assertFalse (Qualified::isValidQualifier("a.&b"))
    	assertFalse (Qualified::isValidQualifier("a.&b^"))
    	assertFalse (Qualified::isValidQualifier("a.&b("))
    	assertFalse (Qualified::isValidQualifier("a.b)"))
    	assertFalse (Qualified::isValidQualifier("a.b{"))
    	assertFalse (Qualified::isValidQualifier("a.b}"))
    	assertFalse (Qualified::isValidQualifier("a.b["))
    	assertFalse (Qualified::isValidQualifier("a.b]"))
    	assertFalse (Qualified::isValidQualifier("a.b|"))
    	assertFalse (Qualified::isValidQualifier("a.b\\"))
    	assertFalse (Qualified::isValidQualifier("a.b,"))
    	assertFalse (Qualified::isValidQualifier("a.b'"))
    	assertFalse (Qualified::isValidQualifier("a.b?"))
    	assertFalse (Qualified::isValidQualifier("a.b/"))
	}
    	
	@Test
    def void toQualifiedTest1() {
        val Qualified t = Qualified::toQualified("a")
    	assertEquals ("a", t.toString)
	}

	@Test
    def void toQualifiedTest2() {
        val Qualified t = Qualified::toQualified("a.b")
    	assertEquals ("a", t.toString)
    	assertEquals ("a.b", t.toFullString)
	}
 
	@Test (expected=typeof(IllegalArgumentException))
    def void toQualifiedTest3() {
        Qualified::toQualified("a.")
	}

	@Test (expected=typeof(IllegalArgumentException))
    def void qualifyTest1() {
        Qualified::qualify("a.");
	}

	@Test 
    def void qualifyTest2() {
        val Qualified t = Qualified::qualify("a")
        assertEquals("a", t.toString)
	}

	@Test 
    def void qualifyTest3() {
        val Qualified t = Qualified::qualify("a.b")
        assertEquals("a.b", t.toString)
        assertEquals("a.b", t.toFullString)
	}

	@Test 
    def void qualifyTest4() {
        val Qualified t = Qualified::qualify("a.b").add("c").getRoot()
        assertEquals("a.b", t.toString)
        assertEquals("a.b.c", t.toFullString)
	}
	
	@Test 
    def void unQualifyTest1() { 
        val Qualified t = Qualified::qualify("a").add("b")
        assertEquals("b", t.unqualify)
	}

	@Test 
    def void unQualifyTest2() { 
        val Qualified t = Qualified::qualify("a")
        assertEquals("a", t.unqualify)
	}
	
	@Test 
    def void unQualifyRootTest1() { 
    	val Qualified t = Qualified::qualify("a")
        assertEquals("a", t.unqualifyRoot)
	}

	@Test 
    def void unQualifyRootTest2() { 
    	val Qualified t = Qualified::qualify("a").add("b")
        assertEquals("a", t.unqualifyRoot)
	}

	@Test 
    def void unQualifyRootTest3() { 
    	val Qualified t = Qualified::qualify("a.c").add("b")
        assertEquals("a.c", t.unqualifyRoot)
	}

    @Test 
    def void unQualifyRootTest4() { 
        val hql.QualifiedName qName = HqlFactory::eINSTANCE.createQualifiedName
        var hql.Identifier id1 = HqlFactory::eINSTANCE.createIdentifier
        var hql.Identifier id2 = HqlFactory::eINSTANCE.createIdentifier
        var hql.Identifier id3 = HqlFactory::eINSTANCE.createIdentifier
        var hql.Identifier id4 = HqlFactory::eINSTANCE.createIdentifier
        id1.setId("a")
        id2.setId("a.b")
        id3.setId("a.b.c")
        id4.setId("a.b.c.d")
        qName.qualifiers.add(id1)
        qName.qualifiers.add(id2)
		qName.qualifiers.add(id3)
        qName.qualifiers.add(id4)
        
        val Qualified q = Qualified::toQualified(qName)
        
        assertEquals("a.a.b.a.b.c", q.getTail.unqualifyRoot)
	}
    
    @Test 
    def void unQualifyRootTest5() { 
    	val Qualified t = Qualified::qualify("a").add("b").add("c")
    	val Qualified s = t.get(t.search("b"))
        assertEquals("a", s.unqualifyRoot)
	}

    @Test 
    def void unQualifyRootTest6() { 
    	val Qualified t = Qualified::qualify("a").add("b").add("c")
    	val Qualified s = t.get(t.search("c"))
        assertEquals("a.b", s.unqualifyRoot)
	}
    
    @Test
    def void addTest1() {
		val Qualified q = Qualified::qualify("a.b.c").add("d")
		assertEquals("d", q.toString)
		assertEquals("a.b.c.d", q.toFullString)
	}
	
    @Test (expected=typeof(IllegalArgumentException))
    def void addTest2() {
		Qualified::qualify("a").add("")
	}

    @Test (expected=typeof(IllegalArgumentException))
    def void addTest3() {
		Qualified::qualify("a").add("a%")
	}
	
	@Test
    def void getIndexTest1() {
    	val Qualified t = Qualified::qualify("a").add("b")
    	assertEquals (1, t.index)
    	assertEquals (0, t.getRoot().index)
    	assertEquals (2, t.numElements)
    }	
    
	@Test
    def void nextBeforeTest1() {
    	val Qualified t = Qualified::qualify("a").add("b").add("c")
    	val Qualified r = t.getRoot
    	assertNull (r.before)
    	assertEquals ("b", r.next.toString)
    	assertNotNull (r.next.before)
    	assertTrue (r.next.hasNext)
    	assertEquals ("c", r.next.next.toString)
    	assertNull(r.next.next.next)
    	assertFalse (r.next.next.hasNext)
    }
    
    @Test
    def void searchTest1() {
    	val Qualified t = Qualified::qualify("a").add("b")
    	assertEquals (0, t.search("a"))
    	assertEquals (1, t.search("b"))
    	assertEquals (-1, t.search("c"))
    }	

    @Test
    def void toFullStringTest1() {
    	val Qualified t = Qualified::qualify("a").add("b")
    	assertEquals ("a.b", t.toFullString)
    }	

    @Test
    def void toFullStringTest2() {
    	val Qualified t = Qualified::qualify("a")
    	assertEquals ("a", t.toFullString)
    }	
    
    @Test
    def void containsTest1() {
    	val Qualified t1 = Qualified::qualify("a")
    	val Qualified t2 = Qualified::qualify("a")
    	val Map<Integer, Integer> m = t1.contains(t2)
    	assertTrue (m.containsKey(0))
    	assertTrue (m.containsValue(0))
    	assertEquals(0, m.get(0))
    }	
    
    @Test
    def void containsTest2() {
    	val Qualified t1 = Qualified::qualify("a").add("b")
    	val Qualified t2 = Qualified::qualify("a")
    	val Map<Integer, Integer> m = t1.contains(t2)
    	assertTrue (m.containsKey(0))
    	assertTrue (m.containsValue(0))
    	assertEquals(0, m.get(0))
    }
    
    @Test
    def void containsTest3() {
    	val Qualified t1 = Qualified::qualify("a").add("b")
    	val Qualified t2 = Qualified::qualify("a").add("b")
    	val Map<Integer, Integer> m = t1.contains(t2.getRoot())
    	assertEquals(1, m.get(0))
    }	
    
    @Test
    def void containsTest4() {
    	val Qualified t1 = Qualified::qualify("a")
    	val Qualified t2 = Qualified::qualify("a").add("b")
    	val Map<Integer, Integer> m = t1.contains(t2.getRoot())
    	assertEquals(-1, m.get(-1))
    }
    
    @Test
    def void containsTest5() {
    	val Qualified t1 = Qualified::qualify("a").add("b").add("c")
    	val Qualified t2 = Qualified::qualify("a").add("c")
    	val Map<Integer, Integer> m = t1.contains(t2.getRoot())
    	assertEquals(-1, m.get(-1))
    }
    
    @Test
    def void containsTest6() {
    	val Qualified t1 = Qualified::qualify("a").add("b").add("c")
    	val Qualified t2 = Qualified::qualify("b")
    	val Map<Integer, Integer> m = t1.contains(t2.getRoot())
    	assertEquals(1, m.get(1))
    }	

    @Test
    def void containsTest7() {
    	val Qualified t1 = Qualified::qualify("a").add("b").add("c")
    	val Qualified t2 = Qualified::qualify("b").add("c")
    	val Map<Integer, Integer> m = t1.contains(t2.getRoot())
    	assertEquals(2, m.get(1))
    }	
    	
}