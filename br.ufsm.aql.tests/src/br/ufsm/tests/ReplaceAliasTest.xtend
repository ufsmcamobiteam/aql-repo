package br.ufsm.tests

import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import br.ufsm.AQLInjectorProvider
import org.junit.Test
import hql.HqlFactory
import br.ufsm.hql.transformation.HqlReplaceAlias
import static org.junit.Assert.*
import br.ufsm.aql.resource.Qualified
 
@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AQLInjectorProvider))
 
class ReplaceAliasTest {
	private hql.QualifiedName qName
	private hql.Identifier id1
	private hql.Identifier id2
	private hql.Identifier id3
	private hql.Identifier id4
	private extension HqlReplaceAlias replace = new HqlReplaceAlias()
	
	@Test
    def void replaceAliasTest1() {
    	qName = HqlFactory::eINSTANCE.createQualifiedName
    	id1 = HqlFactory::eINSTANCE.createIdentifier
    	id2 = HqlFactory::eINSTANCE.createIdentifier
    	id1.setId("a")
    	id2.setId("pointcut")
    	qName.qualifiers.add(id1)
    	qName.qualifiers.add(id2)
    	qName.replaceAlias("a", "AOJAspectDeclaration")
    	val Qualified q = Qualified::toQualified(qName)
    	assertEquals("AOJAspectDeclaration.pointcut", q.toFullString)
    }
	
	
	@Test
    def void replaceAliasTest2() {
    	qName = HqlFactory::eINSTANCE.createQualifiedName
    	id1 = HqlFactory::eINSTANCE.createIdentifier
    	id2 = HqlFactory::eINSTANCE.createIdentifier
    	id3 = HqlFactory::eINSTANCE.createIdentifier
    	id1.setId("a")
    	id2.setId("pointcut")
    	id3.setId("modifier")
    	qName.qualifiers.add(id1)
    	qName.qualifiers.add(id2)
    	qName.qualifiers.add(id3)
    	qName.replaceAlias("pointcut.modifier", "pointcut.members.modifier")
    	val Qualified q = Qualified::toQualified(qName)
    	assertEquals("a.pointcut.members.modifier", q.toFullString)
    }
    
    @Test
    def void replaceAliasTest3() {
    	qName = HqlFactory::eINSTANCE.createQualifiedName
    	id1 = HqlFactory::eINSTANCE.createIdentifier
    	id2 = HqlFactory::eINSTANCE.createIdentifier
    	id3 = HqlFactory::eINSTANCE.createIdentifier
    	id1.setId("a")
    	id2.setId("pointcut")
    	id3.setId("modifier")
    	qName.qualifiers.add(id1)
    	qName.qualifiers.add(id2)
    	qName.qualifiers.add(id3)
    	qName.replaceAlias("naoexiste", "pointcut.members.modifier")
    	val Qualified q = Qualified::toQualified(qName)
    	assertEquals("a.pointcut.modifier", q.toFullString)
    }
    
    @Test
    def void replaceAliasTest4() {
    	qName = HqlFactory::eINSTANCE.createQualifiedName
    	id1 = HqlFactory::eINSTANCE.createIdentifier
    	id2 = HqlFactory::eINSTANCE.createIdentifier
    	id3 = HqlFactory::eINSTANCE.createIdentifier
    	id1.setId("a")
    	id2.setId("pointcut")
    	id3.setId("modifier")
    	qName.qualifiers.add(id1)
    	qName.qualifiers.add(id2)
    	qName.qualifiers.add(id3)
    	qName.replaceAlias("modifier", "members.modifier")
    	val Qualified q = Qualified::toQualified(qName)
    	assertEquals("a.pointcut.members.modifier", q.toFullString)
    }
    
    @Test
    def void replaceAliasTest5() {
    	qName = HqlFactory::eINSTANCE.createQualifiedName
    	id1 = HqlFactory::eINSTANCE.createIdentifier
    	id2 = HqlFactory::eINSTANCE.createIdentifier
    	id3 = HqlFactory::eINSTANCE.createIdentifier
    	id1.setId("a")
    	id2.setId("pointcut")
    	id3.setId("name")
    	qName.qualifiers.add(id1)
    	qName.qualifiers.add(id2)
    	qName.qualifiers.add(id3)
    	qName.replaceAlias("a.pointcut", "aojpointcut_1")
    	val Qualified q = Qualified::toQualified(qName)
    	assertEquals("aojpointcut_1.name", q.toFullString)
    }
}